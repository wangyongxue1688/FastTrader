TARGET = FastTrader
TEMPLATE = app
CONFIG += qt
QT += core gui widgets


INCLUDEPATH += ../sdk/include
INCLUDEPATH += ../common
INCLUDEPATH += ../spdlog/include

DEFINES += _USE_SPDLOG
CONFIG += debug_and_release c++11

linux-g++|macx-g++{
    QMAKE_LFLAGS= -m64 -Wall -DNDEBUG  -O2
    QMAKE_CFLAGS = -arch x86_64 -lpthread std=c++11

    INCLUDEPATH += /home/rmb338/boost_1_64_0
    LIBS += -L/home/rmb338/boost_1_64_0/stage/lib -lboost_system -lboost_thread -lboost_filesystem -lboost_serialization -lboost_date_time
}

win32{
    DEFINES += WIN32_LEAN_AND_MEAN _CRT_SECURE_NO_WARNINGS
    INCLUDEPATH += F:/boost_1_63_0
    LIBS += -LF:/boost_1_63_0/lib32-msvc-12.0
}

CONFIG(debug, debug|release) {
        DESTDIR = ../build/v120/debug
        LIBS  += -L$$PWD/../build/v120/debug/ -lFacilityBaseLib -lTechLib -lServiceLib -lDataStoreLib -lSimpleStrategyLib -lConfigLib
} else {
        DESTDIR = ../build/v120/release
        LIBS = -L$$PWD/../build/v120/release/ -lFacilityBaseLib lTechLib -lServiceLib -lDataStoreLib -lSimpleStrategyLib -lConfigLib
}

include(./FastTrader.pri)



