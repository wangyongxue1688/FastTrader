﻿#ifndef _PLATFORM_STRUCT_H_
#define _PLATFORM_STRUCT_H_
#include "complier.h"
#include <map>
#include <algorithm>
#include <sstream>
#include <vector>
#include <stdint.h>

enum ENUM_ID_LENGTH
{
	MAX_INSTRUMENT_ID_LENGTH=31,
	MAX_INSTRUMENT_NAME_LENGTH = 31,
	MAX_EXCHANGE_ID_LENGTH=31,
	MAX_DATE_LENGTH=9,
	MAX_TIME_LENGTH=9,
};  


//
enum ExchangeTypes{
	///上海期货交易所;
	EIDT_SHFE = 'S',
	///郑州商品交易所;
	EIDT_CZCE = 'Z',
	///大连商品交易所;
	EIDT_DCE = 'D',
	///中国金融期货交易所;
	EIDT_CFFEX = 'J',
	///上海国际能源交易中心股份有限公司;
	EIDT_INE = 'N',
	//其他未知交易所;
	EIDT_UNKNOW = 5,
};

//字节对齐属性;
#pragma pack(1)

#define DECLARE_INSTRUMENT_BASE_T \
char	szCode[MAX_INSTRUMENT_ID_LENGTH];\
char	szName[MAX_INSTRUMENT_NAME_LENGTH];\
char	szExchange[MAX_EXCHANGE_ID_LENGTH];	
struct instrument_base_t
{
	char	szCode[MAX_INSTRUMENT_ID_LENGTH]; \
		char	szName[MAX_INSTRUMENT_NAME_LENGTH]; \
		char	szExchange[MAX_EXCHANGE_ID_LENGTH];
};
//K线数据结构体;
typedef struct kdata_t
{
	DECLARE_INSTRUMENT_BASE_T
	char  TradingDay[MAX_DATE_LENGTH];//交易日;
    DWORD TradingDate;//交易时间;
	time_t TradingTime;//交易时间;
	double OpenPrice;//开盘价;
	double HighestPrice;//最高价;
	double LowestPrice;//最低价;
	double ClosePrice;//收盘价;
	int64_t  Volume;//成交量;
	double Turnover;//成交额;
	double AveragePrice;//平均价;
	double PreSettlementPrice;//昨结算;
	double PreClosePrice;//昨收盘;
	double OpenInterest;//开仓量;
	double PreOpenInterest;//昨开量;
    DWORD Reserved;//保留字段;
	DWORD	Advance;		// 仅指数有效;
	DWORD	Decline;		// 仅指数有效;
}KDATA,*PKDATA;



//分钟数据;
typedef struct minute_t
{
	DECLARE_INSTRUMENT_BASE_T
	DWORD	Type;	// 1 min, 5 min, 15 min, 30 min line
	char    Tradingday[MAX_DATE_LENGTH];
	time_t	TradingTime;				//时间;
	int		TradingMillisec;         //毫秒;
	double	HighestPrice;			// 最高价;
	double	LowestPrice;				// 最低价; 
	double	LastPrice;				// 最新价;
	double  AveragePrice;       //平均价;
	long long  	Volume;			// 手数;
	double	Turnover;			// 成交额;
	double	BidPrice[5];				// 申买价;
	int	    BidVolume[5];			// 申买量;
	double	AskPrice[5];			    //申卖价; 
	int	    AskVolume[5];			// 申卖量;
} MINUTE, *PMINUTE;

typedef struct multisort_item_t{
	BYTE  Type;	//类型;
	char  InstrumentID[MAX_INSTRUMENT_ID_LENGTH];
	long  Data1;
	long  Data2;
}MULTISORT_ITEM;

typedef struct multisort_t{
	DWORD Class;
	DWORD Tag;
	MULTISORT_ITEM Instruments[6];
}MULTISORT,*PMULTISORT;


//除权数据 CStock::dataDR
typedef struct drdata_t
{
	DWORD	m_dwMarket;						// 交易所,see CStock::StockMarket
	char	m_szCode[MAX_INSTRUMENT_ID_LENGTH];		// 代码;

	DWORD	m_date;				//日期	Format is XXMMDDHHMM for 5min, Format is YYYYMMDD for day
	time_t	m_time;				//时间;

	float	m_fGive;			// 
	float	m_fPei;				// 
	float	m_fPeiPrice;		// 
	float	m_fProfit;			// 

	DWORD	m_dwReserved;
} DRDATA, * PDRDATA;

// 代码;
typedef	struct stockcode {
	DWORD	m_dwMarket;						// 交易所,see CStock::StockMarket
	char	m_szCode[MAX_INSTRUMENT_ID_LENGTH];		// 代码;
} STOCKCODE, *PSTOCKCODE;
//  CStock::dataOutline
typedef struct outline_t {
	time_t	m_time;
	DWORD	m_dwShHq;
	DWORD	m_dwSzHq;
	DWORD	m_dwShPriceUp;
	DWORD	m_dwShPriceDown;
	DWORD	m_dwSzPriceUp;
	DWORD	m_dwSzPriceDown;
	DWORD	m_dwShWbUp;
	DWORD	m_dwShWbDown;
	DWORD	m_dwSzWbUp;
	DWORD	m_dwSzWbDown;
	DWORD	m_dwShStockCount;
	DWORD	m_dwSzStockCount;
} OUTLINE, *POUTLINE;
//基本信息;

typedef struct base_info_t{
	///
	char	ExchangeInstID[MAX_INSTRUMENT_ID_LENGTH];
	///
	char ExchangeName[31];
	///
	int ExchangeProperty;
	//
	time_t m_time;
	DWORD m_date;

	///
	char	ProductID[MAX_INSTRUMENT_ID_LENGTH];
	///
	int	ProductClass;
	///
	int	DeliveryYear;
	///
	int	DeliveryMonth;
	///
	int	MaxMarketOrderVolume;
	///
	int	MinMarketOrderVolume;
	///
	int	MaxLimitOrderVolume;
	///
	int	MinLimitOrderVolume;
	///
	int	VolumeMultiple;
	///
	double	PriceTick;
	///
	char	CreateDate[9];
	///
	char	OpenDate[9];
	///
	char	ExpireDate[9];
	///
	char	StartDelivDate[9];
	///
	char	EndDelivDate[9];
	///
	int	InstLifePhase;
	///
	int	IsTrading;
	///
	int	PositionType;
	///
	int	PositionDateType;
	///
	double	LongMarginRatio;
	///
	double	ShortMarginRatio;
}BASEINFO,*PBASEINFO;

typedef struct basedata_t:public instrument_base_t,public base_info_t{
}BASEDATA,*PBASEDATA;
//
#define	STKLIB_COMMPACKET_TAG	'KPMC'
struct Tick;
typedef struct commpacket_t {
	DWORD	m_dwTag;			// = STKLIB_COMMPACKET_TAG
	DWORD	m_dwDataType;		// see CStock::DataType
	DWORD	m_dwCount;
	union
	{
		Tick *	m_pReport;		// 
		MINUTE *	m_pMinute;		// 
		MULTISORT *	m_pMultisort;	// 
		OUTLINE *	m_pOutline;		// 
		KDATA *		m_pKdata;		// 
		DRDATA *	m_pDrdata;		// 
		STOCKCODE *	m_pStockcode;	// 
		BASEDATA * m_pBasedata;
		void *		m_pData;
	};
} COMMPACKET, *PCOMMPACKET;
#pragma pack()

//
struct InstrumentField
{
	///
	char	InstrumentID[31];
	///
	char	ExchangeID[MAX_INSTRUMENT_ID_LENGTH];
	///
	char	InstrumentName[21];
	///
	char	ExchangeInstID[31];
	///
	char	ProductID[31];
	///
	int	ProductClass;
	///
	int	DeliveryYear;
	///
	int	DeliveryMonth;
	///
	int	MaxMarketOrderVolume;
	///
	int	MinMarketOrderVolume;
	///
	int	MaxLimitOrderVolume;
	///
	int	MinLimitOrderVolume;
	///
	int	VolumeMultiple;
	///
	double	PriceTick;
	///
	char	CreateDate[9];
	///
	char	OpenDate[9];
	///
	char	ExpireDate[9];
	///
	char	StartDelivDate[9];
	///
	char	EndDelivDate[9];
	///
	int	InstLifePhase;
	///
	int	IsTrading;
	///
	int	PositionType;
	///
	int	PositionDateType;
	//
	double	LongMarginRatio;
	///
	double	ShortMarginRatio;
};
///
struct OnceMarketDataField
{
	///ºÏÔŒŽúÂë
	char	InstrumentID[31];
	///œ»Ò×ÈÕ
	char	TradingDay[9];
	///ÉÏŽÎœáËãŒÛ
	double	PreSettlementPrice;
	///±ŸŽÎœáËãŒÛ
	double	SettlementPrice;
	///×òÊÕÅÌ
	double	PreClosePrice;
	///×ò³Ö²ÖÁ¿
	double	PreOpenInterest;
	///œñ¿ªÅÌ
	double	OpenPrice;
	///×îžßŒÛ
	double	HighestPrice;
	///×îµÍŒÛ
	double	LowestPrice;
	///³Ö²ÖÁ¿
	double	OpenInterest;
	///œñÊÕÅÌ
	double	ClosePrice;
	///×òÐéÊµ¶È
	double	PreDelta;
	///œñÐéÊµ¶È
	double	CurrDelta;
	///µ±ÈÕŸùŒÛ
	double	AveragePrice;
};
///µ±Ê±ÊýŸÝ
struct MultipleMarketDataField
{
	///
	char	InstrumentID[31];
	///
	char	TradingDay[9];
	///
	double	LastPrice;
	///
	int	Volume;
	///
	double	Turnover;
	///
	double	UpperLimitPrice;
	///
	double	LowerLimitPrice;
	///
	double	BidPrice1;
	///
	int	BidVolume1;
	///
	double	AskPrice1;
	///
	int	AskVolume1;
	///
	double	BidPrice2;
	///
	int	BidVolume2;
	///
	double	AskPrice2;
	///
	int	AskVolume2;
	///
	double	BidPrice3;
	///
	int	BidVolume3;
	///
	double	AskPrice3;
	///
	int	AskVolume3;
	///
	double	BidPrice4;
	///
	int	BidVolume4;
	///
	double	AskPrice4;
	///
	int	AskVolume4;
	///
	double	BidPrice5;
	///
	int	BidVolume5;
	///
	double	AskPrice5;
	//
	int	AskVolume5;
	//
	char	UpdateTime[9];
	//
	int	UpdateMillisec;
};



#endif
