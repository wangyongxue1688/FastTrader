﻿#include "FieldInterceptor.h"
#include "../FacilityBaseLib/Express.h"
#include "../CTP/ThostFtdcUserApiDataType.h"
#include "UserInfo.h"
#include "Tick.h"

static string err_msg[]={
	"综合交易平台：正确",
	"综合交易平台：不在已同步状态",
	"综合交易平台：会话信息不一致",
	"综合交易平台：不合法的登录",
	"综合交易平台：用户不活跃",
	"综合交易平台：重复的登录",
	"综合交易平台：还没有登录",
	"综合交易平台：还没有初始化",
	"综合交易平台：前置不活跃",
	"综合交易平台：无此权限",
	"综合交易平台：修改别人的口令",
	"综合交易平台：找不到该用户",
	"综合交易平台：找不到该经纪公司",
	"综合交易平台：找不到投资者",
	"综合交易平台：原口令不匹配",
	"综合交易平台：报单字段有误",
	"综合交易平台：找不到合约",
	"综合交易平台：合约不能交易",
	"综合交易平台：经纪公司不是交易所的会员",
	"综合交易平台：投资者不活跃",
	"综合交易平台：投资者未在交易所开户",
	"综合交易平台：该交易席位未连接到交易所",
	"综合交易平台：报单错误：不允许重复报单",
	"综合交易平台：错误的报单操作字段",
	"综合交易平台：撤单已报送，不允许重复撤单",
	"综合交易平台：撤单找不到相应报单",
	"综合交易平台：报单已全成交或已撤销，不能再撤",
	"综合交易平台：不支持的功能",
	"综合交易平台：没有报单交易权限",
	"综合交易平台：只能平仓",
	"综合交易平台：平仓量超过持仓量",
	"综合交易平台：资金不足",
	"综合交易平台：主键重复",
	"综合交易平台：找不到主键",
	"综合交易平台：设置经纪公司不活跃状态失败",
	"综合交易平台：经纪公司正在同步",
	"综合交易平台：经纪公司已同步",
	"综合交易平台：现货交易不能卖空",
	"综合交易平台：不合法的结算引用",
	"综合交易平台：交易所网络连接失败",
	"综合交易平台：交易所未处理请求超过许可数",
	"综合交易平台：交易所每秒发送请求数超过许可数",
	"综合交易平台：结算结果未确认",
	"综合交易平台：没有对应的入金记录",
	"综合交易平台：交易所已经进入连续交易状态",
	"综合交易平台：找不到预埋（撤单）单",
	"综合交易平台：预埋（撤单）单已经发送",
	"综合交易平台：预埋（撤单）单已经删除",
	"综合交易平台：无效的投资者或者密码",
	"综合交易平台：不合法的登录IP地址",
	"综合交易平台：平今仓位不足",
	"综合交易平台：平昨仓位不足",
	"综合交易平台：经纪公司没有足够可用的条件单数量",
	"综合交易平台：投资者没有足够可用的条件单数量",
	"综合交易平台：经纪公司不支持条件单",
	"综合交易平台：重发未知单经济公司/投资者不匹配",
	"综合交易平台：同步动态令牌失败",
	"综合交易平台：动态令牌校验错误",
	"综合交易平台：找不到动态令牌配置信息",
	"综合交易平台：不支持的动态令牌类型",
	"综合交易平台：用户在线会话超出上限",
	"综合交易平台：该交易所不支持套利类型报单",
	"综合交易平台：没有条件单交易权限",
	"综合交易平台：客户端认证失败",
	"综合交易平台：客户端未认证",
	"综合交易平台：该合约不支持互换类型报单",
	"综合交易平台：连续登录失败次数超限，登录被禁止"
};


FieldInterceptor::FieldInterceptor(void)
{

}


FieldInterceptor::~FieldInterceptor(void)
{
}

std::string FieldInterceptor::get_disp_str( int id,const string& field_type_name/*=""*/ )
{
	return "";
}

std::string FieldInterceptor::get_disp_str( const BASEDATA& rsp,int id )
{
	return "";
}

std::string FieldInterceptor::get_disp_str( const MINUTE& rsp,int id )
{
	return "";
}

std::string FieldInterceptor::get_disp_str( const Tick& rsp,int id )
{
	return "";
}

string FieldInterceptor::get_price_type( char type_id )
{
	if (PriceTypeMap.empty())
	{
		PriceTypeMap.insert(make_pair(THOST_FTDC_OPT_AnyPrice,"任意价"));
		PriceTypeMap.insert(make_pair(THOST_FTDC_OPT_LimitPrice,"限价"));
		PriceTypeMap.insert(make_pair(THOST_FTDC_OPT_BestPrice,"最优价"));
		PriceTypeMap[THOST_FTDC_OPT_LastPrice]="最新价";
		PriceTypeMap[THOST_FTDC_OPT_LastPricePlusOneTicks]="最新价浮动上浮1个ticks";
		PriceTypeMap[THOST_FTDC_OPT_LastPricePlusTwoTicks]="最新价浮动上浮2个ticks";
		PriceTypeMap[THOST_FTDC_OPT_LastPricePlusThreeTicks]="最新价浮动上浮3个ticks";

		PriceTypeMap[THOST_FTDC_OPT_AskPrice1]="卖一价";
		PriceTypeMap[THOST_FTDC_OPT_AskPrice1PlusOneTicks]="卖一价浮动上浮1个ticks";
		PriceTypeMap[THOST_FTDC_OPT_AskPrice1PlusTwoTicks]="卖一价浮动上浮2个ticks";
		PriceTypeMap[THOST_FTDC_OPT_AskPrice1PlusThreeTicks]="卖一价浮动上浮3个ticks";

		PriceTypeMap[THOST_FTDC_OPT_BidPrice1]="买一价";
		PriceTypeMap[THOST_FTDC_OPT_BidPrice1PlusOneTicks]="买一价浮动上浮1个ticks";
		PriceTypeMap[THOST_FTDC_OPT_BidPrice1PlusTwoTicks]="买一价浮动上浮2个ticks";
		PriceTypeMap[THOST_FTDC_OPT_BidPrice1PlusThreeTicks]="买一价浮动上浮3个ticks";
	}
	if (type_id>=THOST_FTDC_OPT_AnyPrice && type_id<=THOST_FTDC_OPT_BidPrice1PlusThreeTicks)
	{
		return PriceTypeMap[type_id];
	}
	return "--";
}

string FieldInterceptor::get_product_type( char type_id )
{
	if (ProductTypeMap.empty())
	{
		ProductTypeMap[THOST_FTDC_PC_Futures]="期货";
		ProductTypeMap[THOST_FTDC_PC_Options]="期权";
		ProductTypeMap[THOST_FTDC_PC_Combination]="组合";
		ProductTypeMap[THOST_FTDC_PC_Spot]="即期";
		ProductTypeMap[THOST_FTDC_PC_EFP]="期转现";
	}
	if (type_id>=THOST_FTDC_PC_Futures && type_id<=THOST_FTDC_PC_EFP)
	{
		return ProductTypeMap[type_id];
	}
	return "--";
}

string FieldInterceptor::get_order_status_type( char type_id )
{
	if (OrderStatusTypeMap.empty())
	{
		OrderStatusTypeMap[THOST_FTDC_OST_AllTraded]="全部成交";
		OrderStatusTypeMap[THOST_FTDC_OST_PartTradedQueueing]="部分成交还在队列中";
		OrderStatusTypeMap[THOST_FTDC_OST_PartTradedNotQueueing]="部分成交不在队列中";
		OrderStatusTypeMap[THOST_FTDC_OST_NoTradeQueueing]		  ="未成交还在队列中";
		OrderStatusTypeMap[THOST_FTDC_OST_NoTradeNotQueueing] ="未成交不在队列中";
		OrderStatusTypeMap[THOST_FTDC_OST_Canceled] ="撤单";
		OrderStatusTypeMap[THOST_FTDC_OST_Unknown] ="未知";
		OrderStatusTypeMap[THOST_FTDC_OST_NotTouched] ="尚未触发";
		OrderStatusTypeMap[THOST_FTDC_OST_Touched] ="已触发";
	}
	if (type_id>=THOST_FTDC_OST_AllTraded && type_id<=THOST_FTDC_OST_Touched)
	{
		return OrderStatusTypeMap[type_id];
	}
	return "--";
}

string FieldInterceptor::get_offset_flag_type( char type_id )
{
	string ret_str="--";
	if (OffsetFlagTypeMap.empty())
	{
		OffsetFlagTypeMap[OF_Open]="开仓";
		OffsetFlagTypeMap[OF_Close]="平仓";
		OffsetFlagTypeMap[OF_ForceClose]="强平";
		OffsetFlagTypeMap[OF_CloseToday]="平今";
		OffsetFlagTypeMap[OF_CloseYesterday]="平昨";
		OffsetFlagTypeMap[OF_ForceOff]="强减";
		OffsetFlagTypeMap[OF_LocalForceClose]="本地强平";
	}
	if (type_id>= OF_Open && type_id<= OF_LocalForceClose)
	{
		ret_str= OffsetFlagTypeMap[type_id];
	}
	return ret_str;
}

string FieldInterceptor::get_contingent_cond_type( char type_id )
{
	string ret_str="--";
	if (ContingentConditionTypeMap.empty())
	{
		ContingentConditionTypeMap[CC_Immediately]="立即";
		ContingentConditionTypeMap[CC_Touch]="止损";
		ContingentConditionTypeMap[CC_TouchProfit]="止赢";
		ContingentConditionTypeMap[CC_ParkedOrder]="预埋单";
		ContingentConditionTypeMap[CC_LastPriceGreaterThanStopPrice]
		="最新价大于条件价";
		ContingentConditionTypeMap[CC_LastPriceGreaterEqualStopPrice]
		="最新价大于等于条件价";
		ContingentConditionTypeMap[CC_LastPriceLesserEqualStopPrice]
		="最新价小于等于条件价";
		ContingentConditionTypeMap[CC_AskPriceGreaterThanStopPrice]
		="卖一价大于条件价";
		ContingentConditionTypeMap[CC_AskPriceGreaterEqualStopPrice]
		="卖一价大于等于条件价";
		ContingentConditionTypeMap[CC_AskPriceLesserEqualStopPrice]
		="卖一价小于等于条件价";
		ContingentConditionTypeMap[CC_BidPriceGreaterThanStopPrice]
		="买一价大于条件价";
		ContingentConditionTypeMap[CC_BidPriceGreaterEqualStopPrice]
		="买一价大于等于条件价";
		ContingentConditionTypeMap[CC_BidPriceLesserThanStopPrice]
		="买一价小于条件价";
		ContingentConditionTypeMap[CC_BidPriceLesserEqualStopPrice]
		="买一价小于等于条件价";
		//ContingentConditionTypeMap.insert(make_pair(CC_ManualContinous,"手动发出"));
		//ContingentConditionTypeMap[CC_EnterContinousTrade]="重新进入交易状态";
	}
	if (type_id>=CC_Immediately && type_id<= CC_BidPriceLesserEqualStopPrice)
	{
		ret_str= ContingentConditionTypeMap[type_id]; 
	}
	return ret_str;
}

string FieldInterceptor::get_hedge_flag_type( char type_id )
{
	switch (type_id)
	{
	case HF_Speculation:
		return "投机";
	case HF_Arbitrage:
		return "套利";
	case HF_Hedge:
		return "套保";
	default:
		return "--";
		break;
	}
	return "--";
}

string FieldInterceptor::get_direction_type( char type_id )
{
	if (type_id==D_Buy)
	{
		return "买";
	}
	else if (type_id==D_Sell)
	{
		return "卖";
	}
	else
	{
		return "--";
	}
}

string FieldInterceptor::get_volume_cond_type( char type_id )
{
	switch (type_id)
	{
	case VC_AV:
		return "任何数量";
	case VC_MV:
		return "最小数量";
	case VC_CV:
		return "全部数量";
	default:
		return "--";
		break;
	}
	return "--";
}

string FieldInterceptor::get_force_close_reason( char type_id )
{
	static map<char,string> force_close_reason;
	if (force_close_reason.empty())
	{
		force_close_reason[THOST_FTDC_FCC_NotForceClose]="非强平";
		force_close_reason[THOST_FTDC_FCC_LackDeposit]="资金不足";
		force_close_reason[THOST_FTDC_FCC_ClientOverPositionLimit]="客户超仓";
		force_close_reason[THOST_FTDC_FCC_MemberOverPositionLimit]="会员超仓";
		force_close_reason[THOST_FTDC_FCC_NotMultiple]="持仓非整数倍";
		force_close_reason[THOST_FTDC_FCC_Violation]="违规";
		force_close_reason[THOST_FTDC_FCC_Other]="其他";
		force_close_reason[THOST_FTDC_FCC_PersonDeliv]="自然人临近交割";
	}
	if(type_id<=THOST_FTDC_FCC_PersonDeliv && type_id>=THOST_FTDC_FCC_NotForceClose)
		return force_close_reason[type_id];
	return "--";
}

string FieldInterceptor::get_time_cond_type( char type_id )
{
	static map<char,string> time_cond_types;
	if (time_cond_types.empty())
	{
		time_cond_types[TC_IOC]="立即完成，否则撤销";
		time_cond_types[TC_GFS]="本节有效";
		time_cond_types[TC_GFD]="当日有效";
		time_cond_types[TC_GTD]="指定日期前有效";
		time_cond_types[TC_GTC]="撤销前有效";
		time_cond_types[TC_GFA]="集合竞价有效";
	}
	if(type_id<=TC_GFA && type_id>=TC_IOC)
		return time_cond_types[type_id];
	return "--";
}

string FieldInterceptor::get_position_type( char type_id )
{
	if (type_id==THOST_FTDC_PT_Net)
	{
		return "净持仓";
	}
	if (type_id==THOST_FTDC_PT_Gross)
	{
		return "综合持仓";
	}
	return "--";
}

string FieldInterceptor::get_order_submit_status_type( char type_id )
{
	static map<char,string> OrderSubmitStatusMap;
	if (OrderSubmitStatusMap.empty())
	{
		OrderSubmitStatusMap[THOST_FTDC_OSS_InsertSubmitted]="已经提交";
		OrderSubmitStatusMap[THOST_FTDC_OSS_CancelSubmitted]="撤单已经提交";
		OrderSubmitStatusMap[THOST_FTDC_OSS_ModifySubmitted]="修改已经提交";
		OrderSubmitStatusMap[THOST_FTDC_OSS_Accepted]="已经接受";
		OrderSubmitStatusMap[THOST_FTDC_OSS_InsertRejected]="报单已经被拒绝";
		OrderSubmitStatusMap[THOST_FTDC_OSS_CancelRejected]="撤单已经被拒绝";
		OrderSubmitStatusMap[THOST_FTDC_OSS_ModifyRejected]="改单已经被拒绝";
	}
	if (type_id<=THOST_FTDC_OSS_ModifyRejected && type_id>=THOST_FTDC_OSS_InsertSubmitted)
	{
		return OrderSubmitStatusMap[type_id];
	}
	return "--";
}

string FieldInterceptor::get_inst_life_phase_type( char type_id )
{
	switch (type_id)
	{
	case THOST_FTDC_IP_NotStart:
		return "未上市";
	case THOST_FTDC_IP_Started:
		return "上市";
	case THOST_FTDC_IP_Pause:
		return "停牌";
	case THOST_FTDC_IP_Expired:
		return "到期";
	default:
		break;
	}
	return "--";
}

string FieldInterceptor::get_order_type( char type_id )
{
	static map<char,string> OrderTypeMap;
	if (OrderTypeMap.empty())
	{
		OrderTypeMap[THOST_FTDC_ORDT_Normal]="正常";
		OrderTypeMap[THOST_FTDC_ORDT_DeriveFromQuote]="报价衍生";
		OrderTypeMap[THOST_FTDC_ORDT_DeriveFromCombination]="组合衍生";
		OrderTypeMap[THOST_FTDC_ORDT_Combination]="组合报单";
		OrderTypeMap[THOST_FTDC_ORDT_ConditionalOrder]="条件单";
		OrderTypeMap[THOST_FTDC_ORDT_Swap]="互换单";
	}
	if (type_id<=THOST_FTDC_ORDT_Swap && type_id>=THOST_FTDC_ORDT_Normal)
	{
		return OrderTypeMap[type_id];
	}
	return "--";
}

string FieldInterceptor::get_exchange_property( char type_id )
{
	if (THOST_FTDC_EXP_Normal==type_id)
	{
		return "正常";
	}
	else
	{
		return "根据成交生成报单";
	}
}

string FieldInterceptor::get_position_date_type( char type_id )
{
	if (type_id==THOST_FTDC_PDT_UseHistory)
	{
		return "使用历史持仓";
	}
	if (type_id==THOST_FTDC_PDT_NoUseHistory)
	{
		return "不使用历史持仓";
	}
	return "--";
}

map<char,string> FieldInterceptor::get_price_type_map()
{
	if (ContingentConditionTypeMap.empty())
	{
		get_contingent_cond_type('0');
	}
	return ContingentConditionTypeMap;
}

string FieldInterceptor::get_disconnect_reason( int reason )
{
	static map<int,string> disconnect_reason_map;
	if (disconnect_reason_map.empty())
	{
		disconnect_reason_map[0x1001]="网络读失败";
		disconnect_reason_map[0x1002]="网络写失败";
		disconnect_reason_map[0x2001]="接收心跳超时";
		disconnect_reason_map[0x2002]="发送心跳失败";
		disconnect_reason_map[0x2003]="收到错误报文";
	}
	map<int,string>::iterator rit=disconnect_reason_map.find(reason);
	if (rit==disconnect_reason_map.end())
	{
		disconnect_reason_map[reason]="未知原因";
	}
	return disconnect_reason_map[reason];
}

string FieldInterceptor::get_price_source_type( char price_src )
{
	switch(price_src)
	{
	case THOST_FTDC_PSRC_LastPrice:
		return "前成交价";
	case THOST_FTDC_PSRC_Buy:
		return "买委托价";
	case THOST_FTDC_PSRC_Sell:
		return "卖委托价";
	}
	return "--";
}

string FieldInterceptor::get_trade_type( char trade_type )
{
	static map<char,string> trade_type_map;
	if (trade_type_map.empty())
	{
		trade_type_map[THOST_FTDC_TRDT_Common]="普通成交";
		trade_type_map[THOST_FTDC_TRDT_OptionsExecution]="期权执行";
		trade_type_map[THOST_FTDC_TRDT_OTC]="OTC成交";
		trade_type_map[THOST_FTDC_TRDT_EFPDerived]="期转现衍生成交";
		trade_type_map[THOST_FTDC_TRDT_CombinationDerived]="组合衍生成交";
	}
	if (trade_type<=THOST_FTDC_TRDT_CombinationDerived &&
		trade_type>=THOST_FTDC_TRDT_Common)
	{
		return trade_type_map[trade_type];
	}
	return "--";
}

string FieldInterceptor::get_trade_role_type( char trade_role )
{
	switch(trade_role)
	{
	case THOST_FTDC_ER_Broker:
		return "代理";
	case THOST_FTDC_ER_Host:
		return "自营";
	case THOST_FTDC_ER_Maker:
		return "做市商";
	}
	return "--";
}

string FieldInterceptor::get_instrument_status( char status )
{
	static map<char,string> InstrumentStatusTypeMap;
	//if (InstrumentStatusTypeMap.empty())
	//{
	//	InstrumentStatusTypeMap[BeforeTrading]="开盘前";
	//	InstrumentStatusTypeMap[NoTrading]="非交易";
	//	InstrumentStatusTypeMap[Continous]="连续交易";
	//	InstrumentStatusTypeMap[AuctionOrdering]="集合竞价报单";
	//	InstrumentStatusTypeMap[AuctionBalance]="集合竞价价格平衡";
	//	InstrumentStatusTypeMap[AuctionMatch]="集合竞价撮合";
	//	InstrumentStatusTypeMap[Closed]="收盘";
	//}
	//if (status<=Closed &&
	//	status>=BeforeTrading)
	//{
	//	return InstrumentStatusTypeMap[status];
	//}
	return "--";
}

string FieldInterceptor::get_park_order_status( char status )
{
	//switch(status)
	//{
	//case NotSend:
	//	return "未发送";
	//case Send:
	//	return "已发送";
	//case Deleted:
	//	return "已删除";
	//}
	return "--";
}

std::string FieldInterceptor::get_position_direction_type( char posiDirection )
{
	switch(posiDirection)
	{
	case PD_Net :
		return "净";
			/// <summary>
			/// 多头
			/// </summary>
	case PD_Long:
		return "多头";

			/// <summary>
			/// 空头
			/// </summary>
	case PD_Short:
		return "空头";
	}
	return "--";
}

std::string FieldInterceptor::get_event_type( int eventId )
{
	static std::map<int,std::string> eventIdMap;
	//if (eventIdMap.empty())
	//{
	//	eventIdMap[IObserver::InitTrader]="初始化交易前端";
	//	eventIdMap[IObserver::InitMarket]="初始化行情前端";
	//	eventIdMap[IObserver::TraderFrontConnected]="连接到行情前端";
	//	eventIdMap[IObserver::MarketFrontConnected]="连接到交易前端";
	//	eventIdMap[IObserver::TraderFrontDisconnected]="交易前端断开";
	//	eventIdMap[IObserver::MarketFrontDisconnected]="行情前端断开";
	//	eventIdMap[IObserver::TraderUserLogined]="用户登录交易服务器";
	//	eventIdMap[IObserver::MarketUserLogined]="用户登录交易服务器";
	//}
	std::map<int,std::string>::iterator evtit=eventIdMap.find(eventId);
	if (evtit==eventIdMap.end())
	{
		return "...";
	}
	else
	{
		return evtit->second;
	}
}

map<char,string> FieldInterceptor::ContingentConditionTypeMap;

map<char,string> FieldInterceptor::OffsetFlagTypeMap;

map<char,string> FieldInterceptor::OrderStatusTypeMap;

map<char,string> FieldInterceptor::ProductTypeMap;

map<char,string> FieldInterceptor::PriceTypeMap;



std::string GetVariantDispString(const InputOrder& rsp,int id)
{
	stringstream ostrBuf;
	switch(id)
	{
	case SLH_BrokerID:
		ostrBuf<<rsp.BrokerID;
		break;
	case SLH_UserID:
		ostrBuf<<rsp.UserID;
		break;
	case SLH_InvestorID:
		ostrBuf<<rsp.InvestorID;
		break;
	case SLH_CODE:
		ostrBuf<<rsp.InstrumentID;
		break;
	case SLH_OrderPriceType:
		ostrBuf<<FieldInterceptor::get_price_type(rsp.OrderPriceType);
		break;
	case SLH_Direction:
		ostrBuf<<FieldInterceptor::get_direction_type(rsp.Direction);
		break;
	case SLH_CombOffsetFlag_0:
		ostrBuf<<rsp.CombOffsetFlag[0];
		break;
	case SLH_CombOffsetFlag_1:
		ostrBuf<<rsp.CombOffsetFlag[1];
		break;
	case SLH_CombOffsetFlag_2:
		ostrBuf<<rsp.CombOffsetFlag[2];
		break;
	case SLH_CombOffsetFlag_3:
		ostrBuf<<rsp.CombOffsetFlag[3];
		break;
	case SLH_CombOffsetFlag_4:
		ostrBuf<<rsp.CombOffsetFlag[4];
		break;
	case SLH_CombHedgeFlag_0:
		ostrBuf<<rsp.CombHedgeFlag[0];
		break;
	case SLH_CombHedgeFlag_1:
		ostrBuf<<rsp.CombHedgeFlag[1];
		break;
	case SLH_CombHedgeFlag_2:
		ostrBuf<<rsp.CombHedgeFlag[2];
		break;
	case SLH_CombHedgeFlag_3:
		ostrBuf<<rsp.CombHedgeFlag[3];
		break;
	case SLH_CombHedgeFlag_4:
		ostrBuf<<rsp.CombHedgeFlag[4];
		break;
	case SLH_LimitPrice:
		ostrBuf<<rsp.LimitPrice;
		break;
	case SLH_TimeCondition:
		ostrBuf<<FieldInterceptor::get_time_cond_type(rsp.TimeCondition);
		break;
	case SLH_GTDDate:
		ostrBuf<<rsp.GTDDate;
		break;
	case SLH_VolumeCondition:
		ostrBuf<<FieldInterceptor::get_volume_cond_type(rsp.VolumeCondition);
		break;
	case SLH_MinVolume:
		ostrBuf<<rsp.MinVolume;
		break;
	case SLH_ContingentCondition:
		ostrBuf<<FieldInterceptor::get_contingent_cond_type(rsp.ContingentCondition);
		break;
	case  SLH_StopPrice:
		ostrBuf<<rsp.StopPrice;
		break;
	//case SLH_ForceCloseReason:
	//	ostrBuf<<FieldInterceptor::get_force_close_reason(rsp.ForceCloseReason);
	//	break;
	//case SLH_IsAutoSuspend:
	//	ostrBuf<<rsp.IsAutoSuspend;
	//	break;
	//case SLH_BusinessUnit:
	//	ostrBuf<<rsp.BusinessUnit;
	//	break;
	//case SLH_RequestID:
	//	ostrBuf<<rsp.RequestID;
	//	break;
	//case SLH_UserForceClose:
	//	ostrBuf<<rsp.UserForceClose;
	//	break;
	}
	return ostrBuf.str();
}

string GetVariantDispString(const BASEDATA& rsp, int id)
{
	stringstream ostrBuf;
	switch(id)
	{
	case SLH_CODE:
		ostrBuf<<rsp.szCode;
		break;
	case SLH_NAME:
		ostrBuf<<rsp.szName;
		break;
	case SLH_EXCHANGEID:
		ostrBuf<<rsp.szExchange;
		break;
	case SLH_ExchangeName:
		ostrBuf<<rsp.ExchangeName;
		break;
	case SLH_ExchangeProperty:
		ostrBuf<<FieldInterceptor::get_exchange_property(rsp.ExchangeProperty);
		break;
	case SLH_ProductID:
		ostrBuf<<rsp.ProductID;
		break;
	case SLH_ProductClass:
		ostrBuf<<FieldInterceptor::get_product_type(rsp.ProductClass);
		break;
	case SLH_DeliveryYear:
		ostrBuf<<rsp.DeliveryYear;
		break;
	case SLH_DeliveryMonth:
		ostrBuf<<rsp.DeliveryMonth;
		break;
	case SLH_MaxMarketOrderVolume:
		ostrBuf<<rsp.MaxMarketOrderVolume;
		break;
	case SLH_MinMarketOrderVolume:
		ostrBuf<<rsp.MinMarketOrderVolume;
		break;
	case SLH_MaxLimitOrderVolume:
		ostrBuf<<rsp.MaxLimitOrderVolume;
		break;
	case SLH_MinLimitOrderVolume:
		ostrBuf<<rsp.MinLimitOrderVolume;
		break;
	case SLH_VolumeMultiple:
		ostrBuf<<rsp.VolumeMultiple;
		break;
	case SLH_PriceTick:
		ostrBuf<<rsp.PriceTick;
		break;
	case SLH_CreateDate:
		ostrBuf<<rsp.CreateDate;
		break;
	case SLH_OpenDate:
		ostrBuf<<rsp.OpenDate;
		break;
	case SLH_ExpireDate:
		ostrBuf<<rsp.ExpireDate;
		break;
	case SLH_StartDelivDate:
		ostrBuf<<rsp.StartDelivDate;
		break;
	case SLH_EndDelivDate:
		ostrBuf<<rsp.EndDelivDate;
		break;
	case SLH_InstLifePhase:
		ostrBuf<<FieldInterceptor::get_inst_life_phase_type(rsp.InstLifePhase);
		break;
	case SLH_IsTrading:
		ostrBuf<<rsp.IsTrading?"是":"否";
		break;
	case  SLH_PositionType:
		ostrBuf<<FieldInterceptor::get_position_type(rsp.PositionType);
		break;
	case SLH_PositionDateType:
		ostrBuf<<FieldInterceptor::get_position_date_type(rsp.PositionDateType);
		break;
	case SLH_LongMarginRatio:
		ostrBuf<<rsp.LongMarginRatio;
		break;
	case SLH_ShortMarginRatio:
		ostrBuf<<rsp.ShortMarginRatio;
		break;
	}
	return ostrBuf.str();
}

//std::string GetVariantDispString(const TransferSerial& rsp, int id)
//{
//	stringstream ostrBuf;
//	switch(id)
//	{
//	case SLH_PlateSerial:
//		ostrBuf<<rsp.PlateSerial;
//		break;
//	case SLH_TradeDate:
//		ostrBuf<<rsp.TradeDate;
//		break;
//	case SLH_TradeTime:
//		ostrBuf<<rsp.TradeTime;
//		break;
//	case SLH_TradeCode:
//		ostrBuf<<rsp.TradeCode;
//		break;
//	case SLH_SessionID:
//		ostrBuf<<rsp.SessionID;
//		break;
//	case SLH_BankID:
//		ostrBuf<<rsp.BankID;
//		break;
//	case SLH_BankBranchID:
//		ostrBuf<<rsp.BankBranchID;
//		break;
//	case SLH_BankAccType:
//		ostrBuf<<rsp.BankAccType;
//		break;
//	case SLH_BankAccount:
//		ostrBuf<<rsp.BankAccount;
//		break;
//	case SLH_BankSerial:
//		ostrBuf<<rsp.BankSerial;
//		break;
//	case SLH_BrokerBranchID:
//		ostrBuf<<rsp.BrokerBranchID;
//		break;
//	case SLH_FutureAccType:
//		ostrBuf<<rsp.FutureAccType;
//		break;
//	case SLH_AccountID:
//		ostrBuf<<rsp.AccountID;
//		break;
//	case SLH_FutureSerial:
//		ostrBuf<<rsp.FutureSerial;
//		break;
//	case SLH_IdCardType:
//		ostrBuf<<rsp.IdCardType;
//		break;
//	case SLH_IdentifiedCardNo:
//		ostrBuf<<rsp.IdentifiedCardNo;
//		break;
//	case SLH_CurrencyID:
//		ostrBuf<<rsp.CurrencyID;
//		break;
//	case SLH_TradeAmount:
//		ostrBuf<<rsp.TradeAmount;
//		break;
//	case SLH_CustFee:
//		ostrBuf<<rsp.CustFee;
//		break;
//	case SLH_BrokerFee:
//		ostrBuf<<rsp.BrokerFee;
//		break;
//	case SLH_AvailabilityFlag:
//		ostrBuf<<rsp.AvailabilityFlag;
//		break;
//	case SLH_OperatorCode:
//		ostrBuf<<rsp.OperatorCode;
//		break;
//	case SLH_BankNewAccount:
//		ostrBuf<<rsp.BankNewAccount;
//		break;
//	case SLH_ErrorID:
//		ostrBuf<<rsp.ErrorID;
//		break;
//	case SLH_ErrorMsg:
//		ostrBuf<<rsp.ErrorMsg;
//		break;
//	}
//	return ostrBuf.str();
//}

std::string GetVariantDispString(const UserInfo& rsp, int id)
{
	stringstream ostrBuf;
	switch(id)
	{
	case SLH_DATE:
		ostrBuf<<rsp.TradingDay;
		break;
	case SLH_LoginTime:
		ostrBuf<<rsp.LoginTime;
		break;
	case SLH_SessionID:
		ostrBuf<<rsp.SessionID;
		break;
	case SLH_SystemName:
		ostrBuf<<rsp.SystemName;
		break;
	case SLH_FrontID:
		ostrBuf<<rsp.FrontID;
		break;
	case SLH_MaxOrderRef:
		ostrBuf<<rsp.MaxOrderRef;
		break;
	case SLH_SHFETime:
		ostrBuf<<rsp.ExchangeTime;
		break;
	case SLH_DCETime:
		ostrBuf<<rsp.ExchangeTime;
		break;
	case SLH_CZCETime:
		ostrBuf<<rsp.ExchangeTime;
		break;
	case SLH_FFEXTime:
		ostrBuf<<rsp.ExchangeTime;
		break;
	case SLH_InvestorID:
		ostrBuf << rsp.InvestorID;
		break;
	}
	return ostrBuf.str();
}

std::string GetVariantDispString(const InvestorPosition& ta, int id)
{
	stringstream ostrBuf;
	switch(id)
	{
	case SLH_CODE:
		ostrBuf<<ta.InstrumentID;
		break;
	case SLH_DATE:
		ostrBuf<<ta.TradingDay;
		break;
	case SLH_BrokerID:
		ostrBuf<<ta.BrokerID;
		break;
	case SLH_UserID:
		case SLH_InvestorID:
		ostrBuf<<ta.InvestorID;
		break;
	case SLH_PosiDirection:
		ostrBuf<<FieldInterceptor::get_position_direction_type(ta.PosiDirection);
		break;
	case SLH_HedgeFlag:
		ostrBuf<<FieldInterceptor::get_hedge_flag_type(ta.HedgeFlag);
		break;
	case SLH_YdPosition:
		ostrBuf<<ta.YdPosition;
		break;
	case SLH_Position:
		ostrBuf<<ta.Position;
		break;
	case SLH_LongFrozen:
		ostrBuf<<ta.LongFrozen;
		break;
	case SLH_ShortFrozen:
		ostrBuf<<ta.ShortFrozen;
		break;
	case SLH_LongFrozenAmount:
		ostrBuf<<ta.LongFrozenAmount;
		break;
	case SLH_ShortFrozenAmount:
		ostrBuf<<ta.ShortFrozenAmount;
		break;
	case SLH_OpenVolume:
		ostrBuf<<ta.OpenVolume;
		break;
	case SLH_CloseVolume:
		ostrBuf<<ta.CloseVolume;
		break;
	case SLH_OpenAmount:
		ostrBuf<<ta.OpenAmount;
		break;
	case SLH_CloseAmount:
		ostrBuf<<ta.CloseAmount;
		break;
	case SLH_PositionCost:
		ostrBuf<<ta.PositionCost;
		break;
	case SLH_PreMargin:
		ostrBuf<<ta.PreMargin;
		break;
	case SLH_UseMargin:
		ostrBuf<<ta.UseMargin;
		break;
	case SLH_FrozenMargin:
		ostrBuf<<ta.FrozenMargin;
		break;
	case SLH_FrozenCash:
		ostrBuf<<ta.FrozenCash;
		break;
	case SLH_FrozenCommission:
		ostrBuf<<ta.FrozenCommission;
		break;
	case SLH_CashIn:
		ostrBuf<<ta.CashIn;
		break;
	case SLH_Commission:
		ostrBuf<<ta.Commission;
		break;
	case SLH_CloseProfit:
		ostrBuf<<ta.CloseProfit;
		break;
	case SLH_PositionProfit:
		ostrBuf<<ta.PositionProfit;
		break;
	case SLH_PreSettlementPrice:
		ostrBuf<<ta.PreSettlementPrice;
		break;
	case SLH_SettlementPrice:
		ostrBuf<<ta.SettlementPrice;
		break;
	case SLH_SettlementID:
		ostrBuf<<ta.SettlementID;
		break;
	case SLH_OpenCost:
		ostrBuf<<ta.OpenCost;
		break;
	case SLH_ExchangeMargin:
		ostrBuf<<ta.ExchangeMargin;
		break;
	case SLH_CombPosition:
		ostrBuf<<ta.FrozenCash;
		break;
	case SLH_CombLongFrozen:
		ostrBuf<<ta.CombLongFrozen;
		break;
	case SLH_CombShortFrozen:
		ostrBuf<<ta.CombShortFrozen;
		break;
	case SLH_CloseProfitByDate:
		ostrBuf<<ta.CloseProfitByDate;
		break;
	case SLH_CloseProfitByTrade:
		ostrBuf<<ta.CloseProfitByTrade;
		break;
	case SLH_TodayPosition:
		ostrBuf<<ta.TodayPosition;
		break;
	case SLH_MarginRateByMoney:
		ostrBuf<<ta.MarginRateByMoney;
		break;
	case SLH_MarginRateByVolume:
		ostrBuf<<ta.MarginRateByVolume;
		break;
	}
	return ostrBuf.str();
}

std::string GetVariantDispString(const TradingAccount& ta, int id)
{
	stringstream ostrBuf;
	switch(id)
	{
	case SLH_DATE:
		ostrBuf<<ta.TradingDay;
		break;
	case SLH_BrokerID:
		ostrBuf<<ta.BrokerID;
		break;
	case SLH_PreMortgage:
		ostrBuf<<ta.PreMortgage;
		break;
	case SLH_PreCredit:
		ostrBuf<<ta.PreCredit;
		break;
	case SLH_Deposit:
		ostrBuf<<ta.PreDeposit;
		break;
	case SLH_PreBalance:
		ostrBuf<<ta.PreBalance;
		break;
	case SLH_PreMargin:
		ostrBuf<<ta.PreMargin;
		break;
	case SLH_InterestBase:
		ostrBuf<<ta.InterestBase;
		break;
	case SLH_Interest:
		ostrBuf<<ta.Interest;
		break;
	case SLH_Withdraw:
		ostrBuf<<ta.Withdraw;
		break;
	case SLH_FrozenMargin:
		ostrBuf<<ta.FrozenMargin;
		break;
	case SLH_FrozenCash:
		ostrBuf<<ta.FrozenCash;
		break;
	case SLH_FrozenCommission:
		ostrBuf<<ta.FrozenCommission;
		break;
	case SLH_CurrMargin:
		ostrBuf<<ta.CurrMargin;
		break;
	case SLH_CashIn:
		ostrBuf<<ta.CashIn;
		break;
	case SLH_Commission:
		ostrBuf<<ta.Commission;
		break;
	case SLH_CloseProfit:
		ostrBuf<<ta.CloseProfit;
		break;
	case SLH_PositionProfit:
		ostrBuf<<ta.PositionProfit;
		break;
	case SLH_Balance:
		ostrBuf<<ta.Balance;
		break;
//case SLH_StaticBalance:
//	ostrBuf<<ta.StaticBalance;
//	break;
//case SLH_DynamicBalance:
//	ostrBuf<<ta.DynamicBalance;
//	break;
	}
	return ostrBuf.str();
}

std::string GetVariantDispString(const Trade& rsp, int id)
{
	stringstream ostrBuf;
	switch(id)
	{
	case SLH_BrokerID:
		ostrBuf<<rsp.BrokerID;
		break;
	case SLH_InvestorID:
		ostrBuf<<rsp.InvestorID;
		break;
	case SLH_CODE:
		ostrBuf<<rsp.InstrumentID;
		break;
	case SLH_OrderRef:
		ostrBuf<<rsp.OrderRef;
		break;
	case SLH_UserID:
		ostrBuf<<rsp.UserID;
		break;
	case SLH_EXCHANGEID:
		ostrBuf<<rsp.ExchangeID;
		break;
	case SLH_TradeID:
		ostrBuf<<rsp.TradeID;
		break;
	case SLH_Direction:
		ostrBuf<<FieldInterceptor::get_direction_type(rsp.Direction);
		break;
	case SLH_OrderSysID:
		ostrBuf<<rsp.OrderSysID;
		break;
	case SLH_ParticipantID:
		ostrBuf<<rsp.ParticipantID;
		break;
	case SLH_ClientID:
		ostrBuf<<rsp.ClientID;
		break;
	case SLH_TradingRole:
		ostrBuf<<FieldInterceptor::get_trade_role_type(rsp.TradingRole);
		break;
	case SLH_ExchangeInstID:
		ostrBuf<<rsp.ExchangeInstID;
		break;
	case SLH_OffsetFlag:
		ostrBuf<<FieldInterceptor::get_offset_flag_type(rsp.OffsetFlag);
		break;
	case SLH_HedgeFlag:
		ostrBuf<<FieldInterceptor::get_hedge_flag_type(rsp.HedgeFlag);
		break;
	case SLH_LimitPrice:
		ostrBuf<<rsp.Price;
		break;
	case SLH_VOLUME:
		ostrBuf<<rsp.Volume;
		break;
	case SLH_TradeDate:
		ostrBuf<<rsp.TradeDate;
		break;
	case SLH_TradeTime:
		ostrBuf<<rsp.TradeTime;
		break;
	case SLH_TradeType:
		ostrBuf<<FieldInterceptor::get_trade_type(rsp.TradeType);
		break;
	case SLH_PriceSource:
		ostrBuf<<FieldInterceptor::get_price_source_type(rsp.PriceSource);
		break;
	case SLH_TraderID:
		ostrBuf<<rsp.TraderID;
		break;
	case SLH_OrderLocalID:
		ostrBuf<<rsp.OrderLocalID;
		break;
	case SLH_ClearingPartID:
		ostrBuf<<rsp.ClearingPartID;
		break;
	case SLH_BusinessUnit:
		ostrBuf<<rsp.BusinessUnit;
		break;
	case SLH_SequenceNo:
		ostrBuf<<rsp.SequenceNo;
		break;
	case SLH_TradingDay:
		ostrBuf<<rsp.TradingDay;
		break;
	case SLH_SettlementID:
		ostrBuf<<rsp.SettlementID;
		break;
	case SLH_BrokerOrderSeq:
		ostrBuf<<rsp.BrokerOrderSeq;
		break;
	}
	return ostrBuf.str();
}

//std::string GetVariantDispString(const ContractBank& rsp, int id)
//{
//	stringstream ostrBuf;
//	switch(id)
//	{
//	case SLH_BrokerID:
//		ostrBuf<<rsp.BrokerID;
//		break;
//	case SLH_BankID:
//		ostrBuf<<rsp.BankID;
//		break;
//	case SLH_BankName:
//		ostrBuf<<rsp.BankName;
//		break;
//	case SLH_BankBranchID:
//		ostrBuf<<rsp.BankBrchID;
//		break;
//	}
//	return ostrBuf.str();
//}
//
std::string GetVariantDispString(const ParkedOrder& rsp, int id)
{

	stringstream ostrBuf;
	switch(id)
	{
	case SLH_BrokerID:
		ostrBuf<<rsp.BrokerID;
		break;
	case SLH_InvestorID:
		ostrBuf<<rsp.InvestorID;
		break;
	case SLH_CODE:
		ostrBuf<<rsp.InstrumentID;
		break;
	case SLH_OrderPriceType:
		ostrBuf<<FieldInterceptor::get_price_type((char)rsp.OrderPriceType);
		break;
	case SLH_Direction:
		ostrBuf<<FieldInterceptor::get_direction_type(rsp.Direction);
		break;
	case SLH_CombOffsetFlag_0:
		ostrBuf<<FieldInterceptor::get_offset_flag_type(rsp.CombOffsetFlag[0]);
		break;
	case SLH_CombHedgeFlag_0:
		ostrBuf << FieldInterceptor::get_hedge_flag_type(rsp.CombHedgeFlag[0]);
		break;
	case SLH_LimitPrice:
		ostrBuf<<rsp.LimitPrice;
		break;
	case SLH_VolumeTotalOriginal:
		ostrBuf<<rsp.VolumeTotalOriginal;
		break;
	case SLH_VolumeCondition:
		ostrBuf<<FieldInterceptor::get_volume_cond_type(rsp.VolumeCondition);
		break;
	case SLH_ContingentCondition:
		ostrBuf<<FieldInterceptor::get_contingent_cond_type(rsp.ContingentCondition);
		break;
	case SLH_EXCHANGEID:
		ostrBuf<<rsp.ExchangeID;
		break;
	case SLH_ParkedOrderID:
		ostrBuf<<rsp.ParkedOrderID;
		break;
	case SLH_Status:
		ostrBuf<<FieldInterceptor::get_park_order_status(rsp.Status);
		break;

	}
	return ostrBuf.str();
}

string GetVariantDispString(const MINUTE& rsp, int id)
{
	stringstream ostrBuf;
	switch(id)
	{
	case SLH_CODE:
		ostrBuf<<rsp.szCode;
		break;
	case SLH_EXCHANGEID:
		ostrBuf<<rsp.szExchange;
		break;
	case SLH_HIGH:
		ostrBuf<<rsp.HighestPrice;
		break;
	case SLH_LOW:
		ostrBuf<<rsp.LowestPrice;
		break;
	case SLH_LASTPRICE:
		ostrBuf<<rsp.LastPrice;
		break;
	case SLH_AVERAGE:
		ostrBuf<<rsp.AveragePrice;
		break;
	case SLH_VOLUME:
		ostrBuf<<rsp.Volume;
		break;
	case SLH_TURNOVER:
		ostrBuf<<rsp.Turnover;
		break;
	}
	return ostrBuf.str();
}

std::string GetVariantDispString(const Tick& rsp, int id)
{
	stringstream ostrBuf;
	switch(id)
	{
	case SLH_CODE:
		ostrBuf<<rsp.szCode;
		break;
	case SLH_EXCHANGEID:
		ostrBuf<<rsp.szExchange;
		break;
	case SLH_NAME:
		ostrBuf<<rsp.szCode;
		break;
	case SLH_TradeTime:
		ostrBuf<<rsp.UpdateTime;
		break;
	case SLH_PRECLOSE:
		ostrBuf<<rsp.PreClosePrice;
		break;
	case SLH_OPEN:
		ostrBuf<<rsp.OpenPrice;
		break;
	case SLH_CLOSE:
		ostrBuf<<rsp.ClosePrice;
		break;
	case SLH_HIGH:
		ostrBuf<<rsp.HighestPrice;
		break;
	case SLH_LOW:
		ostrBuf<<rsp.LowestPrice;
		break;
	case SLH_LASTPRICE:
		ostrBuf<<rsp.LastPrice;
		break;
	case SLH_AVERAGE:
		ostrBuf<<rsp.AveragePrice;
		break;
	case SLH_VOLUME:
		ostrBuf<<rsp.Volume;
		break;
	case SLH_TURNOVER:
		ostrBuf<<rsp.Turnover;
		break;
	}
	return ostrBuf.str();
}
//std::string GetVariantDispString(const OrderField& rsp, int id)
//{
//	stringstream ostrBuf;
//	switch(id)
//	{
//	case SLH_BrokerID:
//		ostrBuf<<rsp.BrokerID;
//		break;
//	case SLH_UserID:
//		ostrBuf<<rsp.UserID;
//		break;
//	case SLH_InvestorID:
//		ostrBuf<<rsp.InvestorID;
//		break;
//	case SLH_CODE:
//		ostrBuf<<rsp.InstrumentID;
//		break;
//	case SLH_OrderPriceType:
//		ostrBuf<<FieldInterceptor::get_price_type(rsp.OrderPriceType);
//		break;
//	case SLH_Direction:
//		ostrBuf<<FieldInterceptor::get_direction_type(rsp.Direction);
//		break;
//	case SLH_CombOffsetFlag_0:
//		ostrBuf<<FieldInterceptor::get_offset_flag_type(rsp.CombOffsetFlag[0]);
//		break;
//	case SLH_CombOffsetFlag_1:
//		ostrBuf<<FieldInterceptor::get_offset_flag_type(rsp.CombOffsetFlag[1]);
//		break;
//	case SLH_CombOffsetFlag_2:
//		ostrBuf<<FieldInterceptor::get_offset_flag_type(rsp.CombOffsetFlag[2]);
//		break;
//	case SLH_CombOffsetFlag_3:
//		ostrBuf<<FieldInterceptor::get_offset_flag_type(rsp.CombOffsetFlag[3]);
//		break;
//	case SLH_CombOffsetFlag_4:
//		ostrBuf<<FieldInterceptor::get_offset_flag_type(rsp.CombOffsetFlag[4]);
//		break;
//	case SLH_CombHedgeFlag_0:
//		ostrBuf<<FieldInterceptor::get_hedge_flag_type(rsp.CombHedgeFlag[0]);
//		break;
//	case SLH_CombHedgeFlag_1:
//		ostrBuf<<FieldInterceptor::get_hedge_flag_type(rsp.CombHedgeFlag[1]);
//		break;
//	case SLH_CombHedgeFlag_2:
//		ostrBuf<<FieldInterceptor::get_hedge_flag_type(rsp.CombHedgeFlag[2]);
//		break;
//	case SLH_CombHedgeFlag_3:
//		ostrBuf<<FieldInterceptor::get_hedge_flag_type(rsp.CombHedgeFlag[3]);
//		break;
//	case SLH_CombHedgeFlag_4:
//		ostrBuf<<FieldInterceptor::get_hedge_flag_type(rsp.CombHedgeFlag[4]);
//		break;
//	case SLH_LimitPrice:
//		ostrBuf<<rsp.LimitPrice;
//		break;
//	case SLH_VolumeTotalOriginal:
//		ostrBuf<<rsp.VolumeTotalOriginal;
//		break;
//	case SLH_TimeCondition:
//		ostrBuf<<FieldInterceptor::get_time_cond_type(rsp.TimeCondition);
//		break;
//	case SLH_GTDDate:
//		ostrBuf<<rsp.GTDDate;
//		break;
//	case SLH_VolumeCondition:
//		ostrBuf<<FieldInterceptor::get_volume_cond_type(rsp.VolumeCondition);
//		break;
//	case SLH_MinVolume:
//		ostrBuf<<rsp.MinVolume;
//		break;
//	case SLH_ContingentCondition:
//		ostrBuf<<FieldInterceptor::get_contingent_cond_type(rsp.ContingentCondition);
//		break;
//	case  SLH_StopPrice:
//		ostrBuf<<rsp.StopPrice;
//		break;
//	case SLH_ForceCloseReason:
//		ostrBuf<<FieldInterceptor::get_force_close_reason(rsp.ForceCloseReason);
//		break;
//	case SLH_IsAutoSuspend:
//		ostrBuf<<rsp.IsAutoSuspend;
//		break;
//	case SLH_BusinessUnit:
//		ostrBuf<<rsp.BusinessUnit;
//		break;
//	case SLH_RequestID:
//		ostrBuf<<rsp.RequestID;
//		break;
//	case SLH_UserForceClose:
//		ostrBuf<<FieldInterceptor::get_force_close_reason(rsp.UserForceClose);
//		break;
//	case SLH_ZCETotalTradedVolume:
//		ostrBuf<<rsp.ZCETotalTradedVolume;
//		break;
//	case SLH_RelativeOrderSysID:
//		ostrBuf<<rsp.RelativeOrderSysID;
//		break;
//	case SLH_AcitveUserID:
//		ostrBuf<<rsp.ActiveUserID;
//		break;
//	case SLH_UserProductInfo:
//		ostrBuf<<rsp.UserProductInfo;
//		break;
//	case SLH_ExchangeID:
//		ostrBuf<<rsp.ExchangeID;
//		break;
//	case SLH_CancelTime:
//		ostrBuf<<rsp.CancelTime;
//		break;
//	case SLH_UpdateTime:
//		ostrBuf<<rsp.UpdateTime;
//		break;
//	case SLH_SuspendTime:
//		ostrBuf<<rsp.SuspendTime;
//		break;
//	case SLH_ActiveTime:
//		ostrBuf<<rsp.ActiveTime;
//		break;
//	case SLH_InsertTime:
//		ostrBuf<<rsp.InsertTime;
//		break;
//	case SLH_InsertDate:
//		ostrBuf<<rsp.InsertDate;
//		break;
//	case SLH_VolumeTotal:
//		ostrBuf<<rsp.VolumeTotal;
//		break;
//	case SLH_VolumeTraded:
//		ostrBuf<<rsp.VolumeTraded;
//		break;
//	case SLH_OrderType:
//		ostrBuf<<FieldInterceptor::get_order_type(rsp.OrderType);
//		break;
//	case SLH_OrderStatus:
//		ostrBuf<<FieldInterceptor::get_order_status_type(rsp.OrderStatus);
//		break;
//	case SLH_OrderRef:
//		ostrBuf<<rsp.OrderRef;
//		break;
//	case SLH_OrderLocalID:
//		ostrBuf<<rsp.OrderLocalID;
//		break;
//	case SLH_InstallID:
//		ostrBuf<<rsp.InstallID;
//		break;
//	case SLH_SettlementID:
//		ostrBuf<<rsp.SettlementID;
//		break;
//	case SLH_ClientID:
//		ostrBuf<<rsp.ClientID;
//		break;
//	case SLH_TraderID:
//		ostrBuf<<rsp.TraderID;
//		break;
//	case SLH_ExchangeInstID:
//		ostrBuf<<rsp.ExchangeInstID;
//		break;
//	case SLH_OrderSubmitStatus:
//		ostrBuf<<FieldInterceptor::get_order_submit_status_type(rsp.OrderSubmitStatus);
//		break;
//	case SLH_OrderSource:
//		ostrBuf<<rsp.OrderSource;
//		break;
//	case SLH_NotifySequence:
//		ostrBuf<<rsp.NotifySequence;
//		break;
//	case SLH_StatusMsg:
//		ostrBuf<<rsp.StatusMsg;
//		break;
//	case SLH_ParticipantID:
//		ostrBuf<<rsp.ParticipantID;
//		break;
//	case SLH_SequenceNo:
//		ostrBuf<<rsp.SequenceNo;
//		break;
//	case SLH_OrderSysID:
//		ostrBuf<<rsp.OrderSysID;
//		break;
//	default:
//		ostrBuf<<"--";
//	}
//	return ostrBuf.str();
//}

std::string GetVariantDispString(const InvestorPositionDetail& ta, int id)
{
	stringstream ostrBuf;
	switch(id)
	{
	case SLH_CODE:
		ostrBuf<<ta.InstrumentID;
		break;
	case SLH_InvestorID:
		ostrBuf<<ta.InvestorID;
		break;
	case SLH_UserID:
		ostrBuf<<ta.InvestorID;
		break;
	case SLH_BrokerID:
		ostrBuf<<ta.BrokerID;
		break;
	case SLH_HedgeFlag:
		ostrBuf<<FieldInterceptor::get_hedge_flag_type(ta.HedgeFlag);
		break;
	case SLH_Direction:
		ostrBuf<<FieldInterceptor::get_direction_type(ta.Direction);
		break;
	case SLH_OpenDate:
		ostrBuf<<ta.OpenDate;
		break;
	case SLH_TradeID:
		ostrBuf<<ta.TradeID;
		break;
	case SLH_VOLUME:
		ostrBuf<<ta.Volume;
		break;
	case SLH_OPEN:
		ostrBuf<<ta.OpenPrice;
		break;
	case SLH_TradingDay:
		ostrBuf<<ta.TradingDay;
		break;
	case SLH_SettlementID:
		ostrBuf<<ta.SettlementID;
		break;
	case SLH_TradeType:
		ostrBuf<<FieldInterceptor::get_trade_type(ta.TradeType);
		break;
	case SLH_CombInstrumentID:
		ostrBuf<<ta.CombInstrumentID;
		break;
	case SLH_EXCHANGEID:
		ostrBuf<<ta.ExchangeID;
		break;
	case SLH_CloseProfitByDate:
		ostrBuf<<ta.CloseProfitByDate;
		break;
	case SLH_CloseProfitByTrade:
		ostrBuf<<ta.CloseProfitByTrade;
		break;
	case SLH_PositionProfitByDate:
		ostrBuf<<ta.PositionProfitByDate;
		break;
	case SLH_Margin:
		ostrBuf<<ta.Margin;
		break;
	case SLH_ExchangeMargin:
		ostrBuf<<ta.ExchMargin;
		break;
	case SLH_MarginRateByMoney:
		ostrBuf<<ta.MarginRateByMoney;
		break;
	case SLH_MarginRateByVolume:
		ostrBuf<<ta.MarginRateByVolume;
		break;
	//case SLH_PreSettlementPrice:
	//	ostrBuf<<ta.PreSettlementPrice;
	//	break;
	case SLH_SettlementPrice:
		ostrBuf<<ta.SettlementPrice;
		break;
	case SLH_CloseVolume:
		ostrBuf<<ta.CloseVolume;
		break;
	case SLH_CloseAmount:
		ostrBuf<<ta.CloseAmount;
		break;
	default:
		ostrBuf<<"--";
	}
	return ostrBuf.str();
}

std::string GetVariantDispString(const InvestorPositionCombineDetail &ta, int id)
{
	stringstream ostrBuf;
	switch(id)
	{
	case SLH_CODE:
		ostrBuf<<ta.InstrumentID;
		break;
	case SLH_InvestorID:
		ostrBuf<<ta.InvestorID;
		break;
	case SLH_BrokerID:
		ostrBuf<<ta.BrokerID;
		break;
	case SLH_HedgeFlag:
		ostrBuf<<ta.HedgeFlag;
		break;
	case SLH_Direction:
		ostrBuf<<ta.Direction;
		break;
	case SLH_OpenDate:
		ostrBuf<<ta.OpenDate;
		break;
	case SLH_TradeID:
		ostrBuf<<ta.TradeID;
		break;
	case SLH_TradingDay:
		ostrBuf<<ta.TradingDay;
		break;
	case SLH_SettlementID:
		ostrBuf<<ta.SettlementID;
		break;
	case SLH_CombInstrumentID:
		ostrBuf<<ta.CombInstrumentID;
		break;
	case SLH_EXCHANGEID:
		ostrBuf<<ta.ExchangeID;
		break;
	case SLH_TotalAmt:
		ostrBuf<<ta.TotalAmt;
		break;
	case SLH_Margin:
		ostrBuf<<ta.Margin;
		break;
	case SLH_ExchangeMargin:
		ostrBuf<<ta.ExchMargin;
		break;
	case SLH_MarginRateByMoney:
		ostrBuf<<ta.MarginRateByMoney;
		break;
	case SLH_MarginRateByVolume:
		ostrBuf<<ta.MarginRateByVolume;
		break;
	case SLH_LegID:
		ostrBuf<<ta.LegID;
		break;
	case SLH_LegMultiple:
		ostrBuf<<ta.LegMultiple;
		break;
	}
	return ostrBuf.str();
}
