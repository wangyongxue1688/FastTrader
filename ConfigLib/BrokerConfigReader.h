#ifndef _BROKER_CONFIG_READER_H_
#define _BROKER_CONFIG_READER_H_
#include "ConfigReader.h"
#include <unordered_map>
//经纪公司配置文件读取类;
class CONFIGLIB_API BrokerConfigReader
	:public ConfigReader
{
public:
	BrokerConfigReader(void);
	virtual ~BrokerConfigReader(void);
	std::unordered_map<std::string,ServerInfo>& GetBrokers(){return m_brokers;}
public:
	virtual bool Unload();
	virtual bool Load(const std::string& config_file="brokers.xml",
		bool bForceLoad=false);
protected:
	bool LoadBrokers();
protected:
	std::unordered_map<std::string,ServerInfo> m_brokers;
public:
	static BrokerConfigReader& GetInstance();
};

#endif

