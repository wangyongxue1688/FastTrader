#ifndef  _DATA_STORE_CONFIG_READER_H_
#define  _DATA_STORE_CONFIG_READER_H_


#include "ConfigReader.h"
#include "../common/DataTypes.h"


//配置参数;
struct DataStoreItem
{
	std::string DataStoreID;
	std::string Path;
	std::string Uri;
	//与之关联的用户;
	std::string UserID;
	int StoreType;
	size_t Threads; //0表示与CPU核心数一致;
	size_t MaxItemPerThread;//每个线程的任务量;
	DataStoreItem()
	{
		Threads = 4;
		MaxItemPerThread = 1000;
		StoreType = 0x08; //dbtypeSqlite3
	}
};

class CONFIGLIB_API DataStoreConfigReader :
	public ConfigReader
{
public:
	DataStoreConfigReader(void);
	~DataStoreConfigReader(void);
	std::map<std::string,DataStoreItem>& GetDataStores();
	bool LoadInstruments(std::vector<InstrumentCode>& stockItems,
		const std::string& config_file="instruments.xml");
protected:
	bool LoadDataStores();
public:
	virtual bool UnLoad();
	virtual bool Load(const std::string& config_file="user.xml",bool bForceLoad=false);
protected:
	std::map<std::string,DataStoreItem> m_DataStores;
public:
	static DataStoreConfigReader& GetInstance();
};

#endif

