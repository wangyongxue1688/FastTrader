//#include "stdafx.h"
#include "DataStoreConfigReader.h"


DataStoreConfigReader::DataStoreConfigReader(void)
{
}


DataStoreConfigReader::~DataStoreConfigReader(void)
{
}

std::map<std::string,DataStoreItem>& DataStoreConfigReader::GetDataStores()
{
	return m_DataStores;
}

bool DataStoreConfigReader::LoadInstruments(std::vector<InstrumentCode>& stockItems, const std::string& config_file/*="instruments.xml"*/)
{
	TiXmlDocument instDoc(config_file.c_str());
	if (!instDoc.LoadFile())
	{
		return false;
	}

	//获得根元素，即Persons。
	TiXmlElement* instRoot=instDoc.RootElement();
	if (NULL==root)
	{
		return false;
	}
	TiXmlElement* insElem=instRoot->FirstChildElement("ins");
	InstrumentCode sc;
	while (NULL!=insElem)
	{
		do{
			sc.MarketCode=GetAttribute(insElem,"exch");
			if (sc.MarketCode=="sh")
			{
				sc.ExchangeID="SSE";
			}
			else if(sc.MarketCode=="sz")
			{
				sc.ExchangeID="SZE";
			}
			else if (sc.MarketCode=="hg")
			{
				sc.ExchangeID="HGE";
			}
			else
			{
				//不知道是啥交易所了;
				break;
			}
			sc.InstrumentID=GetAttribute(insElem,"code");
			stockItems.push_back(sc);
		}while(0);
		insElem=insElem->NextSiblingElement("ins");
	}
	return true;
}

bool DataStoreConfigReader::LoadDataStores()
{
	//读取配置文件;
	bool bRet=false;
	std::string broker_tag_name="DataStoreItem";
	TiXmlElement* elem=root->FirstChildElement(broker_tag_name.c_str());
	while (elem)
	{
		do 
		{
			DataStoreItem dsi;
			dsi.DataStoreID =GetAttribute(elem,"DataStoreID");
			if (dsi.DataStoreID.empty())
			{
				break;
			}
			dsi.Path=GetAttribute(elem,"Path");
			dsi.Uri=GetAttribute(elem,"Uri");
			dsi.UserID=GetAttribute(elem,"UserID");
			//保存起来;
			m_DataStores[dsi.DataStoreID]=dsi;
			bRet=true;
		} while (false);
		elem=elem->NextSiblingElement(broker_tag_name.c_str());
	}
	return bRet;
}

bool DataStoreConfigReader::UnLoad()
{
	bool bUnLoad=ConfigReader::Unload();
	m_DataStores.clear();
	return bUnLoad;
}

bool DataStoreConfigReader::Load(const std::string& config_file/*="user.xml"*/,bool bForceLoad/*=false*/)
{
	if (!ConfigReader::Load(config_file,bForceLoad))
	{
		return false;
	}
	return LoadDataStores();
}

DataStoreConfigReader& DataStoreConfigReader::GetInstance()
{
	static DataStoreConfigReader g_DataStoreConfigReader;
	return g_DataStoreConfigReader;
}
