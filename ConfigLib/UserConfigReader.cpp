//#include "stdafx.h"
#include "UserConfigReader.h"
#include "../tinyxml/tinyxml.h"
#include "../tinyxml/tinystr.h"
#include <utility>


UserConfigReader::UserConfigReader(void)
	:ConfigReader()
{
}


UserConfigReader::~UserConfigReader(void)
{
}

bool UserConfigReader::LoadStrategyParams()
{
	//读取配置文件;
	bool bRet=false;
	string broker_tag_name="Strategy";
	TiXmlElement* elem=root->FirstChildElement(broker_tag_name.c_str());
	if (!elem)
	{
		bRet=false;
	}
	while (NULL!=elem)
	{
		do 
		{
			std::string StrategyID=GetAttribute(elem,"StrategyID");
			StrategyParam  sp;
			sp.StrategyID=StrategyID;

			sp.StrategyPath=GetAttribute(elem,"Path");

			if (sp.StrategyPath.empty())
			{
				break;
			}
			sp.UserID = GetAttribute(elem,"UserID");

			sp.Available = 0;
			std::string mainAvailable = GetAttribute(elem, "Available");
			if (!mainAvailable.empty())
			{
				sp.Available = std::stod(mainAvailable);
			}

			sp.TradingDay = GetAttribute(elem, "StartDay");

			TiXmlElement* inst_elem=elem->FirstChildElement("Instruments");
			if (!inst_elem)
			{
				break;
			}
			std::vector<TiXmlElement*> instruments_elems=GetSubElements(inst_elem);
			for (std::size_t i=0;i<instruments_elems.size();++i)
			{
				std::string instId=GetAttribute(instruments_elems[i],"InstrumentID");
				std::string DefaultHands=GetAttribute(instruments_elems[i],"DefaultHands");
				int iHands=atoi(DefaultHands.c_str());
				if (!instId.empty() && iHands>0)
				{
					sp.Instruments.insert(std::pair<std::string,int>(instId,iHands));
				}
			}
			TiXmlElement* exchg_elem=elem->FirstChildElement("Exchanges");
			if (exchg_elem)
			{
				std::vector<TiXmlElement*> exchange_elems=GetSubElements(exchg_elem);
				for (std::size_t i=0;i<exchange_elems.size();++i)
				{
					std::string ExchangeID=GetAttribute(exchange_elems[i],"ExchangeID");
					sp.Exchanges.push_back(ExchangeID);
				}
			}
			TiXmlElement* multiuser_elem=elem->FirstChildElement("MultiUser");
			if (inst_elem)
			{
				sp.SubUsers.clear();
				std::vector<TiXmlElement*> users_elems=GetSubElements(multiuser_elem);
				for (std::size_t k=0;k<users_elems.size();++k)
				{
					std::string muid=GetInnerText(users_elems[k]);
					
					if (!muid.empty())
					{
						double Available = 0;
						std::string szAvailable = GetAttribute(users_elems[k], "Available");
						if (!szAvailable.empty())
						{
							Available = std::stod(szAvailable);
						}
						sp.SubUsers.insert(std::make_pair(muid, Available));
					}
				}
			}
			m_StrategyParams.push_back(sp);
			bRet=true;
		} while (0);
		elem=elem->NextSiblingElement(broker_tag_name.c_str());
	}
	return bRet;
}

bool UserConfigReader::LoadUsers()
{
	//读取配置文件;
	bool bRet=false;
	std::string broker_tag_name="user";
	TiXmlElement* elem=root->FirstChildElement(broker_tag_name.c_str());
	if (!elem)
	{
		bRet=false;
	}
	while (NULL!=elem)
	{
		do 
		{
			UserLoginParam ulp;
			ulp.BrokerID=GetAttribute(elem,"BrokerID");
			if (ulp.BrokerID.empty())
			{
				break;
			}
			ulp.UserID=GetAttribute(elem,"UserID");
			if (ulp.UserID.empty())
			{
				break;
			}
			ulp.Server=GetAttribute(elem,"Server");
			ulp.Password=GetAttribute(elem,"Password");
			ulp.MdPassword=GetAttribute(elem,"MdPassword");
			if (ulp.MdPassword.empty())
			{
				ulp.MdPassword = ulp.Password;
			}
			ulp.MdBrokerID = GetAttribute(elem, "MdBrokerID");
			if (ulp.MdBrokerID.empty())
			{
				ulp.MdBrokerID = ulp.BrokerID;
			}

			ulp.MdUserID = GetAttribute(elem, "MdUserID");
			if (ulp.MdUserID.empty())
			{
				ulp.MdUserID = ulp.UserID;
			}
			ulp.InvestorID=GetAttribute(elem,"InvestorID");
			m_users.insert(std::pair<std::string,UserLoginParam>(ulp.UserID,ulp));
			bRet=true;
		} while (false);
		
		elem=elem->NextSiblingElement(broker_tag_name.c_str());
	}
	return bRet;
}

bool UserConfigReader::Unload()
{
	bool bUnLoad=ConfigReader::Unload();
	m_users.clear();
	m_StrategyParams.clear();
	return bUnLoad;
}

bool UserConfigReader::Load( const std::string& config_file/*="users.xml"*/, bool bForceLoad/*=false*/ )
{
	if (!ConfigReader::Load(config_file,bForceLoad))
	{
		return false;
	}
	if (!LoadUsers())
	{
		return false;
	}
	if (!LoadStrategyParams())
	{
		return false;
	}

	return true;
}

std::map<std::string,UserLoginParam>& UserConfigReader::GetUsers()
{
	return m_users;
}

UserConfigReader& UserConfigReader::GetInstance()
{
	static UserConfigReader g_UserConfigReader;
	return g_UserConfigReader;
}

std::vector<StrategyParam>& UserConfigReader::GetStrtegyParams()
{
	return m_StrategyParams;
}


