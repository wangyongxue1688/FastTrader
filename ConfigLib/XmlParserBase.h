#ifndef _XML_PARSER_BASE_H_
#define _XML_PARSER_BASE_H_
#include "configlib.h"
#include <string>
#include <vector>
#include <map>
using namespace std;
#include "tinyxml/tinystr.h"
#include "tinyxml/tinyxml.h"
/**
 * \class CXMLParserBase
 * \brief 自定义的XML解析器的基类
 * \remark 其他解析器可定制此类从而实现特定类型的解析
 *
 */
namespace configlib{
template<typename DOCUMENT,typename ELEMENT,typename NODE,typename XMLSTR>
class CONFIGLIB_API XmlParserBase
{
public:
	/**
	 * \public 
	 * \brief 构造函数和析构函数
	 */
	XmlParserBase(void){}
	explicit XmlParserBase(string file_path){}
	virtual ~XmlParserBase(void){}
public:
	typedef DOCUMENT xml_doc_type;
	typedef ELEMENT xml_elem_type;
	typedef NODE    xml_node_type;
	typedef XMLSTR   xml_str_type;
	bool load(const std::string& file_path){return false;}
	/**
	 * \brief 定义更多的公共操作
	 */
	virtual vector<ELEMENT*> GetSubElements(ELEMENT* parent_element) const
	{
		vector<ELEMENT*> v;
		return v;
	}
	//取值函数
	virtual string GetInnerText(ELEMENT* element) const
	{
		return "";
	}
	virtual int GetInnerInt(ELEMENT* element)
	{
		return 0;
	}
	virtual ELEMENT* GetElementByTagName(string tag_name) const
	{
		return NULL;
	}
	virtual void SetInnerText(ELEMENT* element,const string& text){}
	virtual void SetInnerText(ELEMENT* element,const int& digit){}
	virtual string GetAttributeValue(ELEMENT* element,string attrName){return "";}
	virtual void SetAttributeValue(ELEMENT* element,string attrName,string attrValue){}
	virtual void SetAttributeValue(ELEMENT* element,string attrName,int attrValue){}
	virtual bool HasAttribute(ELEMENT* element,string attrName){return false;}
	//virtual void SetSubElements(DOMElement* element,vector<string> values);
	virtual ELEMENT* GetSubElementByTagName(ELEMENT* parentElement,string tag_name){return NULL;}
	//创建一个换行结点
	virtual NODE* CreateNewLineNode(){return NULL;}
	//创建一个带属性的子结点
	virtual ELEMENT* CreateElementWithAttributes(ELEMENT* parentElement,
		string nodeName
		,map<string,string> attributes,string innerText){return NULL;}
	//移除所有子结点
	virtual void RemoveAllChildNodes(ELEMENT* parentElement){}
	//移除指定的子结点
	//virtual void RemoveChildByTagName(string tagName);
	virtual void RemoveChild(ELEMENT* parentElement,ELEMENT* subElement){}
	virtual void save(){}
	virtual string GetXmlFilePath() const{return xml_file_path;}
	virtual DOCUMENT* GetXmlDocument() const{return document;}
	virtual ELEMENT* GetRootNode(){return root;}
	
	
protected:
//#if _MSC_VER==1500
//	template class SERVICELIB_API vector<ELEMENT*>;

//#elif _MSC_VER==1600

//#else

//#endif
	
	//subElements
	vector<ELEMENT*> subElements;
	/**
	 * \var document
	 * \brief 文档对象
	 */
	DOCUMENT* document;
	/**
	 * \var root
	 * \brief 文档的根元素
	 */
	ELEMENT* root;

	/**
	 * \var xml_file_path
	 * \brief 配置文件的路径
	 */
	string xml_file_path;

protected:
	/**
	 * \protected 隐藏的接口
	 * \brief 拷贝构造函数和赋值操作符
	 *
	 */
	XmlParserBase(const XmlParserBase& );
	XmlParserBase& operator=(const XmlParserBase& );
};







template<>
class CONFIGLIB_API XmlParserBase<TiXmlDocument,TiXmlElement,TiXmlNode,TiXmlString>
{
public:
	/**
	 * \public 
	 * \brief 构造函数和析构函数
	 */
	XmlParserBase(void);
	explicit XmlParserBase(string file_path);
	virtual ~XmlParserBase(void);
public:
	typedef TiXmlDocument xml_doc_type;
	typedef TiXmlElement xml_elem_type;
	typedef TiXmlNode    xml_node_type;
	typedef TiXmlString   xml_str_type;
	bool load(const std::string& file_path);
	/**
	 * \brief 定义更多的公共操作
	 */
	virtual vector<TiXmlElement*> GetSubElements(TiXmlElement* parent_element) const;
	//取值函数
	virtual string GetInnerText(TiXmlElement* element) const;
	virtual int GetInnerInt(TiXmlElement* element);
	
	virtual TiXmlElement* GetElementByTagName(string tag_name) const;
	virtual void SetInnerText(TiXmlElement* element,const string& text);
	virtual void SetInnerText(TiXmlElement* element,const int& digit);
	virtual string GetAttributeValue(TiXmlElement* element,string attrName);
	virtual void SetAttributeValue(TiXmlElement* element,string attrName,string attrValue);
	virtual void SetAttributeValue(TiXmlElement* element,string attrName,int attrValue);
	virtual bool HasAttribute(TiXmlElement* element,string attrName);
	//virtual void SetSubElements(DOMElement* element,vector<string> values);
	virtual TiXmlElement* GetSubElementByTagName(TiXmlElement* parentElement,string tag_name);
	//创建一个带属性的子结点
	virtual TiXmlElement* CreateElementWithAttributes(TiXmlElement* parentElement,
		string nodeName
		,map<string,string> attributes,string innerText);
	//移除所有子结点
	virtual void RemoveAllChildNodes(TiXmlElement* parentElement);
	//移除指定的子结点
	//virtual void RemoveChildByTagName(string tagName);
	virtual void RemoveChild(TiXmlElement* parentElement,TiXmlElement* subElement);
	virtual void save();
	virtual string GetXmlFilePath() const;
	virtual TiXmlDocument* GetXmlDocument() const;

	virtual TiXmlElement* GetRootNode();
protected:
//#if _MSC_VER == 1500
//	template class SERVICELIB_API vector<TiXmlElement*>;
//#endif
	//subElements
	vector<TiXmlElement*> subElements;
	/**
	 * \var document
	 * \brief 文档对象
	 */
	TiXmlDocument* document;
	/**
	 * \var root
	 * \brief 文档的根元素
	 */
	TiXmlElement* root;

	/**
	 * \var number_of_references
	 * \brief 解析器的引用计数
	 */
	static int number_of_references;
	/**
	 * \var xml_file_path
	 * \brief 配置文件的路径
	 */
	string xml_file_path;

protected:
	/**
	 * \protected 隐藏的接口
	 * \brief 拷贝构造函数和赋值操作符
	 *
	 */
	XmlParserBase(const XmlParserBase& );
	XmlParserBase& operator=(const XmlParserBase& );
};
};

typedef configlib::XmlParserBase<TiXmlDocument,TiXmlElement,TiXmlNode,TiXmlString> TinyXmlParser;
CONFIGLIB_API TinyXmlParser&  AfxGetXmlParser();

#endif
