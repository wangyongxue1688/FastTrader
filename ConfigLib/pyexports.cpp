#include <boost/python.hpp>
#include <boost/python/suite/indexing/map_indexing_suite.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>
#include "UserConfigReader.h"
#include "BrokerConfigReader.h"

BOOST_PYTHON_MODULE(ConfigLib)
{
	using namespace boost::python;

	class_<std::map<std::string, UserLoginParam> >("UserLoginParamMap")
		.def(map_indexing_suite<std::map<std::string, UserLoginParam> >());

	class_<std::map<std::string, int> >("StrIntMap")
		.def(map_indexing_suite<std::map<std::string, int> >());

	class_<std::vector<std::string> >("StrVector")
		.def(vector_indexing_suite<std::vector<std::string> >());

	class_<std::vector<StrategyParam> >("StrategyParamVector")
		.def(vector_indexing_suite<std::vector<StrategyParam> >());

	class_<UserConfigReader >("UserConfigReader")
		.def("GetUsers", &UserConfigReader::GetUsers, return_value_policy<reference_existing_object>())
		.def("GetStrtegyParams", &UserConfigReader::GetStrtegyParams, return_value_policy<reference_existing_object>())
		.def("GetInstance", &UserConfigReader::GetInstance, return_value_policy<reference_existing_object>())
 		.staticmethod("GetInstance")
		;

	class_<std::unordered_map<std::string, ServerInfo> >("ServerInfoMap")
		.def(map_indexing_suite<std::unordered_map<std::string, ServerInfo> >())
		;

	class_<BrokerConfigReader>("BrokerConfigReader")
		.def("GetBrokers", &BrokerConfigReader::GetBrokers, return_value_policy<reference_existing_object>())
		.def("GetInstance", &BrokerConfigReader::GetInstance, return_value_policy<reference_existing_object>())
 		.staticmethod("GetInstance")
		;
}