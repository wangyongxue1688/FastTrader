#ifndef _MULTISORT_CONFIG_H_
#define _MULTISORT_CONFIG_H_
#include "ViewConfig.h"
#include "AllConfig.h"

class CONFIGLIB_API MultiSortConfig :
	public ViewConfig
{
public:
	static MultiSortConfig*  GetInstance();
public:
	MultiSortConfig(void);
	virtual ~MultiSortConfig(void);
public:
	virtual bool load(TinyXmlParser* parser,string section,int sectionId);
	virtual bool save();
public:
	tagMultiSortConfig m_config;
protected:
	static MultiSortConfig* g_Instance;
};
#endif

