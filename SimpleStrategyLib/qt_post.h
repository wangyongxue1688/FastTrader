﻿#ifndef QT_POST_H_
#define QT_POST_H_


#include <QObject>
#include <boost/function.hpp>

#include "simplestrategylib_global.h"

#ifndef _WIN32
#define __cdecl
#endif

//用于把函数发到主线程去掉用;
typedef boost::function<void()> qt_post_fun;
typedef boost::function<void __cdecl(void)> qt_c_post_fun;


class SIMPLESTRATEGYLIB_EXPORT qt_post : public QObject
{
	Q_OBJECT
	void do_post(qt_post_fun func,bool bBlock);
	void do_post_c(qt_c_post_fun func,bool bBlock);
private slots:
	void on_post(qt_post_fun qfunc_ptr);
	void on_post_c(qt_c_post_fun qfunc_ptr);
signals:
	void post(qt_post_fun fun);
	void post_block(qt_post_fun);

	void post_c(qt_c_post_fun fun);
	void post_c_block(qt_c_post_fun fun);
public:
	qt_post();
	friend SIMPLESTRATEGYLIB_EXPORT void  post_ui(qt_post_fun);
	friend SIMPLESTRATEGYLIB_EXPORT void  post_c_ui(qt_c_post_fun);
	friend SIMPLESTRATEGYLIB_EXPORT void  post_ui_block(qt_post_fun);
	friend SIMPLESTRATEGYLIB_EXPORT void  post_c_ui_block(qt_c_post_fun);
	static qt_post& the_post();
};

#endif
