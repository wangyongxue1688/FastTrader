#include "StrategyMgr.h"
#include "../Log/logging.h"
#include "IStrategy.h"
#include <algorithm>
#include <functional>
#include <boost/dll.hpp>
#include <boost/function.hpp>


StrategyMgr::~StrategyMgr(void)
{
	
}

StrategyMgr& StrategyMgr::GetInstance()
{
	static StrategyMgr g_StrategyMgr;
	return g_StrategyMgr;
}



// bool StrategyMgr::UnLoadStrategy( const std::string& strategy_path )
// {
// 	std::vector<StrategyInfo>::iterator sIter=std::find(m_Strategies.begin(),m_Strategies.end(),strategy_path);
// 	if (sIter==m_Strategies.end())
// 	{
// 		//策略不存在;
// 		return false;
// 	}
// 	//策略卸载之前，要确保所有的卸载了;
// 	m_Strategies.erase(sIter);
// 	return true;
// }


bool StrategyMgr::LoadStrategy( const std::string& strategy_path )
{
	if (m_StrategyLibs.find(strategy_path) != m_StrategyLibs.end())
	{
		return true;
	}
	boost::filesystem::path shared_library_path(strategy_path);

	boost::system::error_code ec;
	boost::shared_ptr<boost::dll::shared_library> strategy_lib_ptr =
		boost::make_shared<boost::dll::shared_library>(shared_library_path,ec, boost::dll::load_mode::append_decorations);
	if (strategy_lib_ptr->is_loaded())
	{
		boost::function<strategy_create_t> strategy_creator =
			strategy_lib_ptr->get_alias<strategy_create_t>(STRATEGY_CREATE_FUNCTION_NAME);
		if (strategy_creator)
		{
			StrategyLibParam param;
			param.strategy_lib_ptr = strategy_lib_ptr;
			param.strategy_creator = strategy_creator;
			m_StrategyLibs.insert(std::make_pair(strategy_path, param));
			return true;
		}
	}
	else
	{
		LOGDEBUG("LoadStrategy {} {}",strategy_path,ec.message());
	}
	return false;
}



std::map<std::string, boost::shared_ptr<IStrategy> >& StrategyMgr::GetStrategies()
{
	return m_StrategiesMap;
}

boost::shared_ptr<IStrategy> StrategyMgr::AddStrategyPtr( const std::string& StrategyID,const std::string& StrategyPath )
{
	auto sptriter = m_StrategiesMap.find(StrategyID);
	if (sptriter != m_StrategiesMap.end())
	{
		return sptriter->second;
	}
	//创建,添加;
	//使用策略加载器加载策略;
	if(!LoadStrategy(StrategyPath))
	{
		//策略加载失败;
		LOGDEBUG("{}{}",StrategyPath,"加载失败...");
		return nullptr;
	}
	auto strategy_lib_iter = m_StrategyLibs.find(StrategyPath);
	if (strategy_lib_iter == m_StrategyLibs.end())
	{
		LOGDEBUG("{}{}", StrategyPath, "Not In StrategyLib Pool...");
		return nullptr;
	}
	//创建策略对象;
	boost::shared_ptr<IStrategy> pStrategy = strategy_lib_iter->second.strategy_creator(StrategyID);
	if (pStrategy)
	{
		pStrategy->SetFilePath(StrategyPath);
		boost::filesystem::path spath(StrategyPath);
		pStrategy->SetName(spath.stem().string());
		m_StrategiesMap.insert(std::make_pair(StrategyID,pStrategy));
		strategy_lib_iter->second.strategies.push_back(pStrategy);
	}
	return pStrategy;
}

boost::shared_ptr<IStrategy> StrategyMgr::GetStrategyPtr( const std::string& StrategyID )
{
	std::map<std::string,boost::shared_ptr<IStrategy> >::iterator sptriter = m_StrategiesMap.find(StrategyID);
	if (sptriter != m_StrategiesMap.end())
	{
		return sptriter->second;
	}
	return nullptr;
}


bool StrategyMgr::RemoveStrategyPtr( boost::shared_ptr<IStrategy>& ptrStrtegy )
{
	if (!ptrStrtegy)
	{
		return false;
	}
	return RemoveStrategyPtr(ptrStrtegy->GetId());
}

bool StrategyMgr::RemoveStrategyPtr( const std::string& StrategyID )
{
	std::map<std::string, boost::shared_ptr<IStrategy> >::iterator sptriter =
		m_StrategiesMap.find(StrategyID);
	if (sptriter != m_StrategiesMap.end())
	{
		if (sptriter->second)
		{
			auto libIter=m_StrategyLibs.find(sptriter->second->GetFilePath());
			if (libIter!=m_StrategyLibs.end())
			{
				auto siter = std::find(libIter->second.strategies.begin(),
					libIter->second.strategies.end(),sptriter->second);
				if (siter!=libIter->second.strategies.end())
				{
					libIter->second.strategies.erase(siter);
				}
				if (libIter->second.strategies.empty())
				{
					//删除库;
					m_StrategyLibs.erase(libIter);
				}
			}
		}
		m_StrategiesMap.erase(sptriter);

		return true;
	}
	return false;
}


bool StrategyMgr::Join()
{
	//通过ID进行查找,不必每个都发送一次;
	for (auto i = m_StrategiesMap.begin(); i!=m_StrategiesMap.end(); ++i)
	{
		if (i->second && i->second->IsRun())
		{
			i->second->Join();
		}
	}
	//对事件进行分发;
	return true;
}

void StrategyMgr::Stop()
{
	for (auto i = m_StrategiesMap.begin(); i != m_StrategiesMap.end(); ++i)
	{
		if (i->second && i->second->IsRun())
		{
			i->second->Stop();
		}
	}
}

StrategyMgr::StrategyMgr()
{
	LOGINIT("StrategyMgr");
}
