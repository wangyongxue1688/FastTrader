#include <boost/python.hpp>
#include <boost/python/suite/indexing/map_indexing_suite.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>
#include "IStrategy.h"
#include "StrategyMgr.h"
#include "TradingAccount.h"

bool operator ==(const BaseTradingAccount& bta1, const BaseTradingAccount& bta2)
{
	return bta1.TradingDay == bta2.TradingDay;
}

bool operator ==(const TradingAccount& bta1, const TradingAccount& bta2)
{
	return bta1.TradingDay == bta2.TradingDay
		&& bta1.AccountID ==  bta2.AccountID;
}

BOOST_PYTHON_MODULE(SimpleStrategyLib)
{
	using namespace boost::python;



	class_<std::vector<TradingAccount>, boost::shared_ptr<std::vector<TradingAccount>> >("TradingAccountVector")
		.def(vector_indexing_suite<std::vector<TradingAccount> >())
		;

	class_<std::vector<boost::shared_ptr<TradingAccount> >, boost::shared_ptr<std::vector<boost::shared_ptr<TradingAccount> > > >("TradingAccountPtrVector")
		.def(vector_indexing_suite<std::vector<boost::shared_ptr<TradingAccount> >, true >())
		;

	class_<IStrategy, boost::shared_ptr<IStrategy>, boost::noncopyable >("IStrategy", no_init)
		.def("IsRun", &IStrategy::IsRun)
		.def("GetId", &IStrategy::GetId, return_value_policy<copy_const_reference>())
		.def("GetVersion", &IStrategy::GetVersion, return_value_policy<copy_const_reference>())
		.def("GetName", &IStrategy::GetName, return_value_policy<copy_const_reference>())
		.def("GetFilePath", &IStrategy::GetFilePath, return_value_policy<copy_const_reference>())
		.def("GetInstruments", &IStrategy::GetInstruments, return_value_policy<reference_existing_object>())
		.def("GetOrders", &IStrategy::GetOrders)
		.def("GetTrades", &IStrategy::GetTrades)
		.def("GetInputOrders", &IStrategy::GetInputOrders)
		.def("GetInputOrderActions", &IStrategy::GetInputOrderActions)
		.def("GetPositions", &IStrategy::GetPositions)
		.def("GetClosedPositions", &IStrategy::GetClosedPositions)
		.def("GetTradingAccounts", &IStrategy::GetTradingAccounts)
		.def("GetTradingAccountsToday", &IStrategy::GetTradingAccountsToday)
		.def("GetSignedPosition", &IStrategy::GetSignedPosition)
		.def("GetMainTrader",&IStrategy::GetMainTrader)
		.def("GetDataStoreFace",&IStrategy::GetDataStoreFace)
		;

	class_<std::map<std::string,boost::shared_ptr<IStrategy> > >("StrategyPtrMap")
		.def(map_indexing_suite<std::map<std::string,boost::shared_ptr<IStrategy> >,true >())
		;

	class_<StrategyMgr,boost::noncopyable>("StrategyMgr",no_init)
		.def("GetStrategyPtr", &StrategyMgr::GetStrategyPtr)
		.def("GetStrategies", &StrategyMgr::GetStrategies, return_value_policy<reference_existing_object>())
		.def("GetInstance", &StrategyMgr::GetInstance, return_value_policy<reference_existing_object>())
		.staticmethod("GetInstance")
		;
}