#ifndef _STRATEGY_IMPL_H_
#define _STRATEGY_IMPL_H_

#include "DataTypes.h"
#include "UserInfo.h"
#include <vector>
#include <functional>

#include <boost/bind.hpp>
#include <boost/function.hpp>
#include <boost/asio.hpp>

#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

#include <queue>
#include <list>
#ifdef _WIN32
#include <WinSock2.h>
#include <sstream>
#else
#include <sys/time.h>
#include <sys/select.h>
#include <time.h>
#endif

#ifdef HTTP_MONITOR
#include "../../common/Statistic.h"
#endif
#include "IStrategy.h"
#include "../TechLib/Strategy.h"
#include "../ServiceLib/Portfolio.h"

struct TimerInfo;
class  StrategyImpl:public Portfolio,public std::enable_shared_from_this<StrategyImpl>
{
public:
	StrategyImpl(const std::string& id);
	virtual ~StrategyImpl(void);

	bool IsRun() const;
	virtual void Start();
	virtual void Stop();
	virtual const std::string& GetId() const;

	void SetStartDay(const std::string& startDay);
	std::string GetStartDay();
	//数据接口;
	boost::shared_ptr<DataStoreFace> GetDataStoreFace();
	void SetDataStoreFace(boost::shared_ptr<DataStoreFace> pDataStore);
	//报单操作接口;
	boost::shared_ptr<std::map<std::string, boost::shared_ptr<Order> > > GetOrder(boost::shared_ptr<Trader> traderID);
	boost::shared_ptr<std::map<std::string, boost::shared_ptr<Trade> > > GetTrade(boost::shared_ptr<Trader> traderID);

	//获取所有的报单;
	boost::shared_ptr<std::map<std::string, boost::shared_ptr<InputOrder> > > GetInputOrder(boost::shared_ptr<Trader> traderID);

	//获取所有的撤单;
	boost::shared_ptr<std::map<std::string, boost::shared_ptr<InputOrderAction> > > GetInputOrderAction(boost::shared_ptr<Trader> traderID);

	//注册回调函数;
	void SetCallback(StrategyCallback& scb);
	//报单,限价;
	boost::shared_ptr<InputOrder> OrderInsert(boost::shared_ptr<InputOrder> inputOrder, boost::shared_ptr<Trader> traderId);

	boost::shared_ptr<InputOrder> OrderInsert(const std::string& instId,int Volume,double Price,
		EnumOffsetFlag OpenOrClose,EnumDirection BuyOrSell, boost::shared_ptr<Trader> traderId);
	//撤单;
	boost::shared_ptr<InputOrderAction> OrderCancel(int frontId,int sessionId,const std::string& orderRef);
	boost::shared_ptr<InputOrderAction> OrderCancel(const std::string& ExchangeID, const std::string& OrderSysId,
		const std::string& instId);
	boost::shared_ptr<InputOrderAction> OrderCancel(boost::shared_ptr<InputOrderAction> inputOrderAction, boost::shared_ptr<Trader> traderId);

	//直接撤单;
	boost::shared_ptr<InputOrderAction> SafeCancelOrder(boost::shared_ptr<Order> order, boost::shared_ptr<InputOrder> inputOrder = nullptr);
	boost::shared_ptr<InputOrderAction> SafeCancelOrder(boost::shared_ptr<InputOrder> order);

	static boost::shared_ptr<Trader> GetTraderIDByFronIDSessionID(int frontId, int SessionId);

	boost::shared_ptr<Trader> GetTraderByInvestorID(const std::string& InvestorID);

	bool SetEnableTrading(boost::shared_ptr<Trader> traderId,bool bEnableTrading=true);
	bool IsEnableTrading(boost::shared_ptr<Trader> traderId);
	//平掉所有仓位,平掉所有仓位之前,不允许开新仓;
	bool CloseAllPositions(const std::string& InstrumentID="");
	//平仓;
	boost::shared_ptr<InputOrder> ClosePosition(const InvestorPosition& ip);
	//开仓,默认使用对手价;
	boost::shared_ptr<InputOrder> OpenPosition(const std::string& InstrumentID, EnumDirection BuyOrSell, int Volume, boost::shared_ptr<Trader> pTrader);

	//获取某个合约的持仓;
	boost::shared_ptr<InvestorPosition> GetPosition(const std::string& InstrumentID);
	//持仓量;
	int GetSignedPosition(const std::string& InstrumentID);

	bool GetPositionCost(const std::string& InstrumentID, double& PositionAvgPrice);

	//默认的交易对象ID;
	void SetMainTrader(const boost::shared_ptr<Trader> uid,double Available=0);
	boost::shared_ptr<Trader> GetMainTrader();
	//其他交易对象的ID;
	void SetSubTraders(const std::vector<boost::shared_ptr<Trader> >& subId);
	//默认的交易对象ID;
	void SetMainMarket(const boost::shared_ptr<Market> uid);
	//其他交易对象的ID;
	void SetSubMarkets(const std::vector<boost::shared_ptr<Market> >& subId);
	//设置合约;
	void SetInstruments(const std::map<std::string,int>& instruments);
	//获取合约;
	std::map<std::string,int>& GetInstruments();
	void SubInstruments(const std::vector<std::string>& instruments, const std::string& exchange);
	void UnSubInstruments(const std::vector<std::string>& instruments, const std::string& exchange);

	virtual boost::shared_ptr<InstrumentCommisionRate> GetInstCommissionRate(const std::string& instId);
	virtual boost::shared_ptr<InstrumentMarginRate> GetInstMarginRate(const std::string& instId);
	virtual boost::shared_ptr<BrokerTradingParams> GetBrokerTradingParams();
	//行情;
	void OnTick(boost::shared_ptr<Tick> tick);
	//状态;
	void OnStatus(boost::shared_ptr<InstStatus> status);
	//报单;
	void OnOrder(boost::shared_ptr<Order> order);
	//成交;
	void OnTrade(boost::shared_ptr<Trade> trade);
	//撤单失败;
	void OnOrderAction(boost::shared_ptr<InputOrderAction> orderAction);

	//行情;
	void OnTickHandler(boost::shared_ptr<Tick> tick);
	//状态;
	void OnStatusHandler(boost::shared_ptr<InstStatus> status);
	//报单;
	void OnOrderHandler(boost::shared_ptr<Order> order);
	//成交;
	void OnTradeHandler(boost::shared_ptr<Trade> trade);
	//撤单失败;
	void OnOrderActionHandler(boost::shared_ptr<InputOrderAction> orderAction);

	//从本地恢复数据;
	virtual void Restore();
	//保存数据;
	virtual void Resave();
	//发送事件;
	void PostEvent(const OnEventFun& evt);
	bool Join();
	//开启定时器;
	int StartTimer(OnEventFun& evt,int elapsed=1000);
	//关闭定时器;
	bool StopTimer(int timerId);
public:
	virtual bool IsMyOrder(boost::shared_ptr<Order> order);
	virtual bool IsMyTrade(boost::shared_ptr<Trade> trade);
protected:
	void ThreadFun();
	void Initialize();
	void Finalize();
	void DoStop();

protected:
	//是否正在运行;
	bool is_run;
	//起始报单引用;
	std::string m_MaxOrderRef;
	//历史数据起始日期;
	std::string m_StartDay;
	//交易接口;
	boost::shared_ptr<Trader> m_MainTrader;
	std::vector<boost::shared_ptr<Trader> > m_SubTraders;
	//行情;
	boost::shared_ptr<Market> m_MainMarket;
	std::vector<boost::shared_ptr<Market> > m_SubMarkets;
	std::map<std::string,int> m_Instruments;
	//线程对象;
	boost::shared_ptr<boost::thread> m_Thread;

	std::queue<OnEventFun> m_EventQueue;
	//互斥量与条件变量;
	condition_variable  m_CondVar4Event;
	mutex               m_Mutex4Event;
	mutex				m_Mutext4OrderInsert;
	boost::shared_ptr<DataStoreFace> m_pDataStoreFace;

	InvestorPosition m_CloseAllFirstPosition;    //该变量存在则表示全部平仓;
	std::string m_CloseAllInstrumentPattern;     //用于指示平仓合约的模式:具体某个合约,或者合部合约;
protected:
	StrategyCallback    m_StrategyCallback;
protected:
	//实际订阅的合约;
	std::vector<std::string> m_RealSubInstruments;
	//订阅的合约;
	mutex               m_Mutex4SubInstruments;
	std::map<int, shared_ptr<TimerInfo> > m_Timers;
};



#endif

