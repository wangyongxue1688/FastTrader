TARGET = SimpleStrategyLib
TEMPLATE = lib
CONFIG -= qt

INCLUDEPATH += $$PWD/../sdk/include
INCLUDEPATH += ../common
INCLUDEPATH += /home/rmb338/boost_1_64_0
INCLUDEPATH += ../spdlog/include
INCLUDEPATH += /usr/include/python2.7
DEFINES += _USE_SPDLOG SIMPLESTRATEGYLIB_LIB
CONFIG += debug_and_release c++11

linux-g++|macx-g++{
    QMAKE_LFLAGS= -m64 -Wall -DNDEBUG  -O2
    QMAKE_CFLAGS = -arch x86_64 -lpthread
    INCLUDEPATH += /home/rmb338/boost_1_64_0
    LIBS += -L/home/rmb338/boost_1_64_0/stage/lib
}

win32{
    DEFINES += WIN32_LEAN_AND_MEAN _CRT_SECURE_NO_WARNINGS
    INCLUDEPATH += F:/boost_1_63_0
    LIBS += -LF:/boost_1_63_0/lib32-msvc-12.0
}

CONFIG(debug, debug|release) {
        DESTDIR = ../build/v120/debug
        LIBS  += -L$$PWD/../build/v120/debug/ -lFacilityBaseLib -lServiceLib -lDataStoreLib
} else {
        DESTDIR = ../build/v120/release
        LIBS = -L$$PWD/../build/v120/release/ -lFacilityBaseLib -lServiceLib -lDataStoreLib
}

include(./SimpleStrategyLib.pri)



