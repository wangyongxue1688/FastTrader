#include "KData.h"

//#include "DateTime.h"
#include <math.h>
#include <memory.h>
#include <stdlib.h>
#include <stdio.h>
#include "InstrumentInfo.h"
#include "Container.h"
#include <boost/make_shared.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/posix_time/ptime.hpp>
#include "../Log/logging.h"
#include "DateTimeHelper.h"
#include "Report.h"

KdataContainer::KdataContainer()
{
	LOGINIT("kdata_container");
	m_nKType	=	ktypeDay;
	m_nCurFormat=	formatOriginal;
	m_nCurMaindataType	=	mdtypeClose;
	m_pDataOriginal	=	NULL;
	m_nSizeOriginal = m_nMaxSizeOriginal = 0;
	//m_pData	=	NULL;
	//m_nSize = m_nMaxSize = m_nGrowBy = 0;
}

KdataContainer::KdataContainer( int ktype, int maindatatype )
{
	LOGINIT("kdata_container");
	m_nKType	=	ktype;
	m_nCurFormat=	formatOriginal;
	m_nCurMaindataType	=	maindatatype;
	m_pDataOriginal	=	NULL;
	m_nSizeOriginal = m_nMaxSizeOriginal = 0;
	//m_pData = NULL;
	//m_nSize = m_nMaxSize = m_nGrowBy = 0;
}
KdataContainer::KdataContainer( const KdataContainer &src )
{
	LOGINIT("kdata_container");
	m_nKType	=	ktypeDay;
	m_nCurFormat=	formatOriginal;
	m_nCurMaindataType	=	mdtypeClose;
	m_pDataOriginal	=	NULL;
	m_nSizeOriginal = m_nMaxSizeOriginal = 0;
	//m_pData	=	NULL;
	//m_nSize = m_nMaxSize = m_nGrowBy = 0;
	*this	=	src;
}

KdataContainer	& KdataContainer::operator = ( const KdataContainer &src )
{
	Clear();

	m_nKType			=	src.m_nKType;
	m_nCurFormat		=	src.m_nCurFormat;
	m_nCurMaindataType	=	src.m_nCurMaindataType;

	//CopyData( src );
	/**this = src;*/
	//std::copy(src.begin(),src.end(),std::back_inserter(*this));
	std::vector<kdata_t>::operator =(src);
	return	*this;
}
/**
 * \brief 时间周期是按天还时按份钟;
 *
 */
bool KdataContainer::IsDayOrMin( int nKType )
{
	return (nKType == ktypeDay || nKType == ktypeWeek || nKType == ktypeMonth);
}

KdataContainer::~KdataContainer()
{
	Clear();
}

/**
 * \brief 获取最近的时间;
 */
bool KdataContainer::LatestDate( int &nYear, int &nMonth, int &nDay, int &nHour, int &nMinute ) const
{
	nYear	=	nMonth	=	nDay	=	nHour	=	nMinute	=	0;
	if( size() > 0 )
		return DateAt( size()-1, nYear, nMonth, nDay, nHour, nMinute );
	return false;
}
//取得K线数据中某个点的时间;
//如果索引值大于K线数组的元素个数，则返回最后一个元素的日期;
DWORD KdataContainer::GetDate( size_t nIndex )
{
	if( nIndex >=0 && nIndex < size() )
		return at(nIndex).TradingDate;
	if( size() > 0 )
		return at(size()-1).TradingDate;
	return 0;
}

DWORD  KdataContainer::GetDateDay( int nIndex )
{
	DWORD	date	=	GetDate(nIndex);
// 	if( !IsDayOrMin(m_nKType) )
// 	{
// 		DateTime	sptime;
// 		if(!sptime.FromInstrumentTimeMin( date ) )
// 			return sptime.ToInstrumentTimeDay();
// 	}
	return date;
}
//-1为无效索引;
int KdataContainer::GetIndexByDate( DWORD date )
{
	if( size() > 0 && at(size()-1).TradingDate < date )
		return -1;
	if( size() > 0 && at(0).TradingDate > date )
		return -1;
	//这里可以使用二分插入方法;
	for( int k=size()-1; k >= 0 ; k-- )
	{
		if( at(k).TradingDate =date)
			return k;
		else if( at(k).TradingDate<date )
			break;
	}
	return -1;
}
/**
 * \brief 返回小于或等于指定日期的最大值;
 *
 */
int KdataContainer::GetAboutIndexByDate( DWORD date )
{
	for( int k=size()-1; k >= 0 ; k-- )
	{
		if(at(k).TradingDate<=date)
			return k;
	}
	return -1;
}

int KdataContainer::InsertKDataSort(const KDATA& newElement )
{
	for( size_t i=0; i<size(); i++ )
	{
		KDATA	& temp = at(i);
		if( temp.TradingDate == newElement.TradingDate)
		{
			at(i)=newElement;//替换
			return i;
		}
		if( temp.TradingDay > newElement.TradingDay)
		{
			insert(i+begin(),newElement);//插入
			return i;
		}
	}
	push_back( newElement);//追加
	return size()-1;
}

bool KdataContainer::IsNewValue( size_t nIndex, bool bTopOrBottom, size_t nDays )
{
	if( nIndex < nDays-1 || nIndex < 0 || nIndex >= size() || nDays < 3 )
		return false;

	bool	bFirst	=	true;
	double	fLast	=	0;
	size_t		nLast	=	0;
	double	fNow	=	MaindataAt( nIndex );

	for( std::size_t k=nIndex-1; k>=0 && k>nIndex-nDays; k-- )
	{
		double	fCur	=	MaindataAt(k);
		if( bFirst )
		{
			fLast	=	fCur;
			nLast	=	k;
			bFirst	=	false;
		}

		if( bTopOrBottom )
		{
			if( fCur > fNow )
				return false;

			if( fCur > fLast )
			{
				fLast	=	fCur;
				nLast	=	k;
			}
		}
		else
		{
			if( fCur < fNow )
				return false;

			if( fCur < fLast )
			{
				fLast	=	fCur;
				nLast	=	k;
			}
		}
	}
	if( bFirst )
		return false;

	if( nLast >= nIndex-30 && nLast <= nIndex-5 )
		return true;

	return false;
}

bool KdataContainer::GetMinMaxInfo( size_t nStart, size_t nEnd, double * pfMin, double *pfMax )
{
	if( nStart < 0 || nEnd < 0 || nStart > nEnd || nEnd >= size() )
		return false;

	double	fMin = 0, fMax = 0;
	bool	bFirst	=	true;
	for( size_t k=nStart; k<=nEnd; ++k )
	{
		KDATA	& kd	=	at(k);
		if( bFirst )
		{
			fMin	=	kd.LowestPrice;
			fMax	=	kd.HighestPrice;
			bFirst	=	false;
		}
		if( kd.LowestPrice < fMin )
			fMin	=	kd.LowestPrice;
		if( kd.HighestPrice > fMax )
			fMax	=	kd.HighestPrice;
	}

	if( pfMin )		*pfMin	=	fMin;
	if( pfMax )		*pfMax	=	fMax;
	return !bFirst;
}


void KdataContainer::SetKType( int ktype ) {	m_nKType = ktype;	}
void KdataContainer::AutoSetKType( )
{
	if( size() >= 4 )
	{
		long elapse1 = at(1).TradingDate - at(0).TradingDate;
		long elapse2 = at(2).TradingDate - at(1).TradingDate;
		long elapse3 = at(3).TradingDate - at(2).TradingDate;
		long elapse = min(elapse1,elapse2);
		elapse = min(elapse,elapse3);
		if( elapse < 600 )
			m_nKType = ktypeMin5;
		else if( elapse < 1200 )
			m_nKType = ktypeMin15;
		else if( elapse < 2400 )
			m_nKType = ktypeMin30;
		else if( elapse < 7200 )
			m_nKType = ktypeMin60;
		else if( elapse < 172800 )
			m_nKType = ktypeDay;
		else if( elapse < 864000 )
			m_nKType = ktypeWeek;
		else if( elapse < 4320000 )
			m_nKType = ktypeWeek;
	}
}
int KdataContainer::GetKType( ) { return m_nKType; }
int KdataContainer::GetCurFormat( ) { return m_nCurFormat; }
void KdataContainer::SetMaindataType( int type ) { m_nCurMaindataType = type; }
int KdataContainer::GetMaindataType( ) { return m_nCurMaindataType; }


void KdataContainer::ChangeCurFormat( int format, DWORD dateAutoDRBegin, double dAutoDRLimit )
{
//	assert( GetKType() != ktypeMonth && GetKType() != ktypeWeek );

	if( m_nCurFormat == format )
		return;

	switch( format )
	{
	case formatOriginal:
		LoadDataOriginal( );
		m_nCurFormat = format;
		break;
	case formatXDRup:
		StoreDataOriginal( );
		ConvertXDR( true, dateAutoDRBegin, dAutoDRLimit );
		m_nCurFormat = format;
		break;
	case formatXDRdown:
		StoreDataOriginal( );
		ConvertXDR( false, dateAutoDRBegin, dAutoDRLimit );
		m_nCurFormat = format;
		break;
	default:;
//		assert( false );
	}
}

void KdataContainer::Clear( )
{
	if( m_pDataOriginal )
	{
		delete [] (BYTE*)m_pDataOriginal;
		m_pDataOriginal	=	NULL;
	}
// 	if( m_pData )
// 	{
// 		delete [] (BYTE*)m_pData;
// 		m_pData	=	NULL;
// 	}
	clear();
	//m_nKType	=	ktypeDay;
	m_nCurFormat=	formatOriginal;
	m_nCurMaindataType	=	mdtypeClose;
	m_pDataOriginal	=	NULL;
	m_nSizeOriginal = m_nMaxSizeOriginal = 0;
	//m_pData	=	NULL;
	//m_nSize = m_nMaxSize = m_nGrowBy = 0;
}

int KdataContainer::CompareLatestDate( KdataContainer &kd )
{
	// compare this kdata with kd's latest time
	int	nYearThis, nMonthThis, nDayThis, nHourThis, nMinuteThis;
	int nYear, nMonth, nDay, nHour, nMinute;
	LatestDate( nYearThis, nMonthThis, nDayThis, nHourThis, nMinuteThis );
	kd.LatestDate( nYear, nMonth, nDay, nHour, nMinute );

	if( nYearThis > nYear || nMonthThis > nMonth || nDayThis > nDay
		|| nHourThis > nHour || nMinuteThis > nMinute )
		return 1;
	if( nYearThis < nYear || nMonthThis < nMonth || nDayThis < nDay
		|| nHourThis < nHour || nMinuteThis < nMinute )
		return -1;
	return 0;
}

int KdataContainer::Min5ToMin15( KdataContainer &kdm5, KdataContainer &kdm15 )
{
//	assert( ktypeMin5 == kdm5.GetKType() );
//	assert( ktypeMin15 == kdm15.GetKType() );
	return ConvertKData( kdm5, kdm15, 3 );
}

int KdataContainer::Min5ToMin30( KdataContainer &kdm5, KdataContainer &kdm30 )
{
//	assert( ktypeMin5 == kdm5.GetKType() );
//	assert( ktypeMin30 == kdm30.GetKType() );
	return ConvertKData( kdm5, kdm30, 6 );
}


int KdataContainer::Min1ToDay( KdataContainer& kdSrc,KdataContainer& kdday )
{
	if (kdSrc.size()<=0)
	{
		return 0;
	}
	int	nCount		=	0;
	KDATA	dataDest = {0};
	size_t nStart=0;
	for( size_t pos=nStart; pos<kdSrc.size(); pos++ )
	{
		KDATA & dataSrc = kdSrc.at( pos );
		if (kdday.empty() && strlen(dataDest.TradingDay)==0)
		{
			memcpy(&dataDest,&dataSrc,sizeof(KDATA));
			//dataDest.TradingTime = boost::posix_time::to_time_t(boost::posix_time::from_time_t(dataSrc.TradingTime).date());
			//kdday.push_back(dataDest);
		}
		else
		{
			//直接看交易日;
			auto tmSrc = std::stoi(dataSrc.TradingDay);
			auto tmDest = std::stoi(dataDest.TradingDay);
			if (tmSrc!=tmDest)
			{
				//不在同一天;
				kdday.push_back(dataDest);
				memcpy( &dataDest, &dataSrc, sizeof(dataDest) );
			}
			else
			{
				//更新;
				if( dataDest.HighestPrice < dataSrc.HighestPrice )	
					dataDest.HighestPrice	=	dataSrc.HighestPrice;
				if( dataDest.LowestPrice > dataSrc.LowestPrice )		
					dataDest.LowestPrice		=	dataSrc.LowestPrice;
				dataDest.Turnover		+=	dataSrc.Turnover;
				dataDest.Volume		+=	dataSrc.Volume;
				dataDest.ClosePrice		=	dataSrc.ClosePrice;
			}
		}
	}
	//最后一个K永远不会被前的循环加到数据里来，所以手动把它放到这里面来;
	kdday.push_back(dataDest);
	return kdday.size();
}


int KdataContainer::Min5ToMin60( KdataContainer &kdm5, KdataContainer &kdm60 )
{
//	assert( ktypeMin5 == kdm5.GetKType() );
//	assert( ktypeMin60 == kdm60.GetKType() );
	return ConvertKData( kdm5, kdm60, 12 );
}
/**
 * \brief 日线转月线
 *
 */
int KdataContainer::DayToMonth( KdataContainer &kdday, KdataContainer &kdmonth )
{
	// convert day k line to month k line
	assert( ktypeDay == kdday.GetKType() );
	assert( ktypeMonth == kdmonth.GetKType() );

	kdmonth.reserve(  kdday.size() / 20 + 5 );
	
	int nStart		=	kdday.size() % 3;
	KDATA	dataDest;
	memset( &dataDest, 0, sizeof(dataDest) );
	int	nYearCur = 0, nMonthCur = 0 ;
	for( size_t pos=nStart; pos<kdday.size(); pos++ )
	{
		KDATA & dataSrc = kdday.at( pos );

		boost::posix_time::ptime	tm;
		if( !FromInstrumentTimeDayOrMin(tm,std::stoi(dataSrc.TradingDay),true) )
			continue;
		int nYear	=	tm.date().year();
		int nMonth = tm.date().month();

		if( nYear != nYearCur || nMonth != nMonthCur )	// a new month
		{
			if( 0 != pos )
				kdmonth.push_back(dataDest);				// add a month

			memcpy( &dataDest, &dataSrc, sizeof(dataDest) );	// begin a new month
			nYearCur	=	nYear;
			nMonthCur	=	nMonth;
		}
		else
		{
			dataDest.Turnover		+=	dataSrc.Turnover;
			if( dataDest.HighestPrice < dataSrc.HighestPrice )	dataDest.HighestPrice	=	dataSrc.HighestPrice;
			if( dataDest.LowestPrice > dataSrc.LowestPrice )		dataDest.LowestPrice		=	dataSrc.LowestPrice;
			dataDest.Volume		+=	dataSrc.Volume;
			dataDest.ClosePrice		=	dataSrc.ClosePrice;
		}

		if( pos == kdday.size()-1 )	// the latest one
			kdmonth.push_back(dataDest);
 	}

	return kdmonth.size();
}

int KdataContainer::DayToWeek( KdataContainer &kdday, KdataContainer &kdweek )
{
	// convert day k line to week k line
//	assert( ktypeDay == kdday.GetKType() );
//	assert( ktypeWeek == kdweek.GetKType() );

	kdweek.reserve(kdday.size() / 5 + 5);
	
	int nStart		=	kdday.size() % 3;
	KDATA	dataDest;
	memset( &dataDest, 0, sizeof(dataDest) );
	for( size_t pos=nStart; pos<kdday.size(); pos++ )
	{
		KDATA & dataSrc = kdday.at( pos );

		boost::posix_time::ptime	tm;
		if( !FromInstrumentTimeDayOrMin(tm,dataSrc.TradingDate,true) )
			continue;

		if (tm.date().day_of_week().as_enum() == boost::date_time::Tuesday)	// a new week
		{
			if( 0 != pos )
				kdweek.push_back(dataDest);				// add a week

			memcpy( &dataDest, &dataSrc, sizeof(dataDest) );	// begin a new week
		}
		else
		{
			dataDest.Turnover		+=	dataSrc.Turnover;
			if( dataDest.HighestPrice < dataSrc.HighestPrice )	dataDest.HighestPrice	=	dataSrc.HighestPrice;
			if( dataDest.LowestPrice > dataSrc.LowestPrice )		dataDest.LowestPrice		=	dataSrc.LowestPrice;
			dataDest.Volume		+=	dataSrc.Volume;
			dataDest.ClosePrice		=	dataSrc.ClosePrice;
		}

		if( pos == kdday.size()-1 )	// the latest one
			kdweek.push_back(dataDest);
	}

	return kdweek.size();
}

DWORD KdataContainer::ToDayDate( DWORD date )
{
	switch( GetKType() )
	{
	case ktypeMin60:
	case ktypeMin30:
	case ktypeMin15:
	case ktypeMin5:
		return (date / 10000 + 1990 * 10000);
	default:
		return date;
	}
}

int KdataContainer::MergeKData( KdataContainer * pother )
{
 	if( !pother || pother->size() == 0 )
		return 0;
	if( GetKType() != pother->GetKType() )
		return 0;

	if( size() == 0 )
	{
		//CopyData( *pother );
		*this = *pother;
		return size();
	}

	int	nCount		= 0;
	//resize( size(), pother->size()+1 );
	for( size_t i=0; i<pother->size(); i++ )
	{
		KDATA	kdnew	=	pother->at(i);
		if( kdnew.ClosePrice < 1e-4 || kdnew.OpenPrice < 1e-4 || kdnew.HighestPrice < 1e-4 || kdnew.LowestPrice < 1e-4 )
			continue;

		size_t j;
		for( j=0; j<size(); j++ )
		{
			if (IsDayOrMin(GetKType()))
			{
				if( kdnew.TradingDate ==at(j).TradingDate )
				{
					at(j)=kdnew;
					break;
				}
				if( kdnew.TradingDate < at(j).TradingDate )
				{
					insert(j+begin(),kdnew);
					break;
				}
			}
			else
			{
				if( kdnew.TradingTime ==at(j).TradingTime )
				{
					at(j) = kdnew;
					break;
				}
				if( kdnew.TradingTime < at(j).TradingTime )
				{
					insert(j + begin(), kdnew);
					break;
				}
			}
		}
		if( size() == j )
			push_back( kdnew );
		nCount	++;
	}

	return nCount;

/*	The Old Version
	if( !pother || pother->GetSize() == 0 )
		return GetSize();
	if( GetKType() != pother->GetKType() )
		return GetSize();

	if( GetSize() == 0 )
	{
		CopyData( *pother );
		return GetSize();
	}

	int nLen = GetSize();
	int nLenOther = pother->GetSize();

	assert( 0 != nLen && 0 != nLenOther );

	if( ElementAt(nLen-1).m_date < pother->ElementAt(0).m_date )
		CopyData( *pother );
//	else if( ElementAt(0).m_date > pother->ElementAt(nLenOther-1).m_date )
//		;
//	else if( ElementAt(0).m_date <= pother->ElementAt(0).m_date
//		&& ElementAt(nLen-1).m_date >= pother->ElementAt(nLenOther-1).m_date )
//		;
	else if( ElementAt(0).m_date > pother->ElementAt(0).m_date
		&& ElementAt(nLen-1).m_date < pother->ElementAt(nLenOther-1).m_date )
		CopyData( *pother );
	else if( ElementAt(0).m_date <= pother->ElementAt(0).m_date
		&& ElementAt(nLen-1).m_date < pother->ElementAt(nLenOther-1).m_date )
	{
		// append from pother
		DWORD	date	=	ElementAt(nLen-1).m_date;
		SetSize( GetSize(), pother->GetSize() );
		for( int i=0; i<pother->GetSize(); i++ )
		{
			KDATA	& kd	=	pother->ElementAt(i);
			if( kd.m_date > m_data )
				Add( kd );
		}
	}
	else if( ElementAt(0).m_date >= pother->ElementAt(0).m_date
		&& ElementAt(nLen-1).m_date > pother->ElementAt(nLenOther-1).m_date )
	{
		// insert from pother
		kdata_container	temp	=	(*pother);
		DWORD	date	=	pother->ElementAt(nLenOther-1).m_date;
		temp.SetSize( temp.GetSize(), GetSize()+5 );
		for( int i=0; i<GetSize(); i++ )
		{
			KDATA	& kd	=	ElementAt(i);
			if( kd.m_date > date )
				temp.Add( kd );
		}
		CopyData( temp );
	}
	
	return GetSize();
*/
}

int KdataContainer::FullFillKData( KdataContainer & kdataMain, bool bFillToEnd )
{
//	assert( GetKType() == kdataMain.GetKType() );
	if( GetKType() != kdataMain.GetKType() )
		return 0;
	if( size() == 0 || kdataMain.size() == 0 )
		return 0;

	DWORD	dateBegin	=	at(0).TradingDate;
	DWORD	dateMainEnd5	=	(kdataMain.size() >= 5 ? kdataMain.at(kdataMain.size()-5).TradingDate : 0);
	DWORD	dateEnd5		=	(size() >= 5 ? at(size()-5).TradingDate : 0);
	size_t		iMain = 0, iSelf = 0;
	for( iMain=0; iMain<kdataMain.size(); iMain ++ )
	{
		if( dateBegin == kdataMain.at(iMain).TradingDate )
			break;
	}

	resize(kdataMain.size() - iMain - size() > 0 ? kdataMain.size() - iMain - size() : size());
	int	nCount	=	0;
	for( ; iMain <= kdataMain.size() && iSelf <= size(); iMain++, iSelf++ )
	{
		if( !bFillToEnd && iSelf == size() && at(iSelf-1).TradingDate < dateMainEnd5 )
			break;
		if( !bFillToEnd && iMain == kdataMain.size() && kdataMain.at(iMain-1).TradingDate < dateEnd5 )
			break;
		while( iMain > 0 && iMain <= kdataMain.size() && iSelf < size()
				&& ( iMain == kdataMain.size() || kdataMain.at(iMain).TradingDate > at(iSelf).TradingDate ) )
		{
//			KDATA	kd;
//			memset( &kd, 0, sizeof(kd) );
//			kd.m_date	=	ElementAt(iSelf).m_date;
//			kd.open	=	kd.high	=	kd.low	=	kd.close	=	kdataMain.ElementAt(iMain-1).close;
//			kdataMain.InsertAt( iMain, kd );
//			iMain	++;
			iSelf	++;
//			nCount	++;
		}

		while( iMain < kdataMain.size() && iSelf <= size() && iSelf > 0
				&& ( iSelf == size() || kdataMain.at(iMain).TradingDate < at(iSelf).TradingDate ) )
		{
			KDATA	kd;
			memset( &kd, 0, sizeof(kd) );
			kd.TradingDate	=	kdataMain.at(iMain).TradingDate;
			kd.OpenPrice	=	kd.HighestPrice	=	kd.LowestPrice	=	kd.ClosePrice	=	at(iSelf-1).ClosePrice;
			insert( iSelf+begin(), kd );
			iMain	++;
			iSelf	++;
			nCount	++;
		}
	}
	return 0;
}

bool KdataContainer::IsAdjacentDays( size_t nIndex, size_t nDays )
{
	// check parameters
//	assert( nIndex >= 0 && nIndex < GetSize() && nDays >= 1 );
	if( nIndex < 0 || nIndex >= size() || nDays < 1 )
		return false;

	// data not enougy
	if( nDays > nIndex )
		return false;

	boost::posix_time::ptime	sptime1, sptime2;
	FromInstrumentTimeDayOrMin(sptime1, at(nIndex - nDays).TradingDate, KdataContainer::IsDayOrMin(m_nKType));
	FromInstrumentTimeDayOrMin(sptime2, at(nIndex).TradingDate, KdataContainer::IsDayOrMin(m_nKType));

	if( ktypeMonth == m_nKType )
	{
		if( sptime2.date() - sptime1.date() >= boost::gregorian::date_duration(nDays+63) )
			return false;
	}
	else if( ktypeWeek == m_nKType )
	{
		if (sptime2.date() - sptime1.date() >= boost::gregorian::date_duration(nDays + 15))
			return false;
	}
	else
	{
		if (sptime2.date() - sptime1.date() >= boost::gregorian::date_duration(nDays + 8))
			return false;
	}
	
	return true;
}

bool KdataContainer::GetDiff( double * pValue, DWORD dateCur, int nDays )
{
//	assert( pValue && nDays > 0 );

	// Find date Current to calculate from
	int		nIndex	=	GetIndexByDate( dateCur );
	if( -1 == nIndex )
		return false;

	// data not enough
	if( nDays > nIndex )
		return false;

	// 检查是否是相邻成交日
	if( !IsAdjacentDays( nIndex, nDays ) )
		return false;

	if( at(nIndex-nDays).ClosePrice < 1e-4 )
		return false;
	if( pValue )
		*pValue	=	at(nIndex).ClosePrice - at(nIndex-nDays).ClosePrice;
	return true;
}

bool KdataContainer::GetDiffPercent( double * pValue, DWORD dateCur, int nDays )
{
//	assert( pValue && nDays > 0 );

	// Find date Current to calculate from
	int		nIndex	=	GetIndexByDate( dateCur );
	if( -1 == nIndex )
		return false;

	// data not enough
	if( nDays > nIndex )
		return false;

	// 检查是否是相邻成交日
	if( !IsAdjacentDays( nIndex, nDays ) )
		return false;

	if( at(nIndex-nDays).Turnover < 1e-4 )
		return false;
	if( pValue )
		*pValue	=	(100. * at(nIndex).ClosePrice) / at(nIndex-nDays).ClosePrice - 100;
	return true;
}

bool KdataContainer::GetScope( double * pValue, DWORD dateCur, int nDays )
{
//assert( pValue && nDays > 0 );

	// Find date Current to calculate from
	int		nIndex	=	GetIndexByDate( dateCur );
	if( -1 == nIndex )
		return false;

	// data not enough
	if( nDays > nIndex )
		return false;

	// 检查是否是相邻成交日
	if( !IsAdjacentDays( nIndex, nDays ) )
		return false;

	double	dMax = 0, dMin = 0;
	int	nCount	=	0;
	for( int k=nIndex; k>=0; k-- )
	{
		if( nIndex == k )
		{
			dMin	=	at(k).LowestPrice;
			dMax	=	at(k).HighestPrice;
		}
		if( dMin > at(k).LowestPrice )	dMin	=	at(k).LowestPrice;
		if( dMax < at(k).HighestPrice )	dMax	=	at(k).HighestPrice;

		nCount	++;
		if( nCount == nDays )
			break;
	}
	// data not enough
	if( nCount != nDays || nCount <= 0 )
		return false;

	if( at(nIndex-nDays).ClosePrice <= 0 )
		return false;
	if( pValue )
		*pValue	=	( 100. * (dMax-dMin) ) / at(nIndex-nDays).ClosePrice ;
	return true;
}

bool KdataContainer::GetVolumeSum( double * pValue, DWORD dateCur, int nDays )
{
//	assert( pValue && nDays > 0 );

	// Find date Current to calculate from
	int		nIndex	=	GetIndexByDate( dateCur );
	if( -1 == nIndex )
		return false;

	// data not enough
	if( nDays > nIndex )
		return false;

	// 检查是否是相邻成交日
	if( !IsAdjacentDays( nIndex, nDays ) )
		return false;

	// begin calculate
	double	dAll	=	0;
	int		nCount	=	0;
	for( int i=nIndex; i>=0; i-- )
	{
		dAll	+=	at(i).Volume;
		nCount	++;
		if( nCount >= nDays )
			break;
	}
	// data not enough
	if( nCount != nDays || nCount <= 0 )
		return false;

	if( pValue )
		*pValue	=	dAll;
	return true;
}

bool KdataContainer::GetRatioVolume( double * pValue, DWORD dateCur, int nDays )
{
//	assert( pValue && nDays > 0 );

	// Find date Current to calculate from
	int		nIndex	=	GetIndexByDate( dateCur );
	if( -1 == nIndex )
		return false;

	// data not enough
	if( nDays > nIndex )
		return false;

	// 检查是否是相邻成交日
	if( !IsAdjacentDays( nIndex, nDays ) )
		return false;

	// begin calculate
	double	dAll	=	0;
	int		nCount	=	0;
	for( int i=nIndex-1; i>=0; i-- )
	{
		dAll	+=	at(i).Volume;
		nCount	++;
		if( nCount >= nDays )
			break;
	}
	// data not enough
	if( nCount != nDays || nCount <= 0 )
		return false;

	if( fabs(dAll) < 1 )
		return false;

	if( pValue )
		*pValue	=	(at(nIndex).Volume / dAll ) * nCount;
	return true;
}

bool KdataContainer::GetRS( double * pValue, DWORD dateCur, int nDays )
{
//	assert( pValue && nDays > 0 );

	// Find date Current to calculate from
	int		nIndex	=	GetIndexByDate( dateCur );
	if( -1 == nIndex )
		return false;

	// data not enough
	if( nDays > nIndex )
		return false;

	// 检查是否是相邻成交日
	if( !IsAdjacentDays( nIndex, nDays ) )
		return false;

	int	nCount	=	0;
	double	dA = 0, dB = 0;
	for( int k=nIndex; k>=1; k-- )
	{
		if( at(k).ClosePrice > at(k-1).ClosePrice )
			dA	+=	(at(k).ClosePrice - at(k-1).ClosePrice);
		else
			dB	+=	(at(k-1).ClosePrice - at(k).ClosePrice);
		
		nCount	++;
		if( nCount == nDays )
		{
			double	dResult;
			if( fabs(dB) < 1e-4 )
				dResult	=	100;
			else
				dResult	=	dA / dB;
			if( pValue )
				*pValue	=	dResult;
			return true;
		}
	}
	return false;
}

bool KdataContainer::GetMA( double * pValue, size_t nIndex, size_t nDays )
{
	if( nIndex < 0 || nIndex >=size() || nDays <= 0 )
		return false;

	int	nCount	=	0;
	if( nDays > nIndex+1 )
		return false;
	double	dResult	=	0;
	for( int k=nIndex; k>=0; k-- )
	{
		dResult	+=	MaindataAt(k);
		nCount	++;
		if( nCount == nDays )
		{
			if( pValue )
				*pValue	=	dResult / nDays;
			return true;
		}
	}
	return false;
}

/////////////////////////////////////////////////////////////////////////////
// Private Operations
int KdataContainer::ConvertKData( KdataContainer &kdSrc, KdataContainer &kdDest, int multiple )
{
	// convert kdSrc k line to kdDest k line accordding to their multiple
//	assert( multiple > 1 );
	if( multiple < 2 )	return 0;

	kdDest.reserve( /*0,*/ kdSrc.size() / multiple + 5 );
	
	int nStart		=	kdSrc.size() % multiple;
	int	nCount		=	0;
	KDATA	dataDest;
	for( size_t pos=nStart; pos<kdSrc.size(); pos++ )
	{
		KDATA & dataSrc = kdSrc.at( pos );

		nCount	++;
		if( 1 == nCount )
		{
			memcpy( &dataDest, &dataSrc, sizeof(dataDest) );
		}
		else
		{
			dataDest.Turnover		+=	dataSrc.Turnover;
			if( dataDest.HighestPrice < dataSrc.HighestPrice )	
				dataDest.HighestPrice	=	dataSrc.HighestPrice;
			if( dataDest.LowestPrice > dataSrc.LowestPrice )		
				dataDest.LowestPrice		=	dataSrc.LowestPrice;
			dataDest.Volume		+=	dataSrc.Volume;
			dataDest.ClosePrice		=	dataSrc.ClosePrice;
		}
		
		if( multiple == nCount )	// new dataDest is over
		{
			nCount	=	0;
			kdDest.push_back(dataDest);
		}
	}
	
	return kdDest.size();
}

void KdataContainer::LoadDataOriginal( )
{
	if( m_pDataOriginal )
	{
// 		if( m_pData )
// 			delete [] (BYTE*)m_pData;
// 
// 		m_pData	=	m_pDataOriginal;
// 		m_nSize	=	m_nSizeOriginal;
// 		m_nMaxSize	=	m_nMaxSizeOriginal;

		m_pDataOriginal	=	NULL;
		m_nSizeOriginal = m_nMaxSizeOriginal = 0;
	}
}

void KdataContainer::StoreDataOriginal( )
{
	if( NULL == m_pDataOriginal )
	{
// 		m_pDataOriginal		=	m_pData;
// 		m_nSizeOriginal		=	m_nSize;
// 		m_nMaxSizeOriginal	=	m_nMaxSize;

//		m_pData	=	NULL;
//		m_nSize = m_nMaxSize = 0;
	}
}
/*
float kdata_container::GetRatio( float fLastClose, DRDATA & dr )
{
	if( fLastClose < 1e-4 )
		return	1.0000;

	float	fRatio	=	1.0000;
	if( dr.m_fProfit > 1e-4 )
	{
		fRatio	=	fRatio * ( 1 - dr.m_fProfit/fLastClose );
	}
	if( dr.m_fGive > 1e-4 )
	{
		fRatio	=	(float)( fRatio * 1./(1.+dr.m_fGive) );
	}
	if( dr.m_fPei > 1e-4 )
	{
		float	priceNow = (float)( (dr.m_fPeiPrice * dr.m_fPei + fLastClose)/(1.+dr.m_fPei) );
		fRatio	=	fRatio * priceNow / fLastClose;
	}
	return fRatio;
}
*/
void KdataContainer::ConvertXDR( bool bUP, DWORD dateAutoDRBegin, double dAutoDRLimit )
{
	/*
	CDRData	drtemp;
	drtemp	=	m_drdata;
	drtemp.Sort( );

	if( m_pData )
	{
		delete [] (BYTE*)m_pData;
		m_nSize	=	m_nMaxSize	=	0;
		m_pData	=	NULL;
	}

	if( NULL == m_pDataOriginal || 0 == m_nSizeOriginal )
		return;
	*/
/*	if( drtemp.GetSize() == 0 )
	{
		SetSize( m_nSizeOriginal );
		if( m_pData )
			memcpy( m_pData, m_pDataOriginal, sizeof(KDATA)*m_nSize );
		return;
	}
*/
	/*
	dAutoDRLimit	=	dAutoDRLimit / 100;

	if( bUP )
	{
		SetSize( 0, m_nSizeOriginal );
		int	drPos	=	0;
		float	fRatio	=	1.000000;
		for( int i=0; i<m_nSizeOriginal; i++ )
		{
			KDATA	& kd = m_pDataOriginal[i];
			KDATA	newkd	=	kd;
			
			if( drPos < drtemp.GetSize() && ToDayDate(kd.m_date) >= drtemp.ElementAt(drPos).m_date )
			{
				if( i > 0 )
				{
					KDATA	kdLast	=	m_pDataOriginal[i-1];
					fRatio	=	fRatio * GetRatio( kdLast.m_fClose, drtemp.ElementAt(drPos) );
				}
				
				drPos	++;
			}
			else if( ToDayDate(kd.m_date) >= dateAutoDRBegin && i > 0 )	//	Auto XDR
			{
				KDATA	kdLast	=	m_pDataOriginal[i-1];
				if( kdLast.m_fClose > 1e-4 && kd.m_fOpen < kdLast.m_fClose
					&& fabs(kd.m_fOpen/kdLast.m_fClose-1) > dAutoDRLimit )
					fRatio	=	fRatio * kd.m_fOpen / kdLast.m_fClose;
			}

			newkd.m_fOpen	=	(kd.m_fOpen / fRatio);
			newkd.m_fHigh	=	(kd.m_fHigh / fRatio);
			newkd.m_fLow	=	(kd.m_fLow / fRatio);
			newkd.m_fClose	=	(kd.m_fClose / fRatio);
			newkd.m_fVolume	=	(kd.m_fVolume * fRatio);
			Add( newkd );
		}
	}
	else
	{
		SetSize( m_nSizeOriginal );
		int	drPos	=	drtemp.GetSize()-1;
		float	fRatio	=	1.000000;
		for( int i=m_nSizeOriginal-1; i>=0; i-- )
		{
			KDATA	& kd = m_pDataOriginal[i];
			KDATA	newkd	=	kd;
			
			if( drPos >= 0 && ToDayDate(kd.m_date) < drtemp.ElementAt(drPos).m_date )
			{
				if( i < m_nSizeOriginal-1 )
					fRatio	=	fRatio * GetRatio( kd.m_fClose, drtemp.ElementAt(drPos) );
				
				drPos	--;
			}
			else if( ToDayDate(kd.m_date) >= dateAutoDRBegin && i+1 < m_nSizeOriginal )	//	Auto XDR
			{
				KDATA	kdNext	=	m_pDataOriginal[i+1];
				if( kdNext.m_fOpen > 1e-4 && kdNext.m_fOpen < kd.m_fClose
					&& fabs(kdNext.m_fOpen/kd.m_fClose-1) > dAutoDRLimit )
					fRatio	=	fRatio * kdNext.m_fOpen / kd.m_fClose;
			}

			newkd.m_fOpen	=	(kd.m_fOpen * fRatio);
			newkd.m_fHigh	=	(kd.m_fHigh * fRatio);
			newkd.m_fLow	=	(kd.m_fLow * fRatio);
			newkd.m_fClose	=	(kd.m_fClose * fRatio);
			newkd.m_fVolume	=	(kd.m_fVolume / fRatio);
			SetAt( i, newkd );
		}
	}
	*/
}

int KdataContainer::GetUpperBound() const
{ 
	return size()-1; 
}

// void kdata_container::SetAt(int nIndex, KDATA newElement)
// { 
// 	//assert(nIndex >= 0 && nIndex < m_nSize);
// 	m_pData[nIndex] = newElement; 
// }


bool KdataContainer::DateAt(size_t nIndex,
								  int &nYear, int &nMonth, int &nDay, int &nHour, int &nMinute ) const
{
//	assert(nIndex >= 0 && nIndex < m_nSize);
	if( nIndex < 0 || nIndex >= size() )
		return 0;
	DWORD	date	=	(*this)[nIndex].TradingDate;
	boost::posix_time::ptime	sptime;
	bool	bOK	=	false;
	if( ktypeMonth == m_nKType 
		|| ktypeWeek == m_nKType 
		|| ktypeDay == m_nKType )
	{
		bOK = FromInstrumentTimeDayOrMin(sptime,date,true);
	}
	else if( ktypeMin60 == m_nKType
		|| ktypeMin30 == m_nKType
		|| ktypeMin15 == m_nKType
		|| ktypeMin5 == m_nKType )
	{
		bOK = FromInstrumentTimeDayOrMin(sptime, date, false);
	}
	else
	{
		nYear	=	nMonth	=	nDay	=	nHour	=	nMinute	=	0;
		return false;
	}
	if( !bOK )
		return false;

	nYear	=	sptime.date().year();
	nMonth = sptime.date().month();
	nDay	=	sptime.date().day();
	nHour	=	sptime.time_of_day().hours();
	nMinute	=	sptime.time_of_day().minutes();

	return true;
}

double	KdataContainer::MaindataAt(size_t nIndex) const
// 得到kdata_container的nIndex日的主数据，根据主数据类型不同，返回值可能是开盘价、收盘价或者平均价;
{
//	assert(nIndex >= 0 && nIndex < m_nSize);
	if( nIndex < 0 || nIndex >= size() )
		return 0;
	if( mdtypeOpen == m_nCurMaindataType )
		return (*this)[nIndex].OpenPrice;
	else if( mdtypeAverage == m_nCurMaindataType
		&& (*this)[nIndex].Volume > 1e-4 && (*this)[nIndex].Turnover > 1e-4)
	{
		int		nCount	=	0;
		double	average = ((double)((*this)[nIndex].Turnover)) / (*this)[nIndex].Volume;
		while (average < (*this)[nIndex].LowestPrice && nCount < 10)	{ average *= 10;	nCount++; }
		while (average >(*this)[nIndex].HighestPrice && nCount < 20)	{ average /= 10;	nCount++; }
		if (average < (*this)[nIndex].LowestPrice)		//	说明是指数;
			average = ((*this)[nIndex].OpenPrice + (*this)[nIndex].HighestPrice + (*this)[nIndex].LowestPrice + (*this)[nIndex].ClosePrice) / 4;
		return average;
	}
	else if (mdtypeHigh ==  m_nCurMaindataType)
	{
		return (*this)[nIndex].HighestPrice;
	}
	else if (mdtypeLow ==  m_nCurMaindataType)
	{
		return (*this)[nIndex].LowestPrice;
	}
	else if (mdtypeTrunover==m_nCurMaindataType)
	{
		return (*this)[nIndex].Turnover;
	}
	else if (mdtypeVolume ==  m_nCurMaindataType)
	{
		return (*this)[nIndex].Volume;
	}
	else if (mdtypeOpenInterest == m_nCurMaindataType)
	{
		return (*this)[nIndex].OpenInterest;
	}
	else
	{
		return (*this)[nIndex].ClosePrice;
	}

	// WARNING CPV::Calculate( ... ) use the save code.
}

bool KdataContainer::update(const Tick* pReport)
{
	assert(pReport);
	if (!pReport)
		return false;
	if (!m_LastTickPtr)
	{
		m_LastTickPtr = boost::make_shared<Tick>(*pReport);
	}
	else
	{
		if (m_LastTickPtr->UpdateTime==pReport->UpdateTime && m_LastTickPtr->UpdateMillisec == pReport->UpdateMillisec)
		{
			//这个Tick已经更新过了;
			LOGDEBUG("duplicate update err kd:{} {}.{}",pReport->TradingDay,pReport->UpdateTimeStr, pReport->UpdateMillisec);
			return true;
		}
	}
	int dwType = GetKType();
	if (dwType <= 0)
	{
		return false;
	}

	double LastTurnover = m_LastTickPtr->Turnover;
	int64_t LastVolume = m_LastTickPtr->Volume;
	*m_LastTickPtr = *pReport;
	//换算成整点时间(以K线周期的整数倍);

	//看在不在一个周期内;
	time_t tTemp = (pReport->UpdateTime / dwType)*dwType + dwType;

	if (size() == 0)
	{
		KDATA kd;
		strcpy(kd.TradingDay, pReport->TradingDay);
		strcpy(kd.szCode, pReport->szCode);
		strcpy(kd.szExchange, pReport->szExchange);
		kd.OpenPrice = pReport->LastPrice;
		kd.HighestPrice = pReport->LastPrice;
		kd.LowestPrice = pReport->LastPrice;
		kd.ClosePrice = pReport->LastPrice;
		kd.Turnover = pReport->Turnover-LastTurnover;
		kd.Volume = pReport->Volume-LastVolume;
		kd.TradingTime = tTemp;
		kd.AveragePrice = pReport->AveragePrice;
		push_back(kd);
		return true;
	}
	KDATA kd=back();
	bool bNewKData = false;
	if (strcmp(kd.TradingDay, pReport->TradingDay) != 0)
	{
		//日期不一样了,夜盘开盘?;
		//InsertKDataSort(kd);
		kd.OpenPrice = pReport->LastPrice;
		kd.HighestPrice = pReport->LastPrice;
		kd.LowestPrice = pReport->LastPrice;
		kd.ClosePrice = pReport->LastPrice;

		kd.Turnover = pReport->Turnover;
		kd.Volume = pReport->Volume;

		strcpy(kd.TradingDay, pReport->TradingDay);
		kd.TradingTime = tTemp;
		push_back(kd);
		bNewKData = true;
	}
	else
	{
		//下一个周期开始;
		if (kd.TradingTime < tTemp) //这里有BUG,没处理过了0点时的状态
		{
			//新的周期;
			kd.OpenPrice = pReport->LastPrice;
			kd.HighestPrice = pReport->LastPrice;
			kd.LowestPrice = pReport->LastPrice;
			kd.ClosePrice = pReport->LastPrice;

			kd.Volume = (pReport->Volume - LastVolume);
			kd.Turnover = (pReport->Turnover - LastTurnover);

			kd.TradingTime = tTemp;
			push_back(kd);
			bNewKData = true;
		}
		else if (kd.TradingTime ==  tTemp)
		{
			kd.ClosePrice = pReport->LastPrice;
			//最高价与最低价的计算;
			if (pReport->LastPrice > kd.HighestPrice)
			{
				kd.HighestPrice = pReport->LastPrice;
			}
			if (pReport->LastPrice < kd.LowestPrice)
			{
				kd.LowestPrice = pReport->LastPrice;
			}
			kd.Turnover += (pReport->Turnover - kd.Turnover);
			kd.Volume += (pReport->Volume - kd.Volume);
			this->back()=kd;
		}
		else
		{
			//回溯K线;
			LOGDEBUG("update err kd:{} {}->t:{}"
				, kd.szCode
				, boost::posix_time::to_iso_extended_string(boost::posix_time::from_time_t(kd.TradingTime))
				, boost::posix_time::to_iso_extended_string(boost::posix_time::from_time_t(tTemp)));
		}
	}
	
	return true;
}

int KdataContainer::ToReport(ReportContainer & report)
{
	report.clear();

	if (size()==0)
	{
		return 0;
	}

	report.resize(size());
	InstrumentInfo info;
	double price_tick = 0;
	if (InstrumentContainer::get_instance().GetInstrumentInfo((*this)[0].szCode, &info))
	{
		price_tick = info.PriceTick;
	}
	convert_KDATA_to_REPORT(&(*this)[0], &report[0]);

	report[0].AskPrice[0] = report[0].LastPrice + price_tick;
	report[0].BidPrice[0] = report[0].LastPrice - price_tick;
	report[0].OpenPrice = (*this)[0].OpenPrice;
	

	boost::posix_time::ptime prevTickTime = boost::posix_time::from_time_t(report[0].UpdateTime) - boost::posix_time::seconds(1);
	strcpy(report[0].UpdateTimeStr, boost::posix_time::to_simple_string(prevTickTime.time_of_day()).c_str());

	std::string TradingDay((*this)[0].TradingDay);
	for (size_t i = 1; i < size(); ++i)
	{
		convert_KDATA_to_REPORT(&(*this)[i],&report[i]);
		report[i].AskPrice[0] = report[0].LastPrice + price_tick;
		report[i].BidPrice[0] = report[0].LastPrice - price_tick;
		boost::posix_time::ptime prevTickTime = boost::posix_time::from_time_t(report[i].UpdateTime) - boost::posix_time::seconds(1);
		strcpy(report[i].UpdateTimeStr, boost::posix_time::to_simple_string(prevTickTime.time_of_day()).c_str());
		if (strcmp(report[i-1].TradingDay , report[i].TradingDay) == 0)
		{
			//交易日没变,则累加;
			report[i].Volume = report[i-1].Volume + (*this)[i].Volume;
			report[i].Turnover = report[i-1].Turnover + (*this)[i].Turnover;

			if (report[i].HighestPrice < report[i-1].HighestPrice)
			{
				report[i].HighestPrice = report[i - 1].HighestPrice;
			}

			if (report[i].LowestPrice > report[i - 1].LowestPrice)
			{
				report[i].LowestPrice = report[i - 1].LowestPrice;
			}

			report[i].OpenPrice = report[i - 1].OpenPrice;
		}
		else
		{
			report[i - 1].ClosePrice = (*this)[i].ClosePrice;
			report[i].OpenPrice = (*this)[i].OpenPrice;
			report[i].PreClosePrice = report[i - 1].ClosePrice;
			report[i].PreSettlementPrice = report[i - 1].ClosePrice;
		}
	}
	return report.size();
}
