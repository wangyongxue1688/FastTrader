#include "ExchangeContainer.h"
#include <boost/algorithm/string.hpp>
#include <boost/make_shared.hpp>
#include <PlatformStruct.h>
#include "../Log/logging.h"

ExchangeContainer::ExchangeContainer()
{
	LOGINIT("FacilityBaseLib");
	//初始化交易所信息;
	exchange_t SHFE = { EIDT_SHFE ,"SHFE","上海期货交易所",EXP_Normal};
	exchange_t CZCE = { EIDT_CZCE ,"CZCE","郑州商品交易所",EXP_Normal };
	exchange_t DCE = { EIDT_DCE ,"DCE","大连商品交易所",EXP_Normal };
	exchange_t CFFEX = { EIDT_CFFEX ,"CFFEX","中国金融期货交易所",EXP_Normal };
	std::vector<std::pair<boost::posix_time::time_duration, boost::posix_time::time_duration> >
		SHFE_CZCE_DCE_DAY_TRADE_TIME = 
	{ 
		{ { 9,30,0 },{ 10,15,0 } }, 
		{ { 10,30,0 },{ 11,30,0 } },
		{ { 13,30,0 },{ 15,00,0 } },
	};
	std::vector<std::pair<boost::posix_time::time_duration, boost::posix_time::time_duration> >
		SHFE_NIGHT_TRADE_TIME = 
	{
		{ { 21,00,0 },(boost::posix_time::time_duration{ 24,0,0 } + boost::posix_time::time_duration{2,30,0}) },
	};

	std::vector<std::pair<boost::posix_time::time_duration, boost::posix_time::time_duration> >
		CZCE_DCE_NIGHT_TRADE_TIME =
	{
		{ { 21,00,0 },{ 23,30,0 } },
	};
	//商品交易所;
	push_back(boost::make_shared<ExchangeBase>(SHFE, SHFE_CZCE_DCE_DAY_TRADE_TIME, SHFE_NIGHT_TRADE_TIME));
	push_back(boost::make_shared<ExchangeBase>(CZCE, SHFE_CZCE_DCE_DAY_TRADE_TIME, CZCE_DCE_NIGHT_TRADE_TIME));
	push_back(boost::make_shared<ExchangeBase>(DCE, SHFE_CZCE_DCE_DAY_TRADE_TIME, CZCE_DCE_NIGHT_TRADE_TIME));
	//指数交易所;
	std::vector<std::pair<boost::posix_time::time_duration, boost::posix_time::time_duration> >
		CFFEX_DAY_TRADE_TIME =
	{
		{ { 9,30,0 },{ 11,30,0 } },
		{ { 13,00,0 },{ 15,15,0 } },
	};
	//没有夜盘;
	std::vector<std::pair<boost::posix_time::time_duration, boost::posix_time::time_duration> >
		CFFEX_NIGHT_TRADE_TIME;
	push_back(boost::make_shared<ExchangeBase>(CFFEX, CFFEX_DAY_TRADE_TIME, CFFEX_NIGHT_TRADE_TIME));
}

ExchangeContainer & ExchangeContainer::get_instance()
{
	static ExchangeContainer g_exchange_container;
	return g_exchange_container;
}

boost::shared_ptr<ExchangeBase> ExchangeContainer::find_exchange_by_id(const std::string & exchangeId)
{
	auto eIter = std::find_if(begin(), end(),
		[exchangeId](const ExchangeContainer::value_type& v) {
		return v->get_exchange_id() == exchangeId;
	});
	if (eIter == end())
	{
		return nullptr;
	}
	return (*eIter);
}

int ExchangeContainer::get_market_id(const std::string & exchangeid)
{
	auto eIter = std::find_if(begin(), end(), [exchangeid](boost::shared_ptr<ExchangeBase> e) 
	{return e->get_exchange_id() == exchangeid;});
	if (eIter != end())
	{
		return (*eIter)->get_market_id();
	}
	return -1;
}

bool ExchangeContainer::update(boost::shared_ptr<exchange_t> exchange_ptr)
{
	if (!exchange_ptr)
	{
		return false;
	}
	auto eIter = std::find_if(begin(), end(), [exchange_ptr](boost::shared_ptr<ExchangeBase> e) 
	{
		return boost::iequals(e->get_exchange_id() , exchange_ptr->ExchangeID);
	});
	if (eIter == end())
	{
		auto exchange_base_ptr = boost::make_shared<ExchangeBase>(*exchange_ptr);
		push_back(exchange_base_ptr);
		return true;
	}
	else
	{
		(*eIter)->update(*exchange_ptr);
	}
	return false;
}

boost::posix_time::ptime ExchangeContainer::get_next_open_time(const std::string& exchange_id)
{
	//获取肖前时间;
	auto current_time = boost::posix_time::second_clock::local_time();

	//交易日判断;
	return current_time;
}

void ExchangeContainer::set_holiday_list(const std::vector<boost::gregorian::date>& days)
{

}

void ExchangeContainer::set_trading_day(boost::gregorian::date & trading_day)
{
}

boost::gregorian::date ExchangeContainer::get_trading_day()
{
	return boost::gregorian::date();
}

std::string ExchangeContainer::get_default_exchange()
{
	return default_exchange_id;
}

void ExchangeContainer::set_default_exchange(const std::string & exchange_id)
{
	default_exchange_id = exchange_id;
}

