HEADERS += \
    $$PWD/BaseData.h \
    $$PWD/Container.h \
    $$PWD/DateTimeHelper.h \
    $$PWD/ExchangeBase.h \
    $$PWD/ExchangeContainer.h \
    $$PWD/Express.h \
    $$PWD/FacilityBaseLib.h \
    $$PWD/InstrumentConfig.h \
    $$PWD/InstrumentData.h \
    $$PWD/InstrumentInfo.h \
    $$PWD/KData.h \
    $$PWD/Minute.h \
    $$PWD/plate.h \
    $$PWD/Report.h \
    $$PWD/StringMgr.h

SOURCES += \
    $$PWD/BaseData.cpp \
    $$PWD/Container.cpp \
    $$PWD/ExchangeBase.cpp \
    $$PWD/ExchangeContainer.cpp \
    $$PWD/Express.cpp \
    $$PWD/FacilityBaseLib.cpp \
    $$PWD/faciliytbaselib.cpp \
    $$PWD/IMCode.cpp \
    $$PWD/InstrumentConfig.cpp \
    $$PWD/InstrumentData.cpp \
    $$PWD/InstrumentInfo.cpp \
    $$PWD/KData.cpp \
    $$PWD/Minute.cpp \
    $$PWD/plate.cpp \
    $$PWD/Report.cpp \
    $$PWD/StringMgr.cpp
