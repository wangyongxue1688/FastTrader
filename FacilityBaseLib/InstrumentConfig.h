#ifndef _INSTRUMENT_CONFIG_H_
#define _INSTRUMENT_CONFIG_H_
#include "FacilityBaseLib.h"
/*#include "Technique.h"*/
class FACILITY_API InstrumentConfig
{
public:
	static  InstrumentConfig&  GetInstance();

	InstrumentConfig(void);
	virtual ~InstrumentConfig(void);
public:
	void	 SetDiffDays( int nDays );
	int		 GetDiffDays( );
	void	 SetDiffPercentDays( int nDays );
	int		 GetDiffPercentDays( );
	void	 SetScopeDays( int nDays );
	int		 GetScopeDays( );
	void	 SetRatioChangeHandDays( int nDays );
	int		 GetRatioChangeHandDays( );
	void	 SetRatioVolumeDays( int nDays );
	int		 GetRatioVolumeDays( );
	void	 SetRSDays( int nDays );
	int		 GetRSDays( );
	void	 SetYieldAverageDays( int nDays );
	int		 GetYieldAverageDays( );

	size_t		GetCacheDays();
protected:
	int m_iDiffDays;
	int m_iDiffPercentDays;
	int m_iScopeDays;
	int m_iRatioChangeHandDays;
	int m_iRatioVolumeDays;
	int m_iRSDays;
	int m_iYieldAverageDays;
	size_t m_iCacheDays;
};
#endif
