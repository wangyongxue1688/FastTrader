#ifndef _REPORT_H_
#define _REPORT_H_
#include <PlatformStruct.h>
//#include "array_container.h"
#include "FacilityBaseLib.h"
#include "Tick.h"
#include <boost/thread/mutex.hpp>

inline bool operator <(const Tick&  p1, const Tick& p2)
{
	const Tick *pTemp1 = &p1;
	const Tick *pTemp2 = &p2;

	if (pTemp1 && pTemp2 && pTemp1->UpdateTime < pTemp2->UpdateTime)
		return true;
	else if (pTemp1 && pTemp2 && pTemp1->UpdateTime > pTemp2->UpdateTime)
		return false;
	if (pTemp1 && pTemp2 && pTemp1->Volume < pTemp2->Volume)
		return true;
	else if (pTemp1 && pTemp2 && pTemp1->Volume > pTemp2->Volume)
		return false;
	return false;
}
//template class FACILITY_API array_container<Tick>;
class KdataContainer;
class FACILITY_API ReportContainer:public std::vector<Tick>
{
public:
	int		GetUpperBound() const;
	// Accessing elements
	int		InsertReportSort(const Tick& newElement );
	void	sort( );
	void	RemoveDirty();
	void	clear();
	bool	GetMMLD( std::size_t nIndex, double *pdVolBuy, double *pdVolSell, double * pdVolDiff );	// 取得买卖力道指标值;
	bool	GetMMLDMinMaxInfo( double *pdMin, double *pdMax );			// 取得买卖力道指标最大值最小值;
	bool	StatBuySellEx(double * fSellPrice, double * fSellVolume, double * fBuyPrice, double * fBuyVolume, size_t nSize);
	bool	GetIndexWave( double *pdWave, size_t nIndex );
	//转换成K线数据;
	int		ToKData( KdataContainer & kdata );
	friend  FACILITY_API ReportContainer  operator -(const ReportContainer& l1, const ReportContainer& l2);
};
#endif
