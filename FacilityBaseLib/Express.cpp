/*
	Cross Platform Core Code.

	Copyright(R) 2001-2002 Balang Software.
	All rights reserved.

	Using:
		class	CExpress;
		class	CIndex;
		class	CIndexContainer;
*/

#include "Express.h"
#include <math.h>
#include <float.h>
#include "InstrumentConfig.h"
#include "InstrumentInfo.h"
#include "Container.h"

//////////////////////////////////////////////////////////////////////////////////
// 表达式错误提示字符串
#ifdef	CLKLAN_ENGLISH_US
char	express_errparserstack[]	=	"Expression: Expression is too long and stack overflow.";
char	express_errbadrange[]		=	"Expression: Value is out of range.";
char	express_errexpression[]		=	"Expression: Strings error.";
char	express_erroperator[]		=	"Expression: Operator Sign error.";
char	express_erropenparen[]		=	"Expression: Open parenthesis error.";
char	express_errcloseparen[]		=	"Expression: Close parenthesis error.";
char	express_errinvalidnum[]		=	"Expression: Invalid number.";
char	express_errmath[]			=	"Expression: Mathematical error.";
char	express_errunknown[]		=	"Expression: Unknown.";
#else
char	express_errparserstack[]	=	"表达式：表达式太长，栈溢出。";
char	express_errbadrange[]		=	"表达式：数值超出范围。";
char	express_errexpression[]		=	"表达式：字符串出现错误。";
char	express_erroperator[]		=	"表达式：操作符出现错误。";
char	express_erropenparen[]		=	"表达式：左括号出现错误。";
char	express_errcloseparen[]		=	"表达式：右括号出现错误。";
char	express_errinvalidnum[]		=	"表达式：非法数字。";
char	express_errmath[]			=	"表达式：数学计算出现错误。";
char	express_errunknown[]		=	"表达式：未知错误。";
#endif
SLH_DATA	slh_data_array[SLH_MAX+1]	=	{
	{ SLH_NONE,						"",	100,	"" },
	{ SLH_CODE,						"",	100,	"" },
	{ SLH_NAME,						"",	110,	"" },
	//	技术数据
	{ SLH_DATE,						"",	110,	"" },
	{ SLH_PRECLOSE,				"",	100,	"lastclose" },
	{ SLH_OPEN,						"",	100,	"open" },
	{ SLH_CLOSE,					"",	100,	"close" },
	{ SLH_HIGH,						"",	100,	"high" },
	{ SLH_LOW,						"",	100,	"low" },
	{ SLH_AVERAGE,					"",	100,	"average" },
	{ SLH_DIFF,						"",	100,	"diff" },
	{ SLH_DIFFPERCENT,				"",	100,	"diffpercent" },
	{ SLH_SCOPE,					"",	100,	"scope" },
	{ SLH_VOLUME,					"",	140,	"volume" },
	{ SLH_TURNOVER,					"",	140,	"amount" },
	{ SLH_VOLUP,					"",	100,	"volup" },
	{ SLH_VOLDOWN,					"",	100,	"voldown" },
	{ SLH_DIFFPERCENT_MIN5,			"",	100,	"diffpercent_min5" },
	{ SLH_SELLBUYRATIO,				"",	100,	"sellbuyratio" },
	{ SLH_SELLBUYDIFF,				"",	100,	"sellbuydiff" },
	{ SLH_BUYPRICE3,				"",	100,	"buyprice3" },
	{ SLH_BUYPRICE2,				"",	100,	"buyprice2" },
	{ SLH_BUYPRICE1,				"",	100,	"buyprice1" },
	{ SLH_SELLPRICE1,				"",	100,	"sellprice1" },
	{ SLH_SELLPRICE2,				"",	100,	"sellprice2" },
	{ SLH_SELLPRICE3,				"",	100,	"sellprice3" },
	{ SLH_BUYVOLUME3,				"",	100,	"buyvolume3" },
	{ SLH_BUYVOLUME2,				"",	100,	"buyvolume2" },
	{ SLH_BUYVOLUME1,				"",	100,	"buyvolume1" },
	{ SLH_SELLVOLUME1,				"",	100,	"sellvolume1" },
	{ SLH_SELLVOLUME2,				"",	100,	"sellvolume2" },
	{ SLH_SELLVOLUME3,				"",	100,	"sellvolume3" },
	
	//	主要基本指标
	{ SLH_REPORTTYPE,				"",	100,	"" },
	{ SLH_PE,						"",	100,	"pe" },
	{ SLH_PNETASSET,				"",	100,	"pnetasset" },
	{ SLH_PMAININCOME,				"",	100,	"pmainincome" },
	{ SLH_RATIO_PCASH,				"",	100,	"ratio_pcash" },
	{ SLH_RATIO_CURRENCY,			"",	100,	"ratio_currency" },
	{ SLH_RATIO_CHANGEHAND,			"",	100,	"ratio_changehand" },
	{ SLH_RATIO_VOLUME,				"",	100,	"ratio_volume" },
	{ SLH_RS,						"",	100,	"rs" },
	{ SLH_MARKETVALUE,				"",	170,	"marketvalue" },
	{ SLH_MARKETVALUEA,				"",	170,	"marketvaluea" },
	{ SLH_MARKETVALUEB,				"",	170,	"marketvalueb" },
	// ★偿债能力
	{ SLH_RATIO_LIQUIDITY,			"",	140,	"ratio_liquidity" },
	{ SLH_RATIO_QUICK,				"",	140,	"ratio_quick" },
	{ SLH_VELOCITY_RECEIVABLES,		"",	170,	"velocity_receivables" },
	// ★经营能力
	{ SLH_VELOCITY_MERCHANDISE,		"",	150,	"velocity_merchandise" },
	{ SLH_MAIN_INCOME,				"",	160,	"main_income" },
	{ SLH_CASH_PS,					"",	170,	"cash_ps" },
	// ★盈利能力
	{ SLH_PROFIT_MARGIN,			"",	170,	"profit_margin" },
	{ SLH_NETASSET_YIELD,			"",	190,	"netasset_yield" },
	// ★资本结构
	{ SLH_DATE_BEGIN,				"",	160,	"" },
	{ SLH_SHARE_COUNT_TOTAL,		"",	160,	"share_count_total" },
	{ SLH_SHARE_COUNT_A,			"",	140,	"share_count_a" },
	{ SLH_SHARE_COUNT_B,			"",	140,	"share_count_b" },
	{ SLH_SHARE_COUNT_H,			"",	140,	"share_count_h" },
	{ SLH_SHARE_COUNT_NATIONAL,		"",	140,	"share_count_national" },
	{ SLH_SHARE_COUNT_CORP,			"",	140,	"share_count_corp" },
	{ SLH_PROFIT_PSUD,				"",	170,	"profit_psud" },
	{ SLH_ASSET,					"",	140,	"asset" },
	{ SLH_RATIO_HOLDERRIGHT,		"",	170,	"ratio_holderright" },
	{ SLH_RATIO_LONGDEBT,			"",	170,	"ratio_longdebt" },
	{ SLH_RATIO_DEBT,				"",	170,	"ration_debt" },
	// ★投资收益能力
	{ SLH_NETASSET_PS,				"",	150,	"netasset_ps" },
	{ SLH_NETASSET_PS_REGULATE,		"",	200,	"netasset_ps_regulate" },
	{ SLH_EPS,						"",	140,	"eps" },
	{ SLH_EPS_DEDUCT,				"",	170,	"eps_deduct" },
	{ SLH_NET_PROFIT,				"",	140,	"net_profit" },
	{ SLH_MAIN_PROFIT,				"",	140,	"main_profit" },
	{ SLH_TOTAL_PROFIT,				"",	140,	"total_profit" },
	{ SLH_PROFIT_INC,				"",	170,	"profit_inc" },
	{ SLH_INCOME_INC,				"",	170,	"income_inc" },
	{ SLH_ASSET_INC,				"",	190,	"asset_inc" },
	{ SLH_ANNGAINS_AVERAGE,			"",	170,	"anngains_average" },
	{ SLH_ANNGAINS_STDDEV,			"",	170,	"anngains_stddev" },
	{ SLH_BETA,						"",	100,	"beta" },
	{ SLH_SHARP,					"",	100,	"sharp" },
	{ SLH_TRADE,					"",	100,	"" },
	{ SLH_PROVINCE,					"",	100,	"" },
	//{ SLH_PRODUCTID,"",100,"ProductID"},
	//{SLH_PRODUCTCLASS,"",100,"ProductClass"},
	//{SLH_DELIVERYYEAR,"",100,"DeliveryYear"},
	//{SLH_DELIVERYMONTH,"",100,"DeliveryMonth"},
	//{SLH_MAXMARKETORDERVOLUME,"",100,"MaxMarketOrderVolume"},
	//{SLH_MINMARKETORDERVOLUME,"",100,"MinMarketOrderVolume"},
	//{SLH_MAXLIMITORDERVOLUME,"",100,"MaxLimitOrderVolume"},
	//{SLH_MINLIMITORDERVOLUME,"",100,"MinLimitOrderVolume"},
	//{SLH_VOLUMEMULTIPLE,"",100,"VolumeMultiple"},
	//{SLH_PRICETICK,"",100,"PriceTick"},
	//{SLH_CREATEDATE,"",100,"CreateDate"},
	//{SLH_OPENDATE,"",100,"OpenDate"},
	//{SLH_EXPIREDATE,"",100,"ExpireDate"},
	//{SLH_STARTDELIVDATE,"",100,"StartDelivDate"},
	//{SLH_ENDDELIVDATE,"",100,"EndDelivDate"},
	//{SLH_INSTLIFEPHASE,"",100,"InstLifePhase"},
	//{SLH_ISTRADING,"",100,"IsTrading"},
	//{SLH_POSITIONTYPE,"",100,"PositionType"},
	//{SLH_POSITIONDATETYPE,"",100,"PositionDateType"},
	//{SLH_LONGMARGINRATIO,"",100,"LongMarginRatio"},
	//{SLH_SHORTMARGINRATIO,"",100,"ShortMarginRatio"},
	//{SLH_LASTPRICE,"",100,"LastPrice"},
};
string	AfxGetSLHTitle( UINT nSLH )
{
	static	string	slh_titles[SLH_MAX+1];
	//assert( SLH_MAX >= 0 );

	if( slh_titles[0].length() <= 0 )
	{
		// slh_titles
		slh_titles[SLH_NONE]				=	"SLHTitles";

#ifdef	CLKLAN_ENGLISH_US
		slh_titles[SLH_CODE]				=	"Code";
		slh_titles[SLH_NAME]				=	"Name";

		//	技术数据
		slh_titles[SLH_DATE]				=	"Trade Time";
		slh_titles[SLH_LASTCLOSE]			=	"Prev Close";
		slh_titles[SLH_OPEN]				=	"Open";
		slh_titles[SLH_CLOSE]				=	"Last Trade";
		slh_titles[SLH_HIGH]				=	"High";
		slh_titles[SLH_LOW]					=	"Low";
		slh_titles[SLH_AVERAGE]				=	"Average";
		slh_titles[SLH_DIFF]				=	"Change";
		slh_titles[SLH_DIFFPERCENT]			=	"Change(%)";
		slh_titles[SLH_SCOPE]				=	"Range(%)";
		slh_titles[SLH_VOLUME]				=	"Volume(100)";
		slh_titles[SLH_AMOUNT]				=	"Amount(1000)";
		slh_titles[SLH_VOLUP]				=	"Up Vol.";
		slh_titles[SLH_VOLDOWN]				=	"Down Vol.";
		slh_titles[SLH_DIFFPERCENT_MIN5]	=	"5-Minute Change(%)";
		slh_titles[SLH_SELLBUYRATIO]		=	"Consign Ratio(%)";
		slh_titles[SLH_SELLBUYDIFF]			=	"Consign Volume Difference";
		slh_titles[SLH_BUYPRICE3]			=	"Bid 3";
		slh_titles[SLH_BUYPRICE2]			=	"Bid 2";
		slh_titles[SLH_BUYPRICE1]			=	"Bid 1";
		slh_titles[SLH_SELLPRICE1]			=	"Ask 1";
		slh_titles[SLH_SELLPRICE2]			=	"Ask 2";
		slh_titles[SLH_SELLPRICE3]			=	"Ask 3";
		slh_titles[SLH_BUYVOLUME3]			=	"Bid Vol 3(100)";
		slh_titles[SLH_BUYVOLUME2]			=	"Bid Vol 2(100)";
		slh_titles[SLH_BUYVOLUME1]			=	"Bid Vol 1(100)";
		slh_titles[SLH_SELLVOLUME1]			=	"Ask Vol 1(100)";
		slh_titles[SLH_SELLVOLUME2]			=	"Ask Vol 2(100)";
		slh_titles[SLH_SELLVOLUME3]			=	"Ask Vol 3(100)";

		//	主要基本指标
		slh_titles[SLH_REPORTTYPE]			=	"Report Forms";
		slh_titles[SLH_PE]					=	"P/E";
		slh_titles[SLH_PNETASSET]			=	"P/B";
		slh_titles[SLH_PMAININCOME]			=	"P/S"; // "market Capitalisation/Income";
		slh_titles[SLH_RATIO_PCASH]			=	"Price/Cash";
		slh_titles[SLH_RATIO_CURRENCY]		=	"Currency Percent(%)";
		slh_titles[SLH_RATIO_CHANGEHAND]	=	"Change hands Percent(%)";
		slh_titles[SLH_RATIO_VOLUME]		=	"Volume Ratio";
		slh_titles[SLH_RS]					=	"Relative Strength";
		slh_titles[SLH_MARKETVALUE]			=	"Market Capitalisation(10000)";
		slh_titles[SLH_MARKETVALUEA]		=	"A Market Capitalisation(10000)";
		slh_titles[SLH_MARKETVALUEB]		=	"B Market Capitalisation(10000)";

		// ★偿债能力
		slh_titles[SLH_RATIO_LIQUIDITY]		=	"Liquidity Ratio";
		slh_titles[SLH_RATIO_QUICK]			=	"Quickassets Ratio";
		slh_titles[SLH_VELOCITY_RECEIVABLES]=	"Receivables Velocity";

		// ★经营能力
		slh_titles[SLH_VELOCITY_MERCHANDISE]=	"Merchandise Velocity";
		slh_titles[SLH_MAIN_INCOME]			=	"Income(10000)";
		slh_titles[SLH_CASH_PS]				=	"Cash per share";

		// ★盈利能力
		slh_titles[SLH_PROFIT_MARGIN]		=	"Profit Margin(%)";
		slh_titles[SLH_NETASSET_YIELD]		=	"Return On Equity(%)";

		// ★资本结构
		slh_titles[SLH_DATE_BEGIN]			=	"IPO Date";
		slh_titles[SLH_SHARE_COUNT_TOTAL]	=	"Total Shares(10000)";
		slh_titles[SLH_SHARE_COUNT_A]		=	"Issued Shares A(10000)";
		slh_titles[SLH_SHARE_COUNT_B]		=	"Issued Shares B(10000)";
		slh_titles[SLH_SHARE_COUNT_H]		=	"Issued Shares H(10000)";
		slh_titles[SLH_SHARE_COUNT_NATIONAL]=	"National Shares(10000)";
		slh_titles[SLH_SHARE_COUNT_CORP]	=	"Corporation Shares(10000)";
		slh_titles[SLH_PROFIT_PSUD]			=	"Not Assigned Profit per Share";
		slh_titles[SLH_ASSET]				=	"Total Asset(10000)";
		slh_titles[SLH_RATIO_HOLDERRIGHT]	=	"Shareholders Rights Ratio(%)";
		slh_titles[SLH_RATIO_LONGDEBT]		=	"Long Debt Ratio(%)";
		slh_titles[SLH_RATIO_DEBT]			=	"Debt Ratio(%)";

		// ★投资收益能力
		slh_titles[SLH_NETASSET_PS]			=	"Net Asset per Share";
		slh_titles[SLH_NETASSET_PS_REGULATE]=	"Net Asset per Share Regulated";
		slh_titles[SLH_EPS]					=	"EPS"; // "Earnings per Share";
		slh_titles[SLH_EPS_DEDUCT]			=	"EPS Deducted"; // "Earnings per Share Deducted";
		slh_titles[SLH_NET_PROFIT]			=	"Net Profit/Loss(10000)";
		slh_titles[SLH_MAIN_PROFIT]			=	"Main Profit/Loss(10000)";
		slh_titles[SLH_TOTAL_PROFIT]		=	"Total Profit/Loss(10000)";
		slh_titles[SLH_PROFIT_INC]			=	"Profit Increase(%)";
		slh_titles[SLH_INCOME_INC]			=	"Income Increase(%)";
		slh_titles[SLH_ASSET_INC]			=	"Asset Increase(%)";
		slh_titles[SLH_ANNGAINS_AVERAGE]	=	"Average Ann-Gains(%)";
		slh_titles[SLH_ANNGAINS_STDDEV]		=	"Ann-Gains Standard Deviation(%)";
		slh_titles[SLH_BETA]				=	"Beta";
		slh_titles[SLH_SHARP]				=	"Sharp Venture";
		slh_titles[SLH_TRADE]				=	"Business Classification";
		slh_titles[SLH_PROVINCE]			=	"Province";
#else
		slh_titles[SLH_CODE]				=	"代码";
		slh_titles[SLH_NAME]				=	"名称";

		//	技术数据
		slh_titles[SLH_DATE]				=	"日期";
		slh_titles[SLH_PRECLOSE]			=	"昨收";
		slh_titles[SLH_OPEN]				=	"开盘价";
		slh_titles[SLH_CLOSE]				=	"现价";
		slh_titles[SLH_HIGH]				=	"最高价";
		slh_titles[SLH_LOW]					=	"最低价";
		slh_titles[SLH_AVERAGE]				=	"均价";
		slh_titles[SLH_DIFF]				=	"涨跌";
		slh_titles[SLH_DIFFPERCENT]			=	"涨幅%";
		slh_titles[SLH_SCOPE]				=	"震幅%";
		slh_titles[SLH_VOLUME]				=	"成交量(手)";
		slh_titles[SLH_TURNOVER]			=	"成交额(千元)";
		slh_titles[SLH_VOLUP]				=	"外盘";
		slh_titles[SLH_VOLDOWN]				=	"内盘";
		slh_titles[SLH_DIFFPERCENT_MIN5]	=	"五分钟涨幅%";
		slh_titles[SLH_SELLBUYRATIO]		=	"委比%";
		slh_titles[SLH_SELLBUYDIFF]			=	"委量差";
		slh_titles[SLH_BUYPRICE3]			=	"买价三";
		slh_titles[SLH_BUYPRICE2]			=	"买价二";
		slh_titles[SLH_BUYPRICE1]			=	"买价一";
		slh_titles[SLH_SELLPRICE1]			=	"卖价一";
		slh_titles[SLH_SELLPRICE2]			=	"卖价二";
		slh_titles[SLH_SELLPRICE3]			=	"卖价三";
		slh_titles[SLH_BUYVOLUME3]			=	"买量三";
		slh_titles[SLH_BUYVOLUME2]			=	"买量二";
		slh_titles[SLH_BUYVOLUME1]			=	"买量一";
		slh_titles[SLH_SELLVOLUME1]			=	"卖量一";
		slh_titles[SLH_SELLVOLUME2]			=	"卖量二";
		slh_titles[SLH_SELLVOLUME3]			=	"卖量三";

		//	主要基本指标
		slh_titles[SLH_REPORTTYPE]			=	"报表类型";
		slh_titles[SLH_PE]					=	"市盈率";
		slh_titles[SLH_PNETASSET]			=	"市净率";
		slh_titles[SLH_PMAININCOME]			=	"市销率";
		slh_titles[SLH_RATIO_PCASH]			=	"价格净现金比";
		slh_titles[SLH_RATIO_CURRENCY]		=	"流通率%";
		slh_titles[SLH_RATIO_CHANGEHAND]	=	"换手率%";
		slh_titles[SLH_RATIO_VOLUME]		=	"量比";
		slh_titles[SLH_RS]					=	"相对强度";
		slh_titles[SLH_MARKETVALUE]			=	"总市值-万";
		slh_titles[SLH_MARKETVALUEA]		=	"A股市值-万";
		slh_titles[SLH_MARKETVALUEB]		=	"B股市值-万";

		// ★偿债能力
		slh_titles[SLH_RATIO_LIQUIDITY]		=	"流动比率";
		slh_titles[SLH_RATIO_QUICK]			=	"速动比率";
		slh_titles[SLH_VELOCITY_RECEIVABLES]=	"应收账款周率";

		// ★经营能力
		slh_titles[SLH_VELOCITY_MERCHANDISE]=	"存货周转率";
		slh_titles[SLH_MAIN_INCOME]			=	"主营收入-万";
		slh_titles[SLH_CASH_PS]				=	"每股净现金";

		// ★盈利能力
		slh_titles[SLH_PROFIT_MARGIN]		=	"主营利润率%";
		slh_titles[SLH_NETASSET_YIELD]		=	"净资产收益率%";

		// ★资本结构
		slh_titles[SLH_DATE_BEGIN]			=	"上市日期";
		slh_titles[SLH_SHARE_COUNT_TOTAL]	=	"总股本-万";
		slh_titles[SLH_SHARE_COUNT_A]		=	"A股-万";
		slh_titles[SLH_SHARE_COUNT_B]		=	"B股-万";
		slh_titles[SLH_SHARE_COUNT_H]		=	"H股-万";
		slh_titles[SLH_SHARE_COUNT_NATIONAL]=	"国有股-万";
		slh_titles[SLH_SHARE_COUNT_CORP]	=	"法人股-万";
		slh_titles[SLH_PROFIT_PSUD]			=	"每股未分利润";
		slh_titles[SLH_ASSET]				=	"总资产-万";
		slh_titles[SLH_RATIO_HOLDERRIGHT]	=	"股东权益比%";
		slh_titles[SLH_RATIO_LONGDEBT]		=	"长期负债率%";
		slh_titles[SLH_RATIO_DEBT]			=	"资产负债率%";

		// ★投资收益能力
		slh_titles[SLH_NETASSET_PS]			=	"每股净资产";
		slh_titles[SLH_NETASSET_PS_REGULATE]=	"调整每股净资产";
		slh_titles[SLH_EPS]					=	"每股收益";
		slh_titles[SLH_EPS_DEDUCT]			=	"扣除每股收益";
		slh_titles[SLH_NET_PROFIT]			=	"净利润-万";
		slh_titles[SLH_MAIN_PROFIT]			=	"主营业务利润-万";
		slh_titles[SLH_TOTAL_PROFIT]		=	"利润总额-万";
		slh_titles[SLH_PROFIT_INC]			=	"主营利润增长率%";
		slh_titles[SLH_INCOME_INC]			=	"收入增长率%";
		slh_titles[SLH_ASSET_INC]			=	"总资产增长率%";
		slh_titles[SLH_ANNGAINS_AVERAGE]	=	"年平均收益率%";
		slh_titles[SLH_ANNGAINS_STDDEV]		=	"收益标准差%";
		slh_titles[SLH_BETA]				=	"β值";
		slh_titles[SLH_SHARP]				=	"夏普风险指数";
		slh_titles[SLH_TRADE]				=	"行业";
		slh_titles[SLH_PROVINCE]			=	"省份";
		//slh_titles[SLH_PRODUCTID]="产品ID";
		//slh_titles[SLH_PRODUCTCLASS]="产品类型";
		//slh_titles[SLH_DELIVERYYEAR]="交割年份";
		//slh_titles[SLH_DELIVERYMONTH]="交割月份";
		//slh_titles[SLH_MAXMARKETORDERVOLUME]="市价最大下单量";
		//slh_titles[SLH_MINMARKETORDERVOLUME]="市价最小下单量";
		//slh_titles[SLH_MAXLIMITORDERVOLUME]="市价最大限单量";
		//slh_titles[SLH_MINLIMITORDERVOLUME]="市价最小限单量";
		//slh_titles[SLH_VOLUMEMULTIPLE]="VOLUMEMULTIPLE";
		//slh_titles[SLH_PRICETICK]="PRICETICK";
		//slh_titles[SLH_CREATEDATE]="创建日期";
		//slh_titles[SLH_OPENDATE]="开盘日期";
		//slh_titles[SLH_EXPIREDATE]="过期日期";
		//slh_titles[SLH_STARTDELIVDATE]="开始交割日";
		//slh_titles[SLH_ENDDELIVDATE]="结束交割日";
		//slh_titles[SLH_INSTLIFEPHASE]="生命周期";
		//slh_titles[SLH_ISTRADING]="是否交易";
		//slh_titles[SLH_POSITIONTYPE]="持仓类型";
		//slh_titles[SLH_POSITIONDATETYPE]="持仓日期类型";
		//slh_titles[SLH_LONGMARGINRATIO]="多头保证金率";
		//slh_titles[SLH_SHORTMARGINRATIO]="空头保证金率";
		//slh_titles[SLH_LASTPRICE]="最新价";

#endif
	}

	if( nSLH >= SLH_MIN && nSLH <= SLH_MAX )
	{
		return	slh_titles[nSLH];
	}
	return ("");
}
//////////////////////////////////////////////////////////////////////////////////
// 股票列表列名称字符串
#ifdef	CLKLAN_ENGLISH_US
char	slh_hdrday[]	=	"Day";
char	slh_avrcode[]	=	"AVR";
char	slh_avrname[]	=	"Average";
char	slh_wavrcode[]	=	"WAVR";
char	slh_wavrname[]	=	"Weight-Average";
#else
char	slh_hdrday[]	=	"日";
char	slh_avrcode[]	=	"AVR";
char	slh_avrname[]	=	"平均值";
char	slh_wavrcode[]	=	"WAVR";
char	slh_wavrname[]	=	"加权平均";
#endif
string AfxGetVariantName( UINT nVariantID, bool bWithParameter )
{
	if( nVariantID >= SLH_USERDEFINE_BEGIN )
	{
		return ("");
//		CIndexContainer	& aindex	=	CBaseIndexDlg::GetSListColumnsUser();
//		CIndex	index	=	aindex.GetIndex( nVariantID );
//		return index.m_strName;
	}
	if( nVariantID < SLH_MIN || nVariantID > SLH_MAX )
		return ("");
	
	string	strResult;
	if( strResult.empty() )
		strResult	=	slh_data_array[nVariantID].string;
	if( strResult.empty() )
	{
		//assert( nVariantID == slh_data_array[nVariantID].slh_id );
		if(nVariantID != slh_data_array[nVariantID].slh_id)
		{
			return ("");
		}
		strResult	=	AfxGetSLHTitle( nVariantID );

		int	size	=	sizeof(slh_data_array[nVariantID].string);
#ifdef _MSC_VER
		strncpy_s( slh_data_array[nVariantID].string,(strResult).c_str(), size-1 );
#else
		strcpy( slh_data_array[nVariantID].string,(strResult).c_str());
#endif
		slh_data_array[nVariantID].string[size-1]	=	0;
	}

	if( bWithParameter )
	{
		if( SLH_DIFF == nVariantID || SLH_DIFFPERCENT == nVariantID || SLH_SCOPE == nVariantID
			|| SLH_RATIO_CHANGEHAND == nVariantID || SLH_RATIO_VOLUME == nVariantID
			|| SLH_RS == nVariantID || SLH_ANNGAINS_AVERAGE == nVariantID )
		{
			string	strSuf, strTemp;
			char strBuffer[32];
			strSuf	=	slh_hdrday;
			InstrumentConfig* config=&InstrumentConfig::GetInstance();
			if( SLH_DIFF == nVariantID )
			{
#ifdef _MSC_VER
				sprintf_s(strBuffer,"%s-%d%s",strResult.c_str(),config->GetDiffDays(),strSuf.c_str());
#else
				sprintf(strBuffer,"%s-%d%s",strResult.c_str(),config->GetDiffDays(),strSuf.c_str());
#endif
			}
			else if( SLH_DIFFPERCENT == nVariantID )
			{
#ifdef _MSC_VER
				sprintf_s(strBuffer,"%s-%d%s",strResult.c_str(),config->GetDiffPercentDays(),strSuf.c_str());
#else
				sprintf(strBuffer,"%s-%d%s",strResult.c_str(),config->GetDiffPercentDays(),strSuf.c_str());
#endif
			}
			else if( SLH_SCOPE == nVariantID )
			{
#ifdef _MSC_VER
				sprintf_s(strBuffer,"%s-%d%s",strResult.c_str(),config->GetScopeDays(),strSuf.c_str());
#else
				sprintf(strBuffer,"%s-%d%s",strResult.c_str(),config->GetScopeDays(),strSuf.c_str());
#endif
			}
			else if( SLH_RATIO_CHANGEHAND == nVariantID )
			{
#ifdef _MSC_VER
				sprintf_s(strBuffer,"%s-%d%s",strResult.c_str(),config->GetRatioChangeHandDays(),strSuf.c_str());
#else
				sprintf(strBuffer,"%s-%d%s",strResult.c_str(),config->GetRatioChangeHandDays(),strSuf.c_str());
#endif
			}
			else if( SLH_RATIO_VOLUME == nVariantID )
			{
#ifdef _MSC_VER
				sprintf_s(strBuffer,"%s-%d%s",strResult.c_str(),config->GetRatioVolumeDays(),strSuf.c_str());
#else
				sprintf(strBuffer,"%s-%d%s",strResult.c_str(),config->GetRatioVolumeDays(),strSuf.c_str());
#endif
			}
			else if( SLH_RS == nVariantID )
			{
#ifdef _MSC_VER
				sprintf_s(strBuffer,"%s-%d%s",strResult.c_str(),config->GetRSDays(),strSuf.c_str());
#else
				sprintf(strBuffer,"%s-%d%s",strResult.c_str(),config->GetRSDays(),strSuf.c_str());
#endif
			}
			else if( SLH_ANNGAINS_AVERAGE == nVariantID )
			{
#ifdef _MSC_VER
				sprintf_s(strBuffer,"%s-%d%s",strResult.c_str(),config->GetYieldAverageDays(),strSuf.c_str());
#else
				sprintf(strBuffer,"%s-%d%s",strResult.c_str(),config->GetYieldAverageDays(),strSuf.c_str());
#endif
			}
			else
			{

			}
			strResult=strBuffer;
		}
	}
	return strResult;
}

bool AfxGetVariantNameArray( vector<string> & astr, bool bWithParameter )
{
	astr.resize(SLH_MAX+SLH_MIN+1);
	for( int nVariantID = min<int>(0,SLH_MIN); nVariantID <= SLH_MAX; nVariantID ++ )
	{
		if( strlen(slh_data_array[nVariantID].varname) > 0 )
		{
			string	strTemp	=	AfxGetVariantName( nVariantID, bWithParameter );
			astr.push_back( strTemp );
		}
		else
		{
			astr.push_back( "");
		}
	}
	return true;
}

UINT AfxGetVariantID( string strName, bool bWithParameter )
{
	for( int nVariantID = SLH_MIN; nVariantID <= SLH_MAX; nVariantID ++ )
	{
		string	strTemp	=	AfxGetVariantName( nVariantID, bWithParameter );
		if( 0 == strcmp(strTemp.c_str(), strName.c_str() ) )
			return nVariantID;
	}

	CIndexContainer	& aindex	=	CIndexContainer::GetSListColumnsUser();
	CIndex	index	=	aindex.GetIndex( strName );
	if( !index.IsInvalidID() )
		return index.m_nID;

	return SLH_INVALID;
}

std::string AfxGetVariantVarName( int nVariantID )
{
	if( nVariantID >= SLH_MIN && nVariantID <= SLH_MAX )
		return string(slh_data_array[nVariantID].varname);
	else
		return ("");
}

std::string AfxGetVariantVarName( string strName, bool bWithParameter )
{
	UINT nVariantID = AfxGetVariantID( strName, bWithParameter );
	return AfxGetVariantVarName( nVariantID );
}

/* 计算股票列表视图中某一列的值，其数值单位与股票列表视图中相同 */
bool AfxGetVariantValue(uint32_t nVariantID, InstrumentInfo &info, double * pValue,
						InstrumentContainer *pContainer )
{
	if( NULL == pValue )
	{
		return false;
	}
	if( nVariantID >= SLH_MIN && nVariantID <= SLH_MAX )
	{
		switch( nVariantID )
		{
		case	SLH_PRECLOSE:					//	昨收
			*pValue	=	info.PreClosePrice;
			return ( info.PreClosePrice > 1e-4 );
		case	SLH_OPEN:						//	今开
			*pValue	=	info.OpenPrice;
			return ( info.OpenPrice > 1e-4 );
		case	SLH_CLOSE:						//	收盘价
			*pValue	=	info.ClosePrice;
			return ( info.ClosePrice > 1e-4 );
		case	SLH_HIGH:						//	最高价
			*pValue	=	info.HighestPrice;
			return ( info.HighestPrice > 1e-4 );
		case	SLH_LOW:						//	最低价
			*pValue	=	info.LowestPrice;
			return ( info.LowestPrice > 1e-4 );
		case	SLH_AVERAGE:					//	均价
			return info.GetAverage( pValue );
		case	SLH_DIFF:						//	涨跌
			return info.GetDiff( pValue, info.m_datetech, 1/*DockPaneXmlParser::GetInstance()->GetDiffDays()*/ );
		case	SLH_DIFFPERCENT:				//	涨幅
			return info.GetDiffPercent( pValue, info.m_datetech, 1/*DockPaneXmlParser::GetInstance()->GetDiffPercentDays()*/ );
		case	SLH_SCOPE:						//	震幅
			return info.GetScope( pValue, info.m_datetech, 1/*DockPaneXmlParser::GetInstance()->GetScopeDays()*/ );
		case	SLH_VOLUME:						//	成交量
			*pValue	=	info.Volume*0.01;
			return ( info.Volume > 1e-4 );
		case	SLH_TURNOVER:						//	成交额
			*pValue	=	info.Turnover*0.001;
			return ( info.Turnover > 1e-4 );
		case	SLH_VOLUP:						//	外盘
			if( info.m_minute->StatVolumeInfo( NULL, pValue, NULL ) )
			{
				*pValue	=	(*pValue) * 0.01;
				return true;
			}
			break;
		case	SLH_VOLDOWN:					//	内盘
			if( info.m_minute->StatVolumeInfo( NULL, NULL, pValue ) )
			{
				*pValue	=	(*pValue) * 0.01;
				return true;
			}
			break;
 		case	SLH_DIFFPERCENT_MIN5:			//	五分钟涨幅%
 			return info.GetDiffPercentMin5( pValue );
		case	SLH_SELLBUYRATIO:				//	委比%
			return info.GetSellBuyRatio( pValue, NULL );
		case	SLH_SELLBUYDIFF:				//	委量差
			if( info.GetSellBuyRatio( NULL, pValue ) )
			{
				*pValue	=	(*pValue) * 0.01;
				return true;
			}
			break;
		case	SLH_BUYPRICE5:					//	买价4
			*pValue = info.BidPrice[5];
			return (info.BidPrice[5] > 1e-4);
		case	SLH_BUYPRICE4:					//	买价4
			*pValue = info.BidPrice[3];
			return (info.BidPrice[3] > 1e-4);
		case	SLH_BUYPRICE3:					//	买价三
			*pValue	=	info.BidPrice[2];
			return ( info.BidPrice[2] > 1e-4 );
		case	SLH_BUYPRICE2:					//	买价二
			*pValue	=	info.BidPrice[1];
			return ( info.BidPrice[1] > 1e-4 );
		case	SLH_BUYPRICE1:					//	买价一
			*pValue	=	info.BidPrice[0];
			return ( info.BidPrice[0] > 1e-4 );
		case	SLH_SELLPRICE1:					//	卖价一
			*pValue	=	info.AskPrice[0];
			return ( info.AskPrice[0] > 1e-4 );
		case	SLH_SELLPRICE2:					//	卖价二
			*pValue	=	info.AskPrice[1];
			return ( info.AskPrice[1] > 1e-4 );
		case	SLH_SELLPRICE3:					//	卖价三
			*pValue	=	info.AskPrice[2];
			return ( info.AskPrice[2] > 1e-4 );
		case	SLH_SELLPRICE4:					//	卖价4
			*pValue = info.AskPrice[3];
			return (info.AskPrice[3] > 1e-4);
		case	SLH_SELLPRICE5:					//	卖价5
			*pValue = info.AskPrice[4];
			return (info.AskPrice[4] > 1e-4);
		case	SLH_BUYVOLUME5:					//	买量三
			*pValue = info.BidVolume[4] * 0.01;
			return (info.BidVolume[4] > 1e-4);
		case	SLH_BUYVOLUME4:					//	买量三
			*pValue = info.BidVolume[3] * 0.01;
			return (info.BidVolume[3] > 1e-4);
		case	SLH_BUYVOLUME3:					//	买量三
			*pValue	=	info.BidVolume[2] * 0.01;
			return ( info.BidVolume[2] > 1e-4 );
		case	SLH_BUYVOLUME2:					//	买量二
			*pValue	=	info.BidVolume[1] * 0.01;
			return ( info.BidVolume[1] > 1e-4 );
		case	SLH_BUYVOLUME1:					//	买量一
			*pValue	=	info.BidVolume[0] * 0.01;
			return ( info.BidVolume[0] > 1e-4 );
		case	SLH_SELLVOLUME1:				//	卖量一
			*pValue	=	info.AskVolume[0] * 0.01;
			return ( info.AskVolume[0] > 1e-4 );
		case	SLH_SELLVOLUME2:				//	卖量二
			*pValue	=	info.AskVolume[1] * 0.01;
			return ( info.AskVolume[1] > 1e-4 );
		case	SLH_SELLVOLUME3:				//	卖量三
			*pValue	=	info.AskVolume[2] * 0.01;
			return ( info.AskVolume[2] > 1e-4 );
		case	SLH_SELLVOLUME4:				//	卖量4
			*pValue = info.AskVolume[3] * 0.01;
			return (info.AskVolume[3] > 1e-4);
		case	SLH_SELLVOLUME5:				//	卖量5
			*pValue = info.AskVolume[4] * 0.01;
			return (info.AskVolume[4] > 1e-4);
			//	主要基本指标
// 		case	SLH_PE:							//	市盈率
// 			return info.GetPE( pValue );
// 		case	SLH_PNETASSET:					//	市净率
// 			return info.GetPNetAsset( pValue );
// 		case	SLH_PMAININCOME:				//	市销率
// 			return info.GetPMainIncome( pValue );
// 		case	SLH_RATIO_PCASH:				//	价格净现金比
// 			return info.GetRatioPCash( pValue );
// 		case	SLH_RATIO_CURRENCY:				//	流通率
// 			return info.GetRatioCurrency( pValue );
		//case	SLH_RATIO_CHANGEHAND:			//	换手率
		//	return info.GetRatioChangeHand( pValue, info.m_datetech,DockPaneXmlParser::GetInstance()->GetRatioChangeHandDays() );
		//case	SLH_RATIO_VOLUME:				//	量比
		//	return info.GetRatioVolume( pValue, info.m_datetech, DockPaneXmlParser::GetInstance()->GetRatioVolumeDays() );
		//case	SLH_RS:							//	相对强度
		//	return info.GetRS( pValue, info.m_datetech, DockPaneXmlParser::GetInstance()->GetRSDays() );
// 		case	SLH_MARKETVALUE:				//	总市值
// 			if( info.GetMarketValue( pValue ) )
// 			{
// 				*pValue	=	(*pValue) * 0.0001;
// 				return TRUE;
// 			}
// 			break;
// 		case	SLH_MARKETVALUEA:				//	A股市值
// 			if( info.GetMarketValueA( pValue ) )
// 			{
// 				*pValue	=	(*pValue) * 0.0001;
// 				return TRUE;
// 			}
// 			break;
// 		case	SLH_MARKETVALUEB:				//	B股市值
// 			if( info.GetMarketValueB( pValue ) )
// 			{
// 				*pValue	=	(*pValue) * 0.0001;
// 				return TRUE;
// 			}
// 			break;
// 			
// 			// ★偿债能力
// 		case	SLH_RATIO_LIQUIDITY:			//	流动比率
// 			*pValue	=	info.m_fRatio_liquidity;
// 			return ( fabs(info.m_fRatio_liquidity) > 1e-4 );
// 		case	SLH_RATIO_QUICK:				//	速动比率
// 			*pValue	=	info.m_fRatio_quick;
// 			return ( fabs(info.m_fRatio_quick) > 1e-4 );
// 		case	SLH_VELOCITY_RECEIVABLES:		//	应收帐款周率
// 			*pValue	=	info.m_fVelocity_receivables;
// 			return ( fabs(info.m_fVelocity_receivables) > 1e-4 );
// 
// 			// ★经营能力
// 		case	SLH_VELOCITY_MERCHANDISE:		// 存货周转率
// 			*pValue	=	info.m_fVelocity_merchandise;
// 			return ( fabs(info.m_fVelocity_merchandise) > 1e-4 );
// 		case	SLH_MAIN_INCOME:				// 主营业务收入
// 			*pValue	=	factor * info.m_fMain_income * 0.0001;
// 			return ( fabs(info.m_fMain_income) > 1e-4 );
// 		case	SLH_CASH_PS:					// 每股净现金流量
// 			*pValue	=	factor * info.m_fCash_ps;
// 			return ( fabs(info.m_fCash_ps) > 1e-4 );
// 
// 			// ★盈利能力
// 		case	SLH_PROFIT_MARGIN:				// 主营业务利润率
// 			*pValue	=	info.m_fProfit_margin;
// 			return ( fabs(info.m_fProfit_margin) > 1e-4 );
// 		case	SLH_NETASSET_YIELD:				// 净资产收益率
// 			*pValue	=	factor * info.m_fNetasset_yield;
// 			return ( fabs(info.m_fNetasset_yield) > 1e-4 );
// 			
// 			// ★资本结构
// 		case	SLH_SHARE_COUNT_TOTAL:			//	总股本
// 			*pValue	=	info.m_fShare_count_total * 0.0001;
// 			return ( info.m_fShare_count_total > 1e-4 );
// 		case	SLH_SHARE_COUNT_A:				//	流通A股
// 			*pValue	=	info.m_fShare_count_a * 0.0001;
// 			return TRUE;
// 		case	SLH_SHARE_COUNT_B:				//	流通B股
// 			*pValue	=	info.m_fShare_count_b * 0.0001;
// 			return TRUE;
// 		case	SLH_SHARE_COUNT_H:				//	流通H股
// 			*pValue	=	info.m_fShare_count_h * 0.0001;
// 			return TRUE;
// 		case	SLH_SHARE_COUNT_NATIONAL:		//	国有股
// 			*pValue	=	info.m_fShare_count_national * 0.0001;
// 			return TRUE;
// 		case	SLH_SHARE_COUNT_CORP:			//	法人股
// 			*pValue	=	info.m_fShare_count_corp * 0.0001;
// 			return TRUE;
// 		case	SLH_PROFIT_PSUD:				// 每股未分配利润
// 			*pValue	=	info.m_fProfit_psud;
// 			return TRUE;
// 		case	SLH_ASSET:						// 总资产
// 			*pValue	=	info.m_fAsset * 0.0001;
// 			return ( fabs(info.m_fAsset) > 1e-4 );
// 		case	SLH_RATIO_HOLDERRIGHT:			// 股东权益比率
// 			*pValue	=	info.m_fRatio_holderright;
// 			return ( fabs(info.m_fRatio_holderright) > 1e-4 );
// 		case	SLH_RATIO_LONGDEBT:				// 长期负债率
// 			*pValue	=	info.m_fRatio_longdebt;
// 			return TRUE;
// 		case	SLH_RATIO_DEBT:					// 资产负债率
// 			*pValue	=	info.m_fRatio_debt;
// 			return ( fabs(info.m_fRatio_debt) > 1e-4 );
// 
// 			// ★投资收益能力
// 		case	SLH_NETASSET_PS:				// 每股净资产
// 			*pValue	=	info.m_fNetasset_ps;
// 			return ( fabs(info.m_fNetasset_ps) > 1e-4 );
// 		case	SLH_NETASSET_PS_REGULATE:		// 调整每股净资产
// 			*pValue	=	info.m_fNetasset_ps_regulate;
// 			return ( fabs(info.m_fNetasset_ps_regulate) > 1e-4 );
// 		case	SLH_EPS:						// 每股收益
// 			*pValue	=	factor * info.m_fEps;
// 			return ( fabs(info.m_fEps) > 1e-4 );
// 		case	SLH_EPS_DEDUCT:					// 扣除后每股收益
// 			*pValue	=	factor * info.m_fEps_deduct;
// 			return ( fabs(info.m_fEps_deduct) > 1e-4 );
// 		case	SLH_NET_PROFIT:					// 净利润
// 			*pValue	=	factor * info.m_fNet_profit * 0.0001;
// 			return ( fabs(info.m_fNet_profit) > 1e-4 );
// 		case	SLH_MAIN_PROFIT:				// 主营业务利润
// 			*pValue	=	factor * info.m_fMain_profit * 0.0001;
// 			return ( fabs(info.m_fMain_profit) > 1e-4 );
// 		case	SLH_TOTAL_PROFIT:				// 利润总额
// 			*pValue	=	factor * info.m_fTotal_profit * 0.0001;
// 			return ( fabs(info.m_fTotal_profit) > 1e-4 );
// 		case	SLH_PROFIT_INC:					// 主营利润增长率
// 			*pValue	=	info.m_fProfit_inc;
// 			return TRUE;
// 		case	SLH_INCOME_INC:					// 主营收入增长率
// 			*pValue	=	info.m_fIncome_inc;
// 			return TRUE;
// 		case	SLH_ASSET_INC:					// 总资产增长率
// 			*pValue	=	info.m_fAsset_inc;
// 			return TRUE;
// 		case	SLH_ANNGAINS_AVERAGE:			// 年平均收益率
// 			*pValue	=	info.m_fYield_average;
// 			return ( fabs(STKLIB_DATA_INVALID - info.m_fYield_average) > 1e-4 );
// 		case	SLH_ANNGAINS_STDDEV:			// 收益标准差
// 			*pValue	=	info.m_fYield_stddev;
// 			return ( fabs(STKLIB_DATA_INVALID - info.m_fYield_stddev) > 1e-4 );
// 		case	SLH_BETA:						// β值
// 			*pValue	=	info.m_fBeite;
// 			return ( fabs(STKLIB_DATA_INVALID - info.m_fBeite) > 1e-4 );
// 		case	SLH_SHARP:						// 夏普风险指数
// 			return info.GetXiapu( pValue );
//case SLH_PRODUCTID:
//	return 0;
//case SLH_PRODUCTCLASS:
//	return 0;
		default:	;
		}
	}
	else if( nVariantID >= SLH_USERDEFINE_BEGIN )
	{
		bool	bNoValue	=	false;
		if( pContainer && pContainer->GetVariantSaveValue( pValue, nVariantID, info, &bNoValue ) )
			return !bNoValue;

		CIndexContainer	& container = CIndexContainer::GetSListColumnsUser();
		CIndex	& index	=	container.GetIndex( nVariantID );
		bool bSuccess = index.GetExpressResult( pValue, info, &bNoValue, NULL );
		if( pContainer && pValue )
			pContainer->SetVariantSaveValue( *pValue, nVariantID, info, bNoValue );
		
		return bSuccess;
	}
	return false;
}



int	dwordcmp( DWORD dw1, DWORD dw2 )
{
	if( dw1 > dw2 )
		return 1;
	else if( dw1 < dw2 )
		return -1;
	else
		return 0;
}

int	longcmp( long l1, long l2 )
{
	if( l1 > l2 )
		return 1;
	else if( l1 < l2 )
		return -1;
	else
		return 0;
}

int doublecmp( double d1, double d2 )
{
	if( d1 > d2 )
		return 1;
	else if( d1 < d2 )
		return -1;
	else
		return 0;
}

int AfxCompareVariantValue( int nVariantID, InstrumentInfo & info1, InstrumentInfo &info2,
						    InstrumentContainer * pContainer )
{
//	long	x1 = 0, x2 = 0;
	double	d1 = 0., d2 = 0.;
	bool	bGet1 = false, bGet2 = false;

	switch( nVariantID )
	{
	case	SLH_CODE:						//	股票代码
		return strcmp( info1.get_id(), info2.get_id() );
	case	SLH_NAME:						//	股票名称
		return strcmp( info1.get_name(), info2.get_name() );
		//	技术数据
// 	case	SLH_DATE:						//	日期
// 		return dwordcmp( info1.m_datetech, info2.m_datetech );
// 	case	SLH_DATE_BEGIN:					//	上市日期
// 		return dwordcmp( info1.m_datebegin, info2.m_datebegin );
// 		//	主要基本指标
// 	case	SLH_REPORTTYPE:					// 报表类型
// 		return dwordcmp( info1.m_reporttype, info2.m_reporttype );
	case	SLH_PE:							//	市盈率
	case	SLH_PNETASSET:					//	市净率
	case	SLH_PMAININCOME:				//	市销率
	case	SLH_RATIO_PCASH:				//	价格净现金比
		bGet2	=	AfxGetVariantValue( nVariantID, info2, &d2, pContainer );
		bGet1	=	AfxGetVariantValue( nVariantID, info1, &d1, pContainer );
		if( !bGet2 && !bGet1 )
			return 0;
		if( !bGet2 )	return 1;
		if( !bGet1 )	return -1;
		if( d1 < 0 && d2 < 0 )
			return doublecmp( d1, d2 );
		if( d1 < 0 )			return 1;
		if( d2 < 0 )			return -1;
		return doublecmp( d1, d2 );
// 	case	SLH_TRADE:						// 行业
// 		return strcmp( info1.GetStockDomain(), info2.GetStockDomain() );
// 	case	SLH_PROVINCE:					// 省份
// 		return strcmp( info1.GetStockProvince(), info2.GetStockProvince() );
	default:
		bGet2	=	AfxGetVariantValue( nVariantID, info2, &d2, pContainer );
		bGet1	=	AfxGetVariantValue( nVariantID, info1, &d1, pContainer );
		if( !bGet2 && !bGet1 )
			return 0;
		if( !bGet2 )	return 1;
		if( !bGet1 )	return -1;
		return doublecmp( d1, d2 );
	}

	return 0;
}

std::string AfxGetVariantDispString(uint32_t nVariantID, InstrumentInfo &info,
								InstrumentContainer * pContainer )
{
	string	strTemp;
//	int		x	=	0;
	double	dTemp	=	0;
	char tmp[64];

	switch( nVariantID )
	{
	case	SLH_CODE:						
		strTemp	=	info.get_id();
		break;
	case	SLH_NAME:						
		strTemp	=	info.get_name();
		break;
	case SLH_EXCHANGEID:
		strTemp = info.szExchange;
		break;
	//	日期类型的数据处理;
	case	SLH_DATE:						//	日期;
		strTemp=(info.CreateDate);
		break;
 	case	SLH_DATE_BEGIN:					//	上市日期;
		if( 0 != info.OpenDate )
			strTemp=(info.OpenDate);
		break;
	//期货字段;
	case SLH_PRODUCTID:
		if (NULL != info.ProductID)
			strTemp = (info.ProductID);
 		break;
	case SLH_PRODUCTCLASS:
		strTemp=((char)info.ProductClass);
		break;
		//时间字段
	case SLH_DELIVERYYEAR:
		{
			sprintf_s(tmp,"%4d",info.DeliveryYear);
			strTemp=tmp;
		}
		break;
	case SLH_DELIVERYMONTH:
		{
			sprintf_s(tmp,"%4d",info.DeliveryMonth);
			strTemp=tmp;
		}
		break;
	case SLH_CREATEDATE:
		strTemp=(info.CreateDate);
		break;
	case SLH_OPENDATE:
		strTemp=(info.OpenDate);
		break;
	case SLH_EXPIREDATE:
		strTemp=(info.ExpireDate);
		break;
	case SLH_STARTDELIVDATE:
		strTemp=(info.StartDelivDate);
		break;
	case SLH_ENDDELIVDATE:
		strTemp=(info.EndDelivDate);
		break;
	case SLH_MAXMARKETORDERVOLUME:
		{
			sprintf_s(tmp,"%d",info.MaxMarketOrderVolume);
			strTemp=tmp;
		}
		break;
	case SLH_MINMARKETORDERVOLUME:
		{
			sprintf_s(tmp,"%d",info.MinMarketOrderVolume);
			strTemp=tmp;
		}
		break;
	case SLH_MAXLIMITORDERVOLUME:
		{
			sprintf_s(tmp,"%d",info.MaxLimitOrderVolume);
			strTemp=tmp;
		}
		break;
	case SLH_MINLIMITORDERVOLUME:
		{
			sprintf_s(tmp,"%d",info.MinLimitOrderVolume);
			strTemp=tmp;
		}
		break;
	case SLH_VOLUMEMULTIPLE:
		{
			sprintf_s(tmp,"%d",info.VolumeMultiple);
			strTemp=tmp;
		}
		break;
	case SLH_INSTLIFEPHASE:
		{
			sprintf_s(tmp,"%d",info.InstLifePhase);
			strTemp=tmp;
		}
		break;
	case SLH_ISTRADING:
		strTemp=info.IsTrading?("是"):("否");
		break;
	case SLH_POSITIONTYPE:
		{
			sprintf_s(tmp,"%d",info.PositionType);
			strTemp=tmp;
		}
		break;
	case SLH_POSITIONDATETYPE:
		{
			sprintf_s(tmp,"%d",info.PositionDateType);
			strTemp=tmp;
		}
		break;
	case SLH_PRICETICK:
		{
			char strPriceFmt[32];
			if (info.PriceTick==DBL_MAX)
			{
				sprintf_s(strPriceFmt,"%%.%df", info.GetAccuracy());
				sprintf_s(tmp,strPriceFmt, 0.0f);
			}
			else
			{
				sprintf_s(strPriceFmt,"%%.%df", info.GetAccuracy());
				sprintf_s(tmp,strPriceFmt, info.PriceTick);
			}
		}
		break;

	case SLH_LONGMARGINRATIO:
		{
			char strPriceFmt[32];
			if (info.LongMarginRatio==DBL_MAX)
			{
				sprintf_s(strPriceFmt,"%%.%df", info.GetAccuracy());
				sprintf_s(tmp,strPriceFmt, 0.0f);
			}
			else
			{
			sprintf_s(strPriceFmt,"%%.%df", info.GetAccuracy());
			sprintf_s(tmp,strPriceFmt, info.LongMarginRatio);	
			}
		}
		break;
	case SLH_SHORTMARGINRATIO:
		{
			char strPriceFmt[32];
			if (info.ShortMarginRatio==DBL_MAX)
			{
				sprintf_s(strPriceFmt,"%%.%df", info.GetAccuracy());
				sprintf_s(tmp,strPriceFmt, 0.0f);
			}
			else
			{
			sprintf_s(strPriceFmt,"%%.%df", info.GetAccuracy());
			sprintf_s(tmp,strPriceFmt, info.ShortMarginRatio);	
			}
		}
		break;
	case SLH_LASTPRICE:
		{
			char strPriceFmt[32];
			if (info.LastPrice==DBL_MAX)
			{
				sprintf_s(strPriceFmt,"%%.%df", info.GetAccuracy());
				sprintf_s(tmp,strPriceFmt, 0.0f);
			}
			else
			{
			sprintf_s(strPriceFmt,"%%.%df", info.GetAccuracy());
			sprintf_s(tmp,strPriceFmt, info.LastPrice);	
			}
		}
		break;
		//浮点数类型的数据处理
	case	SLH_PRECLOSE:					//	昨收
	case	SLH_OPEN:						//	今开
	case	SLH_CLOSE:						//	收盘价
	case	SLH_HIGH:						//	最高价
	case	SLH_LOW:						//	最低价
	case	SLH_AVERAGE:					//	均价
	case	SLH_DIFF:						//	涨跌
	case	SLH_BUYPRICE5:					//	买价5
	case	SLH_BUYPRICE4:					//	买价4
	case	SLH_BUYPRICE3:					//	买价三
	case	SLH_BUYPRICE2:					//	买价二
	case	SLH_BUYPRICE1:					//	买价一
	case	SLH_SELLPRICE1:					//	卖价一
	case	SLH_SELLPRICE2:					//	卖价二
	case	SLH_SELLPRICE3:					//	卖价三
	case	SLH_SELLPRICE4:					//	卖价三
	case	SLH_SELLPRICE5:					//	卖价三
		if( AfxGetVariantValue( nVariantID, info, &dTemp, pContainer ) )
		{
			char strPriceFmt[16];
#ifdef _MSC_VER
			sprintf_s(strPriceFmt,"%%.%df", info.GetAccuracy());
			sprintf_s(tmp,strPriceFmt, dTemp);
#else
			sprintf(strPriceFmt,"%%.%df", info.GetAccuracy());
			sprintf(tmp,strPriceFmt, dTemp);
#endif
			strTemp=tmp;
		}
		break;
	case	SLH_VOLUME:						//	成交量
	case	SLH_TURNOVER:					//	成交额
	case	SLH_VOLUP:						//	外盘
	case	SLH_VOLDOWN:					//	内盘
	case	SLH_SELLBUYDIFF:				//	委量差
	case	SLH_BUYVOLUME3:					//	买量三
	case	SLH_BUYVOLUME2:					//	买量二
	case	SLH_BUYVOLUME1:					//	买量一
	case	SLH_SELLVOLUME1:				//	卖量一
	case	SLH_SELLVOLUME2:				//	卖量二
	case	SLH_SELLVOLUME3:				//	卖量三
	case	SLH_MARKETVALUE:				//	总市值
	case	SLH_MARKETVALUEA:				//	A股市值
	case	SLH_MARKETVALUEB:				//	B股市值
	case	SLH_SHARE_COUNT_TOTAL:			//	总股本
	case	SLH_SHARE_COUNT_A:				//	流通A股
	case	SLH_SHARE_COUNT_B:				//	流通B股
	case	SLH_SHARE_COUNT_H:				//	流通H股
	case	SLH_SHARE_COUNT_NATIONAL:		//	国有股
	case	SLH_SHARE_COUNT_CORP:			//	法人股
		if( AfxGetVariantValue( nVariantID, info, &dTemp, pContainer ) )
		{
#ifdef _MSC_VER
			sprintf_s(tmp,"%.0f",dTemp);
#else
			sprintf(tmp,"%.0f",dTemp);
#endif
			strTemp=tmp;
			//strTemp.Format( _T("%.0f"), dTemp );
		}
		break;

	case	SLH_MAIN_INCOME:				// 主营业务收入
	case	SLH_ASSET:						// 总资产
	case	SLH_NET_PROFIT:					// 净利润
	case	SLH_MAIN_PROFIT:				// 主营业务利润
	case	SLH_TOTAL_PROFIT:				// 利润总额
		if( AfxGetVariantValue( nVariantID, info, &dTemp, pContainer ) )
		{	
#ifdef _MSC_VER
			sprintf_s(tmp,"%.0f",dTemp);
#else
			sprintf(tmp,"%.0f",dTemp);
#endif
			strTemp=tmp;
		}
		break;
	default:
		if( AfxGetVariantValue( nVariantID, info, &dTemp, pContainer ) )
		{	
#ifdef _MSC_VER
			sprintf_s(tmp,"%.2f",dTemp);
#else
			sprintf(tmp,"%.2f",dTemp);
#endif

			strTemp=tmp;
		}
	}
	if( strTemp.empty() )
		strTemp	=	"-";
	return strTemp;
}



//////////////////////////////////////////////////////////////////////
// CExpress

CExpress::CExpress( )
{
	m_bIsModified	=	false;
	m_nReserved		=	0;
}

CExpress::CExpress( const CExpress &src )
{
	*this	=	src;
}

CExpress::~CExpress( )
{
}

CExpress &CExpress::operator = ( const CExpress &src )
{
	m_strExpressOrg		=	src.m_strExpressOrg;
	m_strExpress		=	src.m_strExpress;
	m_bIsModified		=	src.m_bIsModified;
	m_nReserved			=	src.m_nReserved;
	return *this;
}

// void CExpress::Serialize( CArchive &ar )
// {
// 	if( ar.IsStoring() )
// 	{
// 		ar	<<	m_strExpressOrg;
// 		ar	<<	m_nReserved;
// 	}
// 	else
// 	{
// 		ar	>>	m_strExpressOrg;
// 		ar	>>	m_nReserved;
// 
// 		m_bIsModified	=	TRUE;
// 		m_strExpress	=	m_strExpressOrg;
// 	}
// }

bool CExpress::SetExpressString( string strExpressOrg )
{
	m_strExpressOrg	=	strExpressOrg;

	m_bIsModified	=	true;
	m_strExpress	=	m_strExpressOrg;
	return true;
}

string CExpress::GetExpressString( )
{
	return m_strExpressOrg;
}

string CExpress::GetLastErrorMsg( )
{
// 	if( m_parser.IsError() )
// 	{
// 		int nErrorCode = m_parser.GetErrorCode( );
// 		switch( nErrorCode )
// 		{
// 		case MP_ErrParserStack:	return CString(express_errparserstack);	break;
// 		case MP_ErrBadRange:	return CString(express_errbadrange);		break;
// 		case MP_ErrExpression:	return CString(express_errexpression);	break;
// 		case MP_ErrOperator:	return CString(express_erroperator);		break;
// 		case MP_ErrOpenParen:	return CString(express_erropenparen);	break;
// 		case MP_ErrOpCloseParen:return CString(express_errcloseparen);	break;
// 		case MP_ErrInvalidNum:	return CString(express_errinvalidnum);	break;
// 		case MP_ErrMath:		return CString(express_errmath);			break;
// 		default:				return CString(express_errunknown);
// 		}
// 	}
	return ("");
}

bool CExpress::GetExpressResult( double * pResult, bool * pError )
{
	if( !PreCompile( ) )
	{
		if( pError )	*pError	=	true;
		return false;
	}

	if( !InitParser( ) )
	{
		if( pError )	*pError	=	true;
		return false;
	}

	//m_parser.Parse( );
// 	if( pError )
// 		*pError	=	m_parser.IsError( );
// 	if( m_parser.IsError() )
// 		return FALSE;
// 
// 	if( pResult )
// 		*pResult	=	m_parser.GetResult( );
	return true;
}

// 去掉空格、冒号、回车、换行等字符
bool CExpress::PreCompile( )
{
	if( m_bIsModified )
	{
		string	sTemp = m_strExpress;
		m_strExpress.clear();
		for( size_t i=0; i<sTemp.length(); i++ )
		{
			char ch = sTemp[i];
			if( ':' != ch && ' ' != ch && '\t' != ch 
				&& '\r' != ch && '\n' != ch )
				m_strExpress	+=	ch;
		}

		m_bIsModified	=	false;
	}
	return m_strExpress.length() > 0;
}

// 初始化m_parser变量
bool CExpress::InitParser( )
{
// 	m_parser.SetParserString( m_strExpress );
// 	m_parser.InitVarMap( 10 );
/* examples
	for( int i=0; i<nCount; i++ )
	{
		m_parser.AddVar( dValue, strName );
	}
*/
	return true;
}


/////////////////////////////////////////////////////////////////////////////
// CIndex

CIndex::CIndex( )
{
	m_nID	=	SLH_INVALID;
}

CIndex::CIndex( const CIndex &src )
{
	*this	=	src;
}

CIndex::~CIndex( )
{
}

CIndex &CIndex::operator = ( const CIndex &src )
{
	*((CExpress *)this)	=	*((CExpress *)&src);
	m_nID			=	src.m_nID;
	m_strName		=	src.m_strName;
	m_strDescript	=	src.m_strDescript;
	m_strReserved	=	src.m_strReserved;

	m_adwVariant.assign( src.m_adwVariant.begin(),src.m_adwVariant.end());
	return *this;
}

// void CIndex::Serialize( CArchive & ar )
// {
// 	CExpress::Serialize( ar );
// 
// 	if( ar.IsStoring() )
// 	{
// 		ar	<<	m_nID;
// 		ar	<<	m_strName;
// 		ar	<<	m_strDescript;
// 		ar	<<	m_strReserved;
// 	}
// 	else
// 	{
// 		ar	>>	m_nID;
// 		ar	>>	m_strName;
// 		ar	>>	m_strDescript;
// 		ar	>>	m_strReserved;
// 	}
// }

bool CIndex::IsInvalidID( )
{
	return SLH_INVALID == m_nID;
}

bool CIndex::SetNextID( )
{
	CIndexContainer &container = CIndexContainer::GetSListColumnsUser();
	m_nID	=	container.GetNextID( );
	return true;
}
vector<string>* m_pSortSPStringArray=NULL;
int SortSPString(const void *p1,const void *p2)
{
	if( NULL == p1 || NULL == p2 )
		return 0;

	DWORD dw1 = *(DWORD *)p1;
	DWORD dw2 = *(DWORD *)p2;

	if( NULL == m_pSortSPStringArray
		|| dw1 < 0 || dw1 >= (DWORD)m_pSortSPStringArray->size()
		|| dw2 < 0 || dw2 >= (DWORD)m_pSortSPStringArray->size() )
	{
		//assert(FALSE);
		return 0;
	}

	return m_pSortSPStringArray->at(dw1)==
				m_pSortSPStringArray->at(dw2) ;
}
FACILITY_API bool GetSortIDArray(vector<string>* pStringArray,vector<DWORD>& adwSortID)
{
	if (NULL==pStringArray)
	{
		return false;
	}
	adwSortID.resize( 0/*, pStringArray->GetSize() + 1 */);

// 	for( INT_PTR i=0; i<pStringArray->size(); i++ )
// 		adwSortID.Add( i );
// 	m_pSortSPStringArray=pStringArray;
// 	if( adwSortID.GetData() )
// 		qsort( adwSortID.GetData(), adwSortID.GetSize(), sizeof(DWORD), SortSPString );
// 	m_pSortSPStringArray = NULL;

	return true;
}

bool CIndex::PreCompile( )
{
	if( m_bIsModified )
	{
		// 缓存变量ID
		m_adwVariant.clear();

		// 将中文变量名替换成英文名
		vector<string> astrVarName;
		if( AfxGetVariantNameArray(astrVarName,false) )
		{
			vector<DWORD> adwSortID;
			GetSortIDArray(&astrVarName, adwSortID );
			for( int i=adwSortID.size()-1; i>=0; i-- )
			{
// 				UINT nVariantID = adwSortID.GetAt(i);
// 				if( strlen(slh_data_array[nVariantID].varname) > 0 )
// 				{
// 					if( m_strExpress.Replace( AfxGetVariantName(nVariantID,FALSE), CString(slh_data_array[nVariantID].varname) ) > 0 )
// 						m_adwVariant.Add(nVariantID);
// 				}
			}
		}

		if( !CExpress::PreCompile( ) )
			return false;

		m_bIsModified	=	false;
	}
	return m_strExpress.length() > 0;
}

// 初始化m_parser变量
bool CIndex::InitParser( InstrumentInfo &info, bool *pVariantNoValue )
{
	if( pVariantNoValue )
		*pVariantNoValue	=	false;

// 	m_parser.SetParserString( m_strExpress );
// 	m_parser.InitVarMap( SLH_MAX );
// 
// 	for( int i=0; i<m_adwVariant.GetSize(); i++ )
// 	{
// 		UINT nVariantID = m_adwVariant.ElementAt(i);
// 
// 		// 加入变量及其值
// 		double	value	=	1.0;
// 		if( !AfxGetVariantValue( nVariantID, info, &value, NULL ) && pVariantNoValue )
// 			*pVariantNoValue	=	TRUE;
// 		m_parser.AddVar( value, CString(slh_data_array[nVariantID].varname) );
// 	}
	return true;
}

bool CIndex::CheckExpressValid( )
{
	InstrumentInfo	info;
	bool	bError	=	false;
	double	temp	=	0;
	GetExpressResult( &temp, info, NULL, &bError );
	return !bError;
}

bool CIndex::GetExpressResult( double * pResult, InstrumentInfo &info, bool *pVariantNoValue, bool * pError )
{
	if( !PreCompile( ) )
	{
		if( pVariantNoValue )	*pVariantNoValue	=	false;
		if( pError )	*pError	=	true;
		return false;
	}

	bool	bVariantNoValue	=	false;
	if( !InitParser( info, &bVariantNoValue ) )
	{
		if( pVariantNoValue )	*pVariantNoValue	=	bVariantNoValue;
		if( pError )	*pError	=	true;
		return false;
	}
	if( pVariantNoValue )	*pVariantNoValue	=	bVariantNoValue;

// 	m_parser.Parse( );
// 	if( pError )
// 		*pError	=	m_parser.IsError( );
// 	if( m_parser.IsError() )
// 		return false;
// 
// 	if( bVariantNoValue )
// 		return false;
// 
// 	if( pResult )
// 		*pResult	=	m_parser.GetResult( );
	return true;
}

/////////////////////////////////////////////////////////////////////////////
// CIndexContainer

CIndexContainer::CIndexContainer( )
{
}

CIndexContainer::~CIndexContainer( )
{
	clear( );
}

void CIndexContainer::RemoveIndex( UINT nID )
{
	for( vector<CIndex>::size_type i=0; i<size(); i++ )
	{
		CIndex & col = at(i);
		if( nID == col.m_nID )
		{
			this->erase(begin()+i );
			return;
		}
	}
}

CIndex & CIndexContainer::GetIndex( UINT nID )
{
	static CIndex idx;
	for( size_t i=0; i<size(); i++ )
	{
		CIndex & col = at(i);
		if( nID == col.m_nID )
			return col;
	}
	return idx;
}

CIndex & CIndexContainer::GetIndex( string strName )
{
	static CIndex idx;
	for( size_t i=0; i<size(); i++ )
	{
		CIndex & index = at(i);
		if(strName==index.m_strName)
			return index;
	}
	return idx;
}

bool CIndexContainer::EditIndex( CIndex &indexEdit )
{
	for( size_t i=0; i<size(); i++ )
	{
		CIndex & index = at(i);
		if( indexEdit.m_nID == index.m_nID )
		{
			index	=	indexEdit;
			return true;
		}
	}
	return false;
}

UINT CIndexContainer::GetNextID( )
{
	UINT	nID	=	SLH_USERDEFINE_BEGIN;
	for( vector<CIndex>::size_type i=0; i<size(); i++ )
	{
		CIndex & index = at(i);
		if( index.m_nID >= nID )
		{
			nID	=	index.m_nID + 1;
		}
	}
	return nID;
}

// void CIndexContainer::FileSerialize( CArchive & ar )
// {
// 	if( ar.IsStoring() )
// 	{
// 		ar << GetSize();
// 		for( int i=0; i<GetSize(); i++ )
// 		{
// 			CIndex	& col = ElementAt(i);
// 			col.Serialize( ar );
// 		}
// 	}
// 	else
// 	{
// 		int	size;
// 
// 		ar >> size;
// 		SetSize( size );
// 		for( int i=0; i<size; i++ )
// 		{
// 			CIndex	col;
// 			col.Serialize( ar );
// 			SetAt( i, col );
// 		}
// 	}
// }
CIndexContainer& CIndexContainer::GetSListColumnsUser()
{
	static CIndexContainer m_aSListColumnsUser;
	return m_aSListColumnsUser;
}
