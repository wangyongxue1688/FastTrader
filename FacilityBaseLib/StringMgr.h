#ifndef _STRING_MGR_H_
#define _STRING_MGR_H_
#include <string>
#include <vector>
using namespace std;
#include "Express.h"

extern	char	strategy_noselectedstock[];
extern	char	strategy_noselectedtech[];
typedef struct slh_index{
	int		m_ColumnId;
	char	m_ColumnEName[256];
	int		weight;
	char	m_ColumnName[256];
	char	m_Express[256];//表达式
}SLH_INDEX;
class FACILITY_API StringMgr
{
protected:
	StringMgr(void);
	virtual ~StringMgr(void);
	static StringMgr* _instance;
public:
	static StringMgr* GetInstance();
	string GetTechNameById(int nTech);
	string GetTechShortNameById(int nTech); 
	string GetTechFullNameById(int nTech);
	string GetTechClassNameById(int nTech);
	string GetIntensityString( int nIntensity );
	string GetIntensityCodeString( UINT nCode );
	string GetKTypeString( int ktype );
	string GetMaindataTypeString( int type );
	string GetSLHTitle( UINT nSLH );
	string GetSLHDescript( UINT nSLH );
	string	GetAccelerator( UINT nAcce, int nCharLeft );
	void GetAllDistrict( vector<string> & astr );
	string	FaintlyChsToEnu( const char * szCh );
	string GetTimeString(time_t time,const char* lpszFmt,bool bAddWeekDay);
	//将格式为hh:mm:ss转化为time_t
	static time_t ConvertString2Time(string hhmmss,string YYYYMMDD);
};
#endif

