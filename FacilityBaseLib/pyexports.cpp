#include <boost/python.hpp>
#include <boost/python/suite/indexing/map_indexing_suite.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>
#include "Order.h"
#include "Trade.h"
#include "Investor.h"
#include "TradingAccount.h"
#include "InvestorPosition.h"
#include "SettlementInfo.h"
#include "Tick.h"
#include "UserInfo.h"
#include "ServerInfo.h"
#include "InstrumentData.h"

inline boost::python::list WrapCombOffsetFlag(const InputOrder* io) {
	boost::python::list r;
	for (int n = 0; n < sizeof(io->CombOffsetFlag); ++n)
	{
		r.append(io->CombOffsetFlag[n]);
	}
	return r;
}

inline boost::python::list WrapCombHedgeFlag(const InputOrder* io) {
	boost::python::list r;
	for (int n = 0; n < sizeof(io->CombHedgeFlag); ++n)
	{
		r.append(io->CombHedgeFlag[n]);
	}
	return r;
}

inline std::string getKDataCode(const kdata_t* k)
{ 
	return k->szCode; 
}
inline void setKDataCode (kdata_t* k, const std::string& szCode)
{ 
	strcpy(k->szCode, szCode.c_str()); 
}

inline std::string getKDataTradingDay(const kdata_t* k)
{
	return k->TradingDay;
}



BOOST_PYTHON_MODULE(FacilityBaseLib)
{
	using namespace boost::python;



	class_<ErrorMessage>("ErrorMessage")
		.def_readwrite("ErrorID", &ErrorMessage::ErrorID)
		.def_readwrite("ErrorMsg", &ErrorMessage::ErrorMsg)
		;

	enum_<EnumOrderPriceType>("EnumOrderPriceType")
		.value("OPT_AnyPrice", OPT_AnyPrice)
		.value("OPT_LimitPrice", OPT_LimitPrice)
		;

	enum_<EnumOrderStatus>("EnumOrderStatus")
		.value("OST_AllTraded", OST_AllTraded)
		.value("OST_PartTradedQueueing", OST_PartTradedQueueing)
		.value("OST_PartTradedNotQueueing", OST_PartTradedNotQueueing)
		.value("OST_NoTradeQueueing", OST_NoTradeQueueing)
		.value("OST_NoTradeNotQueueing", OST_NoTradeNotQueueing)
		.value("OST_Canceled", OST_Canceled)
		.value("OST_Unknown", OST_Unknown)
		.value("OST_NotTouched", OST_NotTouched)
		.value("OST_Touched", OST_Touched)
		;

	enum_<EnumHedgeFlag>("EnumHedgeFlag")
		.value("HF_None", HF_None)
		.value("HF_Speculation", HF_Speculation)
		.value("HF_Arbitrage", HF_Arbitrage)
		.value("HF_Hedge", HF_Hedge)
		.value("HF_MarketMaker", HF_MarketMaker)
		;

	enum_<EnumPosiDirection>("EnumPosiDirection")
		.value("PD_Net", PD_Net)
		.value("PD_Long", PD_Long)
		.value("PD_Short", PD_Short)
		;

	enum_<EnumDirection>("EnumDirection")
		.value("D_Buy", D_Buy)
		.value("D_Sell", D_Sell)
		;

	enum_<EnumOffsetFlag>("EnumOffsetFlag")
		.value("OF_Open", OF_Open)
		.value("OF_Close", OF_Close)
		.value("OF_CloseToday", OF_CloseToday)
		.value("OF_CloseYesterday", OF_CloseYesterday)
		;

	enum_<EnumPositionDateType>("EnumPositionDateType")
		.value("PSD_Today", PSD_Today)
		.value("PSD_History", PSD_History)
		;
	
	class_<InputOrder,boost::shared_ptr<InputOrder>,bases<ErrorMessage> >("InputOrder")
		.def_readwrite("BrokerID", &InputOrder::BrokerID)
		.def_readwrite("InvestorID", &InputOrder::InvestorID)
		.def_readwrite("InstrumentID", &InputOrder::InstrumentID)
		.def_readwrite("OrderRef", &InputOrder::OrderRef)
		.def_readwrite("UserID", &InputOrder::UserID)
		.def_readwrite("OrderPriceType", &InputOrder::OrderPriceType)
		.def_readwrite("Direction", &InputOrder::Direction)
		.add_property("CombOffsetFlag", &WrapCombOffsetFlag)
		.add_property("CombHedgeFlag", &WrapCombHedgeFlag)
		.def_readwrite("LimitPrice", &InputOrder::LimitPrice)
		.def_readwrite("VolumeTotalOriginal", &InputOrder::VolumeTotalOriginal)
		.def_readwrite("GTDDate", &InputOrder::GTDDate)
		.def_readwrite("MinVolume", &InputOrder::MinVolume)
		.def_readwrite("RequestID", &InputOrder::RequestID)
		.def_readwrite("FrontID", &InputOrder::FrontID)
		.def_readwrite("SessionID", &InputOrder::SessionID)
		.def_readwrite("OrderSysID", &InputOrder::OrderSysID)
		.def_readwrite("InsertDate", &InputOrder::InsertDate)
		.def_readwrite("InsertTime", &InputOrder::InsertTime)
		;

	class_<Order, boost::shared_ptr<Order>, bases<InputOrder> >("Order")
		.def_readwrite("IsAutoSuspend", &Order::IsAutoSuspend)
		.def_readwrite("OrderLocalID", &Order::OrderLocalID)
		.def_readwrite("BusinessUnit", &Order::BusinessUnit)
		.def_readwrite("ParticipantID", &Order::ParticipantID)
		.def_readwrite("ClientID", &Order::ClientID)
		.def_readwrite("TraderID", &Order::TraderID)
		.def_readwrite("InstallID", &Order::InstallID)
		.def_readwrite("NotifySequence", &Order::NotifySequence)
		.def_readwrite("TradingDay", &Order::TradingDay)
		.def_readwrite("SettlementID", &Order::SettlementID)
		.def_readwrite("VolumeTraded",&Order::VolumeTraded)
		.def_readwrite("VolumeTotal", &Order::VolumeTotal)
// 		.def_readwrite("InsertDate", &Order::InsertDate)
// 		.def_readwrite("InsertTime", &Order::InsertTime)
		.def_readwrite("ActiveTime", &Order::ActiveTime)
		.def_readwrite("SuspendTime", &Order::SuspendTime)
		.def_readwrite("SuspendTime", &Order::UpdateTime)
		.def_readwrite("CancelTime", &Order::CancelTime)
		.def_readwrite("ActiveTraderID", &Order::ActiveTraderID)
		.def_readwrite("ClearingPartID", &Order::ClearingPartID)
		.def_readwrite("SequenceNo", &Order::SequenceNo)
		.def_readwrite("UserProductInfo", &Order::UserProductInfo)
		.def_readwrite("StatusMsg", &Order::StatusMsg)
		.def_readwrite("UserForceClose", &Order::UserForceClose)
		.def_readwrite("ActiveUserID", &Order::ActiveUserID)
		.def_readwrite("BrokerOrderSeq", &Order::BrokerOrderSeq)
		.def_readwrite("RelativeOrderSysID", &Order::RelativeOrderSysID)
		.def_readwrite("ZCETotalTradedVolume", &Order::ZCETotalTradedVolume)
		.def_readwrite("OrderStatus", &Order::OrderStatus)
		.def_readwrite("SeatID", &Order::SeatID)
		;

	class_<InputOrderAction, boost::shared_ptr<InputOrderAction> >("InputOrderAction")
		.def_readwrite("BrokerID", &InputOrderAction::BrokerID)
		.def_readwrite("InvestorID", &InputOrderAction::InvestorID)
		.def_readwrite("InstrumentID", &InputOrderAction::InstrumentID)
		.def_readwrite("OrderActionRef", &InputOrderAction::OrderActionRef)
		.def_readwrite("OrderRef", &InputOrderAction::OrderRef)
		.def_readwrite("RequestID", &InputOrderAction::RequestID)
		.def_readwrite("FrontID", &InputOrderAction::FrontID)
		.def_readwrite("SessionID", &InputOrderAction::SessionID)
		.def_readwrite("OrderSysID", &InputOrderAction::OrderSysID)
		.def_readwrite("ExchangeID", &InputOrderAction::ExchangeID)
		.def_readwrite("InsertDate", &InputOrderAction::InsertDate)
		.def_readwrite("InsertTime", &InputOrderAction::InsertTime)
		;

	class_<Trade, boost::shared_ptr<Trade>>("Trade")
		.def_readwrite("BrokerID", &Trade::BrokerID)
		.def_readwrite("InvestorID", &Trade::InvestorID)
		.def_readwrite("InstrumentID", &Trade::InstrumentID)
		.def_readwrite("UserID", &Trade::UserID)
		.def_readwrite("OrderRef", &Trade::OrderRef)
		.def_readwrite("OrderSysID", &Trade::OrderSysID)
		.def_readwrite("ExchangeID", &Trade::ExchangeID)
		.def_readwrite("TradeID", &Trade::TradeID)
		.def_readwrite("Direction", &Trade::Direction)
		.def_readwrite("ParticipantID", &Trade::ParticipantID)
		.def_readwrite("ClientID", &Trade::ClientID)
		.def_readwrite("OffsetFlag", &Trade::OffsetFlag)
		.def_readwrite("HedgeFlag", &Trade::HedgeFlag)
		.def_readwrite("Price", &Trade::Price)
		.def_readwrite("TradeDate", &Trade::TradeDate)
		.def_readwrite("TradeTime", &Trade::TradeTime)
		.def_readwrite("TraderID", &Trade::TraderID)
		.def_readwrite("OrderLocalID", &Trade::OrderLocalID)
		.def_readwrite("ClearingPartID", &Trade::ClearingPartID)
		.def_readwrite("BusinessUnit", &Trade::BusinessUnit)
		.def_readwrite("SequenceNo", &Trade::SequenceNo)
		.def_readwrite("TradingDay", &Trade::TradingDay)
		.def_readwrite("SettlementID", &Trade::SettlementID)
		.def_readwrite("BrokerOrderSeq", &Trade::BrokerOrderSeq)
		.def_readwrite("SeatID", &Trade::SeatID)
		;

	class_<InvestorPosition,boost::shared_ptr<InvestorPosition> >("InvestorPosition")
		.def_readwrite("BrokerID", &InvestorPosition::BrokerID)
		.def_readwrite("InvestorID", &InvestorPosition::InvestorID)
		.def_readwrite("InstrumentID", &InvestorPosition::InstrumentID)
		.def_readwrite("ExchangeID", &InvestorPosition::ExchangeID)
		.def_readwrite("ClientID", &InvestorPosition::ClientID)
		.def_readwrite("OpenDate", &InvestorPosition::OpenDate)
		.def_readwrite("PosiDirection", &InvestorPosition::PosiDirection)
		.def_readwrite("HedgeFlag", &InvestorPosition::HedgeFlag)
		.def_readwrite("PositionDate", &InvestorPosition::PositionDate)
		.def_readwrite("YdPosition", &InvestorPosition::YdPosition)
		.def_readwrite("Position", &InvestorPosition::Position)
		.def_readwrite("LongFrozen", &InvestorPosition::LongFrozen)
		.def_readwrite("ShortFrozen", &InvestorPosition::ShortFrozen)
		.def_readwrite("LongFrozenAmount", &InvestorPosition::LongFrozenAmount)
		.def_readwrite("ShortFrozenAmount", &InvestorPosition::ShortFrozenAmount)
		.def_readwrite("OpenVolume", &InvestorPosition::OpenVolume)
		.def_readwrite("CloseVolume", &InvestorPosition::CloseVolume)
		.def_readwrite("OpenAmount", &InvestorPosition::OpenAmount)
		.def_readwrite("CloseAmount", &InvestorPosition::CloseAmount)
		.def_readwrite("PositionCost", &InvestorPosition::PositionCost)
		.def_readwrite("PreMargin", &InvestorPosition::PreMargin)
		.def_readwrite("UseMargin", &InvestorPosition::UseMargin)
		.def_readwrite("FrozenMargin", &InvestorPosition::FrozenMargin)
		.def_readwrite("FrozenCash", &InvestorPosition::FrozenCash)
		.def_readwrite("FrozenCommission", &InvestorPosition::FrozenCommission)
		.def_readwrite("CashIn", &InvestorPosition::CashIn)
		.def_readwrite("Commission", &InvestorPosition::Commission)
		.def_readwrite("CloseProfit", &InvestorPosition::CloseProfit)
		.def_readwrite("PositionProfit", &InvestorPosition::PositionProfit)
		.def_readwrite("PreSettlementPrice", &InvestorPosition::PreSettlementPrice)
		.def_readwrite("SettlementPrice", &InvestorPosition::SettlementPrice)
		.def_readwrite("TradingDay", &InvestorPosition::TradingDay)
		.def_readwrite("SettlementID", &InvestorPosition::SettlementID)
		.def_readwrite("OpenCost", &InvestorPosition::OpenCost)
		.def_readwrite("ExchangeMargin", &InvestorPosition::ExchangeMargin)
		.def_readwrite("CombPosition", &InvestorPosition::CombPosition)
		.def_readwrite("CombLongFrozen", &InvestorPosition::CombLongFrozen)
		.def_readwrite("CombShortFrozen", &InvestorPosition::CombShortFrozen)
		.def_readwrite("CloseProfitByDate", &InvestorPosition::CloseProfitByDate)
		.def_readwrite("CloseProfitByTrade", &InvestorPosition::CloseProfitByTrade)
		.def_readwrite("TodayPosition", &InvestorPosition::TodayPosition)
		.def_readwrite("MarginRateByMoney", &InvestorPosition::MarginRateByMoney)
		.def_readwrite("MarginRateByVolume", &InvestorPosition::MarginRateByVolume)
		.def_readwrite("StrikeFrozen", &InvestorPosition::StrikeFrozen)
		.def_readwrite("StrikeFrozenAmount", &InvestorPosition::StrikeFrozenAmount)
		.def_readwrite("AbandonFrozen", &InvestorPosition::AbandonFrozen)
		.def_readwrite("OpenPrice", &InvestorPosition::OpenPrice)
		;

	class_<Investor ,boost::shared_ptr<Investor> >("Investor")
		.def_readwrite("InvestorID", &Investor::InvestorID)
		.def_readwrite("BrokerID", &Investor::BrokerID)
		.def_readwrite("InvestorGroupID", &Investor::InvestorGroupID)
		.def_readwrite("InvestorName", &Investor::InvestorName)
		.def_readwrite("IdentifiedCardNo", &Investor::IdentifiedCardNo)
		.def_readwrite("IsActive", &Investor::IsActive)
		.def_readwrite("Telephone", &Investor::Telephone)
		.def_readwrite("Address", &Investor::Address)
		.def_readwrite("OpenDate", &Investor::OpenDate)
		.def_readwrite("Mobile", &Investor::Mobile)
		.def_readwrite("CommModelID", &Investor::CommModelID)
		.def_readwrite("MarginModelID", &Investor::MarginModelID)
		;
	class_<BaseTradingAccount, boost::shared_ptr<BaseTradingAccount> >("BaseTradingAccount")
		.def_readwrite("TradingDay", &BaseTradingAccount::TradingDay)
		.def_readwrite("UpdateTime", &BaseTradingAccount::UpdateTime)
		.def_readwrite("PreBalance", &BaseTradingAccount::PreBalance)
		.def_readwrite("Commission", &BaseTradingAccount::Commission)
		.def_readwrite("CurrMargin", &BaseTradingAccount::CurrMargin)
		.def_readwrite("CloseProfit", &BaseTradingAccount::CloseProfit)
		.def_readwrite("PositionProfit", &BaseTradingAccount::PositionProfit)
		.def_readwrite("Available", &BaseTradingAccount::Available)
		.def_readwrite("Deposit", &BaseTradingAccount::Deposit)
		.def_readwrite("Withdraw", &BaseTradingAccount::Withdraw)
		;
	class_<TradingAccount, boost::shared_ptr<TradingAccount> >("TradingAccount")
		.def_readwrite("BrokerID", &TradingAccount::BrokerID)
		.def_readwrite("AccountID", &TradingAccount::AccountID)
		.def_readwrite("PreMortgage", &TradingAccount::PreMortgage)
		.def_readwrite("PreCredit", &TradingAccount::PreCredit)
		.def_readwrite("PreDeposit", &TradingAccount::PreDeposit)
		.def_readwrite("PreBalance", &TradingAccount::PreBalance)
		.def_readwrite("PreMargin", &TradingAccount::PreMargin)
		.def_readwrite("InterestBase", &TradingAccount::InterestBase)
		.def_readwrite("Interest", &TradingAccount::Interest)
		.def_readwrite("Deposit", &TradingAccount::Deposit)
		.def_readwrite("Withdraw", &TradingAccount::Withdraw)
		.def_readwrite("FrozenMargin", &TradingAccount::FrozenMargin)
		.def_readwrite("FrozenCash", &TradingAccount::FrozenCash)
		.def_readwrite("FrozenCommission", &TradingAccount::FrozenCommission)
		.def_readwrite("CurrMargin", &TradingAccount::CurrMargin)
		.def_readwrite("CashIn", &TradingAccount::CashIn)
		.def_readwrite("Commission", &TradingAccount::Commission)
		.def_readwrite("CloseProfit", &TradingAccount::CloseProfit)
		.def_readwrite("PositionProfit", &TradingAccount::PositionProfit)
		.def_readwrite("Balance", &TradingAccount::Balance)
		.def_readwrite("Available", &TradingAccount::Available)
		.def_readwrite("WithdrawQuota", &TradingAccount::WithdrawQuota)
		.def_readwrite("Reserve", &TradingAccount::Reserve)
		.def_readwrite("TradingDay", &TradingAccount::TradingDay)
		.def_readwrite("SettlementID", &TradingAccount::SettlementID)
		.def_readwrite("Credit", &TradingAccount::Credit)
		.def_readwrite("Mortgage", &TradingAccount::Mortgage)
		.def_readwrite("ExchangeMargin", &TradingAccount::ExchangeMargin)
		.def_readwrite("DeliveryMargin", &TradingAccount::DeliveryMargin)
		.def_readwrite("ExchangeDeliveryMargin", &TradingAccount::ExchangeDeliveryMargin)
		.def_readwrite("ReserveBalance", &TradingAccount::ReserveBalance)
		.def_readwrite("CurrencyID", &TradingAccount::CurrencyID)
		.def_readwrite("PreFundMortgageIn", &TradingAccount::PreFundMortgageIn)
		.def_readwrite("PreFundMortgageOut", &TradingAccount::PreFundMortgageOut)
		.def_readwrite("FundMortgageIn", &TradingAccount::FundMortgageIn)
		.def_readwrite("FundMortgageOut", &TradingAccount::FundMortgageOut)
		.def_readwrite("FundMortgageAvailable", &TradingAccount::FundMortgageAvailable)
		.def_readwrite("MortgageableFund", &TradingAccount::MortgageableFund)
		.def_readwrite("SpecProductMargin", &TradingAccount::SpecProductMargin)
		.def_readwrite("SpecProductFrozenMargin", &TradingAccount::SpecProductFrozenMargin)
		.def_readwrite("SpecProductCommission", &TradingAccount::SpecProductCommission)
		.def_readwrite("SpecProductFrozenCommission", &TradingAccount::SpecProductFrozenCommission)
		.def_readwrite("SpecProductPositionProfit", &TradingAccount::SpecProductPositionProfit)
		.def_readwrite("SpecProductCloseProfit", &TradingAccount::SpecProductCloseProfit)
		.def_readwrite("SpecProductPositionProfitByAlg", &TradingAccount::SpecProductPositionProfitByAlg)
		.def_readwrite("SpecProductExchangeMargin", &TradingAccount::SpecProductExchangeMargin)
		;

	class_<SettlementInfo,boost::shared_ptr<SettlementInfo> >("SettlementInfo")
		.def_readwrite("TradingDay", &SettlementInfo::TradingDay)
		.def_readwrite("SettlementID", &SettlementInfo::SettlementID)
		.def_readwrite("BrokerID", &SettlementInfo::BrokerID)
		.def_readwrite("InvestorID", &SettlementInfo::InvestorID)
		.def_readwrite("SequenceNo", &SettlementInfo::SequenceNo)
		.def_readwrite("Content", &SettlementInfo::Content)
		;

	class_<UserLoginParam>("UserLoginParam")
		.def_readwrite("BrokerID", &UserLoginParam::BrokerID)
		.def_readwrite("UserID", &UserLoginParam::UserID)
		.def_readwrite("Password", &UserLoginParam::Password)
		.def_readwrite("AutoConfirmSettlement",
		&UserLoginParam::AutoConfirmSettlement)
		.def_readwrite("InvestorID", &UserLoginParam::InvestorID)
		.def_readwrite("Server", &UserLoginParam::Server)
		.def_readwrite("MdBrokerID", &UserLoginParam::MdBrokerID)
		.def_readwrite("MdUserID", &UserLoginParam::MdUserID)
		.def_readwrite("MdPassword", &UserLoginParam::MdPassword)
		;

	class_<UserInfo,bases<UserLoginParam> >("UserInfo")
		.def_readwrite("TradingDay",&UserInfo::TradingDay)
		.def_readwrite("LoginTime", &UserInfo::LoginTime)
		.def_readwrite("SystemName", &UserInfo::SystemName)
		.def_readwrite("FrontID", &UserInfo::FrontID)
		.def_readwrite("SessionID", &UserInfo::SessionID)
		.def_readwrite("MaxOrderRef", &UserInfo::MaxOrderRef)
		.def_readwrite("ExchangeTime", &UserInfo::ExchangeTime)
		.def_readwrite("IsTraderLogined", &UserInfo::IsTraderLogined)
		.def_readwrite("AuthCode", &UserInfo::AuthCode)
		;
	class_<ServerAddr>("ServerAddr")
		.def_readwrite("protocol", &ServerAddr::protocol)
		.def_readwrite("address",&ServerAddr::address)
		.def_readwrite("port",&ServerAddr::port)
		;

	class_<ServerInfo>("ServerInfo")
		.def_readwrite("id",&ServerInfo::id)
		.def_readwrite("name", &ServerInfo::name)
		.def_readwrite("tapi", &ServerInfo::trader_api)
		.def_readwrite("mapi", &ServerInfo::market_api)
		.def_readwrite("market_level", &ServerInfo::market_level)
		;
	typedef std::string(Tick::*pfTickGetInstrumentID)() const;
	typedef void (Tick::*pfTickSetInstrumentID)(const std::string&);

	pfTickGetInstrumentID TickGetInstrumentID = &Tick::InstrumentID;
	pfTickSetInstrumentID TickSetInstrumentID = &Tick::InstrumentID;

	class_<Tick,boost::shared_ptr<Tick> >("Tick")
		.add_property("InstrumentID", TickGetInstrumentID, TickSetInstrumentID)
		.def_readwrite("OpenPrice", &Tick::OpenPrice)
		.def_readwrite("HighestPrice", &Tick::HighestPrice)
		.def_readwrite("LowestPrice", &Tick::LowestPrice)
		.def_readwrite("LastPrice", &Tick::LastPrice)
		.def_readwrite("ClosePrice", &Tick::ClosePrice)
		.def_readwrite("Volume", &Tick::Volume)
		.def_readwrite("Turnover", &Tick::Turnover)
		.def_readwrite("OpenInterest", &Tick::OpenInterest)
		.def_readwrite("UpdateTime", &Tick::UpdateTime)
		.def_readwrite("UpdateMillisec", &Tick::UpdateMillisec)
		;

	

	class_<kdata_t, boost::shared_ptr<kdata_t> >("KDATA")
		.add_property("szCode", getKDataCode,setKDataCode)
		.add_property("TradingDay", getKDataTradingDay)
		.def_readwrite("OpenPrice", &kdata_t::OpenPrice)
		.def_readwrite("HighestPrice", &kdata_t::HighestPrice)
		.def_readwrite("LowestPrice", &kdata_t::LowestPrice)
		.def_readwrite("ClosePrice", &kdata_t::ClosePrice)
		.def_readwrite("Turnover", &kdata_t::Turnover)
		.def_readwrite("Volume", &kdata_t::Volume)
		.def_readwrite("OpenInterest", &kdata_t::OpenInterest)
		.def_readwrite("TradingTime", &kdata_t::TradingTime)
		;

	enum_<KTypes>("KTypes")
		.value("ktypeNone", ktypeNone)
		.value("ktypeSec1", ktypeSec1)        //1.秒;
		.value("ktypeMin", ktypeMin)         //1.分;
		.value("ktypeMin3", ktypeMin3)		   //3.分;
		.value("ktypeMin5", ktypeMin5)        //5.分;
		.value("ktypeMin15", ktypeMin15)      //15.分;
		.value("ktypeMin30", ktypeMin30)      //30.分;
		.value("ktypeMin60", ktypeMin60)      //60.分;
		.value("ktypeDay", ktypeDay)     //日线;
		//以下的秒数,只是理论的秒数;
		.value("ktypeWeek", ktypeWeek)  //周线;
		.value("ktypeMonth", ktypeMonth)  //月线;
		.value("ktypeSeason", ktypeSeason)//季线;
		.value("ktypeYear", ktypeYear) //年线;
		.value("ktypeMax", ktypeMax)      //多年;
		;

	class_<std::vector<kdata_t> >("KDataVector")
		.def(vector_indexing_suite<std::vector<kdata_t> >())
		;

	class_<KdataContainer,boost::shared_ptr<KdataContainer>,bases<std::vector<kdata_t> >  >("kdata_container")
		.def("GetMA",&KdataContainer::GetMA)
		;
	class_<InstrumentData, boost::shared_ptr<InstrumentData> >("InstrumentData")
		.def("GetKData", &InstrumentData::GetKData, return_value_policy<reference_existing_object>())
		;
}