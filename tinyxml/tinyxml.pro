TEMPLATE = lib
CONFIG -= qt
CONFIG += staticlib
TARGET = tinyxml


CONFIG(debug,debug|release){
    DEFINES +=_DEBUG
    DESTDIR  =../build/v120/debug/
    LIBS += -L../build/v120/debug/
} else {
    DESTDIR = ../build/v120/release/
    LIBS += -L../build/v120/release/
}
macx{
    QMAKE_MAC_SDK = macosx10.11
}

HEADERS += \
    tinyxml.h \
    tinystr.h

SOURCES += \
    tinyxml.cpp \
    tinystr.cpp \
    tinyxmlerror.cpp \
    tinyxmlparser.cpp
