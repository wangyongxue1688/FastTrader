#ifndef _SIM_HUB_H_
#define _SIM_HUB_H_



#include <boost/noncopyable.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/thread/mutex.hpp>
#include <map>
#include <string>

class SimTrader;
class SimMarket;

struct Tick;
struct Order;
struct Trade;
//一个类似于总线的类;

class SimHub :protected boost::noncopyable
{
public:
	static SimHub& get_instance();

public:
	void Register(boost::shared_ptr<SimTrader>);
	void Register(boost::shared_ptr<SimMarket>);
public:
	//行情;
	void OnRtnDepthMarket(boost::shared_ptr<Tick> tick);
	//交易;
	boost::shared_ptr<Trade> OnRtnOrder(boost::shared_ptr<Order> order);
	//交易日;
	std::string GetTradingDay();
protected:
	boost::shared_ptr<SimTrader> m_Trader;
	boost::shared_ptr<SimMarket> m_Market;
	//当前行情;
	std::map<std::string, boost::shared_ptr<Tick> > m_Ticks;
	//当前报单;
	std::map<std::string, boost::shared_ptr<Order> > m_Orders;
	//交易日;
	std::string m_TradingDay;
};
#endif