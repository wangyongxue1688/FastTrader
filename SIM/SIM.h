#ifndef _SIM_H_
#define _SIM_H_


#include "../ServiceLib/Trader.h"
#include "../ServiceLib/Market.h"
#include <boost/dll.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/dll/alias.hpp>
#include <string>
#include <boost/make_shared.hpp>


boost::shared_ptr<Trader>  CreateTrader();
boost::shared_ptr<Market>  CreateMarket(const std::string& protocol);
BOOST_DLL_ALIAS(
	CreateTrader, // <-- this function is exported with...
	create_trader                               // <-- ...this alias name
	)
	BOOST_DLL_ALIAS(
	CreateMarket, // <-- this function is exported with...
	create_market                               // <-- ...this alias name
	)

#endif