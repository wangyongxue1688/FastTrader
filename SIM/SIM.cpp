// CTP.cpp : 定义 DLL 应用程序的导出函数。
//

#include "stdafx.h"
#include "SIM.h"
#include "SimTrader.h"
#include "SimMarket.h"
#include "../Log/logging.h"

/*EXTERN_C*/ boost::shared_ptr<Trader> CreateTrader()
{
	//LOGINIT("sim_trader");
	return boost::make_shared<SimTrader>();
}

/*EXTERN_C*/ boost::shared_ptr<Market>  CreateMarket(const std::string& /*protocol*/)
{
	//LOGINIT("sim_market");
	return boost::make_shared<SimMarket>();
}