#pragma once

#include "../MultiPriceMarginStrategy/PriceMarginParams.h"
#include "../SimpleStrategyLib/IStrategy.h"
#include <boost/enable_shared_from_this.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
//双均线策略;

struct FourPrice_Params 
{
	std::string InstrumentID;
	double dYesterdayHighest;//昨日高点;
	double dYesterdayLowest;//昨日低点;
	int nBuyJumps;
	int nSellJumps;
	double PriceTick;
	double dMoveProfit;    //移动止盈;
	double dMaxProfit;		//最大盈利;
	int nProfitJumps;		//移动止盈起始点;
	boost::shared_ptr<InputOrder> openInputOrder;
	boost::shared_ptr<Order> openOrder;
	boost::shared_ptr<Trade> openTrade;
	boost::shared_ptr<InputOrderAction> cancelOpenOrder;

	boost::shared_ptr<InputOrder> closeInputOrder;
	boost::shared_ptr<InputOrderAction> cancelCloseOrder;
	boost::shared_ptr<Order> closeOrder;
	boost::shared_ptr<Trade> closeTrade;

	//平仓停止交易时间;
	boost::posix_time::time_duration ClosePositionTime;
	int nHands;
	int cancelCount;
	boost::shared_ptr<Tick> lastTick;
};

class FourPriceStrategy :public IStrategy, public boost::enable_shared_from_this<FourPriceStrategy>
{
public:
	// 标准构造函数;
	FourPriceStrategy(const std::string& id);
	std::shared_ptr<std::thread> thrd;
	
	virtual ~FourPriceStrategy();
	virtual void Initialize();
	virtual void Finalize();

	virtual void OnTick(boost::shared_ptr<Tick> tick);
	virtual void OnOrder(boost::shared_ptr<Order> order);
	virtual void OnTrade(boost::shared_ptr<Trade> trade);
public:

	boost::shared_ptr<InputOrder> MyOrderInsert(const std::string& instId, int Volume, double Price,
		EnumOffsetFlag OpenOrClose, EnumDirection BuyOrSell);
protected:
	//读取配置文件;
	bool ReadConfig();
	bool WriteConfig();
protected:
	//参数;
	std::map<std::string, FourPrice_Params> m_ParamsMap;
};