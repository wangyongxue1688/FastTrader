#include "VR.h"
#include "Tech.h"
#include "../FacilityBaseLib/KData.h"
//////////////////////////////////////////////////////////////////////
//	CVR
CVR::CVR( )
{
	SetDefaultParameters( );
}

CVR::CVR( KdataContainer * pKData )
	: TechnicalIndicator( pKData )
{
	SetDefaultParameters( );
}

CVR::~CVR()
{
	clear( );
}

void CVR::SetDefaultParameters( )
{
	m_nDays		=	12;
	m_itsLong			=	ITS_BUY;
	m_itsShort			=	ITS_SELL;
}

void CVR::attach( CVR & src )
{
	m_nDays		=	src.m_nDays;
	m_itsLong		=	src.m_itsLong;
	m_itsShort		=	src.m_itsShort;
}

bool CVR::IsValidParameters( )
{
	return ( VALID_DAYS(m_nDays) && VALID_ITS(m_itsLong) && VALID_ITS(m_itsShort) );
}

void CVR::clear( )
{
	TechnicalIndicator::clear( );
}

int CVR::signal( size_t nIndex, uint32_t * pnCode )
{
	if( pnCode )	*pnCode	=	ITSC_NOTHING;
	if( !m_pKData || nIndex < 0 || nIndex >= m_pKData->size() )
		return ITS_NOTHING;

	int	nMaxDays	=	m_nDays;
	double	dLiminalLow = 0, dLiminalHigh = 0;
	if( !intensity_prepare( nIndex, pnCode, nMaxDays, ITS_GETMINMAXDAYRANGE, &dLiminalLow, &dLiminalHigh, 0.309, 0.682 ) )
		return ITS_NOTHING;

	double	dNowClose	=	m_pKData->at(nIndex).ClosePrice;

	double	dVRNow;
	if( !calc( &dVRNow, nIndex, false ) )
		return ITS_NOTHING;

	int	nIntensity	=	GetTrendIntensity1( nIndex, m_itsLong, m_itsShort, pnCode );
	if( dVRNow < dLiminalLow && nIntensity == m_itsLong )
	{	// 低位趋势向上
		if( pnCode )	*pnCode	=	ITSC_LONG;
		return m_itsLong;
	}
	if( dVRNow > dLiminalHigh && nIntensity == m_itsShort )
	{	// 高位趋势向下
		if( pnCode )	*pnCode	=	ITSC_SHORT;
		return m_itsShort;
	}
	return	ITS_NOTHING;
}

bool CVR::min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax )
{
	return AfxGetMinMaxInfo1( nStart, nEnd, pdMin, pdMax, this );
}

/***
	     n日中上涨日成交量+1/2最近n日总成交量
	VR = ————————————---------—- ×100
	     n日中下跌日成交量+1/2最近n日总成交量
*/
bool CVR::calc(double * pValue, size_t nIndex, bool bUseLast )
{
	STT_ASSERT_CALCULATE1( m_pKData, nIndex );

	if( m_nDays > nIndex )
		return false;
	if( load_from_cache( nIndex, pValue ) )
		return true;

	double	dINTV = 0, dDETV = 0, dTV = 0;
	int	nCount	=	0;
	for( int k=nIndex; k>=1; k-- )
	{
		double	dAmount	=	m_pKData->at(k).Volume;
		if( m_pKData->MaindataAt(k) > m_pKData->MaindataAt(k-1) )
			dINTV	+=	dAmount;
		if( m_pKData->MaindataAt(k) < m_pKData->MaindataAt(k-1) )
			dDETV	+=	dAmount;
		dTV	+=	dAmount;

		nCount	++;
		if( nCount == m_nDays )
		{
			if( dDETV + dTV/2 < 1e-4 )
				return false;
			if( pValue )	*pValue	=	(dINTV + dTV/2) * 100 /(dDETV + dTV/2);
			store_to_cache( nIndex, pValue );
			return true;
		}
	}

	return false;
}
