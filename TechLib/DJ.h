#ifndef _TECH_DJ_H_
#define _TECH_DJ_H_

#include "Technique.h"
#include "../FacilityBaseLib/InstrumentData.h"
class CStDatabase;
//	����ͼDJ
 class TECH_API CDJ : public TechnicalIndicator
 {
 public:
 	// Constructors
 	CDJ( );
 	CDJ( KdataContainer * pKData );
 	virtual ~CDJ();
 
 	static	InstrumentData		m_stockSha;
 	static	InstrumentData		m_stockSzn;
 	static	std::string	m_strCodeOrg;
 
 public:
 	virtual	void clear( );
 
 	// Attributes
 	std::string		m_strCodeSha;
 	std::string		m_strCodeSzn;
 	virtual	void	SetDefaultParameters( );
 	void	attach( CDJ & src );
 	virtual	bool	IsValidParameters( );
 	bool	PrepareStockData(CStDatabase * pDatabase, const char * szCodeOrg,
 									int nCurKType, int nCurKFormat, int nCurMaindataType,
 									DWORD dwAutoResumeDRBegin, int nAutoResumeDRLimit );
 
 	// Operations
 };
 #endif