#ifndef _TECH_KLINE_H_
#define _TECH_KLINE_H_
#include "TechLib.h"
#include "Technique.h"
// K��
class TECH_API CKLine : public TechnicalIndicator
{
public:
	// Constructors
	CKLine( );
	CKLine( KdataContainer * pKData );
	virtual ~CKLine();

public:
	virtual	void clear( );

	// Attributes
	virtual	void	SetDefaultParameters( );
	void	attach( CKLine & src );
	virtual	bool	IsValidParameters( );

	// Operations
	virtual	bool	min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax);
};
#endif
