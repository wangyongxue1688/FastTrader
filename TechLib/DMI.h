#pragma once
#include "TechLib.h"
#include "Technique.h"

//	����ָ��DMI
class TECH_API CDMI : public TechnicalIndicator
{
public:
	// Constructors
	CDMI( );
	CDMI( KdataContainer * pKData );
	virtual ~CDMI();

protected:
	double	*	m_pdDMICache;
	int		*	m_pnDMICacheIndex;
	int			m_nDMICacheCurrent;
	bool	CalculateDM(double *pDMPlus, double *pDMMinus, double *pTR, size_t nIndex);
	bool	CalculateDIDX(double *pDIPlus, double *pDIMinus, double *pDX, size_t nIndex, size_t nDays);
public:
	virtual	void clear( );

	// Attributes
	uint32_t		m_nDays;
	virtual	void	SetDefaultParameters( );
	void	attach( CDMI & src );
	virtual	bool	IsValidParameters( );

	// Operations
	virtual	int		signal( size_t nIndex, uint32_t * pnCode = NULL );
	virtual	bool	min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax);
	virtual	bool	calc(double * pDIPlus, double * pDIMinus, double *pADX, double *pADXR, size_t nIndex, bool bUseLast);
};