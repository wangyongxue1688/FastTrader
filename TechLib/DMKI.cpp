#include "stdafx.h"
#include "DMKI.h"
#include "Tech.h"
#include "../FacilityBaseLib/KData.h"
#include "../FacilityBaseLib/Express.h"

//////////////////////////////////////////////////////////////////////
//	CDMKI
CDMKI::CDMKI()
{
	SetDefaultParameters();
}

CDMKI::CDMKI(KdataContainer * pKData)
	: TechnicalIndicator(pKData)
{
	SetDefaultParameters();
}

CDMKI::~CDMKI()
{
	clear();
}

void CDMKI::SetDefaultParameters()
{
	m_nDays = 13;
	m_itsSold = ITS_BUY;
	m_itsBought = ITS_SELL;
}

void CDMKI::AttachParameters(CDMKI & src)
{
	m_nDays = src.m_nDays;
	m_itsSold = src.m_itsSold;
	m_itsBought = src.m_itsBought;
}

bool CDMKI::IsValidParameters()
{
	return (VALID_DAYS(m_nDays) && VALID_ITS(m_itsSold) && VALID_ITS(m_itsBought));
}

void CDMKI::clear()
{
	TechnicalIndicator::clear();
}

int CDMKI::signal(size_t nIndex, uint32_t * pnCode)
{
	if (pnCode)	*pnCode = ITSC_NOTHING;

	double	dDMKI;
	if (!calc(&dDMKI, nIndex, false))
		return ITS_NOTHING;
	if (dDMKI < 0.3)
	{	// 超卖
		if (pnCode)	*pnCode = ITSC_OVERSOLD;
		return m_itsSold;
	}
	if (dDMKI > 0.7)
	{	// 超买
		if (pnCode)	*pnCode = ITSC_OVERBOUGHT;
		return m_itsBought;
	}
	return	ITS_NOTHING;
}

bool CDMKI::min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax)
{
	return AfxGetMinMaxInfo1(nStart, nEnd, pdMin, pdMax, this);
}

/***
DMH = N日内最高价大于昨日最高价日的 （最高价-昨日最高价）
DML = N日内最低价小于昨日最低价日的 （昨日最低价-最低价）
DMKI = DMH / (DMH+DML)
*/
bool CDMKI::calc(double * pValue, size_t nIndex, bool bUseLast)
{
	STT_ASSERT_CALCULATE1(m_pKData, nIndex);

	if (m_nDays > nIndex)
		return false;

	if (load_from_cache(nIndex, pValue))
		return true;

	double	dDMH = 0, dDML = 0;
	int	nCount = 0;
	for (int k = nIndex; k >= 1; k--)
	{
		KDATA&	kd = m_pKData->at(k);
		KDATA&	kdLast = m_pKData->at(k - 1);
		if (kd.HighestPrice > kdLast.HighestPrice)
			dDMH += (((double)kd.HighestPrice) - kdLast.HighestPrice);
		if (kd.LowestPrice < kdLast.LowestPrice)
			dDML += (((double)kdLast.LowestPrice) - kd.LowestPrice);

		nCount++;
		if (nCount == m_nDays)
		{
			if (fabs(dDMH + dDML) < 1e-4)
				return false;
			if (pValue)	*pValue = dDMH / (dDMH + dDML);
			store_to_cache(nIndex, pValue);
			return true;
		}
	}

	return false;
}
