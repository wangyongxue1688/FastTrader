#pragma once
#include "TechLib.h"
#include "Technique.h"
//	���ײ���ָ��EMV
class TECH_API CEMV : public TechnicalIndicator
{
public:
	// Constructors
	CEMV( );
	CEMV( KdataContainer * pKData );
	virtual ~CEMV();

public:
	virtual	void clear( );

	// Attributes
	uint32_t		m_nDays;
	uint32_t		m_nMADays;
	int		m_itsGoldenFork;
	int		m_itsDeadFork;
	virtual	void	SetDefaultParameters( );
	void	attach( CEMV & src );
	virtual	bool	IsValidParameters( );

	// Operations
    virtual	int		signal( size_t nIndex, uint32_t * pnCode = NULL );
    virtual	bool	min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax );
    virtual	bool	calc( double * pValue, size_t nIndex, bool bUseLast );
    virtual	bool	calc( double * pValue, double * pMA, size_t nIndex, bool bUseLast );
};
