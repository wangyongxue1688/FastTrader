#include "stdafx.h"
#include "NVRSI.h"
#include "Tech.h"
#include "../FacilityBaseLib/KData.h"
#include "../FacilityBaseLib/Express.h"

//////////////////////////////////////////////////////////////////////
//	CNVRSI
CNVRSI::CNVRSI()
{
	SetDefaultParameters();
}

CNVRSI::CNVRSI(KdataContainer * pKData)
	: CVRSI(pKData)
{
	SetDefaultParameters();
}

CNVRSI::~CNVRSI()
{
	clear();
}

/***
VP = N日内成交量增加日的总成交量
VQ = N日内成交量减少日的总成交量
VRSI = 100 * VP / (VP+VQ)
*/
bool CNVRSI::calc(double * pValue, int nIndex, bool bUseLast)
{
	STT_ASSERT_CALCULATE1(m_pKData, nIndex);

	if (m_nDays > nIndex)
		return false;

	if (load_from_cache(nIndex, pValue))
		return true;

	double	dUV = 0, dV = 0, dResult = 0;
	int	nCount = 0;
	for (int k = nIndex; k >= 1; k--)
	{
		if (m_pKData->at(k).Volume > m_pKData->at(k - 1).Volume)
			dUV += m_pKData->at(k).Volume;
		dV += m_pKData->at(k).Volume;

		nCount++;
		if (nCount == m_nDays)
		{
			if (dV < 1e-4)
				dResult = 50;
			else
				dResult = 100. * dUV / dV;
			if (pValue)	*pValue = dResult;
			store_to_cache(nIndex, pValue);
			return true;
		}
	}

	return false;
}
