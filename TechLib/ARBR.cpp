#include "ARBR.h"
#include "Tech.h"
#include "../FacilityBaseLib/KData.h"

//////////////////////////////////////////////////////////////////////
//	CARBR
CARBR::CARBR( )
{
	SetDefaultParameters( );
}

CARBR::CARBR( KdataContainer * pKData )
: TechnicalIndicator( pKData )
{
	SetDefaultParameters( );
}

CARBR::~CARBR()
{
	clear( );
}

void CARBR::SetDefaultParameters( )
{
	m_nDays	=	26;
	m_itsGoldenFork		=	ITS_BUYINTENSE;
	m_itsDeadFork		=	ITS_SELLINTENSE;
	m_itsSold			=	ITS_BUY;
	m_itsBought			=	ITS_SELL;
}

void CARBR::attach( CARBR & src )
{
	m_nDays	=	src.m_nDays;
	m_itsGoldenFork		=	src.m_itsGoldenFork;
	m_itsDeadFork		=	src.m_itsDeadFork;
	m_itsSold			=	src.m_itsSold;
	m_itsBought			=	src.m_itsBought;
}

bool CARBR::IsValidParameters( )
{
	return ( VALID_DAYS(m_nDays)
		&& VALID_ITS(m_itsGoldenFork) && VALID_ITS(m_itsDeadFork)
		&& VALID_ITS(m_itsSold) && VALID_ITS(m_itsBought) );
}

void CARBR::clear( )
{
	TechnicalIndicator::clear( );
}

int CARBR::signal( size_t nIndex, uint32_t * pnCode )
{
	if( pnCode )	*pnCode	=	ITSC_NOTHING;

	double	dAR, dBR;
	if( !calc( &dAR, &dBR, nIndex, false ) )
		return ITS_NOTHING;

	int	nForkSignal	=	GetForkSignal( nIndex, m_itsGoldenFork, m_itsDeadFork, pnCode );

	if( (dAR < 50 || dBR < 50 ) && nForkSignal == m_itsGoldenFork )
	{	// 低位金叉
		if( pnCode )	*pnCode	=	ITSC_GOLDENFORK;
		return m_itsGoldenFork;
	}
	if( (dAR > 180 || dBR > 300) && nForkSignal == m_itsDeadFork )
	{	// 高位死叉
		if( pnCode )	*pnCode	=	ITSC_DEADFORK;
		return m_itsDeadFork;
	}
	if( dAR < 50 )
	{	// 超卖
		if( pnCode )	*pnCode	=	ITSC_OVERSOLD;
		return m_itsSold;
	}
	if( dAR > 200 )
	{	// 超买
		if( pnCode )	*pnCode	=	ITSC_OVERBOUGHT;
		return m_itsBought;
	}

	return	ITS_NOTHING;
}

bool CARBR::min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax )
{
	return AfxGetMinMaxInfo2( nStart, nEnd, pdMin, pdMax, this );
}

/***
（H-O）n天之和
AR = ———————— × 100
（O-L）n天之和
（H-C）n天之和
BR = ———————— × 100
（C-L）n天之和

H：最高价	L：最低价	O：开盘价	C：收盘价
*/
bool CARBR::calc( double * pAR, double *pBR, size_t nIndex, bool bUseLast )
{
	STT_ASSERT_CALCULATE1( m_pKData, nIndex );

	if( m_nDays > nIndex )
		return false;

	if( load_from_cache( nIndex, pAR, pBR ) )
		return true;

	double	dUP = 0, dDG = 0, dBS = 0, dSS = 0;
	int	nCount	=	0;
	for( int k=nIndex; k>=1; k-- )
	{
		KDATA	kd		=	m_pKData->at(k);
		KDATA	kdLast	=	m_pKData->at(k-1);
		dUP	+=	(((double)kd.HighestPrice)-kd.OpenPrice);
		dDG	+=	(((double)kd.OpenPrice)-kd.LowestPrice);
		dBS	+=	max( 0.0, ((double)kd.HighestPrice) - kdLast.ClosePrice );
		dSS	+=	max( 0.0, ((double)kdLast.ClosePrice) - kd.LowestPrice );

		nCount	++;
		if( nCount == m_nDays )
			break;
	}

	if( dDG < 1e-4 || dSS < 1e-4 )
		return false;
	if( pAR )
		*pAR	=	dUP * 100 / dDG;
	if( pBR )
		*pBR	=	dBS * 100 / dSS;
	store_to_cache( nIndex, pAR, pBR );
	return true;
}
