#pragma once
#include "Technique.h"

//	量相对强弱指标VRSI
class TECH_API  CVRSI : public TechnicalIndicator
{
public:
	// Constructors
	CVRSI( );
	CVRSI( KdataContainer * pKData );
	virtual ~CVRSI();

public:
	virtual	void clear( );

	// Attributes
	uint32_t		m_nDays;
	int		m_itsDeviateOnBottom;
	int		m_itsDeviateOnTop;
	int		m_itsSold;
	int		m_itsBought;
	virtual	void	SetDefaultParameters( );
	void	AttachParameters( CVRSI & src );
	virtual	bool	IsValidParameters( );

	// Operations
	virtual	int		signal(size_t nIndex, uint32_t * pnCode = NULL);
	virtual	bool	min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax);
	virtual	bool	calc(double * pValue, size_t nIndex, bool bUseLast);
};

