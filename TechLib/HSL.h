#pragma once
#include "Technique.h"
#include "../FacilityBaseLib/InstrumentInfo.h"

//	��Ի�����HSL
class TECH_API CHSL : public TechnicalIndicator
{
public:
	// Constructors
	CHSL();
	CHSL(KdataContainer * pKData);
	virtual ~CHSL();

	static	double GetMainChangeHand(DWORD dwMarket, KdataContainer & kdata, int nIndexKD);

public:
	virtual	void Clear();

	InstrumentInfo	m_stockinfo;

	// Attributes
	int		m_nDays;	// Not Used
	int		m_nMADays;	// Not Used
	int		m_itsGoldenFork;	// Not Used
	int		m_itsDeadFork;		// Not used
	virtual	void	SetDefaultParameters();
	void	AttachParameters(CHSL & src);
	virtual	bool	IsValidParameters();

	// Operations
	virtual	int		signal(int nIndex, UINT * pnCode = NULL);
	virtual	bool	min_max_info(int nStart, int nEnd, double *pdMin, double *pdMax);
	virtual	bool	calc(double * pValue, int nIndex, bool bUseLast);
	virtual	bool	calc(double * pValue, int nIndex, int nDays, bool bUseLast);
};

