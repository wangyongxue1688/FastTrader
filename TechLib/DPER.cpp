#include "stdafx.h"
#include "DPER.h"
#include "Tech.h"
#include "../FacilityBaseLib/KData.h"
#include "../FacilityBaseLib/Express.h"
//////////////////////////////////////////////////////////////////////
//	CDPER
CDPER::CDPER()
{
	SetDefaultParameters();
}

CDPER::CDPER(KdataContainer * pKData)
	: TechnicalIndicator(pKData)
{
	SetDefaultParameters();
}

CDPER::~CDPER()
{
	clear();
}

void CDPER::SetDefaultParameters()
{
	m_nDays = 30;
	m_nMADays = 6;
	m_nDetrendDays = 12;
	m_itsGoldenFork = ITS_BUY;
	m_itsDeadFork = ITS_SELL;
}

void CDPER::AttachParameters(CDPER & src)
{
	m_nDays = src.m_nDays;
	m_nMADays = src.m_nMADays;
	m_nDetrendDays = src.m_nDetrendDays;
	m_itsGoldenFork = src.m_itsGoldenFork;
	m_itsDeadFork = src.m_itsDeadFork;
}

bool CDPER::IsValidParameters()
{
	return (VALID_DAYS(m_nDays) && VALID_DAYS(m_nMADays) && VALID_DAYS(m_nDetrendDays)
		&& VALID_ITS(m_itsGoldenFork) && VALID_ITS(m_itsDeadFork));
}

void CDPER::clear()
{
	TechnicalIndicator::clear();
}

int CDPER::signal(size_t nIndex, uint32_t * pnCode)
{
	if (pnCode)	*pnCode = ITSC_NOTHING;

	int	nMaxDays = m_nDays + m_nMADays + m_nDetrendDays;
	double	dLiminalLow = 0, dLiminalHigh = 0;
	if (!intensity_prepare(nIndex, pnCode, nMaxDays, ITS_GETMINMAXDAYRANGE, &dLiminalLow, &dLiminalHigh, 0.15, 0.7))
		return ITS_NOTHING;

	if (nIndex <= 1)
		return ITS_NOTHING;

	double	dValue;
	if (!calc(&dValue, nIndex, false))
		return ITS_NOTHING;

	int	nSignal = GetForkSignal(nIndex, m_itsGoldenFork, m_itsDeadFork, pnCode);
	if (dValue < dLiminalLow && nSignal == m_itsGoldenFork)
	{	// 低位金叉
		if (pnCode)	*pnCode = ITSC_GOLDENFORK;
		return m_itsGoldenFork;
	}
	if (dValue > dLiminalHigh && nSignal == m_itsDeadFork)
	{	// 高位死叉
		if (pnCode)	*pnCode = ITSC_DEADFORK;
		return m_itsDeadFork;
	}

	return ITS_NOTHING;
}

bool CDPER::min_max_info(size_t nStart, size_t nEnd,
	double *pdMin, double *pdMax)
{
	if (pdMin)	*pdMin = 0;
	if (pdMax)	*pdMax = 100;
    return true;
}

/***
A = 今日收盘价 - （m_nDetrendDays+m_nDetrendDays）日的平均收盘价
N日内A小于今日A的天数
DPER = ----------------------
N

*/
bool CDPER::calc(double * pValue, size_t nIndex, bool bUseLast)
{
	STT_ASSERT_CALCULATE1(m_pKData, nIndex);

	if (m_nDays + m_nDetrendDays > nIndex + 1)
		return false;

	if (load_from_cache(nIndex, pValue))
		return true;

	double	dNow = 0;
	int	nCount = 0;
	double	dCountLow = 0;
	for (int k = nIndex; k >= 0; k--)
	{
		double	dCur = 0;

		double	dCt = 0, dMA = 0;
		double	nDetrendCount = 0;
		for (int l = k - 1; l >= 0; l--)
		{
			dMA += m_pKData->MaindataAt(l);

			nDetrendCount++;
			if (nDetrendCount == m_nDetrendDays + m_nDetrendDays)
			{
				dCt = m_pKData->MaindataAt(k);
				dMA = dMA / (m_nDetrendDays + m_nDetrendDays);
				dCur = dCt - dMA;
				break;
			}
		}

		if (nIndex == k)
		{
			dNow = dCur;
			continue;
		}


		if (dCur < dNow)
			dCountLow += 1;

		nCount++;
		if (nCount >= m_nDays)
		{
			if (pValue)
				*pValue = 100. * dCountLow / m_nDays;
			store_to_cache(nIndex, pValue);
			return true;
		}
	}

	return false;
}

/***
计算CYO及其移动平均值
*/
bool CDPER::calc(double * pValue, double * pMA, size_t nIndex, bool bUseLast)
{
	return calc_ma(pValue, pMA, nIndex, bUseLast, m_nMADays);
}
