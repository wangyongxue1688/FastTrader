#include "stdafx.h"
#include "REI.h"
#include "Tech.h"
#include "../FacilityBaseLib/KData.h"
#include "../FacilityBaseLib/Express.h"


//////////////////////////////////////////////////////////////////////
//	CREI
CREI::CREI()
{
	SetDefaultParameters();
}

CREI::CREI(KdataContainer * pKData)
	: TechnicalIndicator(pKData)
{
	SetDefaultParameters();
}

CREI::~CREI()
{
	clear();
}

void CREI::SetDefaultParameters()
{
	m_nDays = 8;
	m_itsLong = ITS_BUY;
	m_itsShort = ITS_SELL;
}

void CREI::AttachParameters(CREI & src)
{
	m_nDays = src.m_nDays;
	m_itsLong = src.m_itsLong;
	m_itsShort = src.m_itsShort;
}

bool CREI::IsValidParameters()
{
	return (VALID_DAYS(m_nDays) && VALID_ITS(m_itsLong) && VALID_ITS(m_itsShort));
}

void CREI::clear()
{
	TechnicalIndicator::clear();
}

int CREI::signal(size_t nIndex,uint32_t * pnCode)
{
	if (pnCode)	*pnCode = ITSC_NOTHING;

	if (nIndex <= 0)
		return ITS_NOTHING;

	double	dLast, dNow;
	if (!calc(&dLast, nIndex - 1, false)
		|| !calc(&dNow, nIndex, false))
		return ITS_NOTHING;

	if (dLast < -0.6 && dNow > -0.6)
	{	// 低位做多
		if (pnCode)	*pnCode = ITSC_LONG;
		return m_itsLong;
	}
	if (dLast > 0.6 && dNow < 0.6)
	{	// 高位做空
		if (pnCode)	*pnCode = ITSC_SHORT;
		return m_itsShort;
	}

	return	ITS_NOTHING;
}

bool CREI::min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax)
{
	return AfxGetMinMaxInfo1(nStart, nEnd, pdMin, pdMax, this);
}

/***
DIF1 = 今日最高价 - 2日前最高价
DIF2 = 今日最低价 - 2日前最低价
A = N日内除满足以下情况日的（DIF1+DIF2）之和
1. 2日前最高价 小于 7日前收盘价
2. 2日前最高价 小于 8日前收盘价
3. 今日前最高价 小于 5日前最低价
4. 今日前最高价 小于 6日前最低价
5. 2日前最低价 小于 7日前收盘价
6. 2日前最低价 小于 8日前收盘价
7. 今日前最低价 小于 5日前最高价
8. 今日前最低价 小于 6日前最高价
REIA = N日的DIF1绝对值之和 + N日的DIF2绝对值之和
REI  = A / REIA
*/
bool CREI::calc(double * pValue, size_t nIndex, bool bUseLast)
{
	STT_ASSERT_CALCULATE1(m_pKData, nIndex);

	if (m_nDays + 7 > nIndex)
		return false;

	if (load_from_cache(nIndex, pValue))
		return true;

	double	dREI = 0, dREIA = 0;
	int	nCount = 0;
	for (size_t k = nIndex; k >= 8; k--)
	{
		double	dDIF1 = 0, dDIF2 = 0;
		int		num1 = 1, num2 = 1;
		dDIF1 = ((double)m_pKData->at(k).HighestPrice) - m_pKData->at(k - 2).HighestPrice;
		dDIF2 = ((double)m_pKData->at(k).LowestPrice) - m_pKData->at(k - 2).LowestPrice;
		if (m_pKData->at(k - 2).HighestPrice < m_pKData->at(k - 7).ClosePrice
			&& m_pKData->at(k - 2).HighestPrice < m_pKData->at(k - 8).ClosePrice
			&& m_pKData->at(k).HighestPrice < m_pKData->at(k - 5).LowestPrice
			&& m_pKData->at(k).HighestPrice < m_pKData->at(k - 6).LowestPrice)
			num1 = 0;
		if (m_pKData->at(k - 2).LowestPrice > m_pKData->at(k - 7).ClosePrice
			&& m_pKData->at(k - 2).LowestPrice > m_pKData->at(k - 8).ClosePrice
			&& m_pKData->at(k).LowestPrice > m_pKData->at(k - 5).HighestPrice
			&& m_pKData->at(k).LowestPrice > m_pKData->at(k - 6).HighestPrice)
			num2 = 0;
		dREI += (dDIF1 + dDIF2) * num1 * num2;
		dREIA += fabs(dDIF1) + fabs(dDIF2);

		nCount++;
		if (nCount == m_nDays)
		{
			if (fabs(dREIA) < 1e-4)
				return false;
			if (pValue)
				*pValue = dREI / dREIA;
			store_to_cache(nIndex, pValue);
			return true;
		}
	}

	return false;
}
