#ifndef _TECH_BIAS_H_
#define _TECH_BIAS_H_
#include "TechLib.h"
#include "Technique.h"

//	������BIAS
class KdataContainer;
class  TECH_API CBIAS : public TechnicalIndicator
{
public:
	// Constructors
	CBIAS( );
	CBIAS( KdataContainer * pKData );
	virtual ~CBIAS();

public:
	virtual	void clear( );

	// Attributes
	uint32_t		m_nDays;
	int		m_itsSold;
	int		m_itsBought;
	virtual	void	SetDefaultParameters( );
	void	attach( CBIAS & src );
	virtual	bool	IsValidParameters( );

	// Operations
	virtual	int		signal( size_t nIndex, uint32_t * pnCode = NULL );
	virtual	bool	min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax);
	virtual	bool	calc(double * pValue, size_t nIndex, bool bUseLast);
};
#endif