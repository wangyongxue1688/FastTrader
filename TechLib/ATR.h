#pragma once
#include "Technique.h"

//	����ָ��ATR
class TECH_API CATR : public TechnicalIndicator
{
public:
	// Constructors
	CATR( );
	CATR( KdataContainer * pKData );
	virtual ~CATR();

public:
	virtual	void clear( );

	// Attributes
	uint32_t m_nDays;
	int		m_itsSold;
	int		m_itsBought;
	virtual	void	SetDefaultParameters( );
	void	AttachParameters( CATR & src );
	virtual	bool	IsValidParameters( );

	// Operations
	virtual	int		signal( size_t nIndex, uint32_t * pnCode = NULL );
	virtual	bool	min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax);
	virtual	bool	calc(double * pValue, size_t nIndex, bool bUseLast);
};

