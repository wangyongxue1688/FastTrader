#pragma once
#include "Technique.h"


//	负成交量指标NVI
class  CNVI : public TechnicalIndicator
{
public:
	// Constructors
	CNVI( );
	CNVI( KdataContainer * pKData );
	virtual ~CNVI();

public:
	virtual	void clear( );

	// Attributes
	int		m_nMADays;
	int		m_itsGoldenFork;
	int		m_itsDeadFork;
	virtual	void	SetDefaultParameters( );
	void	AttachParameters( CNVI & src );
	virtual	bool	IsValidParameters( );

	// Operations
	virtual	int		signal( int nIndex, UINT * pnCode = NULL );
	virtual	bool	min_max_info( int nStart, int nEnd, double *pdMin, double *pdMax );
	virtual	bool	calc( double * pValue, double *pMA, int nIndex, bool bUseLast );
};

