#include "DJ.h"

CDJ::CDJ()
{
	SetDefaultParameters();
}

CDJ::CDJ( KdataContainer * pKData )
	:TechnicalIndicator(pKData)
{
	SetDefaultParameters( );
}

void CDJ::SetDefaultParameters()
{
	clear();
}

bool CDJ::IsValidParameters()
{
	return true;
}

void CDJ::clear()
{
	TechnicalIndicator::clear();
}

CDJ::~CDJ()
{

}

void CDJ::attach( CDJ & src )
{
	m_strCodeSha	=	src.m_strCodeSha;
	m_strCodeSzn	=	src.m_strCodeSzn;
}
