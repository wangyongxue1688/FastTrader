#pragma once
#include "Technique.h"
#include "MACD.h"



//	量指数平滑异同移动平均线VMACD
class TECH_API CVMACD : public CMACD
{
public:
	// Constructors
	CVMACD( );
	CVMACD( KdataContainer * pKData );
	virtual ~CVMACD();

protected:

public:
	virtual	bool	calc( double *pdEMA1, double *pdEMA2, double *pdDIF, double *pdDEA,
					size_t nIndex, bool bUseLast );
};



