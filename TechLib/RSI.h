#ifndef _TECH_RSI_H_
#define _TECH_RSI_H_
#include "TechLib.h"
#include "Technique.h"

//	相对强弱指标
class TECH_API CRSI : public TechnicalIndicator
{
public:
	// Constructors
	CRSI( );
	CRSI( KdataContainer * pKData );
	virtual ~CRSI();

public:
	virtual	void clear( );

	// Attributes
	std::vector<uint32_t>	m_adwDays;
	int		m_itsSold;
	int		m_itsGoldenFork;
	int		m_itsDeadFork;
	virtual	void	SetDefaultParameters( );
	void	attach( CRSI & src );
	virtual	bool	IsValidParameters( );

	// Operations
    virtual	int		signal( size_t nIndex, uint32_t * pnCode = NULL );
    virtual	bool	min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax );
    virtual	bool	calc( double * pValue, size_t nIndex, size_t nDays, bool bUseLast );
};
#endif
