#ifndef _MIKE_TECH_H_
#define _MIKE_TECH_H_
#include "TechLib.h"
#include "Technique.h"
//	���ָ��MIKE
class TECH_API CMIKE : public TechnicalIndicator
{
public:
	// Constructors
	CMIKE( );
	CMIKE( KdataContainer * pKData );
	virtual ~CMIKE();

public:
	virtual	void clear( );

	// Attributes
	uint32_t		m_nDays;
	virtual	void	SetDefaultParameters( );
	void	attach( CMIKE & src );
	virtual	bool	IsValidParameters( );

	// Operations
	bool	CalculateMIKE(	double *pWR, double *pMR, double *pSR,
		double *pWS, double *pMS, double *pSS, size_t nIndex );
};
#endif