#include "CV.h"
#include "Tech.h"
#include "../FacilityBaseLib/KData.h"
#include <math.h>
//////////////////////////////////////////////////////////////////////
//	CCV
CCV::CCV( )
{
	SetDefaultParameters( );
}

CCV::CCV( KdataContainer * pKData )
	: TechnicalIndicator( pKData )
{
	SetDefaultParameters( );
}

CCV::~CCV()
{
	clear( );
}

void CCV::SetDefaultParameters( )
{
	m_nMAHLDays	=	10;
	m_nCVDays	=	10;
	m_itsSold				=	ITS_BUY;
	m_itsBought				=	ITS_SELL;
}

void CCV::attach( CCV & src )
{
	m_nMAHLDays	=	src.m_nMAHLDays;
	m_nCVDays	=	src.m_nCVDays;
	m_itsSold				=	src.m_itsSold;
	m_itsBought				=	src.m_itsBought;
}

bool CCV::IsValidParameters( )
{
	return ( VALID_DAYS(m_nMAHLDays) && VALID_DAYS(m_nCVDays)
		&& VALID_ITS(m_itsSold) && VALID_ITS(m_itsBought) );
}

void CCV::clear( )
{
	TechnicalIndicator::clear( );
}

int CCV::signal(size_t nIndex, uint32_t * pnCode)
{
	if( pnCode )	*pnCode	=	ITSC_NOTHING;

	if( nIndex <= 1 )
		return ITS_NOTHING;

	double	dNow, dLast, dLastLast;
	if( !calc( &dLastLast, nIndex-2, false )
		|| !calc( &dLast, nIndex-1, false )
		|| !calc( &dNow, nIndex, false ) )
		return ITS_NOTHING;

	if( dLastLast < 0 && dLast < 0 && dNow < 0 && dLast <= dLastLast && dNow > dLast )
	{	// 超卖
		if( pnCode )	*pnCode	=	ITSC_OVERSOLD;
		return m_itsSold;
	}
	if( dLastLast > 0 && dLast > 0 && dNow > 0 && dLast >= dLastLast && dNow < dLast )
	{	// 超买
		if( pnCode )	*pnCode	=	ITSC_OVERBOUGHT;
		return m_itsBought;
	}

	return	ITS_NOTHING;
}

bool CCV::min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax)
{
	return AfxGetMinMaxInfo1( nStart, nEnd, pdMin, pdMax, this );
}

/***
	A = 最高价 - 最低价
	MAHL	= A的m_nMAHLDays日平均值
	MAHLLast= m_nCVDays日前的MAHL
	CCI		= 100 * (MAHL - MAHLLast) / MAHLLast;
*/
bool CCV::calc(double * pValue, size_t nIndex, bool bUseLast)
{
	STT_ASSERT_CALCULATE1( m_pKData, nIndex );
	
	if( m_nMAHLDays+m_nCVDays > nIndex+2 )
		return false;

	if( load_from_cache( nIndex, pValue ) )
		return true;

	double	dMAHLNow = 0, dMAHLLast = 0;
	int	nCount	=	0;
	for( int k=nIndex; k>=0; k-- )
	{
		KDATA	kd	=	m_pKData->at(k);
		dMAHLNow	+=	(kd.HighestPrice-kd.LowestPrice);
		
		nCount	++;
		if( nCount == m_nMAHLDays )
			break;
	}

	nCount	=	0;
	for( int k=nIndex-m_nCVDays+1; k>=0; k-- )
	{
		KDATA	kd	=	m_pKData->at(k);
		dMAHLLast	+=	(((double)kd.HighestPrice)-kd.LowestPrice);
		
		nCount	++;
		if( nCount == m_nMAHLDays )
			break;
	}
	
	dMAHLNow	=	dMAHLNow / m_nMAHLDays;
	dMAHLLast	=	dMAHLLast / m_nMAHLDays;
	if( fabs(dMAHLLast) < 1e-4 )
		return false;
	if( pValue )
		*pValue	=	(dMAHLNow - dMAHLLast) * 100 / dMAHLLast;
	store_to_cache( nIndex, pValue );
	return true;
}
