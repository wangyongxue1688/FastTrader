#include "WVAD.h"
#include "Tech.h"
#include "../FacilityBaseLib/KData.h"
//////////////////////////////////////////////////////////////////////
//	CWVAD
CWVAD::CWVAD( )
{
	//SetDefaultParameters( );
}

CWVAD::CWVAD( KdataContainer * pKData )
	: TechnicalIndicator( pKData )
{
	SetDefaultParameters( );
}

CWVAD::~CWVAD()
{
	clear( );
}

void CWVAD::SetDefaultParameters( )
{
	m_nDays		=	24;
	m_itsLong				=	ITS_BUY;
	m_itsShort				=	ITS_SELL;
}

void CWVAD::attach( CWVAD & src )
{
	m_nDays		=	src.m_nDays;
	m_itsLong				=	src.m_itsLong;
	m_itsShort				=	src.m_itsShort;
}

bool CWVAD::IsValidParameters( )
{
	return ( VALID_DAYS( m_nDays )
		&& VALID_ITS(m_itsLong) && VALID_ITS(m_itsShort) );
}

void CWVAD::clear( )
{
	TechnicalIndicator::clear( );
}

int CWVAD::signal( size_t nIndex, uint32_t * pnCode )
{
	if( pnCode )	*pnCode	=	ITSC_NOTHING;

	int	nMaxDays	=	m_nDays;
	double	dLiminalLow = 0, dLiminalHigh = 0;
	if( !intensity_prepare( nIndex, pnCode, nMaxDays, ITS_GETMINMAXDAYRANGE, &dLiminalLow, &dLiminalHigh ) )
		return ITS_NOTHING;

	if( nIndex <= 0 )
		return ITS_NOTHING;

	double	dLast, dNow;
	if( !calc( &dLast, nIndex-1, false )
		|| !calc( &dNow, nIndex, false ) )
		return ITS_NOTHING;

	if( dNow < dLiminalLow )
	{	// 低位
		if( pnCode )	*pnCode	=	ITSC_LONG;
		return m_itsLong;
	}
	if( dNow > dLiminalHigh  )
	{	// 高位
		if( pnCode )	*pnCode	=	ITSC_SHORT;
		return m_itsShort;
	}

	return	ITS_NOTHING;
}

bool CWVAD::min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax )
{
	return AfxGetMinMaxInfo1( nStart, nEnd, pdMin, pdMax, this );
}

/***
	A = 当天收盘价 - 当天开盘价
	B = 当天最高价 - 当天最低价
	C = A÷B×V(成交量)
	WVAD = 累计n天的C值
*/
bool CWVAD::calc( double * pValue, size_t nIndex, bool bUseLast )
{
	STT_ASSERT_CALCULATE1( m_pKData, nIndex );

	if( m_nDays > nIndex+1 )
		return false;

	if( load_from_cache( nIndex, pValue ) )
		return true;

	int	nCount	=	0;
	double	dResult = 0;
	for( int k=nIndex; k>=0; k-- )
	{
		KDATA	kd	=	m_pKData->at(k);
		if( kd.HighestPrice > kd.LowestPrice)
			dResult	+=	(((double)kd.ClosePrice) - kd.OpenPrice)*kd.Volume/(((double)kd.HighestPrice)-kd.LowestPrice);

		nCount	++;
		if( nCount == m_nDays )
		{
			if( pValue )	*pValue	=	dResult;
			store_to_cache( nIndex, pValue );
			return true;
		}
	}
	return false;
}
