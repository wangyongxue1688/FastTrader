#include "stdafx.h"
#include "CDP.h"
#include "Tech.h"
#include "../FacilityBaseLib/KData.h"
#include "../FacilityBaseLib/Express.h"

//////////////////////////////////////////////////////////////////////
//	CCDP
CCDP::CCDP()
{
	SetDefaultParameters();
}

CCDP::CCDP(KdataContainer * pKData)
	: TechnicalIndicator(pKData)
{
	SetDefaultParameters();
}

CCDP::~CCDP()
{
	clear();
}

void CCDP::SetDefaultParameters()
{
}

void CCDP::AttachParameters(CCDP & src)
{
}

bool CCDP::IsValidParameters()
{
	return true;
}

void CCDP::clear()
{
	TechnicalIndicator::clear();
}

int CCDP::signal(size_t nIndex, uint32_t * pnCode)
{
	if (pnCode)	*pnCode = ITSC_NOTHING;
	// 无买卖信号
	return	ITS_NOTHING;
}

bool CCDP::min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax)
{
	return AfxGetMinMaxInfo4(nStart, nEnd, pdMin, pdMax, this);
}

/***
TP = (昨日收盘价+昨日收盘价+昨日最高价+昨日最低价)/4
AH = TP + 昨日最高价 - 昨日最低价
AL = TP - 最日最高价 + 昨日最低价
NH = TP + TP - 最日最低价
NL = TP + TP - 最日最高价
*/
bool CCDP::calc(double * pAH, double * pNH, double * pAL, double * pNL, size_t nIndex, bool bUseLast)
{
	STT_ASSERT_CALCULATE1(m_pKData, nIndex);

	if (nIndex < 1)
		return false;

	if (load_from_cache(nIndex, pAH, pNH, pAL, pNL))
		return true;

	KDATA&	kdLast = m_pKData->at(nIndex - 1);
	double	dTP = (kdLast.HighestPrice + kdLast.LowestPrice + kdLast.ClosePrice * 2) / 4.;
	if (pAH)	*pAH = (dTP + kdLast.HighestPrice - kdLast.LowestPrice);
	if (pNH)	*pNH = (dTP + dTP - kdLast.LowestPrice);
	if (pAL)	*pAL = (dTP - kdLast.HighestPrice + kdLast.LowestPrice);
	if (pNL)	*pNL = (dTP - kdLast.HighestPrice + dTP);
	store_to_cache(nIndex, pAH, pNH, pAL, pNL);
	return true;
}
