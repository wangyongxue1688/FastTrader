#include "stdafx.h"
#include "CYO.h"
#include "Tech.h"
#include "../FacilityBaseLib/KData.h"
#include "../FacilityBaseLib/Express.h"

//////////////////////////////////////////////////////////////////////
//	CCYO
CCYO::CCYO()
{
	SetDefaultParameters();
}

CCYO::CCYO(KdataContainer * pKData)
	: TechnicalIndicator(pKData)
{
	SetDefaultParameters();
}

CCYO::~CCYO()
{
	clear();
}

void CCYO::SetDefaultParameters()
{
	m_adwMTMDays.clear();
	m_adwMTMDays.push_back(9);
	m_adwMTMDays.push_back(12);
	m_adwMTMDays.push_back(18);
	m_adwMTMDays.push_back(24);
	m_nMADays = 6;
	m_itsGoldenFork = ITS_BUY;
	m_itsDeadFork = ITS_SELL;
}

void CCYO::AttachParameters(CCYO & src)
{
	m_adwMTMDays/*.copy*/=(src.m_adwMTMDays);
	m_nMADays = src.m_nMADays;
	m_itsGoldenFork = src.m_itsGoldenFork;
	m_itsDeadFork = src.m_itsDeadFork;
}

bool CCYO::IsValidParameters()
{
	STT_VALID_DAYSARRAY(m_adwMTMDays);
	return (VALID_DAYS(m_nMADays) && VALID_ITS(m_itsGoldenFork) && VALID_ITS(m_itsDeadFork));
}

void CCYO::clear()
{
	TechnicalIndicator::clear();
}

int CCYO::signal(size_t nIndex, uint32_t * pnCode)
{
	if (pnCode)	*pnCode = ITSC_NOTHING;

	int	nMaxDays = AfxGetMaxDays(m_adwMTMDays) + m_nMADays;
	double	dLiminalLow = 0, dLiminalHigh = 0;
	if (!intensity_prepare(nIndex, pnCode, nMaxDays, ITS_GETMINMAXDAYRANGE, &dLiminalLow, &dLiminalHigh, 0.2, 0.6))
		return ITS_NOTHING;

	if (nIndex <= 1)
		return ITS_NOTHING;

	double	dValue;
	if (!calc(&dValue, nIndex, false))
		return ITS_NOTHING;

	int	nSignal = GetForkSignal(nIndex, m_itsGoldenFork, m_itsDeadFork, pnCode);
	if (dValue < dLiminalLow && nSignal == m_itsGoldenFork)
	{	// 低位金叉
		if (pnCode)	*pnCode = ITSC_GOLDENFORK;
		return m_itsGoldenFork;
	}
	if (dValue > dLiminalHigh && nSignal == m_itsDeadFork)
	{	// 高位死叉
		if (pnCode)	*pnCode = ITSC_DEADFORK;
		return m_itsDeadFork;
	}

	return ITS_NOTHING;
}

bool CCYO::min_max_info(size_t nStart, size_t nEnd,
	double *pdMin, double *pdMax)
{
	return AfxGetMinMaxInfo2(nStart, nEnd, pdMin, pdMax, this);
}

/***
9日MTM + 12日MTM×2 + 18日MTM×3 + 24日MTM×4
CYO = ------------------------------------------------
10
*/
bool CCYO::calc(double * pValue, size_t nIndex, bool bUseLast)
{
	STT_ASSERT_CALCULATE1(m_pKData, nIndex);

	if (load_from_cache(nIndex, pValue))
		return true;

	uint32_t	nMaxDays = AfxGetMaxDays(m_adwMTMDays);
	if ((int)nMaxDays > nIndex)
		return false;

	double	dValue = 0;
	int	nCount = 0;
	for (size_t m = 0; m<m_adwMTMDays.size(); m++)
	{
		if (int(nIndex - m_adwMTMDays[m]) < 0)
			return false;

		double dMTM = 100;
		if (m_pKData->MaindataAt(nIndex - m_adwMTMDays[m]) > 0)
			dMTM = 100. * m_pKData->MaindataAt(nIndex) / m_pKData->MaindataAt(nIndex - m_adwMTMDays[m]);

		dValue += dMTM*(m + 1);
		nCount += (m + 1);
	}

	if (nCount <= 0)
		return false;

	dValue = dValue / nCount;

	if (pValue)
		*pValue = dValue;
	store_to_cache(nIndex, pValue);
	return true;
}

/***
计算CYO及其移动平均值
*/
bool CCYO::calc(double * pValue, double * pMA, size_t nIndex, bool bUseLast)
{
	return calc_ma(pValue, pMA, nIndex, bUseLast, m_nMADays);
}

