#include "PV.h"
#include "Tech.h"
#include "../FacilityBaseLib/KData.h"


//////////////////////////////////////////////////////////////////////
//	CPV
CPV::CPV( )
{
	SetDefaultParameters( );
}

CPV::CPV( KdataContainer * pKData )
	: TechnicalIndicator( pKData )
{
	SetDefaultParameters( );
}

CPV::~CPV()
{
	clear( );
}

void CPV::SetDefaultParameters( )
{
}

void CPV::attach( CPV & src )
{
}

bool CPV::IsValidParameters( )
{
	return true;
}

void CPV::clear( )
{
	TechnicalIndicator::clear( );
}

int CPV::signal( int nIndex, UINT * pnCode )
{
	if( pnCode )	*pnCode	=	ITSC_NOTHING;
	// 无买卖信号
	return	ITS_NOTHING;
}

bool CPV::min_max_info(int nStart, int nEnd,
				   double *pdMin, double *pdMax )
{
	return AfxGetMinMaxInfo1( nStart, nEnd, pdMin, pdMax, this );
}

/***
	PV就是当日成交均价，成交额除以成交量
*/
bool CPV::calc( double * pValue, int nIndex, bool bUseLast )
{
	STT_ASSERT_CALCULATE1( m_pKData, nIndex );

	if( load_from_cache( nIndex, pValue ) )
		return true;

	KDATA	kd	=	m_pKData->at(nIndex);
	if( kd.Volume <= 1e-4 || kd.Turnover <= 1e-4 )
		return  false;
	
	int		nCount	=	0;
	double	average	=	((double)(kd.Turnover)) / kd.Volume;
	while( average < kd.LowestPrice && nCount < 10 )	{	average	*=	10;	nCount ++;	}
	while( average > kd.HighestPrice && nCount < 20 )	{	average	/=	10;	nCount ++;	}
	if( average < kd.LowestPrice )		//	说明是指数
		average	=	(kd.OpenPrice+kd.HighestPrice+kd.LowestPrice+kd.ClosePrice)/4;

	double	dPV	=	average;
	if( pValue )
		*pValue	=	dPV;

	store_to_cache( nIndex, pValue );
	return true;
}
