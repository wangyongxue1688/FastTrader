#include "stdafx.h"
#include "DCYO.h"
#include "Tech.h"
#include "../FacilityBaseLib/KData.h"
#include "../FacilityBaseLib/Express.h"

//////////////////////////////////////////////////////////////////////
//	CDCYO
CDCYO::CDCYO()
{
	SetDefaultParameters();
}

CDCYO::CDCYO(KdataContainer * pKData)
	: TechnicalIndicator(pKData)
{
	SetDefaultParameters();
}

CDCYO::~CDCYO()
{
	clear();
}

void CDCYO::SetDefaultParameters()
{
	m_adwMTMDays.clear();
	m_adwMTMDays.push_back(9);
	m_adwMTMDays.push_back(12);
	m_adwMTMDays.push_back(18);
	m_adwMTMDays.push_back(24);
	m_nMADays = 6;
	m_itsGoldenFork = ITS_BUY;
	m_itsDeadFork = ITS_SELL;
}

void CDCYO::AttachParameters(CDCYO & src)
{
	m_adwMTMDays/*.Copy*/=(src.m_adwMTMDays);
	m_nMADays = src.m_nMADays;
	m_itsGoldenFork = src.m_itsGoldenFork;
	m_itsDeadFork = src.m_itsDeadFork;
}

bool CDCYO::IsValidParameters()
{
	STT_VALID_DAYSARRAY(m_adwMTMDays);
	return (VALID_DAYS(m_nMADays)
		&& VALID_ITS(m_itsGoldenFork) && VALID_ITS(m_itsDeadFork));
}

void CDCYO::clear()
{
	TechnicalIndicator::clear();
}

int CDCYO::signal(size_t nIndex, uint32_t * pnCode)
{
	if (pnCode)	*pnCode = ITSC_NOTHING;

	int	nMaxDays = AfxGetMaxDays(m_adwMTMDays) + m_nMADays;
	double	dLiminalLow = 0, dLiminalHigh = 0;
	if (!intensity_prepare(nIndex, pnCode, nMaxDays, ITS_GETMINMAXDAYRANGE, &dLiminalLow, &dLiminalHigh, 0.45, 0.55))
		return ITS_NOTHING;

	if (nIndex <= 1)
		return ITS_NOTHING;

	double	dValue;
	if (!calc(&dValue, nIndex, false))
		return ITS_NOTHING;

	int	nSignal = GetForkSignal(nIndex, m_itsGoldenFork, m_itsDeadFork, pnCode);
	if (dValue < dLiminalLow && nSignal == m_itsGoldenFork)
	{	// 低位金叉
		if (pnCode)	*pnCode = ITSC_GOLDENFORK;
		return m_itsGoldenFork;
	}
	if (dValue > dLiminalHigh && nSignal == m_itsDeadFork)
	{	// 高位死叉
		if (pnCode)	*pnCode = ITSC_DEADFORK;
		return m_itsDeadFork;
	}

	return ITS_NOTHING;
}

bool CDCYO::min_max_info(size_t nStart, size_t nEnd,
	double *pdMin, double *pdMax)
{
	return AfxGetMinMaxInfo2(nStart, nEnd, pdMin, pdMax, this);
}

/***
9日MTM + 12日MTM×2 + 18日MTM×3 + 24日MTM×4
CYO = ------------------------------------------------
10
DCYO = CYO的m_nMADays日平均值
*/
bool CDCYO::calc(double * pValue, size_t nIndex, bool bUseLast)
{
	STT_ASSERT_CALCULATE1(m_pKData, nIndex);

	if (load_from_cache(nIndex, pValue))
		return true;

	uint32_t	nMaxDays = AfxGetMaxDays(m_adwMTMDays);
	if ((int)nMaxDays > nIndex)
		return false;

	int	nMACount = 0;
	double	dMA = 0;
	for (int k = nIndex; k >= 0; k--)
	{
		double	dValue = 0;
		double	nCount = 0;
		for (size_t m = 0; m<m_adwMTMDays.size(); m++)
		{
			if (int(k - m_adwMTMDays[m]) < 0)
				return false;

			double dMTM = 100;
			if (m_pKData->MaindataAt(k - m_adwMTMDays[m]) > 0)
				dMTM = 100. * m_pKData->MaindataAt(k) / m_pKData->MaindataAt(k - m_adwMTMDays[m]);

			dValue += dMTM*(m + 1);
			nCount += (m + 1);
		}

		if (nCount <= 0)
			return false;

		dValue = dValue / nCount;

		dMA += dValue;
		nMACount++;
		if (nMACount >= m_nMADays)
		{
			dMA = dMA / m_nMADays;
			if (pValue)
				*pValue = dMA;
			store_to_cache(nIndex, pValue);
			return true;
		}
	}
	return false;
}

/***
计算DCYO及其移动平均值
*/
bool CDCYO::calc(double * pValue, double * pMA, size_t nIndex, bool bUseLast)
{
	return calc_ma(pValue, pMA, nIndex, bUseLast, m_nMADays);
}

