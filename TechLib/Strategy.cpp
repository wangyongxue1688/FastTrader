﻿/*
	Cross Platform Core Code.

	Copyright(R) 2001-2002 Balang Software.
	All rights reserved.

	Using:
		class	CRateParam;
		class	COpParam;
		class	CStockOwnContainer;
		class	COpRecordContainer;
		class	CAssetSerialContainer;
		class	CTechInstrument;
		class	CTechInstrumentContainer;
		class	CStrategy;
*/

#include "Strategy.h"
#include "Tech.h"
#include <math.h>
#include "StringMgr.h"
#include "../FacilityBaseLib/Container.h"
#include "../FacilityBaseLib/InstrumentData.h"

/////////////////////////////////////////////////////////////////////////////
// CRateParam

CRateParam::CRateParam( )
{
	SetDefault( );
}

// 交易费用参数是否合法
bool CRateParam::IsValid( )
{
	return (m_dShaa >= 0 && m_dShaa < 0.1
		&& m_dShab >= 0 && m_dShab < 0.1
		&& m_dShafund >= 0 && m_dShafund < 0.1
		&& m_dSzna >= 0 && m_dSzna < 0.1
		&& m_dSznb >= 0 && m_dSznb < 0.1
		&& m_dSznfund >= 0 && m_dSznfund < 0.1 );
}

// 缺省交易费用
void CRateParam::SetDefault( )
{
	m_dShaa		=	0.003;
	m_dShab		=	0.003;
	m_dShafund	=	0.001;
	m_dSzna		=	0.003;
	m_dSznb		=	0.003;
	m_dSznfund	=	0.001;
}

// 保存交易费用至文件
// void CRateParam::Serialize( CArchive &ar )
// {
// 	if( ar.IsStoring( ) )
// 	{
// 		ar << m_dShaa;
// 		ar << m_dShab;
// 		ar << m_dShafund;
// 		ar << m_dSzna;
// 		ar << m_dSznb;
// 		ar << m_dSznfund;
// 	}
// 	else
// 	{
// 		ar >> m_dShaa;
// 		ar >> m_dShab;
// 		ar >> m_dShafund;
// 		ar >> m_dSzna;
// 		ar >> m_dSznb;
// 		ar >> m_dSznfund;
// 	}
// }

// 
double CRateParam::GetRate( instrument_info & info )
{
 	long	stocktype	=	info.get_type();
// 	if( CInstrument::typeshA == stocktype || CInstrument::typeshIndex == stocktype )
// 		return m_dShaa;
// 	else if( CInstrument::typeshB == stocktype )
// 		return m_dShab;
// 	else if( info.IsShangHai() && info.IsFund() )
// 		return m_dShafund;
// 	else if( CStock::typeszA == stocktype || CStock::typeszIndex == stocktype )
// 		return m_dSzna;
// 	else if( CStock::typeszB == stocktype )
// 		return m_dSznb;
// 	else if( info.IsShenZhen() && info.IsFund() )
// 		return m_dSznfund;

	return 0;
}

/////////////////////////////////////////////////////////////////////////////
// COpParam

// 逻辑字符串，全部指标 任一指标
std::string AfxGetLogicString( int nLogic )
{
// 	switch( nLogic )
// 	{
// 	case	COpParam::logicAnd:		return	strategy_logicand;
// 	case	COpParam::logicOr:		return	strategy_logicor;
// 	default:;
// 		assert( false );
// 	}
	return ("");
}


// 仓位字符串
std::string AfxGetStoreDivString( int nStoreDiv )
{
// 	switch( nStoreDiv )
// 	{
// 	case	COpParam::storedivOnce:			return strategy_sdonce;
// 	case	COpParam::storedivTwice:		return strategy_sdtwice;
// 	case	COpParam::storedivThird:		return strategy_sdthird;
// 	case	COpParam::storedivForth:		return strategy_sdforth;
// 	case	COpParam::storedivFifth:		return strategy_sdfifth;
// 	case	COpParam::storedivSixth:		return strategy_sdsixth;
// 	case	COpParam::storedivSeventh:		return strategy_sdseventh;
// 	case	COpParam::storedivEighth:		return strategy_sdeighth;
// 	case	COpParam::storedivNinth:		return strategy_sdninth;
// 	case	COpParam::storedivTenth:		return strategy_sdtenth;
// 	default:
// 		assert( false );
// 	}
	return ("");
}

COpParam::COpParam( )
{
	SetDefault( );
}

// 操作条件是否合法;
bool COpParam::IsValid( )
{
	if( ktypeDay != m_nKType && ktypeWeek != m_nKType && ktypeMonth != m_nKType )
		return false;
	if( m_nMaindataType < kdata_container::mdtypeMin || m_nMaindataType > kdata_container::mdtypeMax )
		return false;
	if( m_nBuyLogic < logicMin || m_nBuyLogic > logicMax )
		return false;
	if( m_nBuyLimit < ITS_MIN || m_nBuyLimit > ITS_MAX )
		return false;
	if( m_dBuyMulti <= 0.89 || m_dBuyMulti > 1.11 )
		return false;
	if( m_nSellLogic < logicMin || m_nSellLogic > logicMax )
		return false;
	if( m_nSellLimit < ITS_MIN || m_nSellLimit > ITS_MAX )
		return false;
	if( m_dSellMulti <= 0.89 || m_dSellMulti > 1.11 )
		return false;
	if( m_atmBegin.size() <= 0 || m_atmEnd.size() <= 0
		|| m_atmBegin.size() != m_atmEnd.size() )
		return false;
	// TIMEZONES
	if( m_nStartAmount < 1000 || m_nStartAmount > 2000000000 )
		return false;
	if( m_nStoreDiv < storedivMin || m_nStoreDiv > COpParam::storedivMax )
		return false;

	if( m_bStopLosing && ( m_dStopLosing <= 0 || m_dStopLosing >= 100 ) )
		return false;
	if( m_bStopProfit && m_dStopProfit <= 0 )
		return false;
	if( m_bLongTrend && ktypeDay != m_nKTypeLong && ktypeWeek != m_nKTypeLong && ktypeMonth != m_nKTypeLong )
		return false;
	if( m_bIndexTrend && ktypeDay != m_nKTypeIndex && ktypeWeek != m_nKTypeIndex && ktypeMonth != m_nKTypeIndex )
		return false;
	return true;
}

// 缺省操作条件
void COpParam::SetDefault( )
{
	m_nKType		=	ktypeDay;
	m_nMaindataType	=	kdata_container::mdtypeClose;
	m_nBuyLogic		=	logicAnd;
	m_nBuyLimit		=	ITS_BUY;
	m_dBuyMulti		=	1.0;
	m_nSellLogic	=	logicOr;
	m_nSellLimit	=	ITS_SELL;
	m_dSellMulti	=	0.98;
	time_t	tmCurrent	=	time(nullptr);
	m_atmBegin.clear();
	m_atmEnd.clear();
	m_atmBegin.push_back( tmCurrent/* - TimeSpan( 365 * 3, 0, 0, 0 ) */-3*365*24*60);
	m_atmEnd.push_back(tmCurrent);
	m_nStartAmount	=	1000000;
	m_nStoreDiv		=	storedivThird;

	m_bStopLosing		=	false;
	m_bStopProfit		=	false;
	m_dStopLosing		=	0.1;
	m_dStopProfit		=	0.3;
	m_bLongTrend		=	false;
	m_bIndexTrend		=	false;
	m_nKTypeLong	=	ktypeWeek;
	m_nKTypeIndex	=	ktypeWeek;
}

// 设定不合法的操作条件为缺省值;
void COpParam::SetDefaultOfInvalidMember( )
{
	if( ktypeDay != m_nKType && ktypeWeek != m_nKType && ktypeMonth != m_nKType )
		m_nKType		=	ktypeDay;
	if( m_nMaindataType < kdata_container::mdtypeMin || m_nMaindataType > kdata_container::mdtypeMax )
		m_nMaindataType	=	kdata_container::mdtypeClose;
	if( m_nBuyLogic < logicMin || m_nBuyLogic > logicMax )
		m_nBuyLogic	=	logicAnd;
	if( m_nBuyLimit < ITS_MIN || m_nBuyLimit > ITS_MAX )
		m_nBuyLimit		=	ITS_BUY;
	if( m_dBuyMulti <= 0.89 || m_dBuyMulti > 1.11 )
		m_dBuyMulti		=	1.0;
	if( m_nSellLogic < logicMin || m_nSellLogic > logicMax )
		m_nSellLogic	=	logicOr;
	if( m_nSellLimit < ITS_MIN || m_nSellLimit > ITS_MAX )
		m_nSellLimit	=	ITS_SELL;
	if( m_dSellMulti <= 0.89 || m_dSellMulti > 1.11 )
		m_dSellMulti	=	0.98;
	if( m_atmBegin.size() <= 0 || m_atmEnd.size() <= 0
		|| m_atmBegin.size() != m_atmEnd.size() )
	{
		m_atmBegin.clear();
		m_atmEnd.clear();
		time_t	tmCurrent	=	time(nullptr);
		m_atmBegin.push_back( tmCurrent - 365 * 3 * 24 * 60 );
		m_atmEnd.push_back( tmCurrent );
	}
	// TIMEZONES
	if( m_nStartAmount < 1000 || m_nStartAmount > 2000000000 )
		m_nStartAmount	=	1000000;
	if( m_nStoreDiv < storedivMin || m_nStoreDiv > storedivMax )
		m_nStoreDiv		=	storedivThird;

	if( m_bStopLosing && ( m_dStopLosing <= 0 || m_dStopLosing >= 100 ) )
		m_dStopLosing	=	0.1;
	if( m_bStopProfit && m_dStopProfit <= 0 )
		m_dStopProfit	=	0.3;
	if( m_bLongTrend && ktypeDay != m_nKTypeLong && ktypeWeek != m_nKTypeLong && ktypeMonth != m_nKTypeLong )
		m_nKTypeLong		=	ktypeWeek;
	if( m_bIndexTrend && ktypeDay != m_nKTypeIndex && ktypeWeek != m_nKTypeIndex && ktypeMonth != m_nKTypeIndex )
		m_nKTypeIndex		=	ktypeWeek;
}

// 保存或者读取硬盘文件
// void COpParam::Serialize( CArchive &ar )
// {
// 	if( ar.IsStoring( ) )
// 	{
// 		ar << m_nKType;
// 		ar << m_nMaindataType;
// 		ar << m_nBuyLogic;
// 		ar << m_nBuyLimit;
// 		ar << m_dBuyMulti;
// 		ar << m_nSellLogic;
// 		ar << m_nSellLimit;
// 		ar << m_dSellMulti;
// 		m_atmBegin.Serialize( ar );
// 		m_atmEnd.Serialize( ar );
// 		ar << m_nStartAmount;
// 		ar << m_nStoreDiv;
// 
// 		ar << m_bStopLosing;
// 		ar << m_bStopProfit;
// 		ar << m_dStopLosing;
// 		ar << m_dStopProfit;
// 		ar << m_bLongTrend;
// 		ar << m_bIndexTrend;
// 		ar << m_nKTypeLong;
// 		ar << m_nKTypeIndex;
// 	}
// 	else
// 	{
// 		ar >> m_nKType;
// 		ar >> m_nMaindataType;
// 		ar >> m_nBuyLogic;
// 		ar >> m_nBuyLimit;
// 		ar >> m_dBuyMulti;
// 		ar >> m_nSellLogic;
// 		ar >> m_nSellLimit;
// 		ar >> m_dSellMulti;
// 		m_atmBegin.Serialize( ar );
// 		m_atmEnd.Serialize( ar );
// 		ar >> m_nStartAmount;
// 		ar >> m_nStoreDiv;
// 
// 		ar >> m_bStopLosing;
// 		ar >> m_bStopProfit;
// 		ar >> m_dStopLosing;
// 		ar >> m_dStopProfit;
// 		ar >> m_bLongTrend;
// 		ar >> m_bIndexTrend;
// 		ar >> m_nKTypeLong;
// 		ar >> m_nKTypeIndex;
// 	}
// }

// 给定时间是不是在模拟时间内
bool COpParam::IsInTimeZones( time_t tm )
{
	for( size_t i=0; i<m_atmBegin.size() && i<m_atmEnd.size(); i++ )
	{
		if( tm >= m_atmBegin[i] && tm <= m_atmEnd[i] )
			return true;
	}
	return false;
}

// 获取模拟开始时间
time_t COpParam::GetBeginTime( )
{
	if( m_atmBegin.size() > 0 )
		return m_atmBegin.front();
	return time(nullptr);
}

// 获取模拟结束时间
time_t COpParam::GetEndTime()
{
	if( m_atmEnd.size() > 0 )
		return m_atmEnd[m_atmEnd.size()-1];
	return time(nullptr);
}

// 获取下一个交易时间
bool COpParam::GetNextTradeTime(time_t tmNow, time_t &tmNext)
{
// 	DateTime	sptime( tmNow );
// 	DWORD	dwDate		=	sptime.ToInstrumentTime( kdata_container::IsDayOrMin(m_nKType) );
// 	DWORD	dwDateNext	=	DateTime::GetInstrumentTimeNext( dwDate, m_nKType );
// 	if( sptime.FromInstrumentTime( dwDateNext, kdata_container::IsDayOrMin(m_nKType) ) )
// 	{
// 		tmNext	=	sptime.GetTime();
// 		return true;
// 	}
	return false;
}

// 获取当前模拟进度
DWORD COpParam::GetProgress( time_t tmNow, DWORD dwProgressMax )
{
	// TIMEZONES
	if( !IsInTimeZones( tmNow ) )
		return 0;
	time_t	tmBegin	=	GetBeginTime( );
	time_t	tmEnd = GetEndTime();
	double	dBeginNow	=	( tmNow - tmBegin );
	double	dBeginEnd	=	( tmEnd - tmBegin );
	double	dProgress	=	0;
	if( fabs(dBeginEnd) > 1e-4 )
	{
		dProgress	=	( dBeginNow / dBeginEnd ) * dwProgressMax;
	}
	return (DWORD)dProgress;
}

/////////////////////////////////////////////////////////////////////////////
// CStockOwnContainer

// 拥有股票数组，加入股票
bool CInstrumentOwnContainer::AddInstruemnt( instrument_info & info, DWORD dwShare, double dBuyPrice )
{
	if( dwShare <= 0 )
		return false;

	// 如果已经有了，改变数量
	for( size_t i=0; i<size(); i++ )
	{
		INSTRUMENTOWN	own	=	ElementAt(i);
		if( info.is_equal( own.szMarket, own.szCode ) )
		{
			if( dBuyPrice > 0.005 && own.dwShare + dwShare > 0 )
			{
				own.dBuyPrice	=	(own.dBuyPrice * own.dwShare + dBuyPrice * dwShare) / (own.dwShare + dwShare);
			}
			own.dwShare	+=	dwShare;
			return true;
		}
	}
	// 加入新的
	INSTRUMENTOWN	ownnew;
	memset( &ownnew, 0, sizeof(ownnew) );
	strncpy( ownnew.szCode, info.get_id(), min(sizeof(ownnew.szCode)-1,strlen(info.get_id())) );
	ownnew.dwShare		=	dwShare;
	ownnew.dBuyPrice	=	dBuyPrice;
	//ownnew.dwMarket		=	info.GetMarket();
	push_back(ownnew);
	return (  size()>= 0 );
}
// 
// 拥有股票数组，移除股票
bool CInstrumentOwnContainer::RemoveInstrument( instrument_info & info, DWORD dwShare )
{
	for( size_t i=0; i<size(); i++ )
	{
		INSTRUMENTOWN	own	=	ElementAt(i);
		if( info.is_equal( own.szMarket, own.szCode ) )
		{
			if( own.dwShare < dwShare )
				return false;
			own.dwShare	-=	dwShare;
			if( 0 == own.dwShare )
				erase(begin()+i);
			return true;
		}
	}
	return false;
}
// 
// 拥有股票数组，是否有这个股票，如果有，返回至lpOwn
bool CInstrumentOwnContainer::HasThisInstrument( instrument_info & info, LPINSTRUMENTOWN lpOwn )
{
	for( size_t i=0; i<size(); i++ )
	{
		INSTRUMENTOWN	own	=	ElementAt(i);
		if( info.is_equal( own.szMarket, own.szCode ) )
		{
			if( lpOwn )
				memcpy( lpOwn, &own, sizeof(own) );
			return ( own.dwShare > 0 );
		}
	}
	return false;
}

/////////////////////////////////////////////////////////////////////////////
// COpRecordContainer
// 操作描述
string	AfxGetStrategyOpTypeString( long lOpType )
{
	string	strOp;
// 	if( STRATEGY_OPTYPE_BUY == lOpType )
// 		return strategy_optype_buy;
// 	else if( STRATEGY_OPTYPE_SELL == lOpType )
// 		return strategy_optype_sell;
// 	else if( STRATEGY_OPTYPE_ADDSTOCK == lOpType )
// 		return strategy_optype_addstock;
// 	else if( STRATEGY_OPTYPE_REMOVESTOCK == lOpType )
// 		return strategy_optype_removestock;
// 	else if( STRATEGY_OPTYPE_ADDCASH == lOpType )
// 		return strategy_optype_addcash;
// 	else if( STRATEGY_OPTYPE_REMOVECASH == lOpType )
// 		return strategy_optype_removecash;
	return strOp;
}

// 操作记录数组，加入记录
int COpRecordContainer::AddRecord(long lOpType, time_t tm, const char *szCode,
								   DWORD dwShare, double dSharePrice, double dRateCost )
{
	assert( NULL != szCode && strlen(szCode) > 0 );
	if( NULL == szCode || strlen(szCode) <= 0 )
		return -1;

	OPRECORD	record;
	memset( &record, 0, sizeof(record) );

	record.lOpType		=	lOpType;
	record.time			=	tm/*.GetTime()*/;
	strncpy( record.szCode, szCode, min(sizeof(record.szCode)-1,strlen(szCode)) );
	record.dwShare		=	dwShare;
	record.dSharePrice	=	dSharePrice;
	record.dRateCost	=	dRateCost;
	//record.dwMarket		=	CStock::marketUnknown;
	return Add( record );
}

// 操作记录数组，加入记录，记录中如果已经有该股票的记录，更改为现在的信息
int COpRecordContainer::AddRecordUniqueStock(long lOpType, time_t tm, const char *szCode,
								   DWORD dwShare, double dSharePrice, double dRateCost )
{
	//ASSERT( NULL != szCode && strlen(szCode) > 0 );
	if( NULL == szCode || strlen(szCode) <= 0 )
		return -1;

	OPRECORD	record;
	memset( &record, 0, sizeof(record) );

	for( size_t i=0; i<GetSize(); i++ )
	{
		if( 0 == strncmp( ElementAt(i).szCode, szCode, sizeof(record.szCode) ) )
		{
			ElementAt(i).lOpType		=	lOpType;
			ElementAt(i).time			=	tm/*.GetTime()*/;
			strncpy( ElementAt(i).szCode, szCode, min(sizeof(ElementAt(i).szCode)-1,strlen(szCode)) );
			ElementAt(i).dwShare		=	dwShare;
			ElementAt(i).dSharePrice	=	dSharePrice;
			ElementAt(i).dRateCost		=	dRateCost;
			//ElementAt(i).dwMarket		=	CStock::marketUnknown;
			return i;
		}
	}

	record.lOpType		=	lOpType;
	record.time			=	tm/*.GetTime()*/;
	strncpy( record.szCode, szCode, min(sizeof(record.szCode)-1,strlen(szCode)) );
	record.dwShare		=	dwShare;
	record.dSharePrice	=	dSharePrice;
	record.dRateCost	=	dRateCost;
	//record.dwMarket		=	CStock::marketUnknown;
	return Add( record );
}

// 操作记录数组，得到买入记录数
int COpRecordContainer::GetBuyRecordCount()
{
	int	nCount	=	0;
	for( size_t i=0; i<GetSize(); i++ )
	{
		OPRECORD & record	=	ElementAt(i);
		if( STRATEGY_OPTYPE_BUY == record.lOpType )
			nCount	++;
	}
	return nCount;
}

// 操作记录数组，是否有szCode股票的买入记录
bool COpRecordContainer::HasBuyStock( const char * szCode )
{
	if( NULL == szCode || strlen(szCode) < 0 )
		return false;

	for( size_t i=0; i<GetSize(); i++ )
	{
		OPRECORD & record	=	ElementAt(i);
		if( STRATEGY_OPTYPE_BUY == record.lOpType )
		{
			if( 0 == strncmp( record.szCode, szCode, min(sizeof(record.szCode),strlen(szCode)) ) )
				return true;
		}
	}
	return false;
}

// 操作记录数组，统计结果：操作次数，成功次数，最大收益，最低收益
bool COpRecordContainer::StatResults( int *pnTimes, int *pnVictoryTimes, double *pdYieldMax, double *pdYieldMin )
{
	int	nTimes = 0, nVictoryTimes = 0;
	double	dYieldMax = 0, dYieldMin = 0;

	for( size_t i=0; i<GetSize(); i++ )
	{
		OPRECORD & record	=	ElementAt(i);
		if( STRATEGY_OPTYPE_BUY != record.lOpType )
			continue;

		// 查找当前买入操作的对应卖出操作
		for( size_t j=i+1; j<GetSize(); j++ )
		{
			OPRECORD & record2	=	ElementAt(j);
			if( STRATEGY_OPTYPE_SELL == record2.lOpType && 0 == strncmp(record.szCode,record2.szCode,sizeof(record.szCode)) )
			{	// 如果是卖出操作，根据买入价格和卖出价格计算收益率
				if( record.dwShare > 0 && record2.dwShare > 0 )
				{
					double	dBuy	=	record.dSharePrice + record.dRateCost/record.dwShare;
					double	dSell	=	record2.dSharePrice - record2.dRateCost/record2.dwShare;
					if( dBuy > 1e-4 )
					{
						double	dYield	=	(dSell - dBuy)/dBuy;
						nTimes	++;
						if( dYield > 0 )	nVictoryTimes	++;
						if( 1 == nTimes )	dYieldMax	=	dYieldMin	=	dYield;
						if( dYieldMax < dYield )	dYieldMax	=	dYield;
						if( dYieldMin > dYield )	dYieldMin	=	dYield;
					}
				}
				break;
			}
		}
	}
	if( pnTimes )			*pnTimes		=	nTimes;
	if( pnVictoryTimes )	*pnVictoryTimes	=	nVictoryTimes;
	if( pdYieldMax )		*pdYieldMax		=	dYieldMax;
	if( pdYieldMin )		*pdYieldMin		=	dYieldMin;
	return true;
}

//////////////////////////////////////////////////////////////////////
// CAssetSerialContainer 
// 资产序列数组，按时间排序插入;
bool CAssetSerialContainer::SortInsert( ASSETSERIAL serial )
{
	bool	bAdded	=	false;
	CAssetSerialContainer::reverse_iterator riter=rbegin();
	
	for( ; riter!=rend();  ++riter)
	{
		ASSETSERIAL se	=	*riter;
		if( se.time == serial.time )
		{
			//如果时间相等,则替换;
			riter->dAsset	=	serial.dAsset;
			riter->dCash	=	serial.dCash;
			bAdded		=	true;
			break;
		}
		else if( se.time < serial.time )
		{
			CAssetSerialContainer::iterator iter=(riter.base());
			insert(iter,serial);
			bAdded		=	true;
			break;
		}
	}
	if( !bAdded )
	{
		push_front(serial);
	}
	return true;
}

// 资产序列数组，获得资产序列标准差
bool CAssetSerialContainer::GetStdDev( double *pdStdDev, double *pdXiapu )
{
	if( GetSize() <= 0 )
		return false;
	double	dInit	=	ElementAt(0).dAsset;
	if( dInit < 1e-4 )
		return false;

	double	dSum = 0., dSquareSum = 0.;
	for( size_t k = 0; k<GetSize(); k++ )
	{
		double	dNow	=	(ElementAt(k).dAsset - dInit) / dInit;
		dSum	+=	dNow;
		dSquareSum	+=	dNow*dNow;
	}

	double	dAverage	=	dSum / GetSize();
	if( dAverage < 1e-4 )
		return false;
	double	dStdDev	=	dSquareSum / GetSize() - ( dAverage*dAverage );
	if( dStdDev > 0 )
		dStdDev	=	sqrt( dStdDev );
	if( pdStdDev )	*pdStdDev	=	dStdDev;
	if( pdXiapu )	*pdXiapu	=	(dStdDev / dAverage);
	return true;
}

//////////////////////////////////////////////////////////////////////
// CTechInstrument

CTechInstrument::CTechInstrument( )
{
	m_bAutoDelete	=	true;

	m_kdata.SetKType(ktypeDay);
	m_kdataLong.SetKType(ktypeDay);
}

CTechInstrument::CTechInstrument( const CTechInstrument & src )
{
	*this	=	src;
}

CTechInstrument & CTechInstrument::operator = ( const CTechInstrument &src )
{
	m_bAutoDelete	=	false;

	m_info		=	src.m_info;
	m_kdata		=	src.m_kdata;
	m_kdataLong	=	src.m_kdataLong;
	m_techs=( src.m_techs );
	m_techsLong=( src.m_techsLong );
	return *this;
}

void CTechInstrument::SetAutoDelete( bool bAutoDelete )
{
	m_bAutoDelete	=	bAutoDelete;
}

CTechInstrument::~CTechInstrument( )
{
	Clear( );
}

// 清除内存
void CTechInstrument::Clear( )
{
	if( m_bAutoDelete )
	{
		for( size_t i=0; i<m_techs.size(); i++ )
		{
			technical_indicator * pTech	=	(technical_indicator *)m_techs[i];
			if( pTech )
				delete	pTech;
		}
		for( size_t i=0; i<m_techsLong.size(); i++ )
		{
			technical_indicator * pTech	=	(technical_indicator *)m_techsLong[i];
			if( pTech )
				delete	pTech;
		}
	}
	m_techs.clear();
	m_techsLong.clear();
}

// 指定日期，指定操作，指定委托价格，得到股票的成交价格
bool CTechInstrument::GetPriceOK( long lOpType, time_t tmCur, DWORD dwShare, double dSharePrice, double *pdPriceOK )
{
	double	dPriceOK = dSharePrice;

	//DateTime	sptimeCur(tmCur.GetTime());
	DWORD	dwDate	/*=	sptimeCur.ToInstrumentTimeDay()*/;
	int nIndex = m_kdata.GetIndexByDate( dwDate );
	if( -1 == nIndex )
		return false;

	KDATA	&	kd	=	m_kdata.at(nIndex);
	if( STRATEGY_OPTYPE_BUY == lOpType )
	{
		if( dSharePrice < kd.LowestPrice )
			return false;
		if( dSharePrice > kd.OpenPrice )
			dPriceOK	=	kd.OpenPrice;
		else
			dPriceOK	=	dSharePrice;
	}
	else if( STRATEGY_OPTYPE_SELL == lOpType )
	{
		if( dSharePrice > kd.HighestPrice )
			return false;
		if( dSharePrice < kd.OpenPrice )
			dPriceOK	=	kd.OpenPrice;
		else
			dPriceOK	=	dSharePrice;
	}
	else
		return false;

	if( pdPriceOK )	*pdPriceOK	=	dPriceOK;
	return true;
}

// 指定日期是否停牌
bool CTechInstrument::IsStopTrading( time_t tmCur )
{
	//DateTime	sptimeCur(tmCur.GetTime());
	DWORD	dwDate	/*=	sptimeCur.ToInstrumentTimeDay()*/;
	int nIndex = m_kdata.GetIndexByDate( dwDate );
	if( -1 == nIndex )
	{
		if( -1 != m_kdata.GetAboutIndexByDate( dwDate ) )
			return true;
		return false;
	}

	KDATA	&	kd	=	m_kdata.at(nIndex);
	float	fLastClose	=	kd.OpenPrice;
	if( nIndex > 0 )
		fLastClose	=	m_kdata.at(nIndex-1).ClosePrice;

	if( fabs(kd.OpenPrice-kd.ClosePrice) < 1e-4 && fabs(kd.OpenPrice-kd.HighestPrice) < 1e-4
		&& fabs(kd.OpenPrice-kd.LowestPrice) < 1e-4 && fabs(kd.OpenPrice-fLastClose) < 1e-4 )
		return true;
	return false;
}

// 得到股票的收盘价
bool CTechInstrument::GetClosePrice( time_t tmCur, double *pdPrice )
{
	//DateTime	sptimeCur(tmCur.GetTime());
	DWORD	dwDate/*	=	sptimeCur.ToInstrumentTimeDay()*/;
	int nIndex = m_kdata.GetAboutIndexByDate( dwDate );
	if( -1 == nIndex )
	{
		assert( false );
		return false;
	}

	KDATA	&	kd	=	m_kdata.at(nIndex);
	if( pdPrice )	*pdPrice	=	kd.ClosePrice;
	return true;
}

// 得到股票的收盘价涨幅%
bool CTechInstrument::GetCloseDiffPercent( time_t tmCur, double *pdDiffPercent )
{
	if( pdDiffPercent ) *pdDiffPercent = 0;

	//DateTime	sptimeCur(tmCur.GetTime());
	DWORD	dwDate	/*=	sptimeCur.ToInstrumentTimeDay()*/;
	int nIndex = m_kdata.GetIndexByDate( dwDate );
	if( -1 == nIndex || nIndex <= 0 )
	{
		return false;
	}

	KDATA	&	kd		=	m_kdata.at(nIndex);
	KDATA	&	kdLast	=	m_kdata.at(nIndex-1);
	if( pdDiffPercent && kdLast.ClosePrice > 1e-4 )
	{
		*pdDiffPercent	=	100. * (kd.ClosePrice-kdLast.ClosePrice)/kdLast.ClosePrice;
	}
	return true;
}

/////////////////////////////////////////////////////////////////////////////
// CTechInstrumentContainer

CTechInstrumentContainer::~CTechInstrumentContainer( )
{
	Clear( );
}

void CTechInstrumentContainer::Clear( )
{
	m_infoIndex.clear();
	m_kdataIndex.Clear();
	m_kdataIndex.SetKType(ktypeDay);

	for( size_t i=0; i<m_techsIndex.size(); i++ )
	{
		technical_indicator * pTech	=	(technical_indicator *)m_techsIndex[i];
		if( pTech )
			delete	pTech;
	}
	m_techsIndex.clear();

	for( size_t i=0; i<size(); i++ )
	{
		CTechInstrument	stock	=	ElementAt(i);
		stock.Clear( );
	}
	clear();
}

// 给定CTechInstrument，日期tmCur，资金dCash，交易费率rate，操作条件opparam，
// 现在拥有股票数量nOwnedStockCount，计算需要买入的股票数量和委托价格及委托时间，
// 如果不需要买入，返回false
bool CTechInstrumentContainer::GetShouldBuyShare( CTechInstrument & techstock, time_t tmCur, double dCash, CRateParam &rate, COpParam &opparam, int nOwnedStockCount,
	time_t *ptmOp, DWORD *pdwShare, double *pdSharePrice)
{
	time_t	tmOp = tmCur;
	DWORD	dwShare = 0;
	double	dSharePrice = 0;

	//DateTime	sptimeCur(tmCur/*.GetTime()*/);
	DWORD	dwDate	/*=	sptimeCur.ToInstrumentTimeDay()*/;
	int nIndex = techstock.m_kdata.GetIndexByDate( dwDate );	// 得到nIndex，指向techstock.m_kdata数组的当前日期位置
	if( -1 == nIndex )
		return false;
	double	dPriceNow	=	techstock.m_kdata.at(nIndex).ClosePrice;	// 当前价

	// 买入判断
	bool	bBuy	=	true;
	if( COpParam::logicOr == opparam.m_nBuyLogic )	// 全部条件还是任一条件
		bBuy	=	false;
	for( size_t i=0; i<techstock.m_techs.size(); i++ )	// 每一个设定指标，分别判断
	{
		technical_indicator * pTech	=	(technical_indicator *)techstock.m_techs[i];	// 技术指标
		assert( NULL != pTech );
		if( NULL == pTech )
			continue;

		int nIntensity	=	pTech->signal( nIndex );		// 当前买卖信号
		if( COpParam::logicOr == opparam.m_nBuyLogic )
		{
			if( nIntensity >= opparam.m_nBuyLimit )			// 买卖信号是否达到所需条件
				bBuy	=	true;
		}
		else
		{
			if( nIntensity < opparam.m_nBuyLimit )
				bBuy	=	false;
		}
	}

	// Long and Index Trend
	if( bBuy )
	{
		// 如果技术指标判断为买入，判断是否满足长周期趋势和指数趋势
		if( opparam.m_bLongTrend )	// 长周期趋势
		{
			int	nIndexLong	=	techstock.m_kdataLong.GetAboutIndexByDate( dwDate );
			if( opparam.m_nKTypeLong > opparam.m_nKType )
				nIndexLong	--;
			if( nIndexLong >= 0 )
			{
				for( size_t i=0; i<techstock.m_techsLong.size(); i++ )
				{
					technical_indicator * pTech	=	(technical_indicator *)techstock.m_techsLong[i];
					assert( NULL != pTech );
					if( NULL == pTech )
						continue;
					pTech->clear_last_intensity();
					int nIntensity	=	pTech->intensity( nIndexLong );	// 得到当前趋势
					if( !ITS_ISBUY(nIntensity) )
						bBuy	=	false;
				}
			}
		}
		if( opparam.m_bIndexTrend )	// 指数趋势
		{
			int	nIndexIndex	=	m_kdataIndex.GetAboutIndexByDate( dwDate );
			if( opparam.m_nKTypeIndex > opparam.m_nKType )
				nIndexIndex	--;
			if( nIndexIndex >= 0 )
			{
				for( size_t i=0; i<m_techsIndex.size(); i++ )
				{
					technical_indicator * pTech	=	(technical_indicator *)m_techsIndex[i];
					assert( NULL != pTech );
					if( NULL == pTech )
						continue;
					pTech->clear_last_intensity();
					int nIntensity	=	pTech->intensity( nIndexIndex );
					if( !ITS_ISBUY(nIntensity) )
						bBuy	=	false;
				}
			}
		}
	}

	if( bBuy )	// 确定买入了
	{
		if( !opparam.GetNextTradeTime(tmCur, tmOp) )	// 操作日期，下一个交易日
			return false;

		if( opparam.m_nStoreDiv-nOwnedStockCount <= 0 )	// 股票已经够多了，不能再买入新的了
			return false;
		double	dUseCash	=	dCash / (opparam.m_nStoreDiv-nOwnedStockCount);	// 需使用资金

		if( dCash < dUseCash )
			dUseCash	=	dCash;	// 资金不够，则有多少用多少

		dSharePrice		=	opparam.m_dBuyMulti * dPriceNow;
		double	dTemp	=	dSharePrice * ( 1 + rate.GetRate( techstock.m_info ) );
		if( fabs(dTemp) < 1e-4 || dUseCash < 1e-4 )
			return false;

		dwShare		=	(DWORD)( dUseCash / dTemp );	// 买入股数
		dwShare		=	( dwShare / 100 ) * 100;		// 取整
		if( 0 == dwShare )
			return false;

		if( ptmOp )			*ptmOp		=	tmOp;
		if( pdwShare )		*pdwShare	=	dwShare;
		if( pdSharePrice )	*pdSharePrice	=	dSharePrice;
		return true;
	}
	
	return false;
}

// 给定CTechInstrument，日期tmCur，资金dCash，拥有股票own(含有买入时价格)，操作条件opparam，
// 现在拥有股票数量nOwnedStockCount，计算需要卖出的股票数量和委托价格及委托时间，
// 如果不需要卖出，返回false
bool CTechInstrumentContainer::GetShouldSellShare( CTechInstrument & techstock, time_t tmCur, INSTRUMENTOWN & own, COpParam &opparam,
	time_t *ptmOp, DWORD *pdwShare, double *pdSharePrice)
{
	time_t	tmOp = tmCur;
	DWORD	dwShare = 0;
	double	dSharePrice = 0;

	//DateTime	sptimeCur(tmCur/*.GetTime()*/);
	DWORD	dwDate	/*=	sptimeCur.ToInstrumentTimeDay()*/;
	int nIndex = techstock.m_kdata.GetIndexByDate( dwDate );	// 得到nIndex，指向techstock.m_kdata数组的当前日期位置
	if( -1 == nIndex )
		return false;
	double	dPriceNow	=	techstock.m_kdata.at(nIndex).ClosePrice;	// 当前价

	// 卖出判断
	bool	bSell	=	false;
	if( COpParam::logicAnd == opparam.m_nSellLogic )	// 全部条件还是任一条件
		bSell	=	true;
	for( size_t i=0; i<techstock.m_techs.size(); i++ )	// 每一个技术指标，分别判断
	{
		technical_indicator * pTech	=	(technical_indicator *)techstock.m_techs[i];
		assert( NULL != pTech );
		if( NULL == pTech )
			continue;

		int nIntensity	=	pTech->signal( nIndex );		// 当前买卖信号
		if( COpParam::logicAnd == opparam.m_nSellLogic )
		{
			if( nIntensity > opparam.m_nSellLimit )			// 买卖信号是否达到所需条件
				bSell	=	false;
		}
		else
		{
			if( nIntensity <= opparam.m_nSellLimit )
				bSell	=	true;
		}
	}

	// StopLosing and StopProfit 止损和止赢
	if( opparam.m_bStopLosing && dPriceNow < own.dBuyPrice * (1-opparam.m_dStopLosing) )	// 止损
		bSell	=	true;
	if( opparam.m_bStopProfit && dPriceNow > own.dBuyPrice * (1+opparam.m_dStopProfit) )	// 止赢
		bSell	=	true;

	if( bSell )	// 确定卖出了
	{
		if( !opparam.GetNextTradeTime(tmCur, tmOp) )	// 操作日期，下一个交易日
			return false;
		dwShare			=	own.dwShare;
		dSharePrice		=	opparam.m_dSellMulti * dPriceNow;

		if( ptmOp )			*ptmOp		=	tmOp;
		if( pdwShare )		*pdwShare	=	dwShare;
		if( pdSharePrice )	*pdSharePrice	=	dSharePrice;
		return true;
	}
	
	return false;
}

// 得到某只股票szCode在日期tmCur的收盘价
bool CTechInstrumentContainer::GetClosePrice( const char * szCode, time_t tmCur, double * pdPrice )
{
	if( NULL == szCode || strlen(szCode) <= 0 || size() <= 0 )
		return false;

	for( size_t j=0; j<size(); j++ )
	{
		CTechInstrument techstock	=	(*this)[j];
		if( techstock.m_info.is_equal( InstrumentData::marketUnknown, szCode ) )
		{
			return techstock.GetClosePrice( tmCur, pdPrice );
		}
	}

	return false;
}

// 得到某日的总资产
bool CTechInstrumentContainer::GetSumAsset( time_t tmCur, CInstrumentOwnContainer &container, double * pdAsset )
{
	if( size() <= 0 )
		return false;

	double	dAsset	=	0;
	for( int i=0; i<container.GetSize(); i++ )
	{
		INSTRUMENTOWN	own	=	container.ElementAt(i);
		double	dPrice	=	0;
		if( !GetClosePrice( own.szCode, tmCur, &dPrice ) )
			return false;
		dAsset	+=	dPrice * own.dwShare;
	}
	
	if( pdAsset )	*pdAsset	=	dAsset;
	return true;
}

// 得到下一个有成交量的交易日
bool CTechInstrumentContainer::GetNextExistTradeTime(time_t tmCur, time_t & tmNext)
{
	//DateTime	sptime(tmCur/*.GetTime()*/);
	DWORD	dateCur		/*=	sptime.ToInstrumentTimeDay( )*/;
	DWORD	dateNext	=	-1;
	for( size_t i=0; i<size(); i++ )
	{
		CTechInstrument	techstock	=	at(i);
		int nIndex	=	techstock.m_kdata.GetAboutIndexByDate( dateCur );
		if( -1 != nIndex && nIndex+1 < techstock.m_kdata.size() )
		{
			if( -1 == dateNext )
				dateNext	=	techstock.m_kdata.at(nIndex+1).TradingDate;
			else if( dateNext > techstock.m_kdata.at(nIndex+1).TradingDate )
				dateNext	=	techstock.m_kdata.at(nIndex+1).TradingDate;
		}
	}
// 	if( -1 != dateNext && sptime.FromInstrumentTimeDay( dateNext ) )
// 	{
// 		tmNext	=	sptime.GetTime();
// 		return true;
// 	}
	return false;
}

/////////////////////////////////////////////////////////////////////////////
// CStrategy

// 给定策略文件，得到策略名称
string CStrategy::GetName( const char* lpszPath )
{
	if( NULL == lpszPath || strlen(lpszPath) <= 0 )
		return ("");

	string	strName;
	CStrategy		doc;
// 	CFile	file;
// 	if( file.Open( lpszPath, CFile::modeRead) )
// 	{
// 		CArchive ar(&file, CArchive::load | CArchive::bNoFlushOnDelete);
// 		if( file.GetLength() > 0 )
// 			doc.Serialize( ar, NULL, 0 );
// 
// 		ar.Close();
// 		file.Close();
// 		strName	=	doc.m_strName;
// 	}

	if( strName.empty() )
		strName	=	lpszPath;
	return strName;
}

/////////////////////////////////////////////////////////////////////////////
// CStrategy construction/destruction

char szCELFileMagic[]	=	"BALANG CEL FILE.";
DWORD	dwCELFileVersion		=	0x20000000;

CStrategy::CStrategy()
{
	SimuReset( );
	RealReset( );
}

CStrategy::~CStrategy()
{
	ClearCache( );
}

// 打开策略文件
bool CStrategy::OpenStrategyFile( const char* lpszPathName, char* lpErr, UINT nMaxSize )
{
// 	CFile	file;
// 	if( NULL != lpszPathName && file.Open( lpszPathName, CFile::modeRead) )
// 	{
// 		if( 0 == file.GetLength() )
// 		{
// 			SetPathName(lpszPathName);
// 			return true;
// 		}
// 
// 		CArchive ar(&file, CArchive::load | CArchive::bNoFlushOnDelete);
// 		bool bSuccess = Serialize( ar, lpErr, nMaxSize );
// 		ar.Close();
// 		file.Close();
// 		if( bSuccess )
// 			SetPathName(lpszPathName);
// 		return bSuccess;
// 	}
// 	
// 	SetPathName( NULL );
	return false;
}

bool CStrategy::SaveStrategyFile( const char* lpszPathName )
{
	string newName;
	if( lpszPathName )
		newName	=	lpszPathName;
	if (newName.empty())
		newName = m_strPathName;

// 	CFile	file;
// 	if( file.Open( newName, CFile::modeCreate | CFile::modeWrite) )
// 	{
// 		CArchive ar(&file, CArchive::store);
// 		Serialize( ar, NULL, 0 );
// 		ar.Close();
// 		file.Close();
// 		SetPathName(newName);
// 		return true;
// 	}

	return true;
}

/////////////////////////////////////////////////////////////////////////////
// CStrategy serialization

// 文件保存和读取
// bool CStrategy::Serialize(CArchive& ar, char* lpErr, UINT nMaxSize )
// {
// 	if( lpErr && nMaxSize>0 )
// 		memset(lpErr,0,nMaxSize);
// 	char	strategy_errfile[]			=	"文件类型不符，指定文件不是策略文件。";
// 	char	strategy_errfilever[]		=	"错误的文件版本：请升级软件，以便可以读取高版本的策略文件。";
//  	if (ar.IsStoring())
//  	{
//  		ar.Write( szCELFileMagic, sizeof(szCELFileMagic) );
//  		
//  		ar << dwCELFileVersion;
//  		ar << m_strName;
//  		ar << m_strDescript;
//  		m_stocks.Serialize( ar );
//  
//  		m_rate.Serialize( ar );
//  
//  		m_paramBuy.Serialize( ar );
//  		m_paramSell.Serialize( ar );
//  		m_paramLongTrend.Serialize( ar );
//  		m_paramIndexTrend.Serialize( ar );
//  
//  		m_anTechsBuy.Serialize( ar );
//  		m_anTechsSell.Serialize( ar );
//  		m_anTechsLongTrend.Serialize( ar );
//  		m_anTechsIndexTrend.Serialize( ar );
//  
//  		m_opparam.Serialize( ar );
//  
//  		// Simulation
//  		//ar << m_SimuCurrentStatus;
//  		//ar << m_SimuCurrentTime;
//  		//ar << m_SimuCurrentCash;
//  		//m_SimuStockOwn.Serialize( ar );
//  		m_SimuOpRecord.Serialize( ar );
//  		m_SimuNextOp.Serialize( ar );
//  		m_SimuAssetSerial.Serialize( ar );
//  
//  		// Real Operation
//  		//ar << m_RealBeginTime;
//  		//ar << m_RealCurrentTime;
//  		//ar << m_RealCurrentCash;
//  		//m_RealStockOwn.Serialize( ar );
//  		m_RealOpRecord.Serialize( ar );
//  		m_RealNextOp.Serialize( ar );
//  		m_RealAssetSerial.Serialize( ar );
//  	}
//  	else
//  	{
//  		char	buffer[sizeof(szCELFileMagic)+1];
//  		if( sizeof(szCELFileMagic) != ar.Read( buffer, sizeof(szCELFileMagic) )
//  			|| 0 != strncmp( buffer, szCELFileMagic, sizeof(szCELFileMagic) ) )
//  		{
//  			if( lpErr )
//  				strncpy(lpErr,strategy_errfile,min(nMaxSize-1,strlen(strategy_errfile)) );
//  			return false;
//  		}
//  
//  		ar >> m_dwFileVersion;
//  		if( m_dwFileVersion > dwCELFileVersion )
//  		{
//  			if( lpErr )
//  				strncpy(lpErr,strategy_errfilever,min(nMaxSize-1,strlen(strategy_errfilever)) );
//  			return false;
//  		}
//  
//  		ar >> m_strName;
//  		ar >> m_strDescript;
//  		m_stocks.Serialize( ar );
//  
//  		m_rate.Serialize( ar );
//  
//  		m_paramBuy.Serialize( ar );
//  		m_paramSell.Serialize( ar );
//  		m_paramLongTrend.Serialize( ar );
//  		m_paramIndexTrend.Serialize( ar );
//  
//  		m_anTechsBuy.Serialize( ar );
//  		m_anTechsSell.Serialize( ar );
//  		m_anTechsLongTrend.Serialize( ar );
//  		m_anTechsIndexTrend.Serialize( ar );
//  
//  		m_opparam.Serialize( ar );
//  
//  		// Simulation
//  		//ar >> m_SimuCurrentStatus;
//  		//ar >> m_SimuCurrentTime;
//  		//ar >> m_SimuCurrentCash;
//  		m_SimuStockOwn.Serialize( ar );
//  		m_SimuOpRecord.Serialize( ar );
//  		m_SimuNextOp.Serialize( ar );
//  		m_SimuAssetSerial.Serialize( ar );
//  
//  		// Real Operation
//  		//ar >> m_RealBeginTime;
//  		//ar >> m_RealCurrentTime;
//  		ar >> m_RealCurrentCash;
//  		//m_RealStockOwn.Serialize( ar );
//  		m_RealOpRecord.Serialize( ar );
//  		m_RealNextOp.Serialize( ar );
//  		m_RealAssetSerial.Serialize( ar );
//  	}
// 	return true;
// }

bool CStrategy::DoFileSave( )
{
	return SaveStrategyFile( GetPathName().c_str() );
}

bool CStrategy::OnClose( )
{
	ClearCache( );
	return true;
}

void CStrategy::OnRealOpViewed( )
{
	for( size_t i=0; i<m_RealNextOp.GetSize(); i++ )
		m_RealNextOp.ElementAt(i).bViewed	=	true;
}


/////////////////////////////////////////////////////////////////////////////
// CStrategy commands
void CStrategy::SetPathName( const char* lpszPathName )
{
	m_strPathName	=	lpszPathName;
}

string CStrategy::GetPathName( )
{
	return m_strPathName;
}

// 设定策略名称
void CStrategy::SetName( const char* lpszName )
{
	m_strName	=	lpszName;
}
// 得到策略名称
string	CStrategy::GetName( )
{
	if( m_strName.empty() )
		return GetPathName();
	return m_strName;
}
// 设定策略描述
void CStrategy::SetDescript( const char* lpszDescript )
{
		m_strDescript	=	lpszDescript;
}
// 得到策略描述
string	CStrategy::GetDescript( )
{
	return m_strDescript;
}
// 得到策略被选股票
std::vector<std::string> & CStrategy::GetInstruments( )
{
	return m_stocks.get();
}
// 设定策略备选股票;
void CStrategy::SetInstruments( std::vector<std::string> & astr )
{
	//copy(astr.begin(),astr.end(),m_stocks.begin());
	m_stocks=astr;
}
// 加入策略备选股票;
void CStrategy::AddInstrument( const char* lpszCode )
{
	m_stocks.add( lpszCode );
}
// 移除策略备选股票
void CStrategy::RemoveInstrument( const char* lpszCode )
{
	m_stocks.remove( lpszCode );
}
// 得到策略的一般性描述文字
string CStrategy::GetInstrumentTechString( )
{
	size_t	nStockShowCount = 3, nTechShowCount = 10;

	string	strStock;
	for( size_t i=0; i<m_stocks.size() && i<nStockShowCount; i++ )
	{
		if( strStock.length() > 0 )
			strStock	+=	",";
		instrument_info	info;
		if( instrument_container::get_instance().GetInstrumentInfo( m_stocks[i].c_str(), &info ) )
//#ifdef CLKLAN_ENGLISH_US
//			strStock	+=	info.GetStockCode();
//#else
			strStock	+=	info.get_name();
//#endif
		else
			strStock	+=	m_stocks[i];
	}
	if( strStock.length() == 0 )
	{
		//strStock	=	strategy_noselectedstock;
	}
	else if( m_stocks.size() > nStockShowCount )
		strStock	+=	"...";
	strStock	+=	";    ";

	string	strTech;
	for( size_t i=0; i<m_anTechsBuy.size() && i<nTechShowCount; i++ )
	{
		if( strTech.length() > 0 )
			strTech	+=	",";
		//strTech	+=	AfxGetSTTShortName(m_anTechsBuy[i]);
	}
	if( strTech.length() == 0 )
	{
		//strTech	=	strategy_noselectedtech;
	}
	else if( m_anTechsBuy.size() > nTechShowCount )
		strTech	+=	"...";
	
	return strStock + strTech;
}
// 交易费率、操作条件等
CRateParam &	CStrategy::GetRateParam( )			{	return m_rate;			}
COpParam &		CStrategy::GetOpParam( )				{	return m_opparam;		}
CTechParameters & CStrategy::GetTechParametersBuy( )	{	return m_paramBuy;		}
CTechParameters & CStrategy::GetTechParametersSell( )	{	return m_paramSell;		}
CTechParameters & CStrategy::GetTechParametersLongTrend( )	{	return m_paramLongTrend;		}
CTechParameters & CStrategy::GetTechParametersIndexTrend( )	{	return m_paramIndexTrend;		}
vector<DWORD> &	CStrategy::GetSelectedTechsBuy( )		{	return m_anTechsBuy;	}
vector<DWORD> &	CStrategy::GetSelectedTechsSell( )		{	return m_anTechsSell;	}
vector<DWORD> &	CStrategy::GetSelectedTechsLongTrend( )	{	return m_anTechsLongTrend;	}
vector<DWORD> &	CStrategy::GetSelectedTechsIndexTrend( )	{	return m_anTechsIndexTrend;	}

/////////////////////////////////////////////////////////////////////////
// Cache
// 准备数据，读取K线数据，长周期K线数据，生成技术指标对象
bool CStrategy::PrepareData( SIMULATION_CALLBACK fnCallback, void * cookie )
{
	if( m_techstocks.size() == m_stocks.size() )
		return true;

	ClearCache( );

	// m_techstocks.m_kdataIndex m_techstocks.m_infoIndex
	if( m_opparam.m_bIndexTrend )	// 指数K线数据
	{
		instrument_info	infoIndex;
		if( instrument_container::get_instance().GetInstrumentInfo( "", &infoIndex ) )
		{
			m_techstocks.m_infoIndex	=	infoIndex;
			
			InstrumentData	stockIndex;
			stockIndex.SetInstrumentInfo( &infoIndex );
			//stockIndex.SetDataStoreOperator( &AfxGetDB() );
			//stockIndex.PrepareData( InstrumentData::dataK, m_opparam.m_nKTypeIndex );
			kdata_container	&	kdataIndex		=	stockIndex.GetKData(m_opparam.m_nKTypeIndex);
			kdataIndex.SetMaindataType( m_opparam.m_nMaindataType );
			m_techstocks.m_kdataIndex	=	kdataIndex;

			// m_techstocks.m_techsIndex
			for( size_t j=0; j<m_anTechsIndexTrend.size(); j++ )
			{
				technical_indicator * pTech	=	technical_indicator::create( m_anTechsIndexTrend[j], &(m_techstocks.m_kdataIndex) );
				m_paramIndexTrend.FindParameters( m_anTechsIndexTrend[j], pTech );
				m_techstocks.m_techsIndex.push_back( pTech );
			}
		}
	}
	
	m_techstocks.SetSize( m_stocks.size() );
	int	nStockCount	=	0;
	for( size_t i=0; i<m_stocks.size(); i++ )	// 读取每一只备选股票的数据
	{
		instrument_info	info;
		if( instrument_container::get_instance().GetInstrumentInfo( m_stocks[i].c_str(), &info ) )
		{
			// CTechInstrument::m_info;
			CTechInstrument	temp;
			temp.m_info	=	info;
			
			m_techstocks[nStockCount]= temp;
			CTechInstrument techstock	=	m_techstocks.ElementAt(nStockCount);
			nStockCount	++;
			
			// CTechInstrument::m_kdata
			InstrumentData	stock;
			stock.SetInstrumentInfo( &info );
			//AfxPrepareInstrumentData( &AfxGetDB(), stock, m_opparam.m_nKType, kdata_container::formatXDRdown, m_opparam.m_nMaindataType, false, false );
			techstock.m_kdata	=	stock.GetKData(m_opparam.m_nKType);

			// CTechInstrument::m_techs
			techstock.SetAutoDelete( true );
			for( size_t j=0; j<m_anTechsBuy.size(); j++ )
			{
				technical_indicator * pTech	=	technical_indicator::create( m_anTechsBuy[j], &(techstock.m_kdata) );
				m_paramBuy.FindParameters( m_anTechsBuy[j], pTech );
				techstock.m_techs.push_back( pTech );
			}

			// CTechInstrument::m_kdataLong
			if( m_opparam.m_bLongTrend )
			{
				//AfxPrepareStockData( &AfxGetDB(), stock, m_opparam.m_nKTypeLong, kdata_container::formatXDRdown, m_opparam.m_nMaindataType, false, false );
				techstock.m_kdataLong	=	stock.GetKData( m_opparam.m_nKTypeLong );

				// CTechInstrument::m_techsLong
				for( size_t j=0; j<m_anTechsLongTrend.size(); j++ )
				{
					technical_indicator * pTech	=	technical_indicator::create( m_anTechsLongTrend[j], &(techstock.m_kdataLong) );
					m_paramLongTrend.FindParameters( m_anTechsLongTrend[j], pTech );
					techstock.m_techsLong.push_back( pTech );
				}
			}
		}

		DWORD	dwProgress	=	(DWORD)((i+1)*STRATEGY_MAXF_PROGRESS / m_stocks.size());
		if( fnCallback && !fnCallback( SIMULATION_PROGRESS, dwProgress, NULL, cookie ) )
		{
			ClearCache( );
			nStockCount	=	0;
			break;
		}
	}

	m_techstocks.SetSize( nStockCount );

	if( fnCallback )
		fnCallback( SIMULATION_PROGRESS, STRATEGY_MAX_PROGRESS, NULL, cookie );

	return ( m_techstocks.size() == m_stocks.size() );
}

// 清除每个指标保存的上次趋势值
void CStrategy::ClearLastIntensity( )
{
	for( size_t i=0; i<m_techstocks.size(); i++ )
	{
		CTechInstrument techstock	=	m_techstocks.ElementAt(i);
			
		for( size_t nTech=0; nTech<techstock.m_techs.size(); nTech++ )
		{
			technical_indicator * pTech	=	(technical_indicator *)techstock.m_techs[nTech];
			if( pTech )
				pTech->clear_last_intensity();
		}
	}
}

void CStrategy::ClearCache( )
{
	m_techstocks.Clear();
}

CTechInstrumentContainer & CStrategy::GetTechInstrumentContainer( )
{
	return m_techstocks;
}

////////////////////////////////////////////////////////////////////////
// Simulation
// 策略模拟：重新设定
void CStrategy::SimuReset( )
{
	SimuSetStatusInit( );
	m_SimuCurrentTime	=	m_opparam.GetBeginTime( );
	m_SimuCurrentCash	=	m_opparam.m_nStartAmount;
	m_SimuOpRecord.RemoveAll();
	m_SimuNextOp.RemoveAll();
	//m_SimuStockOwn.RemoveAll();
	m_SimuAssetSerial.RemoveAll();

	assert( m_opparam.IsValid() );
}
// 策略模拟：进入下一个交易日
bool CStrategy::SimuGotoNextTime( )
{
	time_t	tmNext;
	if( m_opparam.GetNextTradeTime( m_SimuCurrentTime, tmNext )
		&& m_opparam.IsInTimeZones( tmNext ) )
	{
		m_SimuCurrentTime	=	tmNext;
		return true;
	}
	return false;
}
// 策略模拟：模拟的当前时间
time_t CStrategy::SimuGetCurrentTime( )
{
	return m_SimuCurrentTime;
}
// 策略模拟：模拟的当前资金
double CStrategy::SimuGetCurrentCash( )
{
	return m_SimuCurrentCash;
}
// 策略模拟：操作执行，bTimeStrict表示是否严格遵守计划时间
bool CStrategy::SimuOperate( OPRECORD record, bool bTimeStrict )
{
	if( ! m_opparam.IsInTimeZones( record.time ) )
		return false;
	if( bTimeStrict && m_SimuOpRecord.GetSize() > 0 && record.time < m_SimuOpRecord.ElementAt(m_SimuOpRecord.GetSize()-1).time )
		return false;
	instrument_info	info;
	if( strlen(record.szCode)>0
		&& ( !instrument_container::get_instance().GetInstrumentInfo( record.szCode, &info )
		|| !info.is_valid() ) )
		return false;

	double	dAmount		=	record.dwShare * record.dSharePrice;
	double	dRateCost	=	record.dRateCost;
	if( STRATEGY_OPTYPE_BUY == record.lOpType )			// 买入
	{
		if( m_SimuCurrentCash < dAmount+dRateCost )
			return false;
		if( !m_SimuStockOwn.AddInstruemnt( info, record.dwShare, record.dSharePrice ) )
			return false;
		m_SimuCurrentCash	-=	(dAmount+dRateCost);
	}
	else if( STRATEGY_OPTYPE_SELL == record.lOpType )	// 卖出
	{
		if( !m_SimuStockOwn.RemoveInstrument( info, record.dwShare ) )
			return false;
		m_SimuCurrentCash	+=	(dAmount-dRateCost);
	}
	else if( STRATEGY_OPTYPE_ADDSTOCK == record.lOpType )// 添加股票
	{
		if( !m_SimuStockOwn.AddInstruemnt( info, record.dwShare, record.dSharePrice ) )
			return false;
	}
	else if( STRATEGY_OPTYPE_REMOVESTOCK == record.lOpType )// 移除股票
	{
		if( !m_SimuStockOwn.RemoveInstrument( info, record.dwShare ) )
			return false;
	}
	else if( STRATEGY_OPTYPE_ADDCASH == record.lOpType )	// 添加资金
	{
		m_SimuCurrentCash	+=	record.dSharePrice;
	}
	else if( STRATEGY_OPTYPE_REMOVECASH == record.lOpType ) // 移除资金
	{
		if( m_SimuCurrentCash < record.dSharePrice )
			return false;
		m_SimuCurrentCash	-=	record.dSharePrice;
	}
	else
		return false;

	// Record
	m_SimuOpRecord.Add( record );
	return true;
}
// 策略模拟：操作记录
COpRecordContainer & CStrategy::SimuGetOpRecord( )
{
	return m_SimuOpRecord;
}
// 策略模拟：下一步操作
COpRecordContainer & CStrategy::SimuGetNextOp( )
{
	return m_SimuNextOp;
}
// 策略模拟：当前拥有股票
// CStockOwnContainer & CStrategy::SimuGetStockOwn( )
// {
// 	return m_SimuStockOwn;
// }
// 策略模拟：总资产序列
CAssetSerialContainer & CStrategy::SimuGetAssetSerial( )
{
	return m_SimuAssetSerial;
}
// 策略模拟：当前进度
DWORD CStrategy::SimuGetCurrentProgress( DWORD dwProgressMax )
{
	return m_opparam.GetProgress( m_SimuCurrentTime, dwProgressMax );
}
// 策略模拟：给定时间的总资产
double CStrategy::SimuGetAsset( time_t tmCur )
{
	double	dAsset	=	0;
	if( m_techstocks.GetSumAsset( tmCur, m_SimuStockOwn, &dAsset ) )	// 计算总资产
	{
		dAsset	+=	m_SimuCurrentCash;

		ASSETSERIAL	serial;
		memset( &serial, 0, sizeof(serial) );
		serial.time		=	tmCur/*.GetTime()*/;
		serial.dAsset	=	dAsset;
		serial.dCash	=	m_SimuCurrentCash;
		m_SimuAssetSerial.SortInsert( serial );
		return dAsset;
	}
	else
	{
		for( int i=m_SimuAssetSerial.GetSize()-1; i>=0; i-- )
		{
			ASSETSERIAL serial	=	m_SimuAssetSerial.ElementAt(i);
			if( serial.time <= tmCur/*.GetTime()*/ )
				return serial.dAsset;
		}
	}
	return m_opparam.m_nStartAmount;	// 初始总资产
	return 0.0;
}
// 策略模拟：当前收益
double CStrategy::SimuGetCurrentYield( )
{
	if( m_opparam.m_nStartAmount > 0 )
		return STRATEGY_BASEF_YIELD * SimuGetAsset(m_SimuCurrentTime) / m_opparam.m_nStartAmount;
	return STRATEGY_BASEF_YIELD;
}
// 策略模拟：当前指数上涨多少
double CStrategy::SimuGetCurrentYieldIndexPercent( )
{
	//DateTime	sptmBegin( m_opparam.GetBeginTime() );
	//DateTime	sptmNow( m_SimuCurrentTime/*.GetTime()*/ );
	DWORD	dateBegin	/*=	sptmBegin.ToInstrumentTimeDay()*/;
	DWORD	dateNow		/*=	sptmNow.ToInstrumentTimeDay()*/;
	instrument_info info;
	instrument_container::get_instance().GetCurrentInstrument(&info);
	InstrumentData instrument;
	instrument.SetInstrumentInfo(&info);
	kdata_container & kdata	=	instrument.GetKData(m_opparam.m_nKType);
	int	nIndexBegin	=	kdata.GetAboutIndexByDate( dateBegin );
	int	nIndexNow	=	kdata.GetAboutIndexByDate( dateNow );
	if( -1 == nIndexBegin || -1 == nIndexNow )
		return 0;

	if( kdata.at(nIndexBegin).ClosePrice < 1e-4 )
		return 0;

	double	dYield	=	((double)kdata.at(nIndexNow).ClosePrice) - kdata.at(nIndexBegin).ClosePrice;
	dYield	=	dYield / kdata.at(nIndexBegin).ClosePrice;

	return dYield;
	return 0.0;
}
// 策略模拟：当前收益百分数
double CStrategy::SimuGetCurrentYieldPercent( )
{
	return ( SimuGetCurrentYield() - STRATEGY_BASEF_YIELD ) / STRATEGY_BASEF_YIELD;
}
// 策略模拟：执行下一步操作计划
bool CStrategy::SimuOperateNextop( time_t tmCur, COpRecordContainer & nextop, CTechInstrument & techstock )
{
	for( size_t j=0; j<nextop.GetSize(); j++ )	// 每个计划依次执行
	{
		OPRECORD &	record	=	nextop.ElementAt(j);
		if( STRATEGY_OPTYPE_BUY != record.lOpType && STRATEGY_OPTYPE_SELL != record.lOpType )
			continue;

		if( tmCur/*.GetTime()*/ >= record.time
			&& techstock.m_info.is_equal( InstrumentData::marketUnknown, record.szCode ) )
		{
			if( techstock.IsStopTrading(tmCur) )	// 停牌吗
			{
				time_t	tmNext;
				if( m_opparam.GetNextTradeTime(tmCur, tmNext) )
					record.time	=	tmNext/*.GetTime()*/;	// 下次再执行
				continue;
			}

			double	dPriceOK	=	record.dSharePrice;
			if( techstock.GetPriceOK( record.lOpType, tmCur, record.dwShare, record.dSharePrice, &dPriceOK ) )	// 成交价
			{
				record.time			=	tmCur/*.GetTime()*/;
				record.dSharePrice	=	dPriceOK;
				record.dRateCost	=	record.dwShare * record.dSharePrice * m_rate.GetRate(techstock.m_info);
				SimuOperate( record );
			}
			else if( STRATEGY_OPTYPE_SELL == record.lOpType )	// 如果是卖出而没有成功，则顺延下一个交易日，直至卖出
			{
				time_t	tmNext;
				if( m_opparam.GetNextTradeTime(tmCur, tmNext) )
					record.time	=	tmNext/*.GetTime()*/;
				if( techstock.GetClosePrice(tmCur,&dPriceOK) )
					record.dSharePrice	=	dPriceOK * m_opparam.m_dSellMulti;
				continue;
			}
			nextop.RemoveAt(j);
			j --;
		}
	}
	return true;
}
// 策略模拟：模拟运行
bool CStrategy::SimuRun( SIMULATION_CALLBACK fnCallback, void * cookie )
{
	// TIMEZONES
	// 准备数据
	if( !PrepareData( fnCallback, cookie ) )
		return false;

	ClearLastIntensity( );

	DWORD	dwShare = 0;		// Temp Data
	double	dSharePrice = 0;	// Temp Data
	time_t	tmOp;				// Temp Data

	do	{
		time_t	tmCur	=	SimuGetCurrentTime();	// 模拟当前时间

		// Process
		for( size_t i=0; i<m_techstocks.size(); i++ )	// 每只股票依次判断
		{
			CTechInstrument techstock	=	m_techstocks.ElementAt(i);

			// Operate
			SimuOperateNextop( tmCur, m_SimuNextOp, techstock );	// 执行今天的操作计划

			// Judge Whether to operate, if yes, save to nextop
			INSTRUMENTOWN	own;
			memset( &own, 0, sizeof(own) );
			if( m_SimuStockOwn.HasThisInstrument( techstock.m_info, &own ) )	// 如果已经有这支股票，判断是否卖出
			{
				if( m_techstocks.GetShouldSellShare( techstock, tmCur, own, m_opparam, &tmOp, &dwShare, &dSharePrice ) )
				{	// 如果要卖出，加入下一步操作计划，下一个交易日执行
					m_SimuNextOp.AddRecordUniqueStock( STRATEGY_OPTYPE_SELL, tmOp, techstock.m_info.get_id(), dwShare, dSharePrice, dwShare*dSharePrice*m_rate.GetRate(techstock.m_info) );
				}
			}
			else	// 判断是否买入
			{
				if( m_techstocks.GetShouldBuyShare( techstock, tmCur, m_SimuCurrentCash, m_rate , m_opparam, m_SimuStockOwn.GetSize(), &tmOp, &dwShare, &dSharePrice )
					&& ( m_SimuStockOwn.GetSize() + m_SimuNextOp.GetBuyRecordCount() < m_opparam.m_nStoreDiv || m_SimuNextOp.HasBuyStock(techstock.m_info.get_id()) ) )
				{	// 如果要买入，加入下一步操作计划，下一个交易日执行
					m_SimuNextOp.AddRecordUniqueStock( STRATEGY_OPTYPE_BUY, tmOp, techstock.m_info.get_id(), dwShare, dSharePrice, dwShare*dSharePrice*m_rate.GetRate(techstock.m_info) );
				}
			}
		}

		// 进度显示
		DWORD	dwProgress	=	SimuGetCurrentProgress( STRATEGY_MAX_PROGRESS );
		double	dYield		=	SimuGetCurrentYield( );
		if( fnCallback && !fnCallback( SIMULATION_PROGRESS, dwProgress, NULL, cookie ) )
			return false;
		if( fnCallback && !fnCallback( SIMULATION_YIELD, (DWORD)dYield, NULL, cookie ) )
			return false;

	} while( SimuGotoNextTime() );	// 模拟的下一个交易日

	if( fnCallback )
		fnCallback( SIMULATION_PROGRESS, STRATEGY_MAX_PROGRESS, NULL, cookie );
	return true;
}

////////////////////////////////////////////////////////////////////////
// Real
// 策略实战：重新设定
void CStrategy::RealReset( )
{
	time_t	tmLatest	=	time(nullptr);
	//AfxGetDB().GetTimeLocalRange( &tmLatest, NULL, NULL );
	m_RealBeginTime		=	tmLatest;
	m_RealCurrentTime	=	tmLatest;
	m_RealCurrentCash	=	m_opparam.m_nStartAmount;
	m_RealOpRecord.RemoveAll();
	m_RealNextOp.RemoveAll();
	//m_RealStockOwn.RemoveAll();
	m_RealAssetSerial.RemoveAll();

	assert( m_opparam.IsValid() );
}
// 策略实战：下一个交易时间
bool CStrategy::RealGotoNextTime( )
{
	time_t	tmNext;
	if( m_techstocks.GetNextExistTradeTime( RealGetCurrentTime(), tmNext ) )
	{
		m_RealCurrentTime	=	tmNext;
		return true;
	}
	return false;
}
// 策略实战：开始时间
time_t CStrategy::RealGetBeginTime( )
{
	return m_RealBeginTime;
}
// 策略实战：当前时间
time_t CStrategy::RealGetCurrentTime()
{
	return m_RealCurrentTime;
}
// 策略实战：当前资金
double CStrategy::RealGetCurrentCash( )
{
	return m_RealCurrentCash;
}
// 策略实战：执行一个操作
bool CStrategy::RealOperate( OPRECORD record, bool bTimeStrict )
{
	if( bTimeStrict && m_RealOpRecord.GetSize() > 0 && record.time < m_RealOpRecord.ElementAt(m_RealOpRecord.GetSize()-1).time )
		return false;
	instrument_info	info;
	if( strlen(record.szCode)>0
		&& ( !instrument_container::get_instance().GetInstrumentInfo( record.szCode, &info )
		|| !info.is_valid() ) )
		return false;

	double	dAmount		=	record.dwShare * record.dSharePrice;
	double	dRateCost	=	record.dRateCost;
	if( STRATEGY_OPTYPE_BUY == record.lOpType )	// 买入
	{
		if( m_RealCurrentCash < dAmount+dRateCost )
			return false;
		if( !m_RealStockOwn.AddInstruemnt( info, record.dwShare, record.dSharePrice ) )
			return false;
		m_RealCurrentCash	-=	(dAmount+dRateCost);
	}
	else if( STRATEGY_OPTYPE_SELL == record.lOpType )	// 卖出
	{
		if( !m_RealStockOwn.RemoveInstrument( info, record.dwShare ) )
			return false;
		m_RealCurrentCash	+=	(dAmount-dRateCost);
	}
	else if( STRATEGY_OPTYPE_ADDSTOCK == record.lOpType )	// 添加股票
	{
		if( !m_RealStockOwn.AddInstruemnt( info, record.dwShare, record.dSharePrice ) )
			return false;
	}
	else if( STRATEGY_OPTYPE_REMOVESTOCK == record.lOpType )	// 移除股票
	{
		if( !m_RealStockOwn.RemoveInstrument( info, record.dwShare ) )
			return false;
	}
	else if( STRATEGY_OPTYPE_ADDCASH == record.lOpType )		// 加入资金
	{
		m_RealCurrentCash	+=	record.dSharePrice;
	}
	else if( STRATEGY_OPTYPE_REMOVECASH == record.lOpType )	// 移除资金
	{
		if( m_RealCurrentCash < record.dSharePrice )
			return false;
		m_RealCurrentCash	-=	record.dSharePrice;
	}
	else
		return false;

	// Record
	m_RealOpRecord.Add( record );
	return true;
}
// 策略实战：操作记录
COpRecordContainer & CStrategy::RealGetOpRecord( )
{
	return m_RealOpRecord;
}
// 策略实战：下一步操作计划
COpRecordContainer & CStrategy::RealGetNextOp( )
{
	return m_RealNextOp;
}
// 策略实战：当前拥有股票
CInstrumentOwnContainer & CStrategy::RealGetStockOwn( )
{
	return m_RealStockOwn;
}
// 策略实战：总资产序列
CAssetSerialContainer & CStrategy::RealGetAssetSerial( )
{
	return m_RealAssetSerial;
}
// 策略实战：得到指定日期的总资产
double CStrategy::RealGetAsset( time_t tmCur )
{
	double	dAsset	=	0;
	return 0.0;
	if( m_techstocks.GetSumAsset( tmCur, m_RealStockOwn, &dAsset ) )
	{
		dAsset	+=	m_RealCurrentCash;

		ASSETSERIAL	serial;
		memset( &serial, 0, sizeof(serial) );
		serial.time		=	tmCur/*.GetTime()*/;
		serial.dAsset	=	dAsset;
		serial.dCash	=	m_RealCurrentCash;
		m_RealAssetSerial.SortInsert( serial );
		return dAsset;
	}
	else
	{
		for( int i=m_RealAssetSerial.GetSize()-1; i>=0; i-- )
		{
			ASSETSERIAL serial	=	m_RealAssetSerial.ElementAt(i);
			if( serial.time <= tmCur/*.GetTime()*/ )
				return serial.dAsset;
		}
	}
	return m_opparam.m_nStartAmount;
}
// 策略实战：当前收益
double CStrategy::RealGetCurrentYield( )
{
	if( m_opparam.m_nStartAmount > 0 )
		return STRATEGY_BASEF_YIELD * RealGetAsset(RealGetCurrentTime()) / m_opparam.m_nStartAmount;
	return STRATEGY_BASEF_YIELD;
}
// 策略实战：当前指数上涨百分比
double CStrategy::RealGetCurrentYieldIndexPercent( )
{
	//DateTime	sptmBegin( RealGetBeginTime()/*.GetTime()*/ );
	//DateTime	sptmNow( RealGetCurrentTime()/*.GetTime()*/ );
	DWORD	dateBegin	/*=	sptmBegin.ToInstrumentTimeDay()*/;
	DWORD	dateNow		/*=	sptmNow.ToInstrumentTimeDay()*/;
	instrument_info info;
	instrument_container::get_instance().GetCurrentInstrument(&info);
	InstrumentData instrument;
	instrument.SetInstrumentInfo(&info);
	kdata_container & kdata	=	instrument.GetKData(m_opparam.m_nKType);
	int	nIndexBegin	=	kdata.GetAboutIndexByDate( dateBegin );
	int	nIndexNow	=	kdata.GetAboutIndexByDate( dateNow );
	if( -1 == nIndexBegin || -1 == nIndexNow )
		return 0;

	if( kdata.at(nIndexBegin).ClosePrice < 1e-4 )
		return 0;

	double	dYield	=	((double)kdata.at(nIndexNow).ClosePrice) - kdata.at(nIndexBegin).ClosePrice;
	dYield	=	dYield / kdata.at(nIndexBegin).ClosePrice;

	return dYield;
	return 0.0;
}
// 策略实战：当前收益百分比
double CStrategy::RealGetCurrentYieldPercent( )
{
	return ( RealGetCurrentYield() - STRATEGY_BASEF_YIELD ) / STRATEGY_BASEF_YIELD;
}
// 策略实战：执行操作计划
bool CStrategy::RealOperateNextop( time_t tmCur, COpRecordContainer & nextop, CTechInstrument & techstock )
{
	for( size_t j=0; j<nextop.GetSize(); j++ )	// 依次执行每一个计划
	{
		OPRECORD &	record	=	nextop.ElementAt(j);
		if( STRATEGY_OPTYPE_BUY != record.lOpType && STRATEGY_OPTYPE_SELL != record.lOpType )
			continue;

		if( tmCur/*.GetTime()*/ >= record.time
			&& techstock.m_info.is_equal( InstrumentData::marketUnknown, record.szCode ) )
		{
			if( !record.bViewed )	//	Not Viewed
			{
				nextop.RemoveAt(j);
				j --;
				continue;
			}

			if( techstock.IsStopTrading(tmCur) )	// 停牌，转入下一个交易日的计划中
			{
				time_t	tmNext;
				if( m_opparam.GetNextTradeTime(tmCur, tmNext) )
					record.time	=	tmNext/*.GetTime()*/;
				continue;
			}

			double	dPriceOK	=	record.dSharePrice;
			if( techstock.GetPriceOK( record.lOpType, tmCur, record.dwShare, record.dSharePrice, &dPriceOK ) )	// 成交价格
			{
				record.time			=	tmCur/*.GetTime()*/;
				record.dSharePrice	=	dPriceOK;
				record.dRateCost	=	record.dwShare * record.dSharePrice * m_rate.GetRate(techstock.m_info);
				RealOperate( record );
			}
			else if( STRATEGY_OPTYPE_SELL == record.lOpType )	// 如果是卖出不成功，转入下一个交易日，直至卖出成功
			{
				time_t	tmNext;
				if( m_opparam.GetNextTradeTime(tmCur, tmNext) )
					record.time	=	tmNext/*.GetTime()*/;
				if( techstock.GetClosePrice(tmCur,&dPriceOK) )
					record.dSharePrice	=	dPriceOK * m_opparam.m_dSellMulti;
				continue;
			}
			nextop.RemoveAt(j);
			j --;
		}
	}
	return true;
}
// 策略实战：执行
bool CStrategy::RealRun( SIMULATION_CALLBACK fnCallback, void * cookie )
{
	// TIMEZONES
	// 准备数据
 	if( !PrepareData( fnCallback, cookie ) )
 		return false;
 
 	ClearLastIntensity();
 
 	DWORD	dwShare = 0;		// Temp Data
 	double	dSharePrice = 0;	// Temp Data
 	time_t	tmOp;				// Temp Data
 
 	do	{
 		time_t	tmCur	=	RealGetCurrentTime();	// 当前日期
 
 		// Process
 		for( size_t i=0; i<m_techstocks.size(); i++ )	// 每只股票依次判断
 		{
 			CTechInstrument techstock	=	m_techstocks.ElementAt(i);
 
 			// Operate
 			RealOperateNextop( tmCur, m_RealNextOp, techstock );	// 执行今日操作计划
 
 			// Judge Whether to operate, if yes, save to nextop
 			INSTRUMENTOWN	own;
 			memset( &own, 0, sizeof(own) );
 			if( m_RealStockOwn.HasThisInstrument( techstock.m_info, &own ) )	// 如果有这支股票，判断是否卖出
 			{
 				if( m_techstocks.GetShouldSellShare( techstock, tmCur, own, m_opparam, &tmOp, &dwShare, &dSharePrice ) )
 				{	// 如果卖出，加入操作计划中
 					m_RealNextOp.AddRecordUniqueStock( STRATEGY_OPTYPE_SELL, tmOp, techstock.m_info.get_id(), dwShare, dSharePrice, dwShare*dSharePrice*m_rate.GetRate(techstock.m_info) );
 				}
 			}
 			else	// 判断是否买入
 			{
 				if( m_techstocks.GetShouldBuyShare( techstock, tmCur, m_RealCurrentCash, m_rate , m_opparam, m_RealStockOwn.GetSize(), &tmOp, &dwShare, &dSharePrice )
 					&& ( m_RealStockOwn.GetSize() + m_RealNextOp.GetBuyRecordCount() < m_opparam.m_nStoreDiv || m_RealNextOp.HasBuyStock(techstock.m_info.get_id()) ) )
 				{	// 如果买入，加入操作计划中
 					m_RealNextOp.AddRecordUniqueStock( STRATEGY_OPTYPE_BUY, tmOp, techstock.m_info.get_id(), dwShare, dSharePrice, dwShare*dSharePrice*m_rate.GetRate(techstock.m_info) );
 				}
 			}
 
 			// 进度显示
 			DWORD	dwProgress	=	(DWORD)((i+1)*STRATEGY_MAX_PROGRESS/m_techstocks.size());
 			if( fnCallback && !fnCallback( SIMULATION_PROGRESS, dwProgress, NULL, cookie ) )
 				return false;
 		}
 
 		RealGetAsset( RealGetCurrentTime() );
 
 	} while( RealGotoNextTime() );	// 下一个交易日
 
 	if( fnCallback )
 		fnCallback( SIMULATION_PROGRESS, STRATEGY_MAX_PROGRESS, NULL, cookie );
	return true;
}
// 策略实战：加入操作记录，如果成功，同时要删除加入日后的所有操作
bool CStrategy::RealAddOpRecordStrict( OPRECORD record )
{
	COpRecordContainer recordbk;
	recordbk.Copy( m_RealOpRecord );

	// 重新执行操作记录，到加入新操作日为止
	bool	bOK	=	true;
	m_RealCurrentCash	=	m_opparam.m_nStartAmount;
	m_RealOpRecord.RemoveAll();
	m_RealNextOp.RemoveAll();
	//m_RealStockOwn.RemoveAll();
	for( size_t i=0; i<recordbk.GetSize(); i++ )
	{
		OPRECORD	rec	=	recordbk.ElementAt(i);
		if( bOK && rec.time < record.time )
			bOK	&=	RealOperate( rec );
	}
	
	if( bOK && RealOperate( record ) )
	{	// 加入成功，重新计算总资产序列
		for( int i=m_RealAssetSerial.GetSize()-1; i>=0; i-- )
		{
			ASSETSERIAL	serial	=	m_RealAssetSerial.ElementAt(i);
			if( serial.time >= record.time )
				m_RealAssetSerial.RemoveAt(i);
		}
		RealGetAsset( record.time );
		return true;
	}
	else
	{
		// 加入失败，全部重来，恢复原样
		m_RealCurrentCash	=	m_opparam.m_nStartAmount;
		m_RealOpRecord.RemoveAll();
		m_RealNextOp.RemoveAll();
		m_RealStockOwn.RemoveAll();
		for( size_t i=0; i<recordbk.GetSize(); i++ )
		{
			OPRECORD	rec	=	recordbk.ElementAt(i);
			RealOperate( rec );
		}
		return false;
	}
}
// 策略实战：删除操作记录，如果成功，同时要删除日后的所有操作
bool CStrategy::RealDeleteOpRecordStrict( size_t nRecord )
{
	if( nRecord < 0 || nRecord >= m_RealOpRecord.GetSize() )
	{
		assert( false );
		return false;
	}
	OPRECORD	record	=	m_RealOpRecord.ElementAt(nRecord);

	COpRecordContainer recordbk;
	recordbk.Copy( m_RealOpRecord );

	// 重新执行操作记录，到删除日为止
	bool	bOK	=	true;
	m_RealCurrentCash	=	m_opparam.m_nStartAmount;
	m_RealOpRecord.RemoveAll();
	m_RealNextOp.RemoveAll();
	m_RealStockOwn.RemoveAll();
	for( size_t i=0; i<recordbk.GetSize(); i++ )
	{
		OPRECORD	rec	=	recordbk.ElementAt(i);
		if( bOK && rec.time < record.time )
			bOK	&=	RealOperate( rec );
	}
	
	if( bOK )
	{	// 成功删除，重新计算总资产序列
		for( int i=m_RealAssetSerial.GetSize()-1; i>=0; i-- )
		{
			ASSETSERIAL	serial	=	m_RealAssetSerial.ElementAt(i);
			if( serial.time >= record.time )
				m_RealAssetSerial.RemoveAt(i);
		}
		RealGetAsset( record.time );
		return true;
	}
	else
	{
		// 删除失败，恢复原样
		m_RealCurrentCash	=	m_opparam.m_nStartAmount;
		m_RealOpRecord.RemoveAll();
		m_RealNextOp.RemoveAll();
		m_RealStockOwn.RemoveAll();
		for( size_t i=0; i<recordbk.GetSize(); i++ )
		{
			OPRECORD	rec	=	recordbk.ElementAt(i);
			RealOperate( rec );
		}
		return false;
	}
}


// bool CStrategy::RealUnOperate( OPRECORD record )
// {
// 	CInstrumentInfo	info;
// 	if( strlen(record.szCode)>0
// 		&& ( !CInstrumentContainer::GetInstrumentContainer().GetInstrumentInfo( record.szCode, &info )
// 		|| !info.IsValidInstrument() ) )
// 		return false;
// 
// 	double	dAmount		=	record.dwShare * record.dSharePrice;
// 	double	dRateCost	=	record.dRateCost;
// 	if( STRATEGY_OPTYPE_BUY == record.lOpType )
// 	{
// 		if( !m_RealStockOwn.RemoveInstrument( info, record.dwShare ) )
// 			return false;
// 		m_RealCurrentCash	+=	(dAmount+dRateCost);
// 	}
// 	else if( STRATEGY_OPTYPE_SELL == record.lOpType )
// 	{
// 		if( m_RealCurrentCash < dAmount-dRateCost )
// 			return false;
// 		if( !m_RealStockOwn.AddInstruemnt( info, record.dwShare ) )
// 			return false;
// 		m_RealCurrentCash	-=	(dAmount-dRateCost);
// 	}
// 	else if( STRATEGY_OPTYPE_ADDSTOCK == record.lOpType )
// 	{
// 		if( !m_RealStockOwn.RemoveInstrument( info, record.dwShare ) )
// 			return false;
// 	}
// 	else if( STRATEGY_OPTYPE_REMOVESTOCK == record.lOpType )
// 	{
// 		if( !m_RealStockOwn.AddInstruemnt( info, record.dwShare ) )
// 			return false;
// 	}
// 	else if( STRATEGY_OPTYPE_ADDCASH == record.lOpType )
// 	{
// 		if( m_RealCurrentCash < record.dSharePrice )
// 			return false;
// 		m_RealCurrentCash	-=	record.dSharePrice;
// 	}
// 	else if( STRATEGY_OPTYPE_REMOVECASH == record.lOpType )
// 	{
// 		m_RealCurrentCash	+=	record.dSharePrice;
// 	}
// 	else
// 		return false;
// 
// 	return true;
// }


