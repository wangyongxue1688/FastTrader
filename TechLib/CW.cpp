#include "CW.h"
#include <math.h>
#include "../FacilityBaseLib/KData.h"

//////////////////////////////////////////////////////////////////////
//	CCW
CCW::CCW( )
{
	SetDefaultParameters( );
}

CCW::CCW( KdataContainer * pKData )
	: TechnicalIndicator( pKData )
{
	SetDefaultParameters( );
}

CCW::~CCW()
{
	clear( );
}

void CCW::SetDefaultParameters( )
{
	m_dChangeHand	=	1.5;
}

void CCW::attach( CCW & src )
{
	m_dChangeHand	=	src.m_dChangeHand;
}

bool CCW::IsValidParameters( )
{
	return ( m_dChangeHand > 0 );
}

void CCW::clear( )
{
	TechnicalIndicator::clear( );
}

/***
	根据换手率m_dChangeHand和终止日，计算起始日
*/
bool CCW::GetRange(  std::size_t & nStart,  std::size_t & nEnd, InstrumentInfo & info )
{
	if( !m_pKData || m_pKData->size() <= 0 )
		return false;
	if( nEnd < 0 || nEnd >= m_pKData->size() )
		nEnd	=	m_pKData->size()-1;
	
	bool	bIndex = false;
	double	dShareCurrency = 0;
// 	if( !info.GetShareCurrency( &dShareCurrency ) || dShareCurrency < 1e+6 )
// 		bIndex	=	true;

	if( bIndex )
		dShareCurrency	=	100 * m_dChangeHand;
	else
		dShareCurrency	*=	m_dChangeHand;

	double	dVol	=	0;
	int k;
	for( k=nEnd; k>=0; k-- )
	{
		if( bIndex )
			dVol	+=	1;
		else
			dVol	+=	m_pKData->at(k).Volume;

		if( dVol > dShareCurrency )
			break;
	}
	nStart	=	k;
	if( nStart < 0 )
		nStart	=	0;
	return true;
}

bool CCW::min_max_info( std::size_t nStart,  std::size_t nEnd, double dMinPrice, double dMaxPrice, double dStep,
				double *pdMinVolume, double *pdMaxVolume )
{
	STT_ASSERT_GETMINMAXINFO( m_pKData, nStart, nEnd );
	if( dMinPrice >= dMaxPrice || dStep < 1e-4 )
		return false;

	double	dMinVolume = 0, dMaxVolume = 0, dVolume = 0;
	bool	bFirst	=	true;
	for( double dPrice = dMinPrice; dPrice < dMaxPrice; dPrice += dStep )
	{
		if( CalculateCW( &dVolume, nStart, nEnd, dPrice, dStep ) )
		{
			if( bFirst || dVolume < dMinVolume )	dMinVolume	=	dVolume;
			if( bFirst || dVolume > dMaxVolume )	dMaxVolume	=	dVolume;
			bFirst	=	false;
		}
	}

	dMinVolume	-=	fabs(dMinVolume)*0.01;
	dMaxVolume	+=	fabs(dMaxVolume)*0.01;
	if( dMaxVolume - dMinVolume < 3 )
		dMaxVolume	=	dMinVolume + 3;
	if( pdMinVolume )		*pdMinVolume	=	dMinVolume;
	if( pdMaxVolume )		*pdMaxVolume	=	dMaxVolume;

	return !bFirst;
}

/***
	筹码分布图，计算价格区间包括dPrice的日线的成交量;
*/
bool CCW::CalculateCW( double *pdVolume,  std::size_t nStart,  std::size_t nEnd, double dPrice, double dStep )
{
	STT_ASSERT_GETMINMAXINFO( m_pKData, nStart, nEnd );

	double	dVolume	=	0;
	for( size_t k=nStart; k<=nEnd; k++ )
	{
		KDATA	kd	=	m_pKData->at(k);
		if( kd.HighestPrice-kd.LowestPrice > 1e-4
			&& kd.LowestPrice < dPrice && kd.HighestPrice > dPrice )
		{
			// 均匀分布 dVolAve
			double	dVolAve = kd.Volume;
			if( dStep < kd.HighestPrice-kd.LowestPrice )
				dVolAve	=	kd.Volume * dStep / (kd.HighestPrice-kd.LowestPrice);

			// 三角分布
			double	dFactor	=	min(dPrice-kd.LowestPrice, kd.HighestPrice-dPrice);
			dVolume	+=	dVolAve * dFactor * 4 / (kd.HighestPrice-kd.LowestPrice);
		}
	}
	
	if( pdVolume )
		*pdVolume	=	dVolume;
	return true;
}

/***
	筹码分布图计算，计算筹码分布
*/
bool CCW::CalculateCW(	 std::size_t nStart,  std::size_t nEnd, InstrumentInfo & info, double dStep,
						std::vector<uint32_t> & adwPrice, std::vector<uint32_t> & adwVolume,
						double * pdMinVolume, double * pdMaxVolume, double * pdTotalVolume, double * pdVolPercent )
{
	STT_ASSERT_GETMINMAXINFO( m_pKData, nStart, nEnd );
	if( dStep < 1e-4 )
		return false;

	double dMinPrice = 0, dMaxPrice = 0;
	if( !m_pKData->GetMinMaxInfo( nStart, nEnd, &dMinPrice, &dMaxPrice ) )
		return false;

	// Calculate
	int nMaxCount = (int)((dMaxPrice-dMinPrice)/dStep) + 10;
	adwPrice.resize(0);
	adwVolume.resize(0);
	double	dMinVolume = 0, dMaxVolume = 0, dTotalVolume = 0, dVolume = 0;
	bool	bFirst	=	true;
	for( double dPrice = dMinPrice; dPrice < dMaxPrice; dPrice += dStep )
	{
		if( CalculateCW( &dVolume, nStart, nEnd, dPrice, dStep ) )
		{
			if( bFirst || dVolume < dMinVolume )	dMinVolume	=	dVolume;
			if( bFirst || dVolume > dMaxVolume )	dMaxVolume	=	dVolume;
			adwPrice.push_back( DWORD(dPrice * 1000) );
			adwVolume.push_back( DWORD(dVolume) );
			dTotalVolume	+=	dVolume;
			bFirst	=	false;
		}
	}

	// Return
	// Min Max
	dMinVolume	-=	fabs(dMinVolume)*0.01;
	dMaxVolume	+=	fabs(dMaxVolume)*0.01;
	if( dMaxVolume - dMinVolume < 3 )
		dMaxVolume	=	dMinVolume + 3;
	if( pdMinVolume )	*pdMinVolume	=	dMinVolume;
	if( pdMaxVolume )	*pdMaxVolume	=	dMaxVolume;
	if( pdTotalVolume )	*pdTotalVolume	=	dTotalVolume;

	// VolPercent
	double dVolPercent = 1.0;
	double	dShareCurrency = 0;
	dShareCurrency=0.7;
// 	if( (!info.GetShareCurrency( &dShareCurrency ) || dShareCurrency < 1e+6) && nEnd-nStart+1 > 0 )
// 		dShareCurrency	=	dTotalVolume * 100 / (nEnd-nStart+1);
	if( dShareCurrency > 1e-4 )
		dVolPercent	=	dTotalVolume / (dShareCurrency*m_dChangeHand);
	if( dVolPercent > 1.0 )		dVolPercent	=	1.0;
	if( dVolPercent < 0.0 )		dVolPercent	=	0.0;
	if( pdVolPercent )	*pdVolPercent	=	dVolPercent;

	return adwPrice.size() > 0;
}

/***
	筹码分布图计算，计算最近nDays天内的筹码分布
*/
bool CCW::CalculateRecentCW( std::size_t nEnd,  std::size_t nDays, InstrumentInfo & info, double dStep,
						std::vector<uint32_t> & adwPrice, std::vector<uint32_t> & adwVolume,
						double * pdMinVolume, double * pdMaxVolume, double * pdTotalVolume, double * pdVolPercent )
{
	// Prepare
	if( !m_pKData || m_pKData->size() <= 0 )
		return false;
	if( nEnd < 0 || nEnd >= m_pKData->size() )
		nEnd	=	m_pKData->size()-1;
	int	nStart = nEnd - nDays + 1;
	if( nStart < 0 || nStart >= m_pKData->size() )
		return false;

	return CalculateCW( nStart, nEnd, info, dStep, adwPrice, adwVolume, pdMinVolume, pdMaxVolume, pdTotalVolume, pdVolPercent );
}

/***
	筹码分布图计算，计算nDays天前内的筹码分布;
*/
bool CCW::CalculatePastCW( std::size_t nEnd,  std::size_t nDays, InstrumentInfo & info, double dStep,
						std::vector<uint32_t> & adwPrice, std::vector<uint32_t> & adwVolume,
						double * pdMinVolume, double * pdMaxVolume, double * pdTotalVolume, double * pdVolPercent )
{
	// Prepare
	if( !m_pKData || m_pKData->size() <= 0 )
		return false;
	if( nEnd < 0 || nEnd >= m_pKData->size() )
		nEnd	=	m_pKData->size()-1;
	 std::size_t	nStart = nEnd - nDays;
	if( nStart < 0 || nStart >= m_pKData->size() )
		return false;

	nEnd = nStart;
	if( !GetRange( nStart, nEnd, info ) )
		return false;

	bool bOK = CalculateCW( nStart, nEnd, info, dStep, adwPrice, adwVolume, pdMinVolume, pdMaxVolume, pdTotalVolume, pdVolPercent );

	// TotalVolumeRecent
	double dTotalVolumeRecent = 0;
	for (size_t k = nEnd + 1; k <= nEnd + nDays && k<m_pKData->size(); k++)
		dTotalVolumeRecent	+=	m_pKData->at(k).Volume;

	// VolPercent
	double dVolPercent = 1.0;
	double	dShareCurrency = 1.2;
// 	if( (!info.GetShareCurrency( &dShareCurrency ) || dShareCurrency < 1e+6) && nDays > 0 )
// 		dShareCurrency	=	dTotalVolumeRecent * 100 / nDays;
	if( dShareCurrency > 1e-4 )
		dVolPercent	=	dTotalVolumeRecent / (dShareCurrency*m_dChangeHand);
	dVolPercent	=	1.0 - dVolPercent;
	if( dVolPercent > 1.0 )		dVolPercent	=	1.0;
	if( dVolPercent < 0.0 )		dVolPercent	=	0.0;
	if( pdVolPercent )	*pdVolPercent	=	dVolPercent;

	return bOK;
}

/***
	筹码分布图统计，获利比例统计;
*/
bool CCW::StatGainPercent( double *pdGainPercent, std::vector<uint32_t> &adwPrice, std::vector<uint32_t> &adwVolume, double dPriceSel )
{
	double dTotalVolume = 0;
	double dGainVolume = 0;
	for(  std::size_t k=0; k<adwPrice.size() && k<adwVolume.size(); k++ )
	{
		dTotalVolume	+=	adwVolume[k];
		if( adwPrice[k] * 0.001 <= dPriceSel )
			dGainVolume	+=	adwVolume[k];
	}

	double dGainPercent = 0;
	if( dTotalVolume > 1e-4 )
		dGainPercent	=	dGainVolume / dTotalVolume;

	if( pdGainPercent )	*pdGainPercent = dGainPercent;
	return true;
}

/***
	筹码分布图统计，平均成本统计;
*/
bool CCW::StatCostAverage( double *pdCostAve, std::vector<uint32_t> &adwPrice, std::vector<uint32_t> &adwVolume )
{
	double dTotalVolume = 0;
	double dTotalCost = 0;
	for (size_t k = 0; k<adwPrice.size() && k<adwVolume.size(); k++)
	{
		dTotalVolume	+=	adwVolume[k];
		dTotalCost		+=	0.001 * adwPrice[k] * adwVolume[k];
	}

	double dCostAve = 0;
	if( dTotalVolume > 1e-4 )
		dCostAve	=	dTotalCost / dTotalVolume;

	if( pdCostAve )	*pdCostAve = dCostAve;
	return true;
}
	
/***
	筹码分布图统计，集中度统计;
*/
bool CCW::StatMass( double *pdLower, double *pdUpper, double *pdMassPrice,std::vector<uint32_t> &adwPrice, std::vector<uint32_t> &adwVolume, double dMassVol )
{
	if( adwPrice.size() != adwVolume.size() || dMassVol < 0 || dMassVol > 1 )
		return false;

	double dTotalVolume = 0;
	for(  std::size_t k=0; k<adwPrice.size() && k<adwVolume.size(); k++ )
		dTotalVolume	+=	adwVolume[k];

	if( dTotalVolume > 1e-4 )
	{
		double dUpperVolume = dTotalVolume * (1-dMassVol) * 0.5;
		double dLowerVolume = dUpperVolume;
		int nLower = 0, nUpper = adwPrice.size()-1;

		 std::size_t k;
		for( k=0; k<adwPrice.size(); k++ )
		{
			dLowerVolume -= (double)adwVolume[k];
			if( dLowerVolume < 0 )
				break;
		}
		nLower	=	k;

		for( k=adwPrice.size()-1; k>=0; k-- )
		{
			dUpperVolume -= (double)adwVolume[k];
			if( dUpperVolume < 0 )
				break;
		}
		nUpper	=	k;
		
		if( nLower < 0 || nLower > nUpper || nUpper >= adwPrice.size() )
			return false;

		double dLower = 0.001 * adwPrice[nLower];
		double dUpper = 0.001 * adwPrice[nUpper];
		if( pdLower )	*pdLower	=	dLower;
		if( pdUpper )	*pdUpper	=	dUpper;
		if( pdMassPrice && adwPrice.size() >= 2 )
		{
			double dPriceRange = 0.001 * ((double)adwPrice[adwPrice.size()-1] - (double)adwPrice[0]);
			if( dPriceRange > 1e-4 )
				*pdMassPrice	=	(dUpper-dLower)/dPriceRange;
			if( *pdMassPrice < 0 )	*pdMassPrice	=	0;
			if( *pdMassPrice > 1 )	*pdMassPrice	=	1;
		}
	}

	return true;
}
