#include "PSY.h"
#include "Tech.h"
#include "../FacilityBaseLib/KData.h"
//////////////////////////////////////////////////////////////////////
//	CPSY
CPSY::CPSY( )
{
	SetDefaultParameters( );
}

CPSY::CPSY( KdataContainer * pKData )
: TechnicalIndicator( pKData )
{
	SetDefaultParameters( );
}

CPSY::~CPSY()
{
	clear( );
}

void CPSY::SetDefaultParameters( )
{
	m_nDays			=	12;
	m_itsSold		=	ITS_BUY;
	m_itsBought		=	ITS_SELL;
}

void CPSY::attach( CPSY & src )
{
	m_nDays			=	src.m_nDays;
	m_itsSold		=	src.m_itsSold;
	m_itsBought		=	src.m_itsBought;
}

bool CPSY::IsValidParameters( )
{
	return ( VALID_DAYS(m_nDays) && VALID_ITS(m_itsSold) && VALID_ITS(m_itsBought) );
}

void CPSY::clear( )
{
	TechnicalIndicator::clear( );
}

int CPSY::signal( int nIndex, UINT * pnCode )
{
	if( pnCode )	*pnCode	=	ITSC_NOTHING;

	if( nIndex <= 0 )
		return ITS_NOTHING;

	double	dPSY = 0, dPSYLast;
	if( !calc( &dPSYLast, nIndex-1, false )
		|| !calc( &dPSY, nIndex, false ) )
		return ITS_NOTHING;
	if( dPSY < 30 && dPSY >= dPSYLast )
	{	// 超卖
		if( pnCode )	*pnCode	=	ITSC_OVERSOLD;
		return m_itsSold;
	}
	if( dPSY > 70 && dPSY <= dPSYLast )
	{	// 超买
		if( pnCode )	*pnCode	=	ITSC_OVERBOUGHT;
		return m_itsBought;
	}
	return	ITS_NOTHING;
}

bool CPSY::min_max_info(int nStart, int nEnd,
						 double *pdMin, double *pdMax )
{
	if( pdMin )	*pdMin	=	0;
	if( pdMax )	*pdMax	=	100;
	return true;
}

/***
N日内上涨的天数
PSY = —————————— × 100
N
*/
bool CPSY::calc( double * pValue, int nIndex, bool bUseLast )
{
	STT_ASSERT_CALCULATE1( m_pKData, nIndex );

	if( m_nDays > nIndex )
		return false;
	if( load_from_cache( nIndex, pValue ) )
		return true;

	double	UD	=	0;
	int		nCount	=	0;
	for( int k=nIndex; k>=1; k-- )
	{
		if( m_pKData->MaindataAt(k) > m_pKData->MaindataAt(k-1) )
			UD	+=	1;

		nCount	++;
		if( nCount == m_nDays )
		{
			if( pValue )	*pValue	=	UD * 100 / m_nDays;
			store_to_cache( nIndex, pValue );
			return true;
		}
	}
	return false;
}
