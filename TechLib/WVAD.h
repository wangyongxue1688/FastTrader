//	威廉变异离散量WVAD
#ifndef _TECH_WVAD_H_
#define _TECH_WVAD_H_
#include "TechLib.h"
#include "Technique.h"
//	简易波动指标EMV
class TECH_API  CWVAD : public TechnicalIndicator
{
public:
	// Constructors
	CWVAD( );
	CWVAD( KdataContainer * pKData );
	virtual ~CWVAD();

public:
	virtual	void clear( );

	// Attributes
	uint32_t		m_nDays;
	int		m_itsLong;
	int		m_itsShort;
	virtual	void	SetDefaultParameters( );
	void	attach( CWVAD & src );
	virtual	bool	IsValidParameters( );

	// Operations
    virtual	int		signal( size_t nIndex, uint32_t * pnCode = NULL );
    virtual	bool	min_max_info( size_t nStart, size_t nEnd, double *pdMin, double *pdMax );
    virtual	bool	calc( double * pValue, size_t nIndex, bool bUseLast );
};
#endif
