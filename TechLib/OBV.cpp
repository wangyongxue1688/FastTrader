#include "OBV.h"
#include "Tech.h"
#include "../FacilityBaseLib/KData.h"
//////////////////////////////////////////////////////////////////////
//	COBV
COBV::COBV( )
{
	SetDefaultParameters( );
}

COBV::COBV( KdataContainer * pKData )
	: TechnicalIndicator( pKData )
{
	SetDefaultParameters( );
}

COBV::~COBV()
{
	clear( );
}

void COBV::SetDefaultParameters( )
{
	m_itsDeviateOnBottom	=	ITS_BUY;
	m_itsDeviateOnTop		=	ITS_SELL;
}

void COBV::attach( COBV & src )
{
	m_itsDeviateOnBottom	=	src.m_itsDeviateOnBottom;
	m_itsDeviateOnTop		=	src.m_itsDeviateOnTop;
}

bool COBV::IsValidParameters( )
{
	return ( VALID_ITS(m_itsDeviateOnBottom) && VALID_ITS(m_itsDeviateOnTop) );
}

void COBV::clear( )
{
	TechnicalIndicator::clear( );
}

int COBV::signal( int nIndex, UINT * pnCode )
{
	if( pnCode )	*pnCode	=	ITSC_NOTHING;
	prepare_cache( 0, -1, false );

	if( is_deviate_on_bottom( nIndex, m_pdCache1, m_pdCache2 ) )
	{	// 底背离
		if( pnCode )	*pnCode	=	ITSC_DEVIATEONBOTTOM;
		return m_itsDeviateOnBottom;
	}
	if( is_deviate_on_top( nIndex, m_pdCache1, m_pdCache2 ) )
	{	// 顶背离
		if( pnCode )	*pnCode	=	ITSC_DEVIATEONTOP;
		return m_itsDeviateOnTop;
	}

	return	ITS_NOTHING;
}

bool COBV::min_max_info(int nStart, int nEnd, double *pdMin, double *pdMax )
{
	return AfxGetMinMaxInfo1( nStart, nEnd, pdMin, pdMax, this );
}

/***
	当日收盘价比前一日收盘价高，其成交量记为正数
	当日收盘价较前一日收盘价低，其成交量记为负数
	累计每日之正或负成交量，即得OBV值
*/
bool COBV::calc( double * pValue, int nIndex, bool bUseLast )
{
	STT_ASSERT_CALCULATE1( m_pKData, nIndex );

	if( load_from_cache( nIndex, pValue ) )
		return true;

	// Calculate
	double	dValueNew = 0;
	if( bUseLast && pValue )
	{
		if( 0 == nIndex )
			dValueNew	=	m_pKData->at(nIndex).Volume;
		else if( m_pKData->MaindataAt(nIndex) > m_pKData->MaindataAt(nIndex-1) )
			dValueNew	=	*pValue + m_pKData->at(nIndex).Volume;
		else if( m_pKData->MaindataAt(nIndex) < m_pKData->MaindataAt(nIndex-1) )
			dValueNew	=	*pValue - m_pKData->at(nIndex).Volume;
		else
			dValueNew	=	*pValue;

		store_to_cache( nIndex, &dValueNew );
	}
	else
	{
		for( int k=0; k<=nIndex; k++ )
		{
			if( 0 == k )
				dValueNew	=	m_pKData->at(k).Volume;
			else if( m_pKData->MaindataAt(k) > m_pKData->MaindataAt(k-1) )
				dValueNew	+=	m_pKData->at(k).Volume;
			else if( m_pKData->MaindataAt(k) < m_pKData->MaindataAt(k-1) )
				dValueNew	-=	m_pKData->at(k).Volume;

			store_to_cache( k, &dValueNew );
		}
	}

	if( pValue )
		*pValue	=	dValueNew;
	return true;
}
