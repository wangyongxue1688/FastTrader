#include "stdafx.h"
#include "HSL.h"
#include "Tech.h"
#include "../FacilityBaseLib/KData.h"
#include "../FacilityBaseLib/Express.h"
#include "../FacilityBaseLib/InstrumentData.h"
//////////////////////////////////////////////////////////////////////
//	CHSL
CHSL::CHSL()
{
	SetDefaultParameters();
}

CHSL::CHSL(KdataContainer * pKData)
	: TechnicalIndicator(pKData)
{
	SetDefaultParameters();
}

CHSL::~CHSL()
{
	clear();
}

double CHSL::GetMainChangeHand(DWORD dwMarket, KdataContainer & kdata, int nIndexKD)
{
	if (nIndexKD < 0 || nIndexKD >= kdata.size())
		return 0.0;

	KDATA&	kd = kdata.at(nIndexKD);

	// ignore dwMarket
	dwMarket = InstrumentData::marketSHSE;

	InstrumentData  stockMain/* = AfxGetStockMain(dwMarket)*/;
	InstrumentInfo & infoMain = stockMain.GetInstrumentInfo();
	KdataContainer & kdataMain = stockMain.GetKData(kdata.GetKType());
	int nIndexMain = kdataMain.GetIndexByDate(kd.TradingDate);
	if (nIndexMain < 0 || nIndexMain >= kdataMain.size())
		return 0.0;
	KDATA& kdMain = kdataMain.at(nIndexMain);

	double	dPriceAvg = 0.0;
	if (kdMain.Volume > 1)
		dPriceAvg = kdMain.Turnover / kdMain.Turnover;

	double	dCapitalValue = 0.0;
// 	if (0 == strcmp(STKLIB_CODE_MAIN, infoMain.GetStockCode()))
// 	{
// 		double	dFactor = 5.85 * 1e8;
// 		CSPTime	sptime;
// 		if (sptime.FromStockTime(kd.m_date, CKData::IsDayOrMin(kdata.GetKType())))
// 		{
// 			CSPTime	tm0(2004, 5, 19, 0, 0, 0);
// 			CSPTimeSpan	span = sptime - tm0;
// 
// 			if (span.GetDays() < -3000)
// 			{
// 				dFactor *= (1 - 0.08*3000.0 / 365.0);
// 				dFactor *= (1 - (-3000 - span.GetDays()) / 2000.0);
// 			}
// 			else
// 			{
// 				dFactor *= (1 + 0.08*span.GetDays() / 365.0);
// 			}
// 		}
// 		dCapitalValue = dFactor * (kdMain.ClosePrice + kdMain.OpenPrice + kdMain.HighestPrice + kdMain.LowestPrice) / 4;
// 	}
// 	else if (0 == strcmp(STKLIB_CODE_MAINSZN, infoMain.GetStockCode()))
// 	{
// 		// 深证成指
// 		dCapitalValue = 0.41 * 1e8 * (kdMain.ClosePrice + kdMain.OpenPrice + kdMain.HighestPrice + kdMain.LowestPrice) / 4;
// 	}

	double dChangeHand = 0.0;
	if (dCapitalValue > 1e-6)
		dChangeHand = 100. * kdMain.Turnover / dCapitalValue;

	return dChangeHand;
}

void CHSL::SetDefaultParameters()
{
	m_nDays = 30;
	m_nMADays = 6;
	m_itsGoldenFork = ITS_BUY;
	m_itsDeadFork = ITS_SELL;
}

void CHSL::AttachParameters(CHSL & src)
{
	m_nDays = src.m_nDays;
	m_nMADays = src.m_nMADays;
	m_itsGoldenFork = src.m_itsGoldenFork;
	m_itsDeadFork = src.m_itsDeadFork;
}

bool CHSL::IsValidParameters()
{
	return (VALID_DAYS(m_nDays) && VALID_DAYS(m_nMADays) && VALID_ITS(m_itsGoldenFork) && VALID_ITS(m_itsDeadFork));
}

void CHSL::Clear()
{
	TechnicalIndicator::clear();
}

int CHSL::signal(int nIndex, UINT * pnCode)
{
	if (pnCode)	*pnCode = ITSC_NOTHING;
	return ITS_NOTHING;
}

bool CHSL::min_max_info(int nStart, int nEnd,
	double *pdMin, double *pdMax)
{
	return AfxGetMinMaxInfo1(nStart, nEnd, pdMin, pdMax, this);
}

/***
相对换手率
*/
bool CHSL::calc(double * pValue, int nIndex, bool bUseLast)
{
	STT_ASSERT_CALCULATE1(m_pKData, nIndex);

	if (load_from_cache(nIndex, pValue))
		return true;

	double	dRatioChangeHand = 0;
	double	dVolume = m_pKData->at(nIndex).Volume;
	if (m_stockinfo.is_valid() && m_stockinfo.GetRatioChangeHand(&dRatioChangeHand, dVolume))
	{
		// 相对换手率
		double	dMainChangeHand = GetMainChangeHand(/*m_stockinfo.GetMarket()*/0, *m_pKData, nIndex);
		double	dRelativeChangeHand = 1.0;
		if (dMainChangeHand > 1e-6)
		{
			dRelativeChangeHand = dRatioChangeHand / dMainChangeHand;
			if (pValue)
				*pValue = dRelativeChangeHand;
			store_to_cache(nIndex, pValue);
			return true;
		}
	}
	else
	{
		if (pValue)
			*pValue = 1.0;
		store_to_cache(nIndex, pValue);
		return true;
	}

	return false;
}

/***
计算HSL及其移动平均值
*/
bool CHSL::calc(double * pValue, int nIndex, int nDays, bool bUseLast)
{
	STT_ASSERT_CALCULATE1(m_pKData, nIndex);

	if (nDays > nIndex + 1)
		return false;

	double	dValue = 0, dMA = 0;
	int	nCount = 0;
	for (int k = nIndex; k >= 0; k--)
	{
		double	dTemp = 0;
		if (calc(&dTemp, k, false))
		{
			if (nIndex == k)
				dValue = dTemp;
			dMA += dTemp;

			nCount++;
			if (nCount == nDays)
			{
				dMA = dMA / nDays;
				if (pValue)	*pValue = dMA;
				return true;
			}
		}
	}
	return false;
}
