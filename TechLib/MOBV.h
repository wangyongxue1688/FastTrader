#pragma once
#include "OBV.h"


//	��������MOBV
class CMOBV : public COBV
{
public:
	// Constructors
	CMOBV();
	CMOBV(KdataContainer * pKData);
	virtual ~CMOBV();

public:
	virtual	void clear();

	// Attributes
	int		m_nDays1;
	int		m_nDays2;
	int		m_itsGoldenFork;
	int		m_itsDeadFork;
	virtual	void	SetDefaultParameters();
	void	AttachParameters(CMOBV & src);
	virtual	bool	IsValidParameters();

	// Operations
        virtual	int		GetSignal(int nIndex, uint32_t * pnCode = NULL);
	virtual	bool	min_max_info(int nStart, int nEnd, double *pdMin, double *pdMax);
	virtual	bool	calc(double * pValue1, double * pValue2, double * pValue3, int nIndex, bool bUseLast);
};

