#ifndef _TECH_USER_DRV_H_
#define _TECH_USER_DRV_H_
#include "TechLib.h"
#include "../FacilityBaseLib/InstrumentInfo.h"
//////////////////////////////////////////////////////////////////////
//	CTechUserDrv

class custom_indicator_loader
{
public:
	custom_indicator_loader();
	~custom_indicator_loader();

	bool	LoadDriver( );

	UINT	GetTechUserCount( );
	bool	GetTechUserInfo( UINT nID, PTECHUSER_INFO pInfo );
	bool	Calculate( UINT nID, PCALCULATE_INFO pInfo );
	int		GetSignal( UINT nID, PCALCULATE_INFO pInfo );
	friend custom_indicator_loader & GetTechUserDrv( );
protected:
	UINT	(* m_pfnGetTechUserCount)( );
	bool	(* m_pfnGetTechUserInfo)( UINT nID, PTECHUSER_INFO pInfo );
	bool	(* m_pfnCalculate)( UINT nID, PCALCULATE_INFO pInfo );
	int		(* m_pfnGetSignal)( UINT nID, PCALCULATE_INFO pInfo );
	UINT	m_hDrv;
};
#endif

