#pragma once
#include "TechLib.h"
#include "Technique.h"

//	���ڶ�ָ��UOS
class TECH_API CUOS : public TechnicalIndicator
{
public:
	// Constructors
	CUOS( );
	CUOS( KdataContainer * pKData );
	virtual ~CUOS();

public:
	virtual	void clear( );

	// Attributes
	uint32_t		m_nDays1;
	uint32_t		m_nDays2;
	uint32_t		m_nDays3;
	uint32_t		m_nMADays;
	int		m_itsGoldenFork;
	int		m_itsDeadFork;
	virtual	void	SetDefaultParameters( );
	void	AttachParameters( CUOS & src );
	virtual	bool	IsValidParameters( );

	// Operations
	virtual	int		signal( size_t nIndex, uint32_t * pnCode = NULL );
	virtual	bool	min_max_info(size_t nStart, size_t nEnd, double *pdMin, double *pdMax);
	virtual	bool	calc(double * pValue, size_t nIndex, bool bUseLast);
	virtual	bool	calc( double * pValue, double * pMA, int nIndex, bool bUseLast );
};


