#ifndef __CTP_MARKET_H__
#define __CTP_MARKET_H__
#include "ThostFtdcMdApi.h"
#include "ServerInfo.h"
#include "UserInfo.h"
#include "DataTypes.h"
#include <vector>
#include "../ServiceLib/Market.h"

#ifdef _WIN32
#if _MSC_VER >=1700 
#include <mutex>
#include <condition_variable>
using namespace std;
#else
#include <boost/thread/mutex.hpp>
#include <boost/thread/condition_variable.hpp>
using namespace boost;
#endif
#else
#include <mutex>
#include <condition_variable>
using namespace std;
#endif



class CThostFtdcMdApi;
//行情;
class CtpMarket : public CThostFtdcMdSpi,public Market
{
public:
	///当客户端与交易后台建立起通信连接时（还未登录前），该方法被调用。
	virtual void OnFrontConnected();

	///当客户端与交易后台通信连接断开时，该方法被调用。当发生这个情况后，API会自动重新连接，客户端可不做处理。
	///@param nReason 错误原因
	///        0x1001 网络读失败
	///        0x1002 网络写失败
	///        0x2001 接收心跳超时
	///        0x2002 发送心跳失败
	///        0x2003 收到错误报文
	virtual void OnFrontDisconnected(int nReason);

	///心跳超时警告。当长时间未收到报文时，该方法被调用。
	///@param nTimeLapse 距离上次接收报文的时间
	virtual void OnHeartBeatWarning(int nTimeLapse);

	///登录请求响应
	virtual void OnRspUserLogin(CThostFtdcRspUserLoginField *pRspUserLogin, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) ;

	///登出请求响应
	virtual void OnRspUserLogout(CThostFtdcUserLogoutField *pUserLogout, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) ;

	///错误应答
	virtual void OnRspError(CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) ;

	///订阅行情应答
	virtual void OnRspSubMarketData(CThostFtdcSpecificInstrumentField *pSpecificInstrument, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) ;

	///取消订阅行情应答
	virtual void OnRspUnSubMarketData(CThostFtdcSpecificInstrumentField *pSpecificInstrument, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) ;

	///订阅询价应答
	virtual void OnRspSubForQuoteRsp(CThostFtdcSpecificInstrumentField *pSpecificInstrument, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) ;

	///取消订阅询价应答
	virtual void OnRspUnSubForQuoteRsp(CThostFtdcSpecificInstrumentField *pSpecificInstrument, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast) ;

	///深度行情通知
	virtual void OnRtnDepthMarketData(CThostFtdcDepthMarketDataField *pDepthMarketData) ;

	///询价通知
	//virtual void OnRtnForQuoteRsp(CThostFtdcForQuoteRspField *pForQuoteRsp) ;
public:
	//构造函数;
	CtpMarket();
	virtual ~CtpMarket();
	//初始化;
	bool Init(const ServerInfo& s);
	//登录;
	bool Login(const UserLoginParam&  u);
public:
	bool ReqUserLogin();
	//确保这2个函数是线程安全的;
	bool SubscribeMarketData(const std::vector<std::string>& instruments,const std::string& exchg="");
	bool UnSubscribeMarketData(const std::vector<std::string>& instruments,const std::string& exchg="");
protected:
	bool SubscribeMarketDataImpl(const std::vector<std::string>& instruments);
	bool IsErrorRspInfo(CThostFtdcRspInfoField *pRspInfo);
	CtpMarket(const CtpMarket& other);
	CtpMarket& operator=(const CtpMarket& other);
	//行情接口;
	CThostFtdcMdApi* m_MdApi;
};



#endif
