#include "CtpMarket.h"
#include "../Log/logging.h"
#include "file_helper.h"
#include "Tick.h"
#include <iostream>
#include <algorithm>
#include <tuple>
#include <string.h>
#include <iterator>
#include <boost/make_shared.hpp>
#include <boost/date_time/posix_time/ptime.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
//#include "../DateTime/DateTime.h"
#ifdef _MSC_VER
#pragma warning(disable : 4996)
#if _MSC_VER >=1700 
#include <chrono>
using namespace std;
#else
#include <boost/chrono/chrono.hpp>
using namespace boost;
#endif
#else
#include <chrono>
using namespace std;
#endif
#include "../common/Statistic.h"

#ifdef _MSC_VER
#pragma warning(disable : 4996)
#pragma comment(lib,"thostmduserapi.lib")
#endif

CtpMarket::CtpMarket()
	:Market(),m_MdApi(NULL)
{
	LOGINIT("ctp_market");
	m_bFrontDisconnected=false;
	m_bIsLogined=false;
	m_nRequestID = 0;
}


bool CtpMarket::Init( const ServerInfo& s )
{
	m_ServerInfo=s;
	if (!m_MdApi)
	{
		std::string subdir=m_ServerInfo.id,wkdir=get_work_dir();
		create_dir(subdir,wkdir);
		m_MdApi = CThostFtdcMdApi::CreateFtdcMdApi((m_ServerInfo.id+"/").c_str());
		m_MdApi->RegisterSpi((CThostFtdcMdSpi*)this);
		char front_addr[256]={0};
		for (std::size_t i=0;i<m_ServerInfo.market_server_front.size();++i)
		{
			strcpy(front_addr,m_ServerInfo.market_server_front[i].to_string().c_str());
			m_MdApi->RegisterFront(front_addr);
		}
	}
	return true;
}

bool CtpMarket::Login( const UserLoginParam& u )
{
	//DEBUG_METHOD();
	if (!m_MdApi)
	{
		return false;
	}
	m_UserInfo=u;
	STAT_BEGIN("连接行情前置");
	m_MdApi->Init();

	//等待登录完成;
	boost::unique_lock<boost::mutex> lck(m_LoginMutex);
	int login_timeout = m_ServerInfo.market_login_timeout;
	if (login_timeout <= 0)
	{
		login_timeout = 30;
	}
	if (m_LoginCondVar.wait_for(lck, boost::chrono::seconds(login_timeout)) == boost::cv_status::timeout)
	{
		LOGDEBUG("行情登录超时...");
		return false;
	}
	return m_bIsLogined;
}


void CtpMarket::OnRspError(CThostFtdcRspInfoField *pRspInfo,
						int nRequestID, bool bIsLast)
{
	IsErrorRspInfo(pRspInfo);
}

void CtpMarket::OnFrontDisconnected(int nReason)
{
	LOGDEBUG("CtpMarket::OnFrontDisconnected:{}",nReason);
	m_bFrontDisconnected=true;
	m_bIsLogined=false;
}

void CtpMarket::OnHeartBeatWarning(int nTimeLapse)
{
	LOGDEBUG("OnHeartBeatWarning:{}",nTimeLapse);
}


bool CtpMarket::ReqUserLogin()
{
	if (!m_MdApi)
	{
		return false;
	}
	CThostFtdcReqUserLoginField req={0};
	strcpy(req.BrokerID, m_UserInfo.MdBrokerID.c_str());
	strcpy(req.UserID, m_UserInfo.MdUserID.c_str());
	strcpy(req.Password, m_UserInfo.MdPassword.c_str());
	STAT_BEGIN("行情登录");
	int iResult = m_MdApi->ReqUserLogin(&req, ++RequestID());
	if (iResult!=0)
	{
		iResult = m_MdApi->ReqUserLogin(&req, ++RequestID());
	}
	return iResult==0;
}


void CtpMarket::OnFrontConnected()
{
	LOGDEBUG("行情前端连接...");
	if (m_bFrontDisconnected)
	{
		STAT_FINISH("连接行情前置重连");
		m_bFrontDisconnected=false;
	}
	STAT_FINISH("连接行情前置");

	///用户登录请求;
	bool bReq=ReqUserLogin();
	if(!bReq)
	{
		LOGDEBUG("行情发起登录失败...");
	}
}


void CtpMarket::OnRspUserLogin(CThostFtdcRspUserLoginField *pRspUserLogin,
							CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	LOGDEBUG("Market OnRspUserLogin...");
	if (bIsLast && !IsErrorRspInfo(pRspInfo))
	{
		LOGDEBUG("market login success,the market trading day is:{}",m_MdApi->GetTradingDay());
		if (!m_bIsLogined)
		{
			m_bIsLogined=true;
			m_LoginCondVar.notify_one();
		}
		//NotifyApiMgr(RspUserLogined,LPARAM(&m_UserInfo));
		sigOnUserLogin();
	}
	else
	{
		LOGDEBUG("行情登录失败...");
	}

	if (!m_SubInstruments.empty())
	{
		auto instIter=m_SubInstruments.begin();
		for (;instIter!=m_SubInstruments.end();++instIter)
		{
			SubscribeMarketDataImpl(instIter->second);
		}
	}
}

void CtpMarket::OnRspUserLogout(CThostFtdcUserLogoutField *pUserLogout, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	LOGDEBUG("行情前端登出响应...");
	if (bIsLast && !IsErrorRspInfo(pRspInfo))
	{
		STAT_FINISH_MORE("行情登出", "成功");
		if (m_bIsLogined)
		{
			m_bIsLogined = false;
			//m_LoginCondVar.notify_one();
		}
		//NotifyApiMgr(RspUserLogined, LPARAM(&m_UserInfo));
	}
	else
	{
		LOGDEBUG("行情登出失败...");
		STAT_FINISH_MORE("行情登出", "失败");
	}
}

bool CtpMarket::SubscribeMarketDataImpl(const std::vector<std::string>& unSubInstruments)
{
	char **ppInstrumentID = new char*[unSubInstruments.size()];
	for (std::size_t i = 0; i < unSubInstruments.size(); i++)
	{
		ppInstrumentID[i] = new char[sizeof(TThostFtdcInstrumentIDType)];
		if (unSubInstruments[i].empty())
		{
			continue;
		}
		strcpy(ppInstrumentID[i], unSubInstruments[i].c_str());
	}

	//该用户已登录;
	LOGDEBUG("行情订阅合约...");
	int iResult = m_MdApi->SubscribeMarketData(ppInstrumentID, (int)unSubInstruments.size());
	for (unsigned int j = 0; j < unSubInstruments.size(); j++)
	{
		delete ppInstrumentID[j];
		ppInstrumentID[j] = NULL;
	}
	delete ppInstrumentID;
	ppInstrumentID = NULL;
	return iResult == 0;
}


bool CtpMarket::SubscribeMarketData(const std::vector<std::string>& instruments,const std::string& exchg/*=""*/)
{
	if (!m_MdApi)
	{
		return false;
	}
	//获取没有订阅的;
	std::vector<std::string> unSubInstruments;
	if (!SubFilter(instruments,exchg,unSubInstruments))
	{
		return true;
	}
	return SubscribeMarketDataImpl(unSubInstruments);
}

void CtpMarket::OnRspSubMarketData(CThostFtdcSpecificInstrumentField *pSpecificInstrument, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	if (!IsErrorRspInfo(pRspInfo))
	{
		if (pSpecificInstrument)
		{
			unique_lock<mutex> lck(m_MutInst);
			std::string subInst(pSpecificInstrument->InstrumentID);
			auto iiter=std::find_if(m_SubInstruments.begin(),m_SubInstruments.end(),
				[&subInst](const std::map<std::string,std::vector<std::string> >::value_type& v)
			{
				return std::find(v.second.begin(),v.second.end(),subInst)!=v.second.end();
			});
			if (iiter==m_SubInstruments.end())
			{
				auto& vecInsts = m_SubInstruments[""];
				vecInsts.push_back(subInst);
			}
			//NotifyApiMgr(RspSubMarketData,LPARAM(&subInst));
		}
	}
	else
	{
		LOGDEBUG("CTP行情订阅出错");
	}
}

void CtpMarket::OnRspUnSubMarketData(CThostFtdcSpecificInstrumentField *pSpecificInstrument, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{
	if (!IsErrorRspInfo(pRspInfo))
	{
		if (pSpecificInstrument)
		{
			unique_lock<mutex> lck(m_MutInst);
			std::string subInst(pSpecificInstrument->InstrumentID);
			auto iiter=std::find_if(m_SubInstruments.begin(),m_SubInstruments.end(),
				[&subInst](const std::map<std::string,std::vector<std::string> >::value_type& v)
			{
				return std::find(v.second.begin(),v.second.end(),subInst)!=v.second.end();
			});
			if (iiter!=m_SubInstruments.end())
			{
				std::remove(iiter->second.begin(),iiter->second.end(),subInst);
			}
		}
	}
	else
	{
		LOGDEBUG("CTP行情取消订阅出错");
	}
}

void CtpMarket::OnRspSubForQuoteRsp(CThostFtdcSpecificInstrumentField *pSpecificInstrument, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{

}

void CtpMarket::OnRspUnSubForQuoteRsp(CThostFtdcSpecificInstrumentField *pSpecificInstrument, CThostFtdcRspInfoField *pRspInfo, int nRequestID, bool bIsLast)
{

}

void CtpMarket::OnRtnDepthMarketData(CThostFtdcDepthMarketDataField *pDepthMarketData)
{
	//保存深度行情;
	if (pDepthMarketData)
	{
		if (strlen(pDepthMarketData->TradingDay) <= 0)
		{
			LOGDEBUG( "TradingDay is Error!");
			return;
		}
		bool bPriceIsValid = true;
		double InvalidPrice  = (std::numeric_limits<double>::max)();
		//std::cout << "InvalidPrice=" << InvalidPrice << std::endl;
// 		if (InvalidPrice == pDepthMarketData->OpenPrice)
// 		{
// 			//std::cout << pDepthMarketData->InstrumentID<< " OpenPrice is Error!" << std::endl;
// 			bPriceIsValid = false;
// 		}
// 		else 
// 		if (InvalidPrice == pDepthMarketData->HighestPrice)
// 		{
// 			//std::cout << pDepthMarketData->InstrumentID << " HighestPrice is Error!" << std::endl;
// 			bPriceIsValid = false;
// 		}
// 		else if (InvalidPrice == pDepthMarketData->LowestPrice)
// 		{
// 			//std::cout << pDepthMarketData->InstrumentID << " LowestPrice is Error!" << std::endl;
// 			bPriceIsValid = false;
// 		}
// 		else if (InvalidPrice ==  pDepthMarketData->LastPrice)
// 		{
// 			//std::cout << pDepthMarketData->InstrumentID << " LastPrice is Error!" << std::endl;
// 			bPriceIsValid = false;
// 		}
//		else 
		if (InvalidPrice == pDepthMarketData->BidPrice1)
		{
			//std::cout << pDepthMarketData->InstrumentID << " BidPrice1 is Error!" << std::endl;
			bPriceIsValid = false;
		}
		else if (InvalidPrice ==  pDepthMarketData->AskPrice1)
		{
			//std::cout << pDepthMarketData->InstrumentID << " AskPrice1 is Error!" << std::endl;
			bPriceIsValid = false;
		}
// 		else if (InvalidPrice == pDepthMarketData->OpenInterest)
// 		{
// 			//std::cout << pDepthMarketData->InstrumentID << " OpenInterest is Error!" << std::endl;
// 			bPriceIsValid = false;
// 		}
		else
		{
// 			static boost::posix_time::time_duration local_update_time =
// 				boost::posix_time::second_clock::local_time().time_of_day();
// 			auto tick_update_time =
// 				boost::posix_time::time_from_string(pDepthMarketData->UpdateTime);
// 			auto time_delta = (local_update_time - tick_update_time);
// 			if (time_delta.total_seconds() > 30)
// 			{
// 				//相差超过30秒;
// 				bPriceIsValid = false;
// 				LOGDEBUG("{} UpdateTime {} is Error!", pDepthMarketData->InstrumentID,
// 					pDepthMarketData->UpdateTime);
// 			}
			bPriceIsValid = true;
		}
		if (!bPriceIsValid)
		{
			//LOGDEBUG("行情接收中...");
			return;
		}

		//筛选出有问题的行情;
		boost::shared_ptr<Tick> tick = boost::make_shared<Tick>();
		strcpy(tick->szCode,pDepthMarketData->InstrumentID);
		strcpy(tick->szExchange,pDepthMarketData->ExchangeID);
		strcpy(tick->TradingDay,pDepthMarketData->TradingDay);
		tick->AskPrice[0]=pDepthMarketData->AskPrice1;
		tick->AskVolume[0]=pDepthMarketData->AskVolume1;

// 		tick->AskPrice[1] = pDepthMarketData->AskPrice2;
// 		tick->AskVolume[1] = pDepthMarketData->AskVolume2;
// 
// 		tick->AskPrice[2] = pDepthMarketData->AskPrice3;
// 		tick->AskVolume[2] = pDepthMarketData->AskVolume3;
// 
// 		tick->AskPrice[3] = pDepthMarketData->AskPrice4;
// 		tick->AskVolume[3] = pDepthMarketData->AskVolume4;
// 
// 		tick->AskPrice[4] = pDepthMarketData->AskPrice5;
// 		tick->AskVolume[4] = pDepthMarketData->AskVolume5;

		
		tick->BidPrice[0]=pDepthMarketData->BidPrice1;
		tick->BidVolume[0]=pDepthMarketData->BidVolume1;

// 		tick->BidPrice[1] = pDepthMarketData->BidPrice2;
// 		tick->BidVolume[1] = pDepthMarketData->BidVolume2;
// 
// 		tick->BidPrice[2] = pDepthMarketData->BidPrice3;
// 		tick->BidVolume[2] = pDepthMarketData->BidVolume3;
// 
// 		tick->BidPrice[3] = pDepthMarketData->BidPrice4;
// 		tick->BidVolume[3] = pDepthMarketData->BidVolume4;
// 
// 		tick->BidPrice[4] = pDepthMarketData->BidPrice5;
// 		tick->BidVolume[4] = pDepthMarketData->BidVolume5;

		tick->AveragePrice = pDepthMarketData->AveragePrice;
		tick->ClosePrice=pDepthMarketData->ClosePrice;
		tick->HighestPrice=pDepthMarketData->HighestPrice;
		tick->LastPrice=pDepthMarketData->LastPrice;
		tick->LowerLimitPrice=pDepthMarketData->LowerLimitPrice;
		tick->LowestPrice=pDepthMarketData->LowestPrice;
		tick->OpenInterest=pDepthMarketData->OpenInterest;
		tick->OpenPrice=pDepthMarketData->OpenPrice;
		tick->PreClosePrice=pDepthMarketData->PreClosePrice;
		tick->PreSettlementPrice=pDepthMarketData->PreSettlementPrice;
		tick->PreOpenInterest=pDepthMarketData->PreOpenInterest;
		tick->SettlementPrice=pDepthMarketData->SettlementPrice;
		tick->Turnover=pDepthMarketData->Turnover;
		tick->UpdateMillisec=pDepthMarketData->UpdateMillisec;
		strcpy(tick->UpdateTimeStr, pDepthMarketData->UpdateTime);
		tick->UpperLimitPrice=pDepthMarketData->UpperLimitPrice;
		tick->Volume=pDepthMarketData->Volume;

		//LOGDEBUG("RtnDepthMarket:{}--{}.{}", tick->szCode , tick->UpdateTimeStr,tick->UpdateMillisec );
		//LOGDEBUG("begin sigOnTick...");
		sigOnTick(tick);
		//LOGDEBUG("end sigOnTick...");
		//NotifyApiMgr(RtnDepthMarketData,LPARAM(tick.get()));
	}
}

bool CtpMarket::IsErrorRspInfo(CThostFtdcRspInfoField *pRspInfo)
{
	// 如果ErrorID != 0, 说明收到了错误的响应
	bool bResult = ((pRspInfo) && (pRspInfo->ErrorID != 0));
	return bResult;
}

CtpMarket::CtpMarket(const CtpMarket & other)
{
	
}



bool CtpMarket::UnSubscribeMarketData( const std::vector<std::string>& instruments ,const std::string& exchg/*=""*/)
{
	if (!m_MdApi)
	{
		return false;
	}
	std::vector<std::string> unSubInstruments;
	if (!UnSubFilter(instruments,exchg,unSubInstruments))
	{
		return false;
	}
	char **ppInstrumentID=new char*[unSubInstruments.size()];
	for (std::size_t i=0;i<unSubInstruments.size();i++)
	{
		ppInstrumentID[i]=new char[sizeof(TThostFtdcInstrumentIDType)];
		strcpy(ppInstrumentID[i],unSubInstruments[i].c_str());
	}
	//该用户已登录;
	int iResult=m_MdApi->UnSubscribeMarketData(ppInstrumentID,(int)unSubInstruments.size());
	for (unsigned int j=0;j<unSubInstruments.size();j++)
	{
		delete ppInstrumentID[j];
		ppInstrumentID[j]=NULL;
	}
	delete ppInstrumentID;
	ppInstrumentID=NULL;
	return iResult==0;
}

CtpMarket::~CtpMarket()
{
	//释放成员指针...;
	//m_bInited = false;
	//释放对象，参照文档;
	//综合交易平台API开发FAQ.pdf/综合交易平台API开发常见问题列表;
// 	if (NULL != m_MdApi)
// 	{
// 		m_MdApi->RegisterSpi(NULL);
// 		m_MdApi->Release();
// 		m_MdApi = NULL;
// 	}
	LOGUNINIT();
}




