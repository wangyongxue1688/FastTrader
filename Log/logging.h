#ifndef _LOGGING_H_
#define _LOGGING_H_
#ifdef _USE_LOG4CXX
	#include <log4cxx/logger.h>
	#include <log4cxx/basicconfigurator.h>
	#include <log4cxx/propertyconfigurator.h>
	#include <log4cxx/helpers/exception.h>
	#define LOGINIT(file) log4cxx::PropertyConfigurator::configureAndWatch(file)
	#define LOGDEBUG(msg) LOG4CXX_DEBUG(log4cxx::Logger::getLogger("lib"), msg)
#ifdef _MSC_VER
	#pragma comment(lib,"log4cxx.lib")
#endif
#elif defined(_USE_SPDLOG)
#define SPDLOG_DEBUG_ON
#include "spdlog/spdlog.h"
#include <map>
#include <time.h>

struct spdlog_instance 
{
	static std::string& get_name()
	{
		static std::string g_logger("glog");
		return g_logger;
	}

	spdlog_instance(const char* file)
	{
		spdlog::set_async_mode(4096);
		spdlog::set_level(spdlog::level::debug);
		spdlog::set_pattern("%Y%m%d %H:%M:%S.%e [%l][%t] %v");
		
		if (!spdlog::get("console"))
		{
			spdlog::stdout_logger_mt("console");
		}
		if (!spdlog::get(spdlog_instance::get_name()))
		{
			time_t lt = time(0);
			struct tm* pTm = localtime(&lt);
			char szTimeStamp[32] = { 0 };
			strftime(szTimeStamp, sizeof(szTimeStamp), "_%Y%m%d%H%M%S", pTm);
			std::string szFileName = std::string(file) + szTimeStamp + ".log";

			spdlog::rotating_logger_mt(spdlog_instance::get_name(), szFileName, 1024 * 1024 * 50, 3);
		}
	}

	~spdlog_instance()
	{
		
	}

	static void uninit()
	{
		spdlog::drop(spdlog_instance::get_name());
	}
};
#define LOGINIT(file) spdlog_instance(file)
#define LOGUNINIT() spdlog_instance::uninit()
#define LOGDEBUG(...) do{SPDLOG_DEBUG(spdlog::get(spdlog_instance::get_name()),__VA_ARGS__);SPDLOG_DEBUG(spdlog::get("console"),__VA_ARGS__);}while(0)
#elif defined(_USE_GLOG)
	#include <glog/logging.h>
	#include <boost/property_tree/ptree.hpp>
	#include <boost/property_tree/ini_parser.hpp> 
	#include <string>
	class scoped_glog
	{
	public:
		scoped_glog(const char* file)
		{
			boost::property_tree::ptree pt;  
			boost::property_tree::ini_parser::read_ini(file, pt);
			std::string root=pt.get<std::string>("log4j.rootLogger","lib");
			google::InitGoogleLogging(root.c_str());
			FLAGS_log_dir = file;
		}
		~scoped_glog()
		{
			google::ShutdownGoogleLogging();
		}
	};
	#define LOGINIT(file) scoped_glog(file)
	#define LOGDEBUG(msg) LOG(DEBUG) << msg
#else
	#include <iostream>
	#define LOGINIT(file)
	#define LOGDEBUG(msg) std::cout<<msg<<std::endl;
#endif
#endif