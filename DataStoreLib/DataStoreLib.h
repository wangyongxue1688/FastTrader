#ifndef _DATASTORE_LIB_H_
#define _DATASTORE_LIB_H_


#if defined(WIN32)
#ifdef DATASTORELIB_EXPORTS
#define DATASTORE_API __declspec(dllexport)
#else
#define DATASTORE_API __declspec(dllimport)
#endif
#else 
#define	DATASTORE_API
#endif

#ifdef _WIN32
#else
#define MAX_PATH 260
#include <sys/stat.h>
#include <sys/types.h>
#define _mkdir(filename) mkdir(filename, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH)
#endif

#endif
