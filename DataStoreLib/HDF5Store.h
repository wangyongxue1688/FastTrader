#pragma once

#include <boost/shared_ptr.hpp>
#include <string>
#include "../FacilityBaseLib/Instrument.h"

#include <H5Cpp.h>
using namespace H5;

#include <boost/shared_ptr.hpp>
#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/managed_mapped_file.hpp>
#include <boost/interprocess/containers/vector.hpp>
#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/allocators/allocator.hpp>

/*
szip.lib
zlib.lib
hdf5.lib
hdf5_cpp.lib
*/
#pragma  pack(push)  //�������״̬;  
#pragma  pack(1)  

struct report_t
{
	int64_t Time;
	double LastPrice;
	int32_t LVolume;
	double BidPrice;
	int32_t BidVolume;
	double AskPrice;
	int32_t AskVolume;
	double OpenInterest;
	int64_t TradeVolume;
};

#pragma  pack(pop) 

class HDF5Store
{
public:
	typedef boost::interprocess::allocator < report_t,
		boost::interprocess::managed_shared_memory::segment_manager >
		ShmemAllocator;

	typedef boost::interprocess::vector<report_t, ShmemAllocator> MyVector;
public:
	HDF5Store();
	~HDF5Store();
public:
	int Init(const std::string& initStr);
	int LoadCodetable();
	int LoadReport(boost::shared_ptr<instrument> pstock);
protected:
	std::string m_szRootPath;
	std::map<std::string,
	boost::shared_ptr <H5File> > m_TickFiles;
};

