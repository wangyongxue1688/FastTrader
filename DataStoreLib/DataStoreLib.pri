
HEADERS += \
    DataFormat.h \
    DataSql.h \
    DataStoreFace.h \
    DataStoreLib.h \
    IDataStore.h \
    Sqlite3Store.h \
    TradingInfoDB.h

SOURCES += \
    DataStoreFace.cpp \
    IDataStore.cpp \
    Sqlite3Store.cpp \
    TradingInfoDB.cpp

