#ifndef DATA_SQL_H_
#define DATA_SQL_H_

const char* sql_create_table_instruments=
"create table if not exists instruments"
	"(InstrumentID char(31),"
	"ExchangeID char(9),"
	"InstrumentName char(21),"
	"ExchangeInstID char(31),"
	"ProductID char(31),"
	"ProductClass int,"
	"DeliveryYear int,"
	"DeliveryMonth int,"
	"MaxMarketOrderVolume int,"
	"MinMarketOrderVolume int,"
	"MaxLimitOrderVolume int,"
	"MinLimitOrderVolume int,"
	"VolumeMultiple int,"
	"PriceTick double,"
	"CreateDate char(9),"
	"OpenDate char(9),"
	"ExpireDate char(9),"
	"StartDelivDate char(9),"
	"EndDelivDate char(9),"
	"InstLifePhase int,"
	"IsTrading int,"
	"PositionType int,"
	"PositionDateType int,"
	"LongMarginRatio double,"
	"ShortMarginRatio double,"
	"Primary Key(InstrumentID,ExchangeID))";

const char* sql_create_code_table = 
	"create table if not exists code_table"
	"(InstrumentID char(31),"
	"InstrumentName char(21),"
	"InstrumentType int"
	"ShortName char(21),"
	"NameEnu char(21))";

const char* sql_create_table_report=
	"create table if not exists %1%("
	 "InstrumentID char(31),"
	 "ExchangeID char(9),"
	 "TradingDay char(9),"
	 "TradingTime int,"
	 "TradingMillisec int,"
	 "PreClosePrice double,"
	 "OpenPrice double,"
	 "ClosePrice double,"
	 "HighestPrice double,"
	 "LowestPrice double,"
	 "LastPrice double,"
	 "AveragePrice double,"
	 "CurrDelta double,"
	 "PreDelta double,"
	 "PreSettlementPrice double,"
	 "SettlementPrice double,"
	 "UpperLimitPrice double,"
	 "LowerLimitPrice double,"
	 "OpenInterest double,"
	 "Volume double,"
	 "Turnover double,"
	 "BidPrice1 double,"
	 "BidVolume1 int,"
	 "AskPrice1 double,"
	 "AskVolume1 int)";

const char* sql_create_minute=
	"create table if not exists %1%("
	 "InstrumentID char(31),"
	 "ExchangeID char(31),"
	 "InstrumentName char(31),"
	 "LastPrice double,"
	 "HighestPrice double,"
	 "LowestPrice double,"
	 "Volume BIGINT,"
	 "Turnover double,"
	 "UpdateTime int,"
	 "UpdateMillisec int)";
const char* sql_create_exchange=
"create table if not exists exchanges("
	"ExchangeID char(16),"
	"ExchangeName char(32),"
	"ExchangeProperty int,"
	"Primary Key(ExchangeID))";

const char* sql_begin_transaction = "begin transaction;";

const char* sql_commit_transaction = "commit transaction;";
#endif