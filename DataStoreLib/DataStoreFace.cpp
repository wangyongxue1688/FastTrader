#include "DataStoreFace.h"
#include "../FacilityBaseLib/Container.h"
#include "../FacilityBaseLib/InstrumentData.h"
#include "InstrumentStatus.h"
#include <iostream>
#include <boost/bind.hpp>
#include <boost/function.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/algorithm/string.hpp>
#include "../FacilityBaseLib/Container.h"
#include "../FacilityBaseLib/plate.h"
#include "../Log/logging.h"
#include <boost/timer.hpp>


boost::shared_ptr<InstStatus> DataStoreFace::GetStatus(const std::string& InstrumentID, const std::string& UpdateTime)
{
	std::vector<boost::shared_ptr<InstStatus> >& statusTable = m_LocalStatus[InstrumentID];
	if (statusTable.empty())
	{
		return nullptr;
	}
	boost::shared_ptr<InstStatus> pStatusResult = statusTable[0];
	for (size_t i = 0; i < statusTable.size();++i)
	{
		if (statusTable[i]->EnterTime >= UpdateTime)
		{
			pStatusResult = statusTable[i];
		}
		else
		{
			break;
		}
	}
	return pStatusResult;
}


DataStoreFace::DataStoreFace()
	:m_CloseTime(16, 06, 0)
{
	LOGINIT("DataStoreFace");
	m_bStoreDB = true;

	//加载状态文件;
	
	try
	{
		boost::property_tree::ptree ptInstrument;
		boost::property_tree::ini_parser::read_ini("InstrumentStatus.ini", ptInstrument);
		for (auto instIter = ptInstrument.begin(); instIter != ptInstrument.end(); ++instIter)
		{
			std::string InstrumentID = instIter->first;
			auto ptStatus = instIter->second;
			std::vector<boost::shared_ptr<InstStatus> > statusTable;
			for (auto statusIter = ptStatus.begin();statusIter!=ptStatus.end();++statusIter)
			{
				boost::shared_ptr<InstStatus> pStatus = boost::make_shared<InstStatus>();
				pStatus->InstrumentID = InstrumentID;
				pStatus->InstrumentStatus = (EnumInstStatus)std::stoi(statusIter->second.data());
				pStatus->EnterTime = statusIter->first;
				statusTable.push_back(pStatus);
			}
			std::sort(statusTable.begin(), statusTable.end(), 
			[](const boost::shared_ptr<InstStatus>& is1,const boost::shared_ptr<InstStatus>& is2){
				return is1->EnterTime < is2->EnterTime;
			});
			m_LocalStatus[InstrumentID] = statusTable;
		}
	}
	catch (boost::property_tree::ini_parser_error& e1)
	{
		LOGDEBUG("InstrumentStatus.ini parser error:{}", e1.what());
	}
	catch (std::exception& e1)
	{
		LOGDEBUG("InstrumentStatus.ini read error:{}", e1.what());
	}

	try
	{
		boost::property_tree::ptree ptInstrument;
		boost::property_tree::ini_parser::read_ini("LastStatus.ini", ptInstrument);
		for (auto instIter = ptInstrument.begin(); instIter != ptInstrument.end(); ++instIter)
		{
			std::string InstrumentID = instIter->first;
			auto ptStatus = instIter->second;

			boost::shared_ptr<InstStatus> pStatus = boost::make_shared<InstStatus>();
			pStatus->InstrumentID = InstrumentID;
			pStatus->InstrumentStatus = (EnumInstStatus)instIter->second.get<int>("InstrumentStatus");
			pStatus->EnterTime = instIter->second.get<std::string>("EnterTime");
			pStatus->ExchangeID = instIter->second.get<std::string>("ExchangeID");
			m_LastStatus[InstrumentID] = pStatus;
		}
	}
	catch (boost::property_tree::ini_parser_error& e1)
	{
		LOGDEBUG("LastStatus.ini parser error:{}", e1.what());
	}
	catch (std::exception& e1)
	{
		LOGDEBUG("LastStatus.ini read error:{}", e1.what());
	}
}
DataStoreFace::~DataStoreFace()
{
	HandleStore();
	if (m_bStoreDB)
	{
		//交易所信息存盘;
		m_DataStorePtr->StoreExchange(ExchangeContainer::get_instance());
		//合约信息存盘;
		m_DataStorePtr->StoreCodetable(InstrumentContainer::get_instance());
	}
}

void DataStoreFace::HandleStore()
{
	//如果缓存里还有数据，也要存盘;
	if (m_DataStorePtr)
	{
		if (m_bStoreDB)
		{
			//分钟数据存盘;
			m_DataStorePtr->StoreMinute(m_qMinute);
		}
		
		m_qMinute.clear();
		
		//Tick存盘;
		if (m_bStoreDB)
		{
			m_DataStorePtr->StoreReport(m_qReport, false);
		}
		
		m_qReport.clear();
		//LOGDEBUG("the report queue capacity is:{}", m_qReport.capacity());
	}
}

void DataStoreFace::HandleTimer()
{
	//计算时间;
	//当前时间;
	boost::posix_time::ptime ptNow = boost::posix_time::second_clock::local_time();
	//离收盘还有多久;
	boost::posix_time::ptime ptClose = boost::posix_time::ptime(ptNow.date(),m_CloseTime);
	if (ptNow>ptClose)
	{
		//第二天的时间;
		ptClose = boost::posix_time::ptime(ptNow.date() + boost::gregorian::days(1),
			m_CloseTime);
	}
	ptClose - ptNow;

	HandleStore();
	if (m_Timer)
	{
		m_Timer->expires_from_now(std::chrono::seconds(60));
		m_Timer->async_wait(boost::bind(&DataStoreFace::HandleTimer, this));
	}
}
void DataStoreFace::StartStoreTimer()
{
	if (!m_Timer)
	{
		//每隔一分钟保存一次数据;
		m_Timer = boost::make_shared<boost::asio::steady_timer>(*m_IoService, std::chrono::seconds(60));
		m_Timer->async_wait(boost::bind(&DataStoreFace::HandleTimer, this));
	}
}


void DataStoreFace::HandleExchangeTimer(const std::string & exchange_id,bool is_open)
{
	LOGDEBUG("HandleExchangeTimer:{},is_open:{}",  exchange_id , is_open);
	StartStoreTimer();
	auto timer_ptr = m_ExchangeTimer.find(exchange_id);
	if (timer_ptr == m_ExchangeTimer.end())
	{
		//有问题;
		return;
	}
	//查找对应的交易所;
	auto exchange_ptr = ExchangeContainer::get_instance().find_exchange_by_id(exchange_id);
	if (exchange_ptr)
	{
		//计算下一次定时器的启动时间;
		auto current_time = boost::posix_time::second_clock::local_time();
		boost::posix_time::time_duration next_trade_time;
		bool bIsOpen = false;
		exchange_ptr->get_next_trade_segment(current_time.time_of_day(), next_trade_time, bIsOpen);

	}
}


void DataStoreFace::SetDataStore(boost::shared_ptr<IDataStore> dataStorePtr)
{
	m_DataStorePtr = dataStorePtr;
}

boost::shared_ptr<IDataStore> DataStoreFace::GetDataStore()
{
	return m_DataStorePtr;
}


void DataStoreFace::HandleTick(boost::shared_ptr<Tick> tick)
{
	if (tick->UpdateTime==0)
	{
		try
		{
			//strcpy(tick->UpdateTimeStr,"23:00:00");
			//做一次时间转换;
			static boost::gregorian::date TradingDay = 
				boost::gregorian::date_from_iso_string(tick->TradingDay);
			auto TradingTime = boost::posix_time::duration_from_string(tick->UpdateTimeStr);

			auto UpdateTime = boost::posix_time::ptime(TradingDay, TradingTime);
			
	// 			auto statusIter = m_LastStatus.find(tick->InstrumentID());
	// 			if (statusIter != m_LastStatus.end())
	// 			{
	// 				//检查交易状态;
	// 				if (statusIter->second->EnterTime < tick->UpdateTimeStr)
	// 				{
	// 					if (statusIter->second->InstrumentStatus!=IS_Continous)
	// 					{
	// 						LOGDEBUG("Current Time is Not IS_Continous {} {}",tick->InstrumentID(),tick->UpdateTimeStr);
	// 						return;
	// 					}
	// 				}
	// 			}
			//当地时间;
			if (TradingTime >= boost::posix_time::time_duration(0, 0, 0) 
				&& TradingTime <= boost::posix_time::time_duration(2, 30, 0))
			{
				//夜盘里有跨日的阶段;
				UpdateTime = boost::posix_time::ptime(TradingDay, TradingTime);
			}
			else if (TradingTime >= boost::posix_time::time_duration(20, 55, 0)
				&& TradingTime <= boost::posix_time::time_duration(23, 59, 59,999))
			{
				//夜盘里没有跨日的阶段;
				static boost::gregorian::date CurDay = boost::gregorian::day_clock::local_day();
				if (CurDay == TradingDay 
					|| CurDay.day_of_week() == boost::date_time::Saturday) //如果localday是周六;
				{
					//本地日期和交易日相等,说明本地时间走得太快了;
					CurDay = boost::gregorian::day_clock::local_day() - boost::gregorian::days(1);
					LOGDEBUG("TickTime:{}", boost::posix_time::to_iso_extended_string(UpdateTime));
				}
				UpdateTime = boost::posix_time::ptime(CurDay, TradingTime);
			}
			else if (TradingTime >= boost::posix_time::time_duration(8, 55, 0)
				&& TradingTime <= boost::posix_time::time_duration(15, 30, 0))
			{
				if (TradingTime >= boost::posix_time::time_duration(11, 30, 0)
					&& TradingTime < boost::posix_time::time_duration(13, 0, 0))
				{
					//日盘不交易阶段;
					LOGDEBUG("1.The Time Is Not Trading Time:{}", tick->UpdateTimeStr);
					return;
				}
				//日盘阶段;
				UpdateTime = boost::posix_time::ptime(TradingDay, TradingTime);
			}
			else
			{
				LOGDEBUG("2.The Time Is Not Trading Time:{}", tick->UpdateTimeStr);
				return;
			}

			UpdateTime += boost::posix_time::milliseconds(tick->UpdateMillisec);
			tick->UpdateTime = boost::posix_time::to_time_t(UpdateTime);
		}
		catch (std::exception& e)
		{
			LOGDEBUG("convert trading time exception:{}",e.what());
		}
		catch (...)
		{
			LOGDEBUG("convert trading time exception,unknown exception!");
		}
	}



	boost::shared_ptr<Tick> reportLast = boost::make_shared<Tick>();
	if (!InstrumentContainer::update(InstrumentContainer::get_instance(),
		tick.get(), true, reportLast.get()))
	{
		LOGDEBUG("instrument_container::update failed:{} {}",tick->InstrumentID(),tick->UpdateTimeStr);
		return;
	}
	
	//更新注册数据;
	bool bTriggerOnTick = true;
	auto regIter = m_RegInstruments.find(tick->InstrumentID());
	if (regIter!=m_RegInstruments.end())
	{
		if (regIter->second)
		{
			regIter->second->update(tick.get());
			if (regIter->second->GetInstrumentInfo().m_status)
			{
				bTriggerOnTick = regIter->second->GetInstrumentInfo().m_status->InstrumentStatus
					== IS_Continous;
			}
		}
	}

	//更新指数;
	HandleTick4Index(tick);

	//进入队列;
	m_qReport.push_back(tick);

	//转成分钟数据;
	boost::shared_ptr<MINUTE>	minute = boost::make_shared<MINUTE>();
	minute->Type = ktypeMin;
	//将深度行情转换成分钟数据;
	if (convert_REPORT_to_MINUTE(tick.get(), minute.get()))
	{
		//分钟数据的前一次值;
		auto miniter = m_mapPreMinuteCache.find(minute->szCode);
		if (miniter == m_mapPreMinuteCache.end())
		{
			auto ret = m_mapPreMinuteCache.insert(std::pair<std::string, boost::shared_ptr<MINUTE> >(minute->szCode, minute));
			miniter = ret.first;
			//说明这个合约以前还没来过数据，所以第一次来数据时，要加入到队列里去;
		}
		else
		{
			//注意这里的浮点数的比较;
			//todo;
			if (miniter->second->HighestPrice<minute->HighestPrice)
			{
				miniter->second->HighestPrice = minute->HighestPrice;
			}
			if (miniter->second->LowestPrice>minute->LowestPrice)
			{
				miniter->second->LowestPrice = minute->LowestPrice;
			}
		}
		if (miniter->second->TradingTime != minute->TradingTime)
		{
			//上一分钟已经过去,数据放入队列;
			m_qMinute.push_back(miniter->second);
			//更新分时数据;
			miniter->second = minute;
			//LOGDEBUG(minute->TradingTime << "," <<minute->szCode);
			if (m_bStoreDB)
			{
				//存储数据;
				if (m_IoService)
				{
					m_IoService->post(boost::bind(&DataStoreFace::HandleStore, this));
				}
			}
			else
			{
				m_qReport.clear();
				m_qMinute.clear();
			}
		}
	}
	if (bTriggerOnTick)
	{
		//LOGDEBUG("begin sigOnTick...");
		sigOnTick(tick);
		//LOGDEBUG("end sigOnTick...");
	}
	else
	{
		LOGDEBUG("Not triggered tick signal:{},{}.{}",tick->InstrumentID(), 
			tick->UpdateTimeStr,tick->UpdateMillisec);
	}
}


void DataStoreFace::HandleMinute(boost::shared_ptr<MINUTE> minute)
{
	//std::unique_lock<std::mutex> lock(m_mutex_minute);
	//std::map<std::string,IDataStore* >::iterator it=m_mapStores.begin();
	//std::map<std::string,minute_container> temp_minute_container;
	//while(it!=m_mapStores.end())
	//{
	//	//分钟数据归类;
	//	while(!m_qMinute.empty())
	//	{
	//		MINUTE& cur_min=m_qMinute.front();
	//		std::map<std::string,minute_container>::iterator mincontiter= temp_minute_container.find(cur_min.szCode);
	//		if(mincontiter==temp_minute_container.end())
	//		{
	//			minute_container tmp_container;
	//			std::pair<std::map<std::string,minute_container>::iterator,bool> ret=
	//				temp_minute_container.insert(std::pair<std::string,minute_container>(cur_min.szCode,tmp_container));
	//			if (ret.second)
	//			{
	//				mincontiter=ret.first;
	//			}
	//		}
	//		mincontiter->second.InsertMinuteSort(cur_min);
	//		m_qMinute.pop();
	//	}
	//	std::map<std::string,minute_container>::iterator ret_min_iter =  temp_minute_container.begin();
	//	while (ret_min_iter!=temp_minute_container.end())
	//	{
	//		kdata_container kdmin1(ktypeMin);
	//		ret_min_iter->second.ToKData(kdmin1);
	//		it->second->StoreKData(kdmin1);
	//		it->second->StoreMinute(ret_min_iter->second.data(),ret_min_iter->second.size());
	//		++ret_min_iter;
	//	}
	//	++it;
	//}
}

void DataStoreFace::UpdateTick(boost::shared_ptr<Tick> tick)
{
	if (m_IoService)
	{
		m_IoService->post(boost::bind(&DataStoreFace::HandleTick,this,tick));
	}
}

void DataStoreFace::UpdateInstrument(boost::shared_ptr<Instrument> instrument)
{
	if (!InstrumentContainer::update(InstrumentContainer::get_instance(), instrument.get(), true))
	{
		LOGDEBUG("DataStoreFace::UpdateInstrument Failed!");
	}

	//更新版块;
	//1.按交易所更新;
	plate_container::get_domain_container().add(instrument->ExchangeID.c_str());
	plate_container::get_domain_container().add_code(instrument->ExchangeID.c_str(),instrument->InstrumentID.c_str());
	//1.按品种更新;
// 	plate_container::get_domain_container().add(instrument->ProductID.c_str());
// 	plate_container::get_domain_container().add_code(instrument->ProductID.c_str(), instrument->InstrumentID.c_str());
}

void DataStoreFace::UpdateStatus(boost::shared_ptr<InstStatus> instStatus)
{
	if (!instStatus)
	{
		return ;
	}
	std::string RealInstrumentStatus = "RealInstrumentStatus.ini";
	InstrumentContainer& container = InstrumentContainer::get_instance();
	InstrumentInfo& info = container.get_info_by_id(instStatus->InstrumentID, instStatus->ExchangeID);
	boost::property_tree::ptree ptInstrument, ptTradinDay;
	boost::property_tree::ptree ptLastStatus, ptInstStatus;
	try
	{
		boost::property_tree::ini_parser::read_ini(RealInstrumentStatus, ptTradinDay);
		boost::property_tree::ini_parser::read_ini("LastStatus.ini", ptLastStatus);
	}
	catch (boost::property_tree::ini_parser_error& e1)
	{
		LOGDEBUG("InstrumentStatus.ini parser error:{}", e1.what());
	}
	catch (std::exception& e1)
	{
		LOGDEBUG("InstrumentStatus.ini read error:{}", e1.what());
	}
	if (info.is_valid())
	{
		//LOGDEBUG("DataStoreFace::UpdateStatus 1 {}:{}", instStatus->InstrumentID, instStatus->InstrumentStatus);
		info.m_status = boost::make_shared<InstStatus>(*instStatus);
		std::string InstID = /*info.get_id()*/instStatus->InstrumentID;
		auto ptOptInst = ptTradinDay.get_child_optional(InstID);
		
		if (!ptOptInst)
		{
			ptInstrument.put(instStatus->EnterTime, instStatus->InstrumentStatus);
			ptTradinDay.add_child(InstID, ptInstrument);
		}
		else
		{
			ptOptInst.get().put(instStatus->EnterTime, instStatus->InstrumentStatus);
		}

		auto ptOptStatus = ptLastStatus.get_child_optional(InstID);
		if (!ptOptStatus)
		{
			ptInstStatus.put("EnterTime", instStatus->EnterTime);
			ptInstStatus.put("InstrumentStatus", instStatus->InstrumentStatus);
			ptInstStatus.put("InstrumentID", InstID);
			ptInstStatus.put("ExchangeID", instStatus->ExchangeID);
			ptLastStatus.add_child(InstID, ptInstStatus);
		}
		else
		{
			ptOptStatus.get().put("EnterTime", instStatus->EnterTime);
			ptOptStatus.get().put("InstrumentStatus", instStatus->InstrumentStatus);
		}
		boost::property_tree::ini_parser::write_ini(RealInstrumentStatus, ptTradinDay);

		boost::property_tree::ini_parser::write_ini("LastStatus.ini", ptLastStatus);
		return ;
	}
	else
	{
		for (std::size_t i = 0; i < container.size(); ++i)
		{
			InstrumentInfo& info = container.at(i);
			if (boost::algorithm::iequals(info.ProductID, instStatus->InstrumentID))
			{
				info.m_status = instStatus;
			}
		}
		std::string InstID = instStatus->InstrumentID;
		auto ptOptInst = ptTradinDay.get_child_optional(InstID);

		if (!ptOptInst)
		{
			ptInstrument.put(instStatus->EnterTime, instStatus->InstrumentStatus);
			ptTradinDay.add_child(InstID, ptInstrument);
		}
		else
		{
			ptOptInst.get().put(instStatus->EnterTime, instStatus->InstrumentStatus);
		}
		auto ptOptStatus = ptLastStatus.get_child_optional(InstID);
		if (!ptOptStatus)
		{
			ptInstStatus.put("EnterTime", instStatus->EnterTime);
			ptInstStatus.put("InstrumentStatus", instStatus->InstrumentStatus);
			ptInstStatus.put("InstrumentID", InstID);
			ptInstStatus.put("ExchangeID", instStatus->ExchangeID);
			ptLastStatus.add_child(InstID, ptInstStatus);
		}
		else
		{
			ptOptStatus.get().put("EnterTime", instStatus->EnterTime);
			ptOptStatus.get().put("InstrumentStatus", instStatus->InstrumentStatus);
		}
		boost::property_tree::ini_parser::write_ini(RealInstrumentStatus, ptTradinDay);
		boost::property_tree::ini_parser::write_ini("LastStatus.ini", ptLastStatus);
	}

}

void DataStoreFace::UpdateExchange(boost::shared_ptr<exchange_t> exchange)
{
	if ( m_IoService)
	{
		ExchangeContainer::get_instance().update(exchange);
		//启动定时器;
		auto current_time = boost::posix_time::second_clock::local_time();
		auto exchange_ptr=ExchangeContainer::get_instance().find_exchange_by_id(exchange->ExchangeID);
		boost::posix_time::time_duration next_trade_time;
		bool bIsOpen = false;
		if (exchange_ptr && exchange_ptr->get_next_trade_segment(current_time.time_of_day(),next_trade_time, bIsOpen))
		{
			auto time_pd = next_trade_time - current_time.time_of_day();
			//提前5分钟启动一个定时器;
			if (bIsOpen)
			{
				//开盘前提前5分钟;
				auto five_min=boost::posix_time::minutes(5);
				if (time_pd.total_seconds() > five_min.total_seconds())
				{
					//超过5分钟;
					time_pd -= five_min;
				}
				else
				{
					//离开盘不足5分钟了;
					//time_pd -= time_pd;
					//HandleExchangeTimer(exchange_ptr->get_exchange_id(), bIsOpen);
					StartStoreTimer();
				}
			}
			else
			{
				//收盘后延后5分钟;
				//time_pd += boost::posix_time::minutes(5);
				//立即开启定时器;
				//time_pd = time_pd;
				StartStoreTimer();
				//HandleExchangeTimer(exchange_ptr->get_exchange_id(), bIsOpen);
			}
			std::string sz_time_pd = boost::posix_time::to_iso_string(time_pd);
			//LOGDEBUG("a " << exchange->ExchangeID << " timer will timeout on:"<<sz_time_pd);
			auto exhangeTimer = boost::make_shared<boost::asio::steady_timer>(boost::ref(*m_IoService),
				std::chrono::seconds(time_pd.total_seconds()));
			m_ExchangeTimer.insert(std::pair<std::string, boost::shared_ptr<boost::asio::steady_timer> >(exchange->ExchangeID, exhangeTimer));
			exhangeTimer->async_wait(boost::bind(&DataStoreFace::HandleExchangeTimer, this, exchange_ptr->get_exchange_id(), bIsOpen));
		}
	}
}

bool DataStoreFace::start()
{
	//加载以前的数据;
	if (!m_DataStorePtr)
	{
		return false;
	}
	//加载历史数据;
	//1.加载合约信息;
	m_DataStorePtr->LoadBasetable(InstrumentContainer::get_instance());
	m_DataStorePtr->LoadExchange(ExchangeContainer::get_instance());
	//启动服务;
	if (!m_IoService)
	{
		m_IoService = boost::make_shared<boost::asio::io_service>();
	}
	
	if (!m_EmptyWork)
	{
		m_EmptyWork = boost::make_shared<boost::asio::io_service::work>(*m_IoService);
	}
	if (!m_IoThread)
	{
		m_IoThread = boost::make_shared<boost::thread>(boost::bind(&boost::asio::io_service::run, m_IoService));
	}
	
	return true;
}

void DataStoreFace::stop()
{
	if (!m_IoService || m_IoService->stopped())
	{
		return;
	}
	if (m_EmptyWork)
	{
		m_EmptyWork.reset();
	}
	m_IoService->stop();
	HandleStore();
	//交易所信息存盘;
	m_DataStorePtr->StoreExchange(ExchangeContainer::get_instance());
	//合约信息存盘;
	m_DataStorePtr->StoreCodetable(InstrumentContainer::get_instance());

	plate_container::get_domain_container().save("domains.txt");
}

int DataStoreFace::PrepareData(boost::shared_ptr<InstrumentData> pInstrument, int enKtype, int period/*=ktypeDay*/, int nLength, bool bReload/*=false*/)
{
	if (!pInstrument)
	{
		return -1;
	}
	auto regIter = m_RegInstruments.find(pInstrument->GetInstrumentID());
	if (regIter == m_RegInstruments.end())
	{
		m_RegInstruments.insert(std::make_pair(pInstrument->GetInstrumentID(),pInstrument));
	}
	try
	{
		boost::property_tree::ptree ptSim;
		boost::property_tree::ini_parser::read_ini("SIM.ini", ptSim);
		auto current_time = boost::posix_time::second_clock::local_time();
		auto m_StartTradingDay = ptSim.get<std::string>("Mode.TradingDay", boost::gregorian::to_iso_string(current_time.date()));
		if (!m_StartTradingDay.empty())
		{
			pInstrument->m_ptEnd = boost::posix_time::ptime(boost::gregorian::date_from_iso_string(m_StartTradingDay));
		}
		else
		{
			pInstrument->m_ptEnd = current_time;
		}
	}
	catch (boost::property_tree::ini_parser_error& e1)
	{
		LOGDEBUG("SIM.ini parser error:{}", e1.what());
	}
	catch (std::exception& e1)
	{
		LOGDEBUG("SIM.ini read error:{}", e1.what());
	}

	return GetDataStore()->PrepareData(pInstrument, enKtype, period, nLength, bReload);
}

boost::shared_ptr<InstrumentData> DataStoreFace::PrepareData(const std::string& InstrumentID, int enKtype, int period /*= ktypeDay*/, int nLength /*= DEFAULT_LOAD_MIN1_COUNT*/, bool bReload /*= false*/)
{
	auto regIter = m_RegInstruments.find(InstrumentID);
	if (regIter == m_RegInstruments.end())
	{
		auto pInstrument = boost::make_shared<InstrumentData>(InstrumentID.c_str());
		m_RegInstruments.insert(std::make_pair(pInstrument->GetInstrumentID(), pInstrument));
		PrepareData(pInstrument, enKtype, period, nLength, bReload);
		return pInstrument;
	}
	if (bReload)
	{
		PrepareData(regIter->second, nLength, enKtype, period, bReload);
	}
	return regIter->second;
}


boost::shared_ptr<InstrumentData> DataStoreFace::PrepareIndexData(const std::string& IndexID, int enKtype, int period /*= ktypeDay*/, bool bReload /*= false*/)
{
	auto iiIter = m_RegIndex.find(IndexID);
	if (iiIter != m_RegIndex.end())
	{
		return iiIter->second->IndexInstrumentPtr;
	}
	
	//指数的成份信息;
	std::vector < std::string > InstrumentIDs;
	InstrumentContainer::get_instance().get_names_by_product(InstrumentIDs, IndexID);
	if (InstrumentIDs.size() == 0)
	{
		return nullptr;
	}
	auto IndexInstrumentDataPtr = boost::make_shared<IndexInstrumentData>();
	IndexInstrumentDataPtr->IndexID = IndexID;

	for (size_t i = 0; i < InstrumentIDs.size();++i)
	{
		auto regIter = m_RegInstruments.find(InstrumentIDs[i]);
		if (regIter == m_RegInstruments.end())
		{
			auto pInstrument = boost::make_shared<InstrumentData>(InstrumentIDs[i].c_str());
			PrepareData(pInstrument,enKtype, period, bReload);
			m_RegInstruments.insert(std::make_pair(InstrumentIDs[i], pInstrument));
			IndexInstrumentDataPtr->SubInstrumentMap[InstrumentIDs[i]] = pInstrument;
		}
		else
		{
			IndexInstrumentDataPtr->SubInstrumentMap[regIter->first] = regIter->second;
		}
	}
	//指数的历史数据;
	auto pInstrument = boost::make_shared<InstrumentData>(IndexID.c_str());
	int histbars = PrepareData(pInstrument,enKtype, period, bReload);
	if (histbars <= 0)
	{
		time_t d = 0;
		//构建指数;
		auto& m_KDataBarMap = IndexInstrumentDataPtr->SubInstrumentMap;

		std::map < std::string, size_t > cbar_map;
		std::map<std::string, int64_t> sum_of_volume_map;

		//逐个BAR进行计算;
		for (;;)
		{
			std::map<std::string, KDATA*> bar_map;//参与计算的数据;
			for (auto i = m_KDataBarMap.begin(); i != m_KDataBarMap.end(); ++i)
			{
				auto symbol = i->second->GetInstrumentID();
				auto& bar = i->second->GetKData(enKtype);
				if (bar.size() == 0)
				{
					continue;
				}
				size_t cbar = cbar_map[symbol];
				if (bar.size() <= cbar)
				{
					//避免越界;
					continue;
				}
				if (bar_map.empty())
				{
					bar_map[symbol] = &bar[cbar];
					d = bar[cbar].TradingTime;
				}
				else
				{
					if (d > bar[cbar].TradingTime)
					{
						d = bar[cbar].TradingTime;
						bar_map.clear();
						d = bar[cbar].TradingTime;
						bar_map[symbol] = &bar[cbar];
					}
					else if (d == bar[cbar].TradingTime)
					{
						bar_map[symbol] = &bar[cbar];
					}
				}
			}
			if (bar_map.empty())
			{
				break;
			}
			//持仓量总是总的,所以每次重置;
			int64_t sum_of_openinst = 0;
			int64_t sum_of_volume = 0;
			//计算成交量和持仓量;
			for (auto i = bar_map.begin(); i != bar_map.end(); ++i)
			{
				sum_of_openinst += i->second->OpenInterest;
				sum_of_volume += sum_of_volume_map[i->first];
				//计数器递增;
				++cbar_map[i->first];
			}
			if (sum_of_volume > 0)
			{
				//计算各自的价格;
				double sum_of_close_price = 0.0, sum_of_open_price = 0.0, sum_of_high_price = 0.0, sum_of_low_price = 0.0;

				for (auto i = bar_map.begin(); i != bar_map.end(); ++i)
				{
					double price_weight = 1;
					double volume_weight = 0;
					if (sum_of_openinst > 0 && i->second->OpenInterest > 0)
					{
						price_weight = 0.9*i->second->OpenInterest / sum_of_openinst;
						volume_weight = 0.1*sum_of_volume_map[i->first] / sum_of_volume;
					}
					double close_price = i->second->ClosePrice*price_weight + volume_weight;
					double open_price = i->second->OpenPrice*price_weight + volume_weight;
					double high_price = i->second->HighestPrice*price_weight + volume_weight;
					double low_price = i->second->HighestPrice*price_weight + volume_weight;
					sum_of_close_price += close_price;
					sum_of_open_price += open_price;
					sum_of_high_price += high_price;
					sum_of_low_price += low_price;
				}
				//创建K线数据;
				KDATA indexBar;
				indexBar.ClosePrice = sum_of_close_price /*- fmod(sum_of_close_price, m_Accuracy)*/;
				indexBar.TradingTime = d;
				indexBar.OpenPrice = sum_of_open_price /*- fmod(sum_of_open_price, m_Accuracy)*/;
				indexBar.HighestPrice = sum_of_high_price /*- fmod(sum_of_high_price, m_Accuracy)*/;
				indexBar.LowestPrice = sum_of_low_price /*- fmod(sum_of_low_price, m_Accuracy)*/;

				indexBar.Volume = sum_of_volume;
				indexBar.OpenInterest = sum_of_openinst;
				pInstrument->GetKData(enKtype).push_back(indexBar);
			}
		}
	}
	//保存指数;
	IndexInstrumentDataPtr->IndexInstrumentPtr = pInstrument;

	m_RegIndex.insert(std::make_pair(IndexID, IndexInstrumentDataPtr));

	return pInstrument;
}

void DataStoreFace::HandleTick4Index(boost::shared_ptr<Tick> tick)
{
	boost::shared_ptr<IndexInstrumentData> IndexInstrumentDataPtr;
	for (auto iiIter = m_RegIndex.begin(); iiIter != m_RegIndex.end();++iiIter)
	{
		if (iiIter->second)
		{
			auto m_CombTick = IndexInstrumentDataPtr->LastTickPtr;
			auto& m_KDataBarMap = IndexInstrumentDataPtr->SubInstrumentMap;
			auto& m_CurKDataBarMap = IndexInstrumentDataPtr->CurSubInstrumentMap;

			auto kIter = m_KDataBarMap.find(tick->InstrumentID());
			if (kIter == m_KDataBarMap.end())
			{
				continue;
			}
			if (!m_CombTick || strcmp(m_CombTick->TradingDay, tick->TradingDay) != 0)
			{
				m_CombTick = boost::make_shared<Tick>(*tick);
			}
			//和指数时间一致的数据;
			// 	int bdtCompRes = date_time_comp(tick->yyyymmdd, tick->hhmmss,
			// 		m_CombTick->yyyymmdd, m_CombTick->hhmmss, tick->millisec, m_CombTick->millisec);

			int bdtCompRes = 0;
			if (tick->UpdateTime > tick->UpdateTime)
			{
				bdtCompRes = 1;
			}
			else if (tick->UpdateTime == tick->UpdateTime)
			{
				bdtCompRes = tick->UpdateMillisec - tick->UpdateMillisec;
			}
			else
			{
				bdtCompRes = -1;
			}

			if (bdtCompRes > 0)
			{
				int64_t sum_of_openinst = 0, sum_of_volume = 0, sum_of_total_volume = 0;
				for (auto i = m_KDataBarMap.begin(); i != m_KDataBarMap.end(); ++i)
				{
					auto tIter = m_CurKDataBarMap.find(i->first);
					if (tIter != m_CurKDataBarMap.end())
					{
						//对成交量有贡献;
						sum_of_volume += tIter->second->GetLastTick()->Volume;
					}
					auto oTick = i->second->GetLastTick();
					if (oTick && strcmp(oTick->TradingDay, tick->TradingDay) == 0)
					{
						sum_of_openinst += oTick->OpenInterest;
						sum_of_total_volume += oTick->Volume;
					}
				}

				if (sum_of_volume <= 0 || sum_of_openinst <= 0)
				{
					m_CombTick = boost::make_shared<Tick>(*tick);
					return;
				}
				//计算各自的价格和总价格;
				double sum_of_last_price = 0.0, sum_of_ask_price = 0, sum_of_bid_price = 0;
				for (auto i = m_CurKDataBarMap.begin(); i != m_CurKDataBarMap.end(); ++i)
				{
					auto oTick = i->second->GetLastTick();
					double close_price = oTick->LastPrice;
					double openintereset = oTick->OpenInterest;
					int64_t volume = oTick->Volume;
					double last_price = close_price*(0.9*openintereset / sum_of_openinst + 0.1*volume / sum_of_total_volume);
					sum_of_last_price += last_price;
				}
				//计算各成份的占比;
// 				for (auto i = m_CurKDataBarMap.begin(); i != m_CurKDataBarMap.end(); ++i)
// 				{
// 					auto oTick = i->second->GetLastTick();
// 					double close_price = oTick->LastPrice;
// 					int openintereset = oTick->OpenInterest;
// 					int volume = oTick->Volume;
// 					double last_price = close_price*(0.9*openintereset / sum_of_openinst + 0.1*volume / sum_of_total_volume);
// 				}
				boost::shared_ptr<Tick> t1 = boost::make_shared<Tick>();
				strcpy(t1->TradingDay, m_CombTick->TradingDay);
				strcpy(t1->UpdateTimeStr, m_CombTick->UpdateTimeStr);
				t1->UpdateMillisec = m_CombTick->UpdateMillisec;

				t1->AskPrice[0] = sum_of_ask_price/* - fmod(sum_of_ask_price, m_Accuracy)*/;
				t1->BidPrice[0] = sum_of_bid_price /*- fmod(sum_of_bid_price, m_Accuracy)*/;
				t1->LastPrice = sum_of_last_price /*- fmod(sum_of_last_price, m_Accuracy)*/;

				t1->Volume = sum_of_total_volume;
				t1->OpenInterest = sum_of_openinst;

				IndexInstrumentDataPtr->IndexInstrumentPtr->update(t1.get());

				//本轮指数的Tick已经结束,进入下一轮;
				//m_CurKDataBarMap.clear();
				//m_CurKDataBarMap.insert(*kIter);
				m_CurKDataBarMap[kIter->first] = kIter->second;
				//CombTick重置;
				m_CombTick = boost::make_shared<Tick>(*tick);
			}
			//更新指数的时间;
			else if (bdtCompRes < 0)
			{
				assert(false);
				//当前时间比前一个Tick的时间还小;
				m_CurKDataBarMap.insert(*kIter);
			}
			else
			{
				//加入要计算的数据列表;
				m_CurKDataBarMap[kIter->first] = kIter->second;
			}
		}
	}
}

void DataStoreFace::SetStoreToDB(bool bStoreMarket)
{
	m_bStoreDB = bStoreMarket;
}

bool DataStoreFace::IsStoreToDB()
{
	return m_bStoreDB;
}

bool DataStoreFace::IsDayOrNight()
{
	boost::posix_time::ptime cur_time = boost::posix_time::second_clock::local_time();
	if (cur_time.time_of_day() >= boost::posix_time::time_duration(8, 55, 0)
		&& cur_time.time_of_day() <= boost::posix_time::time_duration(15, 30, 0))
	{
		return true;
	}
	return false;
}

boost::shared_ptr<InstrumentData> DataStoreFace::GetRegInstrument(const std::string& InstrumentID)
{
	auto regIter = m_RegInstruments.find(InstrumentID);
	if (regIter == m_RegInstruments.end())
	{
		return nullptr;
	}
	return regIter->second;
}

bool DataStoreFace::join()
{
	if (m_IoThread && m_IoThread->joinable())
	{
		m_IoThread->join();
		return true;
	}
	return false;
}






