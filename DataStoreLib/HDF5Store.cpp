#include "HDF5Store.h"
#include <boost/make_shared.hpp>


HDF5Store::HDF5Store()
{

}


HDF5Store::~HDF5Store()
{
	for (auto fIter = m_TickFiles.begin(); fIter != m_TickFiles.end();++fIter)
	{
		if (fIter->second)
		{
			fIter->second->close();
		}
	}
}

int HDF5Store::Init(const std::string& initStr)
{
	
	m_szRootPath = initStr;

	m_TickFiles["CFFEX"]=
		boost::make_shared<H5File>("P:/py2h5/data2exchange/data.h5", H5F_ACC_RDONLY);

	return 0;
}

int HDF5Store::LoadReport(boost::shared_ptr<instrument> pstock)
{
	const char* szInstrumentID = pstock->GetInstrumentID();
	auto fIter =m_TickFiles.find(szInstrumentID);
	if (fIter!=m_TickFiles.end())
	{
		if (fIter->second)
		{
			DataSet dataset = fIter->second->openDataSet(szInstrumentID);
			auto cmpType = dataset.getCompType();

			DataSpace dataspace = dataset.getSpace();
			dataspace.selectAll();
			hssize_t demisions = dataspace.getSelectNpoints();

			boost::interprocess::managed_shared_memory segment(
				boost::interprocess::create_only,
				"MySharedMemory",
				demisions*sizeof(report_t));

			const ShmemAllocator alloc_inst(segment.get_segment_manager());

			MyVector* s2 = segment.construct<MyVector>("MyVector")(demisions, alloc_inst);
			dataset.read(s2->data(), dataset.getCompType()/*, dataspace, dataspace*/);


			dataset.close();
		}
	}
	
	return 0;
}

int HDF5Store::LoadCodetable()
{
	return 0;
}
