﻿#ifndef _DATABASE_OPERATION_H_
#define _DATABASE_OPERATION_H_
// #define OTL_ODBC_MYSQL
// #include "otlv4.h"
//这里这样做，是因为好像在UNICODE下连接不到数据库
#if defined(_UNICODE) || defined(UNICODE)
#undef _UNICODE
#undef UNICODE
#define OTL_ODBC_MYSQL
#define OTL_EXCEPTION_IS_DERIVED_FROM_STD_EXCEPTION
#define OTL_DESTRUCTORS_DO_NOT_THROW
#define OTL_BIGINT  __int64
#include "otlv4.h"
#else
#define OTL_ODBC_MYSQL
#define OTL_ODBC_UNIX
#include "otlv4.h"
#endif
#define _UNICODE
#define UNICODE
//数据访问层使用OTL 实现
#include "DataStoreLib.h"
#include "IDataStore.h"
//#include "../FacilityBaseLib/DateTime.h"
//#include "Database.h"




class DATASTORE_API DatabaseOperator:public IDataStore
{
protected:
	otl_connect db;
	std::string last_error_msg;
public:
	DatabaseOperator(void);
	virtual int Init(const string& initStr);
	virtual void rlogon(string logonStr);
	virtual ~DatabaseOperator(void);
	//插入无类型数据
	virtual int Insert(void* pStruct);
	//取得数据集的名称
	virtual string GetRootDataSetName();
	//某个数据表是否已经存在
	virtual int IsDataSetExists(string setName);
	//释放
	virtual int Release();
	//设置数据库选项
	virtual int SetOptions(int optionType,int optionValue);

	long ConvertTime2Seconds(string strDay, string strTime );
	//字段列表
    vector<string> GetDataSetFieldNames( string setName );

	virtual	DWORD	GetSelfTempPath( char *szTempPath, int size );	// 得到临时目录

	virtual	bool GetLastErrorMessage(char* lpszError, UINT nMaxError);	// 得到最近错误
	virtual	int	GetMaxNumber( );		// 得到股票数量
	
	//取函数
	virtual	int	LoadCodetable( InstrumentContainer &container );	// 读取所有股票的信息
	virtual	int	LoadKDataCache(InstrumentContainer &container, PROGRESS_CALLBACK fnCallback, void *cookie, int nProgStart, int nProgEnd);	// 读取所有股票的最近日线数据缓冲
	virtual	int	LoadBasetable(InstrumentContainer & container);	// 读取某一股票的财务资料表，包括每股收益，每股净资产等，见CBaseData
	virtual	int	LoadBaseText(InstrumentData *pstock);					// 读取某一股票的基本资料文本
	virtual	int	LoadKData(InstrumentData *pstock, int nKType);		// 读取某一股票的某个周期的K线数据
	virtual int	LoadDRData(InstrumentData *pstock);					// 读取某一股票的除权除息资料
	virtual int	LoadReport(InstrumentData *pstock);					// 读取某一股票的行情刷新数据
	virtual int	LoadMinute(InstrumentData *pstock);					// 读取某一股票的行情分时数据
	virtual int	LoadOutline(InstrumentData *pstock);					// 读取某一股票的行情额外数据
	virtual int StoreExchange(vector<EXCHANGE>& exchanges);
	virtual int LoadExchange(vector<EXCHANGE>& exchanges);
	//存函数
	virtual	int	StoreCodetable(InstrumentContainer & container);	// 保存代码表
	virtual	int	StoreBasetable(InstrumentContainer & container);	//保存基本信息
	virtual	int StoreDRData(InstrumentData *pstock);					// 保存某一股票的除权除息资料
	virtual int	StoreReport( Tick * pReport, int nCount, bool bBigTrade );	// 保存行情刷新数据
	virtual int	StoreMinute( MINUTE * pMinute, int nCount );	// 保存行情分时数据;
	virtual int	StoreOutline( OUTLINE * pOutline, int nCount );	//

	//数据安装函数...
	virtual	int	InstallCodetbl( const char * filename, const char *orgname );		// 安装下载的代码表
	virtual	int	InstallCodetblBlock( const char * filename, const char *orgname );	// 安装下载的板块表
	virtual	int	InstallCodetblFxjBlock( const char * filename, const char *orgname );	// 安装下载的分析家板块表
	virtual	int	InstallKData( KdataContainer & kdata, bool bOverwrite = false );			// 安装K线数据
	virtual	int InstallKDataTy( const char * stkfile, int nKType, PROGRESS_CALLBACK fnCallback, void *cookie );	// 安装下载的K线通用格式数据
	virtual	int InstallKDataFxj( const char * dadfile, int nKType, PROGRESS_CALLBACK fnCallback, void *cookie );	// 安装下载的K线分析家格式数据
//	virtual	int InstallDRData( CDRData & drdata );									// 安装除权除息数据
	virtual	int	InstallDRDataClk( const char * filename, const char *orgname );		// 安装下载的除权除息数据，一只股票一个文件
	virtual	int	InstallDRDataFxj( const char * fxjfilename );						// 安装分析家除权除息数据
	virtual	int	InstallBasetable( const char * filename, const char *orgname );		// 安装财务数据
	virtual	int	InstallBasetableTdx( const char * filename );						// 安装通达信财务数据
	virtual	int	InstallBasetableFxj( const char * filename );						// 安装分析家财务数据
	virtual	int InstallBaseText( const char * filename, const char *orgname );		// 安装下载的基本资料数据，一只股票一个文件
	virtual	int InstallBaseText( const char * buffer, int nLen, const char *orgname );		// 安装基本资料数据

	virtual	int InstallNewsText( const char * filename, const char *orgname );		// 安装新闻数据文件
	virtual	int InstallNewsText( const char * buffer, int nLen, const char *orgname );		// 安装新闻数据

    virtual int GetDBTypeInfo(string& dbtypeName);
	virtual bool GetFileName(string &sFileName, int nDataType, InstrumentInfo * pInfo = NULL, int nKType = ktypeDay);
	virtual int StoreKData( KdataContainer & kdata, bool bOverwrite = false );

};

#endif
