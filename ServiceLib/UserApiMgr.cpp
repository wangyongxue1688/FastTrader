#include "UserApiMgr.h"
#include "Trader.h"
#include "Market.h"
#include "../CTP/CtpTrader.h"
#include <boost/dll.hpp>
#include <boost/shared_ptr.hpp>
#include "../Log/logging.h"
#include <functional>
#include <cctype>
#include <boost/function.hpp>
#include <boost/lambda/lambda.hpp>

UserApiMgr::UserApiMgr(void) BOOST_NOEXCEPT
{
	
}



UserApiMgr::~UserApiMgr(void)
{
	//析构;
}

UserApiMgr& UserApiMgr::GetInstance()
{
	static UserApiMgr g_UserApiMgr;
	return g_UserApiMgr;
}


boost::shared_ptr<Trader> UserApiMgr::GetTrader( const std::string& id )
{
	std::map<std::string, boost::shared_ptr<Trader> >::iterator titer
		=m_pTraders.find(id);
	if (titer!=m_pTraders.end())
	{
		return titer->second;
	}
	return nullptr;
}

boost::shared_ptr<Trader> UserApiMgr::GetTrader(const std::string& uid, const std::string& sname)
{
	using TraderMap = std::map < std::string, boost::shared_ptr<Trader> > ;
	TraderMap::iterator titer = std::find_if(m_pTraders.begin(), m_pTraders.end(), 
		[uid, sname](TraderMap::value_type& t){ return t.second->GetUser().UserID == uid && t.second->GetServer().id == sname; });
	if (titer != m_pTraders.end())
	{
		return titer->second;
	}
	return nullptr;
}

boost::shared_ptr<Market> UserApiMgr::GetMarket(const std::string & id)
{
	std::map<std::string, boost::shared_ptr<Market> >::iterator titer
		= m_pMarkets.find(id);
	if (titer != m_pMarkets.end())
	{
		return titer->second;
	}
	return nullptr;
}

boost::shared_ptr<Market> UserApiMgr::GetMarket(const std::string& uid, const std::string& bid)
{
	using MarketMap = std::map < std::string, boost::shared_ptr<Market> > ;
	MarketMap::iterator titer = std::find_if(m_pMarkets.begin(), m_pMarkets.end(),
		[uid, bid](MarketMap::value_type& t){ return t.second->UserID() == uid && t.second->BrokerID() == bid; });
	if (titer != m_pMarkets.end())
	{
		return titer->second;
	}
	return nullptr;
}

void UserApiMgr::Release()
{
	LOGDEBUG("UserApiMgr Release...");
}



boost::shared_ptr<Trader> UserApiMgr::CreateTrader(const ServerInfo& s)
{
	if (s.trader_api.empty())
	{
		return nullptr;
	}
	boost::filesystem::path shared_library_path = boost::filesystem::current_path();
	//加载动态库;
	shared_library_path /= s.trader_api;
	auto libIter = m_ApiLibs.find(shared_library_path);
	if (libIter == m_ApiLibs.end())
	{
		boost::shared_ptr<boost::dll::shared_library> lib =
			boost::make_shared<boost::dll::shared_library>(shared_library_path, boost::dll::load_mode::append_decorations);
		libIter = m_ApiLibs.insert(std::make_pair(shared_library_path, lib)).first;
	}
	if (libIter == m_ApiLibs.end() || !libIter->second )
	{
		return nullptr;
	}
	boost::function<trader_create_t> trader_creator =
		libIter->second->get_alias<trader_create_t>("create_trader");

	if (!trader_creator)
	{
		return nullptr;
	}
	//创建对象;
	boost::shared_ptr<Trader> td = trader_creator();
	if (!td)
	{
		return nullptr;
	}
	if (!td->Init(s))
	{
		return nullptr;
	}
	return td;
}

boost::shared_ptr<Trader> UserApiMgr::CreateTrader( const UserLoginParam& u,const ServerInfo& s )
{
	if (s.trade_server_front.size() <= 0)
	{
		return nullptr;
	}

	//查找该用户是否已经登录;
	auto tIter = std::find_if(m_pTraders.begin(), m_pTraders.end(), [u,s](std::pair<std::string,boost::shared_ptr<Trader> > pairTrader){
		return pairTrader.second && pairTrader.second->UserID() == u.UserID && pairTrader.second->BrokerID() == s.id;
	});
	if (tIter!=m_pTraders.end())
	{
		return tIter->second;
	}

	boost::shared_ptr<Trader> td = CreateTrader(s);
	if (!td)
	{
		return nullptr;
	}
	//连接信号;
	td->sigUserLogin.connect(this->sigTraderLogin);
	td->sigUserLogout.connect(this->sigTraderLogout);
	if (m_ApiCallback.sltInstrumentInfo)
	{
		td->sigInstrumentInfo.connect(m_ApiCallback.sltInstrumentInfo);
	}
	if (m_ApiCallback.sltInstrumentStatus)
	{
		td->sigInstrumentStatus.connect(m_ApiCallback.sltInstrumentStatus);
	}
	if (m_ApiCallback.sltExchangeInfo)
	{
		td->sigExchangeInfo.connect(m_ApiCallback.sltExchangeInfo);
	}
	if (m_ApiCallback.sltOnOrder)
	{
		td->sigOnOrder.connect(m_ApiCallback.sltOnOrder);
	}
	if (m_ApiCallback.sltOnOrderAction)
	{
		td->sigOnOrderAction.connect(m_ApiCallback.sltOnOrderAction);
	}
	if (m_ApiCallback.sltOnTrade)
	{
		td->sigOnTrade.connect(m_ApiCallback.sltOnTrade);
	}

	for (auto marketIter = m_pMarkets.begin();marketIter!=m_pMarkets.end();++marketIter)
	{
		marketIter->second->sigOnTick.connect(
			boost::bind(&Trader::OnTick,td,boost::lambda::_1));
	}
	
	if (!td->Login(u))
	{
		return nullptr;
	}
	
	return m_pTraders.insert(std::make_pair(td->Id(),td)).first->second;
	//return td;
}

boost::shared_ptr<Market> UserApiMgr::CreateMarket(const ServerInfo& s)
{
	if (s.market_server_front.size() <= 0)
	{
		return nullptr;
	}
	boost::filesystem::path shared_library_path = boost::filesystem::current_path();

	shared_library_path /= s.market_api;
	auto libIter = m_ApiLibs.find(shared_library_path);
	if (libIter == m_ApiLibs.end())
	{
		boost::shared_ptr<boost::dll::shared_library> lib =
			boost::make_shared<boost::dll::shared_library>(shared_library_path, 
			boost::dll::load_mode::append_decorations);
		libIter = m_ApiLibs.insert(std::make_pair(shared_library_path, lib)).first;
	}
	boost::function<market_create_t> market_creator =
		libIter->second->get_alias<market_create_t>("create_market");
	if (!market_creator)
	{
		return nullptr;
	}
	boost::shared_ptr<Market> md = market_creator(s.market_server_front[0].protocol);
	if (!md->Init(s))
	{
		return nullptr;
	}
	return md;
}

boost::shared_ptr<Market> UserApiMgr::CreateMarket(const UserLoginParam & u, const ServerInfo & s)
{
	if (s.market_server_front.size() <= 0)
	{
		return nullptr;
	}

	auto titer = m_pMarkets.begin();
	while (titer != m_pMarkets.end())
	{
		if (titer->second->UserID() == u.UserID && titer->second->BrokerID() == s.id)
		{
			return titer->second;
		}
		++titer;
	}
	
	auto md = CreateMarket(s);
	
	//添加绑定处理;
	md->sigOnTick.connect(m_ApiCallback.sltOnTick);

	for (auto traderIter = m_pTraders.begin(); traderIter != m_pTraders.end(); ++traderIter)
	{
		md->sigOnTick.connect(
			boost::bind(&Trader::OnTick, traderIter->second, boost::lambda::_1));
	}

	if (!md->Login(u))
	{
		return nullptr;
	}
	m_pMarkets.insert(std::pair<std::string, boost::shared_ptr<Market> >(md->Id(), md));
	return md;
}



bool UserApiMgr::OnApiEventSink(ApiEventParam& eventParam)
{
// 	//优先交给策略去处理;
// 	bool bStrategyHandler=StrategyMgr::GetInstance().OnApiEventSink(eventParam);
// 	//然后是数据存储模块;
// 	bStrategyHandler=DataSaverMgr::GetInstance().OnApiEventSink(eventParam);
	return true;
}

std::map<std::string,boost::shared_ptr<Trader> >& UserApiMgr::GetTraders()
{
	return m_pTraders;
}

std::map<std::string, boost::shared_ptr<Market>>& UserApiMgr::GetMarkets()
{
	return m_pMarkets;
}

void UserApiMgr::SetCallback(const user_api_callback& callback)
{
	m_ApiCallback = callback;
}



