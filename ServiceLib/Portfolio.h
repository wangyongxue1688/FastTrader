#ifndef _PORTFOLIO_H_
#define _PORTFOLIO_H_

#include "DataTypes.h"
#include "UserInfo.h"
#include "../FacilityBaseLib/InstrumentInfo.h"
#include "ServiceLib.h"
#include <map>
#include <vector>

class Trader;
class SERVICELIB_API Portfolio
{
public:
	Portfolio(const std::string& Id);
	virtual ~Portfolio();
	//报单操作接口;
	boost::shared_ptr<std::map<std::string, boost::shared_ptr<Order> > > GetOrders(bool bHistory=false);
	boost::shared_ptr<std::map<std::string, boost::shared_ptr<Trade> > > GetTrades(bool bHistory=false);
	//获取所有的预埋单;
	boost::shared_ptr<std::map<std::string, boost::shared_ptr<ParkedOrder> > > GetParkOrders();
	//获取所有的报单;
	boost::shared_ptr<std::map<std::string, boost::shared_ptr<InputOrder> > > GetInputOrders();
	//获取所有的撤单;
	boost::shared_ptr<std::map<std::string, boost::shared_ptr<InputOrderAction> > > GetInputOrderActions();
	//所有持仓;
	boost::shared_ptr<std::vector<boost::shared_ptr<InvestorPosition> > > GetPositions();
	//平仓记录;
	boost::shared_ptr < std::vector<boost::shared_ptr<InvestorPosition> > > GetClosedPositions();
	//账号资金变动记录;
	boost::shared_ptr < std::vector<boost::shared_ptr<TradingAccount> > > GetTradingAccounts();
	boost::shared_ptr < std::vector<boost::shared_ptr<TradingAccount> > > GetTradingAccountsToday();

	virtual boost::shared_ptr<InstrumentCommisionRate> GetInstCommissionRate(const std::string& instId);
	virtual boost::shared_ptr<InstrumentMarginRate> GetInstMarginRate(const std::string& instId);

	//获取合约信息;
	virtual boost::shared_ptr<InstrumentInfo> GetInstrumentInfo(const std::string& InstId);

	//交易计算参数;
	virtual boost::shared_ptr<BrokerTradingParams> GetBrokerTradingParams();
	//报单;
	virtual bool Update(boost::shared_ptr<Order> order,bool bHistory=false);
	//成交;
	virtual bool Update(boost::shared_ptr<Trade> trade, bool bHistory = false);
	//撤单失败;
	virtual bool Update(boost::shared_ptr<InputOrderAction> orderAction);
	//报单;
	virtual bool Update(boost::shared_ptr<InputOrder> orderAction);
	//价格变化要重新计算持仓盈亏;
	virtual bool Update(boost::shared_ptr<Tick> tick);
	//预埋单;
	virtual bool Update(boost::shared_ptr<ParkedOrder> parkedOrder);

	static boost::shared_ptr<InvestorPosition> Trade2Position(boost::shared_ptr<Trade> trade);

	boost::shared_ptr<Trader> GetTraderIDByFronIDSessionID(int frontId, int SessionId);

	virtual bool IsMyOrder(boost::shared_ptr<Order> order);
	virtual bool IsMyTrade(boost::shared_ptr<Trade> trade);

	int GetCancelCount(const std::string& InstrumentID);
	void SaveOrders();
	void SaveInputOrders();
	void SaveInputOrderActions();
	void SavePositions();
	void SaveTrades();
	void SaveTradingAccounts();
	bool SaveTradingAccounts(boost::shared_ptr<std::vector<boost::shared_ptr<TradingAccount> > > tradingAccounts,
		const std::string& keyName);

	bool LoadClosedPositions(boost::shared_ptr<std::vector<boost::shared_ptr<InvestorPosition> > > positions);

	void LoadOrders();
	void LoadPositions();
	void LoadTrades();
	void LoadTradingAccounts();

	virtual std::string GetId();
protected:
	//当前策略的报单;
	boost::shared_ptr<std::map<std::string, boost::shared_ptr<Order> > >  m_MyOrders;
	//历史回报信息;
	boost::shared_ptr<std::map<std::string, boost::shared_ptr<Order> > >  m_HistoryOrders;
protected:
	//发送过的报单;
	boost::shared_ptr<std::map<std::string, boost::shared_ptr<InputOrder> > > m_MyInputOrders;
	//程序之前发送的报单;
	boost::shared_ptr<std::map<std::string, boost::shared_ptr<InputOrder> > > m_HistoryInputOrders;
protected:
	//当前策略的成交单;
	boost::shared_ptr<std::map<std::string, boost::shared_ptr<Trade> > >  m_MyTrades;
	//程序之前的成交记录;
	boost::shared_ptr<std::map<std::string, boost::shared_ptr<Trade> > > m_HistoryTrades;
protected:
	boost::shared_ptr<std::vector<boost::shared_ptr<InvestorPosition> > > m_MyPositions;
	//被平掉的仓位存储到这里;
	boost::shared_ptr<std::vector<boost::shared_ptr<InvestorPosition> > > m_ClosePositions;
protected:
	//当前的撤单;
	boost::shared_ptr<std::map<std::string, boost::shared_ptr<InputOrderAction> > > m_MyInputOrderActions;
	//历史撤单;
	boost::shared_ptr<std::map<std::string, boost::shared_ptr<InputOrderAction> > > m_HistoryInputOrderActions;
protected:
	//发送过的报单;
	boost::shared_ptr<std::map<std::string, boost::shared_ptr<ParkedOrder> > > m_MyParkOrders;
	//程序之前发送的报单;
	boost::shared_ptr<std::map<std::string, boost::shared_ptr<ParkedOrder> > > m_HistoryParkOrders;
protected:
	//资金记录;
	boost::shared_ptr<std::vector<boost::shared_ptr<TradingAccount> > > m_TradingAccounts;

	boost::shared_ptr<TradingAccount> m_TradingAccount;
	//今天的资金变化曲线;
	boost::shared_ptr<std::vector<boost::shared_ptr<TradingAccount> > > m_TradingAccountsToday;
	std::string m_Id;
protected:
	//统计撤单次数;
	std::map<std::string, int> m_OrderCancelCount;
};

#endif
