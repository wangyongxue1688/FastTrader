#include "InvestorAccount.h"
#include "../FacilityBaseLib/Container.h"
#include <algorithm>
#include <boost/make_shared.hpp>

InvestorAccount::InvestorAccount(const std::string& Id)
	:Portfolio(Id)
{
	m_Investor = boost::make_shared<Investor>();
	m_SettlementInfo = boost::make_shared<SettlementInfo>();
}

InvestorAccount::InvestorAccount(const std::string& InvestorID,const std::string& BrokerID)
	:Portfolio(InvestorID)
{
	m_Investor = boost::make_shared<Investor>();
	m_SettlementInfo = boost::make_shared<SettlementInfo>();
	m_Investor->InvestorID=InvestorID;
	m_Investor->BrokerID=BrokerID;
}



InvestorAccount::~InvestorAccount(void)
{
}

void InvestorAccount::Update( const Investor& i)
{
	if (!m_Investor)
	{
		m_Investor = boost::make_shared<Investor>();
	}
	if (m_Investor->InvestorID == i.InvestorID && m_Investor->BrokerID == i.BrokerID)
	{
		*m_Investor = i;
	}
}


void InvestorAccount::Update( const SettlementInfo& si,bool bIsLast)
{
	if (!m_SettlementInfo)
	{
		m_SettlementInfo = boost::make_shared<SettlementInfo>(si);
	}
	else
	{
		*m_SettlementInfo = si;
	}
}

void InvestorAccount::Update( boost::shared_ptr<InvestorPositionDetail> ipd,bool isUserLogin/*=false*/)
{
	if (!ipd)
	{
		return;
	}

	if (ipd->Volume==0)
	{
		return;
	}

	m_InvestorPositionDetail.push_back(ipd);

	//更新持仓;
	auto pdIter = std::find_if(m_MyPositions->begin(), m_MyPositions->end(),
	[ipd](boost::shared_ptr<InvestorPosition> p){
		return p->InstrumentID == ipd->InstrumentID
			&& ipd->Direction == D_Buy? p->PosiDirection ==  PD_Long: p->PosiDirection== PD_Short;
	});
	if (pdIter != m_MyPositions->end())
	{
		//同一个合约方向相同;

	}
	else
	{
		//创建持仓;
		boost::shared_ptr<InvestorPosition> p = boost::make_shared<InvestorPosition>();
		p->BrokerID = ipd->BrokerID;
		p->AbandonFrozen = 0;
		p->CashIn = 0;
		p->CloseAmount = ipd->CloseAmount;
		p->OpenPrice = ipd->OpenPrice;
		p->CloseProfitByDate = ipd->CloseProfitByDate;
		p->CloseProfitByTrade = ipd->CloseProfitByTrade;
		p->CloseVolume = ipd->CloseVolume;
		p->ExchangeID = ipd->ExchangeID;
	}
}

void InvestorAccount::Update( boost::shared_ptr<InvestorPositionCombineDetail> ipcd,bool bIsUserLogin)
{
	if (m_InvestorPositionCombineDetail.empty())
	{
		m_InvestorPositionCombineDetail.push_back(ipcd);
	}
	else
	{
		for (std::size_t i=0;i<m_InvestorPositionCombineDetail.size();++i)
		{
			if (ipcd->InvestorID!=m_InvestorPositionCombineDetail[i]->InvestorID)
			{
				continue;
			}
			if ((ipcd->InstrumentID== m_InvestorPositionCombineDetail[i]->InstrumentID)
				&& (ipcd->Direction==m_InvestorPositionCombineDetail[i]->Direction))
			{
				//更新持仓;
				m_InvestorPositionCombineDetail[i]=ipcd;
			}
		}
	}
}

void InvestorAccount::Update(boost::shared_ptr<InvestorPosition> ip)
{
	if (!ip)
	{
		return;
	}
	if (ip->Position == 0)
	{
		//总持仓为0则表示不用处理;
		return;
	}
	if (m_MyPositions->empty())
	{
		m_MyPositions->push_back(ip);
	}
	else
	{
		bool bFind = false;
		for (std::size_t i = 0; i < m_MyPositions->size(); ++i)
		{
			auto pPosition = (*m_MyPositions)[i];
			if (!pPosition)
			{
				continue;
			}
			if (ip->InvestorID != pPosition->InvestorID)
			{
				continue;
			}
			if ((ip->InstrumentID == pPosition->InstrumentID)
				&& (ip->PosiDirection == pPosition->PosiDirection)
				&& (ip->PositionDate == pPosition->PositionDate)
				)
			{
				//合并持仓方向相同的;
				pPosition->YdPosition += ip->YdPosition;
				pPosition->TodayPosition += ip->TodayPosition;
				int OrigPosition = pPosition->Position;
				pPosition->Position += ip->Position;
				pPosition->LongFrozen += ip->LongFrozen;
				pPosition->ShortFrozen += ip->ShortFrozen;
				pPosition->LongFrozenAmount += ip->LongFrozenAmount;
				pPosition->ShortFrozenAmount += ip->ShortFrozenAmount;
				pPosition->OpenVolume += ip->OpenVolume;
				pPosition->CloseVolume += ip->CloseVolume;
				pPosition->OpenAmount += ip->OpenAmount;
				pPosition->CloseAmount += ip->CloseAmount;
				pPosition->PositionCost += ip->PositionCost;
				pPosition->UseMargin += ip->UseMargin;
				pPosition->Commission += ip->Commission;
				pPosition->PositionProfit += ip->PositionProfit;
				pPosition->OpenCost += ip->OpenCost;
				bFind = true;
				break;
			}
		}
		if (!bFind)
		{
			m_MyPositions->push_back(ip);
		}
	}
}


void InvestorAccount::SafeUpdate( boost::shared_ptr<Order> o,bool bUserIsLogin /* = false */)
{
	boost::unique_lock<boost::mutex> lck(m_Mutex4Order);
	if (!bUserIsLogin)
	{
		Portfolio::Update(o, true);
	}
	else
	{
		Portfolio::Update(o);
	}
}

void InvestorAccount::SafeUpdate(boost::shared_ptr<Trade> t, bool bUserIsLogin /* = false */)
{
	boost::unique_lock<boost::mutex> lck(m_Mutex4Trade);
	if (!bUserIsLogin)
	{
		Portfolio::Update(t,true);
	}
	else
	{
		//OnTrade(t);
		Portfolio::Update(t);
	}
}

void InvestorAccount::SafeUpdate( const Investor& i)
{
	boost::unique_lock<boost::mutex> lck(m_Mutex4Investor);
	Update(i);
}

void InvestorAccount::SafeUpdate(boost::shared_ptr<TradingAccount> ta)
{
	boost::unique_lock<boost::mutex> lck(m_Mutex4Account);
	*m_TradingAccount = *ta;
	if (!(m_TradingAccount->TradingDay.empty() || ta->TradingDay == m_TradingAccount->TradingDay))
	{
		m_TradingAccount = ta;
		m_TradingAccounts->push_back(ta);
	}
}

void InvestorAccount::SafeUpdate( const SettlementInfo& si,bool bIsLast)
{
	boost::unique_lock<boost::mutex> lck(m_Mutex4SettlementInfo);
	Update(si,bIsLast);
}

void InvestorAccount::SafeUpdate( boost::shared_ptr< InvestorPositionDetail> ipd,bool isUserLogin/*=false*/)
{
	boost::unique_lock<boost::mutex> lck(m_Mutex4PosDetail);
	Update(ipd);
}

void InvestorAccount::SafeUpdate(boost::shared_ptr<InvestorPositionCombineDetail> ipcd, bool bUserIsLogin /*= false*/)
{
	boost::unique_lock<boost::mutex> lck(m_Mutex4CmbPosDetail);
	Update(ipcd);
}

void InvestorAccount::SafeUpdate(boost::shared_ptr<InvestorPosition> ip, bool bUserIsLogin /*= false*/)
{
	boost::unique_lock<boost::mutex> lck(m_Mutex4CmbPosDetail);
	Update(ip);
}

void InvestorAccount::SafeUpdate(boost::shared_ptr<Tick> tick)
{
	Portfolio::Update(tick);
}

void InvestorAccount::SafeUpdate(boost::shared_ptr<InputOrder> pInputOrder)
{
	Portfolio::Update(pInputOrder);
}

void InvestorAccount::SafeUpdate(boost::shared_ptr<InputOrderAction> pInputOrder)
{
	Portfolio::Update(pInputOrder);
}

void InvestorAccount::SafeUpdate(boost::shared_ptr<BrokerTradingParams> borkerTradingParam)
{
	m_BrokerTradingParams = borkerTradingParam;
}





std::string InvestorAccount::GetField(int field)
{
	return std::string();
}

void InvestorAccount::setField(int field, const std::string & value)
{
}

std::pair<std::string,std::string> InvestorAccount::GetInvestorIDBrokerID()
{
	return std::pair<std::string,std::string>(m_Investor->InvestorID,m_Investor->BrokerID);
}


boost::shared_ptr<Investor> InvestorAccount::GetInvestor()
{
	boost::unique_lock<boost::mutex> lck(m_Mutex4Investor);
	return m_Investor;
}


boost::shared_ptr<SettlementInfo> InvestorAccount::GetSettlementInfo()
{
	boost::unique_lock<boost::mutex> lck(m_Mutex4SettlementInfo);
	return m_SettlementInfo;
}


std::vector<boost::shared_ptr<InvestorPositionDetail> >& InvestorAccount::GetInvestorPositionDetail()
{
	return m_InvestorPositionDetail;
}

std::vector<boost::shared_ptr<InvestorPositionCombineDetail>>& InvestorAccount::GetInvestorComboPositionsDetail()
{
	return m_InvestorPositionCombineDetail;
}

boost::shared_ptr<BrokerTradingParams> InvestorAccount::GetBrokerTradingParams()
{
	return m_BrokerTradingParams;
}



