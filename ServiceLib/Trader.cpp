#include "../common/platform.h"
#include <iostream>
#include <sstream>

#include <string.h>

#include "Trader.h"
#include "../common/DataTypes.h"
#include "../common/file_helper.h"
#include "../Log/logging.h"
#include "Trader.h"


#pragma warning(disable : 4996)



int& Trader::RequestID()
{
	return m_nRequestID;
}



const ErrorMessage& Trader::LastError() const
{
	return m_ErrorMessage;
}

bool Trader::Init( const ServerInfo& info)
{
	return false;
}


Trader::Trader( )
	/*:SpiImplBase()*/
{
	LOGINIT("trader");
	m_bInited=false;
	m_bFrontDisconnected=false;
	m_bIsLogined=false;
	m_nRequestID = 0;
}

void Trader::Release()
{
	m_bInited=false;
}

bool Trader::Login( const UserLoginParam& u )
{
	return m_bIsLogined;
}


bool Trader::QryMaxOrderVolume(const std::string & instId)
{
	return false;
}

bool Trader::OrderInsert( boost::shared_ptr<InputOrder> pInputOrder )
{
	return false;
}

bool Trader::OrderAction(boost::shared_ptr<InputOrderAction> pInputOrderAction)
{
	return false;
}

bool Trader::ParkedOrderInsert(boost::shared_ptr<ParkedOrder> param)
{
	return false;
}

bool Trader::ParkedOrderAction()
{
	return false;
}

bool Trader::QryDepthMarket(const std::string & instId)
{
	return false;
}

bool Trader::QryOrder(const std::string &,const std::string&)
{
	return false;
}

 bool Trader::SettlementConfirm()
 {
	 return false;
 }

bool Trader::QryTradingParam()
{
	return false;
}

bool Trader::QryTransBank()
{
	return false;
}



bool Trader::QryInvestor()
{
	return false;
}

bool Trader::QryAccount()
{
	return false;
}

bool Trader::QryPosition(const std::string& instId)
{
	return false;
}

Trader::~Trader()
{
	LOGDEBUG("~Trader...");
}

bool Trader::ReqUserLogin()
{
	return false;
}





bool Trader::QryNotice()
{
	return false;
}

bool Trader::QryTradingNotice()
{
	return false;
}

bool Trader::QryExchange( const std::string& exchgId )
{
	return false;
}

bool Trader::QryInstrument( const std::string& instId,const std::string& exchgId )
{
	return false;
}


bool Trader::QryPositionDetail(const std::string& instrument_id)
{
	return false;
}

bool Trader::QryCombPosDetail(const std::string& instrument_id)
{
	return false;
}

bool Trader::QrySettlementConfirm()
{
	return false;
}

bool Trader::QryInstCommissionRate( const std::string& instrument_id)
{
	return false;
}

bool Trader::QryExchgMarginRate(const std::string & instId)
{
	return false;
}

std::string Trader::UserID() const
{
	return m_UserInfo.UserID;
}

std::string Trader::BrokerID() const
{
	return m_UserInfo.BrokerID;
}

bool Trader::QrySettlementInfo()
{
	return false;
}

std::string Trader::Id() const
{
#if _MSC_VER
#if _MSC_VER>=1700
	return std::to_string(size_t(this));
#else
	return std::to_string((unsigned long long)size_t(this));
#endif
#else
	return std::to_string(size_t(this));
#endif
}

bool Trader::Logout()
{
	return false;
}

int Trader::Join()
{
	return -1;
}

UserInfo& Trader::GetUser()
{
	return m_UserInfo;
}

ServerInfo& Trader::GetServer()
{
	return m_ServerInfo;
}

Trader::Trader(const Trader & other)
{
}

Trader & Trader::operator=(const Trader & other)
{
	return *this;
}

boost::shared_ptr<InvestorAccount> Trader::GetInvestorAccount()
{
	return m_InvestorAccount;
}

std::string Trader::GetTradingDay()
{
	time_t local_t = std::time(nullptr);
	auto local_tm = localtime(&local_t);
	char date[16] = {0};
	strftime(date, sizeof(date), "%Y%m%d", local_tm);
	return date;
}

boost::shared_ptr<InstrumentCommisionRate> Trader::GetInstCommissionRate(const std::string& instId)
{
	return nullptr;
}

boost::shared_ptr<InstrumentMarginRate> Trader::GetInstMarginRate(const std::string& instId)
{
	return nullptr;
}

boost::shared_ptr<BrokerTradingParams> Trader::GetBrokerTradingParams()
{
	return nullptr;
}

void Trader::OnTick(boost::shared_ptr<Tick> tick)
{
	if (m_InvestorAccount)
	{
		m_InvestorAccount->SafeUpdate(tick);
	}
}

bool Trader::QryTrade(const std::string&, const std::string& instId)
{
	return false;
}

