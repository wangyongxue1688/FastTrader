#ifndef _USER_API_MGR_H_
#define _USER_API_MGR_H_

#include <map>
#include "UserInfo.h"
#include "ServerInfo.h"
#include "Exchange.h"
#include "DataTypes.h"
#include "ServiceLib.h"
#include <boost/shared_ptr.hpp>
#include <boost/filesystem.hpp>
#include <boost/dll.hpp>
#include <boost/noncopyable.hpp>
#include <boost/signals2.hpp>
//API管理对象;
class Market;
class Trader;

//槽函数;
struct user_api_callback
{
	boost::function<void(boost::shared_ptr<Instrument>)> sltInstrumentInfo;
	boost::function<void(boost::shared_ptr<exchange_t>)> sltExchangeInfo;
	boost::function<void(boost::shared_ptr<InstStatus>)> sltInstrumentStatus;
	boost::function<void(boost::shared_ptr<Order>)> sltOnOrder;
	boost::function<void(boost::shared_ptr<Trade>)> sltOnTrade;
	boost::function<void(boost::shared_ptr<InputOrderAction>)> sltOnOrderAction;
	boost::function<void(boost::shared_ptr<Tick>)> sltOnTick;
};


class SERVICELIB_API UserApiMgr:public boost::noncopyable
{
public:
	~UserApiMgr(void);
public:
	//接口函数;
	boost::shared_ptr<Trader> GetTrader(const std::string& id);
	boost::shared_ptr<Trader> GetTrader(const std::string& uid, const std::string& sname);
	boost::shared_ptr<Market> GetMarket(const std::string& id);
	boost::shared_ptr<Market> GetMarket(const std::string& uid, const std::string& sname);

	//创建交易接口并登录;
	boost::shared_ptr<Trader> CreateTrader(const UserLoginParam& u,const ServerInfo& s);
	//创建交易接口不登录;
	boost::shared_ptr<Trader> CreateTrader(const ServerInfo& s);
	//创建行情接口并登录;
	boost::shared_ptr<Market> CreateMarket(const UserLoginParam& u, const ServerInfo& s);
	//创建行情接口不登录;
	boost::shared_ptr<Market> CreateMarket(const ServerInfo& s);

	std::map<std::string, boost::shared_ptr<Trader> >& GetTraders();
	std::map<std::string, boost::shared_ptr<Market> >& GetMarkets();
	bool OnApiEventSink(ApiEventParam&);
	void Release();
	void SetCallback(const user_api_callback& callback);
protected:
	//依然是单例;
	UserApiMgr(void) BOOST_NOEXCEPT;

public:
	//用户登录与注销;
	boost::signals2::signal<void(const UserInfo& u)> sigTraderLogin;
	boost::signals2::signal<void(const UserInfo& u)> sigTraderLogout;
	boost::signals2::signal<void(const UserInfo& u)> sigTraderDisconnected;
	boost::signals2::signal<void(const UserInfo& u)> sigMarketDisconnected;
public:
	static UserApiMgr& GetInstance();
protected:
	//交易接口;
	std::map<std::string,boost::shared_ptr<Trader> > m_pTraders;
	//行情接口;
	std::map<std::string, boost::shared_ptr<Market> > m_pMarkets;
	//库管理;
	std::map<boost::filesystem::path, boost::shared_ptr<boost::dll::shared_library> > m_ApiLibs;
	//回调函数;
	user_api_callback m_ApiCallback;
};
#endif
