#ifndef _TRADING_ACCOUNT_H_
#define _TRADING_ACCOUNT_H_

#include <string>

struct BaseTradingAccount
{
	///交易日;
	std::string	TradingDay;
	///最新更新时间;
	std::string UpdateTime;
	///上次结算准备金;
	double	PreBalance;
	///手续费;
	double	Commission;
	///当前保证金总额;
	double	CurrMargin;
	///平仓盈亏;
	double	CloseProfit;
	///持仓盈亏;
	double	PositionProfit;
	///可用资金;
	double	Available;
	///入金金额;
	double	Deposit;
	///出金金额;
	double	Withdraw;
	///冻结的保证金;
	double	FrozenMargin;
	///冻结的手续费;
	double	FrozenCommission;
};

///资金账户;
struct TradingAccount
{
	TradingAccount()
	{
		PreMortgage = 0;
		PreCredit = 0;
		PreDeposit = 0;
		PreBalance = 0;
		PreMargin = 0;
		InterestBase = 0;
		Interest = 0;
		Deposit = 0;
		Withdraw = 0;
		FrozenMargin = 0;
		FrozenCash = 0;
		FrozenCommission = 0;
		CurrMargin = 0;
		CashIn = 0;
		Commission = 0;
		CloseProfit = 0;
		Balance = 0;
		Available = 0;
		WithdrawQuota = 0;
		Reserve = 0;
		SettlementID = 0;
		Credit = 0;
		Mortgage = 0;
		ExchangeMargin = 0;
		DeliveryMargin = 0;
		ExchangeDeliveryMargin = 0;
		ReserveBalance = 0;

		///上次货币质入金额;
		PreFundMortgageIn = 0;
		///上次货币质出金额;
		PreFundMortgageOut = 0;
		///货币质入金额;
		FundMortgageIn = 0;
		///货币质出金额;
		FundMortgageOut = 0;
		///货币质押余额;
		FundMortgageAvailable = 0 ;
		///可质押货币金额;
		MortgageableFund = 0;
		///特殊产品占用保证金;
		SpecProductMargin = 0;
		///特殊产品冻结保证金;
		SpecProductFrozenMargin = 0;
		///特殊产品手续费;
		SpecProductCommission = 0;
		///特殊产品冻结手续费;
		SpecProductFrozenCommission = 0;
		///特殊产品持仓盈亏;
		SpecProductPositionProfit = 0;
		///特殊产品平仓盈亏;
		SpecProductCloseProfit = 0;
		///根据持仓盈亏算法计算的特殊产品持仓盈亏;
		SpecProductPositionProfitByAlg = 0;
		///特殊产品交易所保证金;
		SpecProductExchangeMargin = 0;
	}
	///经纪公司代码;
	std::string	BrokerID;
	///投资者帐号;
	std::string	AccountID;
	///上次质押金额;
	double	PreMortgage;
	///上次信用额度;
	double	PreCredit;
	///上次存款额;
	double	PreDeposit;
	///上次结算准备金;
	double	PreBalance;
	///上次占用的保证金;
	double	PreMargin;
	///利息基数;
	double	InterestBase;
	///利息收入;
	double	Interest;
	///入金金额;
	double	Deposit;
	///出金金额;
	double	Withdraw;
	///冻结的保证金;
	double	FrozenMargin;
	///冻结的资金;
	double	FrozenCash;
	///冻结的手续费;
	double	FrozenCommission;
	///当前保证金总额;
	double	CurrMargin;
	///资金差额;
	double	CashIn;
	///手续费;
	double	Commission;
	///平仓盈亏;
	double	CloseProfit;
	///持仓盈亏;
	double	PositionProfit;
	///期货结算准备金;
	double	Balance;
	///可用资金;
	double	Available;
	///可取资金;
	double	WithdrawQuota;
	///基本准备金;
	double	Reserve;
	///交易日;
	std::string	TradingDay;
	///最新更新时间;
	std::string UpdateTime;
	///结算编号;
	int	SettlementID;
	///信用额度;
	double	Credit;
	///质押金额;
	double	Mortgage;
	///交易所保证金;
	double	ExchangeMargin;
	///投资者交割保证金;
	double	DeliveryMargin;
	///交易所交割保证金;
	double	ExchangeDeliveryMargin;
	///保底期货结算准备金;
	double	ReserveBalance;
	///币种代码;
	std::string	CurrencyID;
	///上次货币质入金额;
	double	PreFundMortgageIn;
	///上次货币质出金额;
	double	PreFundMortgageOut;
	///货币质入金额;
	double	FundMortgageIn;
	///货币质出金额;
	double	FundMortgageOut;
	///货币质押余额;
	double	FundMortgageAvailable;
	///可质押货币金额;
	double	MortgageableFund;
	///特殊产品占用保证金;
	double	SpecProductMargin;
	///特殊产品冻结保证金;
	double	SpecProductFrozenMargin;
	///特殊产品手续费;
	double	SpecProductCommission;
	///特殊产品冻结手续费;
	double	SpecProductFrozenCommission;
	///特殊产品持仓盈亏;
	double	SpecProductPositionProfit;
	///特殊产品平仓盈亏;
	double	SpecProductCloseProfit;
	///根据持仓盈亏算法计算的特殊产品持仓盈亏;
	double	SpecProductPositionProfitByAlg;
	///特殊产品交易所保证金;
	double	SpecProductExchangeMargin;
};

#endif