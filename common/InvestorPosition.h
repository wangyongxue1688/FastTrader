#ifndef _INVESTOR_POSITION_H_
#define _INVESTOR_POSITION_H_

#include <string>
#include "platform.h"
#include "Enums.h"
//持仓;




///投资者持仓;
struct InvestorPosition
{
	///合约代码;
	std::string	InstrumentID;
	///经纪公司代码;
	std::string	BrokerID;
	///投资者代码;
	std::string	InvestorID;
	std::string ExchangeID;
	std::string ClientID;
	std::string OpenDate;//开仓日期,交易日;
	///持仓多空方向;
	EnumPosiDirection	PosiDirection;
	///投机套保标志;
	EnumHedgeFlag	HedgeFlag;
	///持仓日期;
	EnumPositionDateType	PositionDate;
	///上日持仓;
	int	YdPosition;
	///今日持仓;
	int	Position;
	///多头冻结;
	int	LongFrozen;
	///空头冻结;
	int	ShortFrozen;
	///开仓冻结金额;
	double	LongFrozenAmount;
	///开仓冻结金额;
	double	ShortFrozenAmount;
	///开仓量;
	int	OpenVolume;
	///平仓量;
	int	CloseVolume;
	///开仓金额;
	double	OpenAmount;
	///平仓金额;
	double	CloseAmount;
	///持仓成本;
	double	PositionCost;
	///上次占用的保证金;
	double	PreMargin;
	///占用的保证金;
	double	UseMargin;
	///冻结的保证金;
	double	FrozenMargin;
	///冻结的资金;
	double	FrozenCash;
	///冻结的手续费;
	double	FrozenCommission;
	///资金差额;
	double	CashIn;
	///手续费;
	double	Commission;
	///平仓盈亏;
	double	CloseProfit;
	///持仓盈亏;
	double	PositionProfit;
	///上次结算价;
	double	PreSettlementPrice;
	///本次结算价;
	double	SettlementPrice;
	///交易日;
	std::string	TradingDay;
	///结算编号;
	int	SettlementID;
	///开仓成本;
	double	OpenCost;
	///交易所保证金;
	double	ExchangeMargin;
	///组合成交形成的持仓;
	int	CombPosition;
	///组合多头冻结;
	int	CombLongFrozen;
	///组合空头冻结;
	int	CombShortFrozen;
	///逐日盯市平仓盈亏;
	double	CloseProfitByDate;
	///逐笔对冲平仓盈亏;
	double	CloseProfitByTrade;
	///今日持仓;
	int	TodayPosition;
	///保证金率;
	double	MarginRateByMoney;
	///保证金率(按手数);
	double	MarginRateByVolume;
	///执行冻结;
	int	StrikeFrozen;
	///执行冻结金额;
	double	StrikeFrozenAmount;
	///放弃执行冻结;
	int	AbandonFrozen;
	//多头开仓价;
	double OpenPrice;
};





///投资者持仓明细;
struct InvestorPositionDetail
{
	///合约代码;
	std::string	InstrumentID;
	///经纪公司代码;
	std::string	BrokerID;
	///投资者代码;
	std::string	InvestorID;
	///投机套保标志;
	EnumHedgeFlag	HedgeFlag;
	///买卖;
	EnumDirection	Direction;
	///开仓日期;
	std::string	OpenDate;
	///成交编号;
	std::string	TradeID;
	///数量;
	int	Volume;
	///开仓价;
	double	OpenPrice;
	///交易日;
	std::string	TradingDay;
	///结算编号;
	int	SettlementID;
	///成交类型;
	EnumTradeType	TradeType;
	///组合合约代码;
	std::string	CombInstrumentID;
	///交易所代码;
	std::string	ExchangeID;
	///逐日盯市平仓盈亏;
	double	CloseProfitByDate;
	///逐笔对冲平仓盈亏;
	double	CloseProfitByTrade;
	///逐日盯市持仓盈亏;
	double	PositionProfitByDate;
	///逐笔对冲持仓盈亏;
	double	PositionProfitByTrade;
	///投资者保证金;
	double	Margin;
	///交易所保证金;
	double	ExchMargin;
	///保证金率;
	double	MarginRateByMoney;
	///保证金率(按手数);
	double	MarginRateByVolume;
	///昨结算价;
	double	LastSettlementPrice;
	///结算价;
	double	SettlementPrice;
	///平仓量;
	int	CloseVolume;
	///平仓金额;
	double	CloseAmount;
};


///投资者组合持仓明细;
struct InvestorPositionCombineDetail
{
	///交易日;
	std::string	TradingDay;
	///开仓日期;
	std::string	OpenDate;
	///交易所代码;
	std::string	ExchangeID;
	///结算编号;
	int	SettlementID;
	///经纪公司代码;
	std::string	BrokerID;
	///投资者代码;
	std::string	InvestorID;
	///组合编号;
	std::string	ComTradeID;
	///撮合编号;
	std::string	TradeID;
	///合约代码;
	std::string	InstrumentID;
	///投机套保标志;
	EnumHedgeFlag	HedgeFlag;
	///买卖;
	EnumDirection	Direction;
	///持仓量;
	int	TotalAmt;
	///投资者保证金;
	double	Margin;
	///交易所保证金;
	double	ExchMargin;
	///保证金率;
	double	MarginRateByMoney;
	///保证金率(按手数);
	double	MarginRateByVolume;
	///单腿编号;
	int	LegID;
	///单腿乘数;
	int	LegMultiple;
	///组合持仓合约编码;
	std::string	CombInstrumentID;
	///成交组号;
	int	TradeGroupID;
};

#endif
