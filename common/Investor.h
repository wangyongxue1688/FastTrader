#ifndef _INVESTOR_H_
#define _INVESTOR_H_

#include <string>

///TFtdcIdCardTypeType是一个证件类型类型
/////////////////////////////////////////////////////////////////////////
enum EnumIdCardType:char
{
	///组织机构代码;
	ICT_EID='0',
	///中国公民身份证;
	ICT_IDCard='1',
	///军官证;
	ICT_OfficerIDCard='2',
	///警官证;
	ICT_PoliceIDCard='3',
	///士兵证;
	ICT_SoldierIDCard='4',
	///户口簿;
	ICT_HouseholdRegister='5',
	///护照;
	ICT_Passport='6',
	///台胞证;
	TaiwanCompatriotIDCard='7',
	///回乡证;
	ICT_HomeComingCard='8',
	///营业执照号;
	ICT_LicenseNo='9',
	///税务登记号/当地纳税ID;
	ICT_TaxNo='A',
	///港澳居民来往内地通行证;
	ICT_HMMainlandTravelPermit='B',
	///台湾居民来往大陆通行证;
	ICT_TwMainlandTravelPermit='C',
	///驾照;
	ICT_DrivingLicense='D',
	///当地社保ID;
	ICT_SocialID='F',
	///当地身份证;
	ICT_LocalID='G',
	///商业登记证;
	ICT_BusinessRegistration='H',
	///港澳永久性居民身份证;
	ICT_HKMCIDCard='I',
	///人行开户许可证;
	ICT_AccountsPermits='J',
	///其他证件;
	ICT_OtherCard='x',

};
///投资者;
struct Investor
{
	///投资者代码;
	std::string	InvestorID;
	///经纪公司代码;
	std::string	BrokerID;
	///投资者分组代码;
	std::string	InvestorGroupID;
	///投资者名称;
	std::string	InvestorName;
	///证件类型;
	EnumIdCardType IdentifiedCardType;
	///证件号码;
	std::string	IdentifiedCardNo;
	///是否活跃;
	int			IsActive;
	///联系电话;
	std::string	Telephone;
	///通讯地址;
	std::string	Address;
	///开户日期;
	std::string	OpenDate;
	///手机;
	std::string	Mobile;
	///手续费率模板代码;
	std::string	CommModelID;
	///保证金率模板代码;
	std::string	MarginModelID;
};

#endif