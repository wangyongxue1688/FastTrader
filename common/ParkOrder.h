#ifndef _PARK_ORDER_H_
#define _PARK_ORDER_H_

#include "Enums.h"

/// <summary>
/// TFtdcParkedOrderStatusType是一个预埋单状态类型
/// </summary>
enum EnumParkedOrderStatus
{
	/// <summary>
	/// 未发送
	/// </summary>
	PAOS_NotSend = '1',

	/// <summary>
	/// 已发送
	/// </summary>
	PAOS_Send = '2',

	/// <summary>
	/// 已删除
	/// </summary>
	PAOS_Deleted = '3'
};
/// <summary>
/// TFtdcUserTypeType是一个用户类型类型
/// </summary>
enum EnumUserType
{
	/// <summary>
	/// 投资者
	/// </summary>
	UT_Investor = '0',

	/// <summary>
	/// 操作员
	/// </summary>
	UT_Operator = '1',

	/// <summary>
	/// 管理员
	/// </summary>
	UT_SuperUser = '2'
};
///预埋单;
struct ParkedOrder
{
	///经纪公司代码
	std::string	BrokerID;
	///投资者代码
	std::string	InvestorID;
	///合约代码
	std::string	InstrumentID;
	///报单引用
	std::string	OrderRef;
	///用户代码
	std::string	UserID;
	///报单价格条件
	EnumOrderPriceType	OrderPriceType;
	///买卖方向
	EnumDirection	Direction;
	///组合开平标志
	EnumOffsetFlag	CombOffsetFlag[5];
	///组合投机套保标志
	EnumHedgeFlag	CombHedgeFlag[5];
	///价格
	double	LimitPrice;
	///数量
	int	VolumeTotalOriginal;
	///有效期类型
	EnumTimeCondition	TimeCondition;
	///GTD日期
	std::string	GTDDate;
	///成交量类型
	EnumVolumeCondition	VolumeCondition;
	///最小成交量
	int	MinVolume;
	///触发条件
	EnumContingentCondition	ContingentCondition;
	///止损价
	double	StopPrice;
	///强平原因
	EnumForceCloseReason	ForceCloseReason;
	///自动挂起标志
	bool	IsAutoSuspend;
	///业务单元
	std::string	BusinessUnit;
	///请求编号
	int	RequestID;
	///用户强评标志
	bool	UserForceClose;
	///交易所代码
	std::string	ExchangeID;
	///预埋报单编号
	std::string	ParkedOrderID;
	///用户类型
	EnumUserType	UserType;
	///预埋单状态
	EnumParkedOrderStatus	Status;
	///错误代码
	int	ErrorID;
	///错误信息
	std::string	ErrorMsg;
	///互换单标志
	bool	IsSwapOrder;
	///报单时间;
	std::string OrderTime;
};

#endif