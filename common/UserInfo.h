#ifndef _USER_INFO_H_
#define _USER_INFO_H_

#include <string>
#include <vector>
#include <functional>
#include <stdint.h>
#include <tuple>
#include "platform.h"

//登录参数;
struct UserLoginParam
{
	std::string BrokerID;
	std::string UserID;
	std::string Password;
	//自动确认结算信息;
	bool AutoConfirmSettlement;
	std::string InvestorID;
	//服务器:电信or联通之类的;
	std::string Server;

	std::string MdBrokerID;

	std::string MdUserID;
	//有些系统需要行情密码;
	std::string MdPassword;
	UserLoginParam()
	{
		AutoConfirmSettlement=true;
	}
};

//用户信息;
struct UserInfo:public UserLoginParam
{
	UserInfo(const UserLoginParam& other)
		:UserLoginParam(other)
	{
		IsTraderLogined=false;
	}
	UserInfo():UserLoginParam()
	{
		IsTraderLogined=false;
	}
	///交易日;
	std::string	TradingDay;
	///登录成功时间;
	std::string	LoginTime;
	///交易系统名称;
	std::string	SystemName;
	///前置编号;
	int	FrontID;
	///会话编号;
	int	SessionID;
	///最大报单引用;
	std::string	MaxOrderRef;
	///交易所时间;
	std::string	ExchangeTime;
	///是否登录;
	bool IsTraderLogined;
	//授权码;
	std::string AuthCode;
};

//事件源类型;
enum EVENT_SOURCE_TYPE
{
	EVENT_FROM_TRADE=1,
	EVENT_FROM_MARKET=2,
	EVENT_FROM_UNKNOWN=3,
};

//事件编号;
enum API_EVENT_TYPE
{
	RspFrontConnected=100, //枚举;
	RspUserLogined=101,
	
	UpdatePassword=103,
	
	
	QryMoney=106,
	OrderInsert=107,
	CancelOrder=108,
	BankFutureTransfer=109,

	UserMarketLogined=114,
	ParkOrder=116,
	
	//合约状态变化;
	RtnInstrumentStatus=118,
	//查询交易参数;
	RspQryBrokerTradingParams=119,
	//查询通知;
	RspQryNotice=120,
	//用户事件;
	RspQryTradingNotice=121,
	//查询转帐银行;
	RspQryTransferBank=122,
	//查询投资者;
	RspQryInvestor=123,
	//查询持仓;
	RspQryPosition=124,
	RspQryPositionDetail=125,
	RspQryCombPosition=126,
	//结算确认;
	RspQrySettlementConfirm=127,
	RspSettlementInfoConfirm=128,
	QuerySettlementInfo=129,
	//查账户资金;
	RspQryTradingAccount=130,
	QryInstruments=131,
	QryExchanges=132,

	RspUnSubMarketData=133,
	RspSubMarketData=134,
	RtnDepthMarketData=135,
	RtnOrder=136,
	RtnTrade=137,
	RspOrderInsert=138,
	RspTradingCode=139,
	RspQryCommisionRate=140,
	RspQryInstMarginRate=141,
	RspQryExchgMarginRate=142,
	RspQryOrder=143,
	RspError=200,

};


//param1:经纪公司ID;
//param2:用户ID;
//param3:事件源;
//param4:事件类型;
//param5:事件内容;
//param6:对象ID;
// struct ApiEventParam 
// {
// 	ApiEventParam(const std::string& bid,const std::string& uid,EVENT_SOURCE_TYPE est,API_EVENT_TYPE aet,LPARAM lParam,const std::string& id)
// 	{
// 		this->bid=bid;
// 		this->uid=uid;
// 		this->est=est;
// 		this->aet=aet;
// 		this->lParam=lParam;
// 		this->id=id;
// 	}
// 	std::string bid;
// 	std::string uid;
// 	EVENT_SOURCE_TYPE est;
// 	API_EVENT_TYPE aet;
// 	LPARAM lParam;
// 	std::string id;
// };
typedef std::tuple<const std::string& ,const std::string& ,EVENT_SOURCE_TYPE,
	API_EVENT_TYPE,LPARAM,const std::string&> ApiEventParam;

typedef std::function<bool(ApiEventParam&)> OnApiEvent;

#endif
