#include "Statistic.h"
#include <algorithm>
#include <iterator>

StatisticMgr & StatisticMgr::GetInstance()
{
	// TODO: 在此处插入 return 语句
	static StatisticMgr g_StatisticMgr;
	return g_StatisticMgr;
}

void StatisticMgr::Begin(const std::string & s, const std::string& desc)
{
	unique_lock<mutex> lck(m_Mutex4Stat);
	auto spiter=std::find_if(m_StatPts.begin(), m_StatPts.end(), StatisticPointPred(s));
	StatisticPoint sp;
	std::get<3>(sp) = desc;
	if (spiter== m_StatPts.end())
	{
		std::get<4>(sp) = 1;
		std::get<2>(sp) = chrono::high_resolution_clock::now();
		m_StatPts.push_back(sp);
	}
	else
	{
		std::get<1>(*spiter) = chrono::high_resolution_clock::now();
		std::get<4>(sp) ++;
	}
}

void StatisticMgr::Finish(const std::string &s, const std::string& desc)
{
	unique_lock<mutex> lck(m_Mutex4Stat);
	auto spiter = std::find_if(m_StatPts.begin(), m_StatPts.end(), StatisticPointPred(s));
	if (spiter != m_StatPts.end())
	{
		std::get<2>(*spiter) = chrono::high_resolution_clock::now();
		std::get<3>(*spiter) = desc;
	}
}

void StatisticMgr::Delete(const std::string & key)
{
	unique_lock<mutex> lck(m_Mutex4Stat);
	std::remove_if(m_StatPts.begin(), m_StatPts.end(), StatisticPointPred(key));
}

std::string StatisticMgr::Get(const std::string & key)
{
	unique_lock<mutex> lck(m_Mutex4Stat);
	//std::vector<StatisticPoint>::iterator spiter = 
	//	std::find_if(m_StatPts.begin(), m_StatPts.end(), StatisticPointPred(key));
	//if (spiter != m_StatPts.end())
	//{
	//	return std::get<3>(*spiter);
	//}
	return std::string();
}


void StatisticMgr::Clear()
{
	unique_lock<mutex> lck(m_Mutex4Stat);
	m_StatPts.clear();
}


StatisticMgr::StatisticMgr()
{
}

StatisticMgr::StatisticMgr(const StatisticMgr & other)
{
}

StatisticMgr & StatisticMgr::operator=(const StatisticMgr & other)
{
	// TODO: 在此处插入 return 语句
	return *this;
}
