#ifndef _TRADE_H_
#define _TRADE_H_

#include <string>
/// <summary>
/// 成交
/// </summary>
///TFtdcTradingRoleType是一个交易角色类型
/////////////////////////////////////////////////////////////////////////
enum EnumTradingRole:char
{
	///代理;
	ER_Broker='1',
	///自营;
	ER_Host='2',
	///做市商;
	ER_Maker='3',
};

///TFtdcPriceSourceType是一个成交价来源类型
/////////////////////////////////////////////////////////////////////////
enum EnumPriceSource:char
{
	///前成交价;
	PSRC_LastPrice='0',
	///买委托价;
	PSRC_Buy='1',
	///卖委托价;
	PSRC_Sell='2',
};

///TFtdcTradeSourceType是一个成交来源类型
/////////////////////////////////////////////////////////////////////////
enum EnumTradeSource:char
{
	///来自交易所普通回报;
	TSRC_NORMAL='0',
	///来自查询;
	TSRC_QUERY='1',
};




struct Trade
{
	/// <summary>
	/// 经纪公司代码
	/// </summary>
	std::string BrokerID;
	/// <summary>
	/// 投资者代码
	/// </summary>
	std::string InvestorID;
	/// <summary>
	/// 合约代码
	/// </summary>
	std::string InstrumentID;
	/// <summary>
	/// 报单引用
	/// </summary>
	std::string OrderRef;
	/// <summary>
	/// 用户代码
	/// </summary>
	std::string UserID;
	/// <summary>
	/// 交易所代码
	/// </summary>
	std::string ExchangeID;
	/// <summary>
	/// 成交编号
	/// </summary>
	std::string TradeID;
	/// <summary>
	/// 买卖方向
	/// </summary>
	EnumDirection Direction;
	/// <summary>
	/// 报单编号
	/// </summary>
	std::string OrderSysID;
	/// <summary>
	/// 会员代码
	/// </summary>
	std::string ParticipantID;
	/// <summary>
	/// 客户代码
	/// </summary>
	std::string ClientID;
	/// <summary>
	/// 交易角色
	/// </summary>
	EnumTradingRole TradingRole;
	/// <summary>
	/// 合约在交易所的代码
	/// </summary>
	std::string ExchangeInstID;
	/// <summary>
	/// 开平标志
	/// </summary>
	EnumOffsetFlag OffsetFlag;
	/// <summary>
	/// 投机套保标志
	/// </summary>
	EnumHedgeFlag HedgeFlag;
	/// <summary>
	/// 价格
	/// </summary>
	double Price;
	/// <summary>
	/// 数量
	/// </summary>
	int Volume;
	/// <summary>
	/// 成交时期
	/// </summary>
	std::string TradeDate;
	/// <summary>
	/// 成交时间
	/// </summary>
	std::string TradeTime;
	/// <summary>
	/// 成交类型
	/// </summary>
	EnumTradeType TradeType;
	/// <summary>
	/// 成交价来源
	/// </summary>
	EnumPriceSource PriceSource;
	/// <summary>
	/// 交易所交易员代码
	/// </summary>
	std::string TraderID;
	/// <summary>
	/// 本地报单编号
	/// </summary>
	std::string OrderLocalID;
	/// <summary>
	/// 结算会员编号
	/// </summary>
	std::string ClearingPartID;
	/// <summary>
	/// 业务单元
	/// </summary>
	std::string BusinessUnit;
	/// <summary>
	/// 序号
	/// </summary>
	int SequenceNo;
	/// <summary>
	/// 交易日;
	/// </summary>
	std::string TradingDay;
	/// <summary>
	/// 结算编号;
	/// </summary>
	int SettlementID;
	/// <summary>
	/// 经纪公司报单编号;
	/// </summary>
	int BrokerOrderSeq;
	///成交来源;
	EnumTradeSource	TradeSource;
	//报单席位号;
	std::string SeatID;
};

struct TradePred 
{
	std::string OrderSysID;
	std::string OrderRef;
	TradePred(const std::string& sysid,const std::string& ref)
	{
		OrderSysID=sysid;
		OrderRef=ref;
	}
	bool operator()(const Trade& o)
	{
		if (o.OrderSysID!=OrderSysID)
		{
			return false;
		}
		int mOrderRef=atoi(OrderRef.c_str());
		int oOrderRef=atoi(o.OrderRef.c_str());
		return mOrderRef==oOrderRef;
	}
};
#endif
