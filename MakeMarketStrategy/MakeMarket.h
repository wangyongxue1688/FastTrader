#pragma once

#include <boost/dll.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/dll/alias.hpp>
#include <string>
#include <boost/make_shared.hpp>

class IStrategy;
boost::shared_ptr<IStrategy>  CreateStrategy(const std::string& );
BOOST_DLL_ALIAS(
	CreateStrategy, // <-- this function is exported with...
	create_strategy                               // <-- ...this alias name
)
